package fr.ca.cats.p0275.s2524.cal9.infrastructure.errors;

import org.springframework.http.HttpStatus;


public class BusinessException extends AbstractError implements Error {
    public static final Integer DEFAULT_STATUS = 400;
    public static final HttpStatus DEFAULT_HTTP_STATUS = HttpStatus.BAD_REQUEST;
    public static final String DEFAULT_CODE = "bad_request";
    public static final String INVALID_REQUEST = "invalid_request";



    public BusinessException(Integer status, String businessErrorCode, String message) {
        this(status, businessErrorCode, message, message);
    }

    public BusinessException(Integer status, String businessErrorCode, String causeValue, String message) {
        super(status, businessErrorCode, causeValue, message);
    }

    public BusinessException(String message) {
        this(DEFAULT_STATUS, DEFAULT_CODE, message);
    }

    public BusinessException(String message, HttpStatus httpStatus) {
        super(message, httpStatus);
    }

    public BusinessException(String message,  String businessErrorCode, HttpStatus httpStatus) {
        super(message, businessErrorCode, httpStatus);
    }

    public BusinessException(String message, String businessErrorCode) {
        super(message, businessErrorCode);
    }

    protected String getDefaultCode() {
        return DEFAULT_CODE;
    }

    protected Integer getDefaultStatus() {
        return DEFAULT_STATUS;
    }

    protected HttpStatus getDefaultHttpStatus() {
        return DEFAULT_HTTP_STATUS;
    }

    public static BusinessException invalidRequest(String message) {
        return new BusinessException(DEFAULT_STATUS, INVALID_REQUEST, message);
    }
}