package fr.ca.cats.p0275.s2524.cal9.infrastructure.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectReunionService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Profile("Mock")
@Service
@Slf4j
@AllArgsConstructor
public class CollectReunionServiceImplMock implements CollectReunionService {
    @Override
    public List<Reunion> getReunion() {
        ObjectMapper mapper = new ObjectMapper();

        List<Reunion> reunions = new ArrayList<>();
        Resource resource = new ClassPathResource("/static/agc9_test_integration.json");
        try {
            reunions = mapper.readValue(resource.getInputStream(), new TypeReference<>() {
            });
        } catch (Exception ignored) {
        }
        return reunions;
    }
}
