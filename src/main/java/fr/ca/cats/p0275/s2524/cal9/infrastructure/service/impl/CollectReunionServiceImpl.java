package fr.ca.cats.p0275.s2524.cal9.infrastructure.service.impl;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.ReunionClientService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectReunionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

@Profile("!Mock")
@Service
@Slf4j
public class CollectReunionServiceImpl implements CollectReunionService {

    private ReunionClientService reunionClientService;

    public CollectReunionServiceImpl(ReunionClientService reunionClientService) {
        this.reunionClientService = reunionClientService;
    }

    @Override
    public List<Reunion> getReunion() {
        return reunionClientService.getReunion();
    }
}
