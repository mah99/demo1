package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.DistributionEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
@Document("employee")
public class AgentDao {

    @Id
    @JsonProperty("id")
    private String idAgent;

    @JsonProperty("regional_bank_id")
    private String structureId;

    @JsonProperty("number")
    private String matriculeAgent;

    @JsonProperty("title")
    private String civilite;

    @JsonProperty("first_name")
    private String prenom;

    @JsonProperty("last_name")
    private String nom;

    @JsonProperty("phone_number")
    private String numeroTelephone;

    @JsonProperty("mobile_phone_number")
    private String numeroTelephoneMobile;

    @JsonProperty("email")
    private String email;

    @JsonProperty("distribution_entities")
    private List<DistributionEntity> distributionEntities;

    private LocalDateTime registeredDate = LocalDateTime.now();

}
