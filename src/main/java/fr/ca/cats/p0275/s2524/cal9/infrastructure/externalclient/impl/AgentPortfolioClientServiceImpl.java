package fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.impl;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Portfolio;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.AbstractCallInterApi;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.AgentPortfolioClientService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.CallInterApiTechnicalException;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;


@Profile("!Mock")
@Service
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class AgentPortfolioClientServiceImpl extends AbstractCallInterApi<Portfolio> implements AgentPortfolioClientService {
    public static final String QUERY_PARAM_SEPARATOR = "?";
    public static final String REGIONAL_BANK_ID = "regional_bank_id=";
    public static final String APPEL_SDG9 = "SDG9";

    @Value("${aia.SDG09.portfolio.uri}")
    private String aiaSDG09PortfolioUri;

    @Override
    public Portfolio getPortfolio(String structureId, String portfolioId) {
        try {
            log.info("collectPortfolio portfolioId={}, structureId={}", portfolioId, structureId);
            String uriFinal = aiaSDG09PortfolioUri + portfolioId + QUERY_PARAM_SEPARATOR + REGIONAL_BANK_ID + structureId;
            return callInterAPI(uriFinal, APPEL_SDG9);
        } catch (CallInterApiTechnicalException e) {
            log.error("Aucune donnée portfolio SDG9 portfolioId={}, structureId={}", portfolioId, structureId);
            return null;
        }
    }

}
