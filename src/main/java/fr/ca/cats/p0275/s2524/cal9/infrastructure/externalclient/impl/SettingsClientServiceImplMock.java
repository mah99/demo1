package fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.SettingsClientService;
import fr.ca.cats.p0275.s2524.cal9.model.Setting;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Profile("Mock")
@NoArgsConstructor
@Service
public class SettingsClientServiceImplMock implements SettingsClientService {

    public HttpHeaders getAiaHeaders() {
        return null;
    }

    @Override
    public List<Setting> getSettings(String structureId) {
        ObjectMapper mapper = new ObjectMapper();
        Resource resource = new ClassPathResource("/static/agc9Settings.json");
        List<Setting> settings = new ArrayList<>();

        try {
            Object obj = mapper.readValue(resource.getInputStream(), Object.class);
            List<Setting> settingsList = new ObjectMapper().convertValue(obj, new TypeReference<>() {
            });
            settings.add(settingsList.get(0));
            settings.add(settingsList.get(1));
            settings.add(settingsList.get(2));
            settings.add(settingsList.get(3));
        } catch (Exception e) {
            return null;
        }
        return settings;
    }
}
