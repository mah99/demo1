package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;


import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Branch;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.BranchDao;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface AgenceMapper {

    AgenceMapper AGENCE_MAPPER = Mappers.getMapper( AgenceMapper.class );
    BranchDao branchtoBranchDao(Branch branch);
    Branch branchDaoToBranch(BranchDao branchDao);

}
