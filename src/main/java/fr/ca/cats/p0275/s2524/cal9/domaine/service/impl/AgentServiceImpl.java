package fr.ca.cats.p0275.s2524.cal9.domaine.service.impl;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.AgentMapper;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.AgentService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.CallInterApiTechnicalException;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectAgentService;
import fr.ca.cats.p0275.s2524.cal9.model.Agent;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.Validator.verifyNull;

@Service
@Slf4j
public class AgentServiceImpl implements AgentService {
    private CollectAgentService collectAgentService;
    private AgentMapper agentMapper;


    public AgentServiceImpl(final CollectAgentService collectAgentService, final AgentMapper agentMapper) {
        this.collectAgentService = collectAgentService;
        this.agentMapper = agentMapper;
    }

    @Override
    public Agent getAgentCreateur(String structureId, Reunion reunion){

        if(reunion.getAction() == null  || StringUtils.isBlank(reunion.getAction().getAgentCreateurId())) {
            log.info("L'id de l'agentCreateur est null");
            return null;
        }
        Agent agent = null;
        try{
            Employee agentEmployeeCreateur = collectAgentService.getEmployee(structureId, reunion.getAction().getAgentCreateurId());

            if(verifyNull(agentEmployeeCreateur, "l'agent créateur")) {
                agent = agentMapper.employeeToAgent(agentEmployeeCreateur);
            }
        } catch (CallInterApiTechnicalException e){
            log.info("Aucune donnée employee SDG9 agentId={}, structureId={}", reunion.getAction().getAgentCreateurId(), structureId);
            return agent;
        }

        return agent;
    }

    @Override
    public Employee getEmployee(String structureId, String agentId){
        return collectAgentService.getEmployee(structureId, agentId);
    }

    @Override
    public Employee getEmployeeLocalisation(String structureId, String agentId){
        return collectAgentService.getEmployeeLocalisation(structureId, agentId);
    }
}
