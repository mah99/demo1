package fr.ca.cats.p0275.s2524.cal9.domaine.enums;

public enum TypeVueAgenda {
    MONO("MONO"),
    MULTI("MULTI");

    private final String label;

    TypeVueAgenda(String label) {
        this.label = label;
    }

    public String getLabel(){
        return this.label;
    }

}
