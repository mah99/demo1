package fr.ca.cats.p0275.s2524.cal9.infrastructure.security;

import fr.ca.cats.p0070.s1889.easyrest.authentication.security.CustomSecurityConfigurer;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.security.processor.SpringfoxHandlerProviderBeanPostProcessor;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;

@Profile("!Mock")
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        prePostEnabled = true,
        securedEnabled = true,
        jsr250Enabled = true)
public class WebSecurityConfig {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http, CustomSecurityConfigurer customSecurityConfigurer) throws Exception {
        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/v2/api-docs/").permitAll()
                .antMatchers(HttpMethod.GET, "/v3/api-docs/").permitAll()
                .antMatchers(HttpMethod.GET, "/actuator/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .csrf().disable()
                .apply(customSecurityConfigurer);
        return http.build();
    }

    /**
     * The purpose of this method is to exclude the URLs' specific to Login, Swagger UI and static files.
     * Any URL that should be excluded from the Spring security chain should be added to the ignored list in this
     * method only
     */
    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
        return web -> web.ignoring().antMatchers("/v2/api-docs/", "/v3/api-docs/", "/actuator/**");
    }

    @Bean
    public static BeanPostProcessor springfoxHandlerProviderBeanPostProcessor() {
        return new SpringfoxHandlerProviderBeanPostProcessor();
    }

}
