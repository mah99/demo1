package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.OmEdsAgent;

import java.util.List;

public interface EdsAgentRepository {

    List<OmEdsAgent> getIdEdsParentByStructureIdAndAgentId(String structureId, String agentId);

    List<OmEdsAgent> getALlByIdEdsParent(String structureId);

    List<OmEdsAgent> getAllByIdCaisseRegionale(String structureId);

    List<OmEdsAgent> getByIdEdsIn(List<String> idEds);

    List<OmEdsAgent> getAllEmployeeIdByIdEds(String idEds);

    List<OmEdsAgent> getAllByIdCaisseRegionaleAndAgentId(String structureId, String agentId);
}
