package fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.impl;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Branch;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.AbstractCallInterApi;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.AgenceClientService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.CallInterApiTechnicalException;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Profile("!Mock")
@Service
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class AgenceClientServiceImpl extends AbstractCallInterApi<Branch> implements AgenceClientService {

    public static final String QUERY_PARAM_SEPARATOR = "?";
    public static final String REGIONAL_BANK_ID = "regional_bank_id=";
    public static final String APPEL_SDG9 = "SDG9";
    public static final Integer FIRST_POSITION_OF_STRUCTURE = 0;
    public static final Integer NUMBER_OF_CHARACTER_TO_TRUNCATE = 5;
    public static final Integer FIRST_POSITION_OF_CODE_EDS = 5;

    @Value("${aia.SDG09.branch.uri}")
    private String aiaSDG09Uri;

    @Override
    public Branch getAgence(String branchId, String structureId){
        log.info("collectAgence branchId={}, structureId={}", branchId, structureId);
        String uriFinal = aiaSDG09Uri + branchId + QUERY_PARAM_SEPARATOR + REGIONAL_BANK_ID + structureId ;
        Branch branch;
        try{
             branch = callInterAPI(uriFinal, APPEL_SDG9);

        }  catch(CallInterApiTechnicalException e){
            log.error("Aucune donnée agence SDG9 branchId={}, structureId={}", branchId, structureId);
            return null;
        }
        if(Objects.nonNull(branch))
        {
            branch.setStructureId(branch.getId().substring(FIRST_POSITION_OF_STRUCTURE, NUMBER_OF_CHARACTER_TO_TRUNCATE));
            branch.setCodeEds(branch.getId().substring(FIRST_POSITION_OF_CODE_EDS, FIRST_POSITION_OF_CODE_EDS+ NUMBER_OF_CHARACTER_TO_TRUNCATE));
        }
        return branch ;
    }
}
