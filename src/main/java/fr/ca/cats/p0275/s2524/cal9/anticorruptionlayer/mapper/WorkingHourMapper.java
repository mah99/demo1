package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.WorkingHour;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.Horaire;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface WorkingHourMapper {

    WorkingHourMapper WORKING_HOUR_MAPPER = Mappers.getMapper( WorkingHourMapper.class );

    @Mapping(target = "day", source = "jour")
    @Mapping(target = "startingHour", source = "heureDebut")
    @Mapping(target = "endingHour", source = "heureFin")
    WorkingHour horaireToWorkingHour(Horaire horaire);
}
