package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.impl;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.OmEdsAgent;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.Dao.EdsAgentRepositoryDao;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.EdsAgentRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EdsAgentRepositoryImpl implements EdsAgentRepository {


    private EdsAgentRepositoryDao edsAgentRepositoryDao;


    public EdsAgentRepositoryImpl(EdsAgentRepositoryDao edsAgentRepositoryDao) {
        this.edsAgentRepositoryDao = edsAgentRepositoryDao;
    }

    @Override
    public List<OmEdsAgent> getIdEdsParentByStructureIdAndAgentId(String structureId, String agentId) {
        return edsAgentRepositoryDao.findIdEdsParentByStructureIdAndAgentId(structureId, agentId);
    }
    @Override
    public List<OmEdsAgent> getAllByIdCaisseRegionaleAndAgentId(String structureId, String agentId) {
        return edsAgentRepositoryDao.findAllByIdCaisseRegionaleAndAgentId(structureId, agentId);
    }

    @Override
    public List<OmEdsAgent> getALlByIdEdsParent(String idParent) {
        return edsAgentRepositoryDao.findALlByIdEdsParent(idParent);
    }

    @Override
    public List<OmEdsAgent> getAllByIdCaisseRegionale(String structureId) {
        return edsAgentRepositoryDao.findAllByIdCaisseRegionale(structureId);
    }

    @Override
    public List<OmEdsAgent> getByIdEdsIn(List<String> idEds) {
        return edsAgentRepositoryDao.findByIdEdsIn(idEds);
    }

    @Override
    public List<OmEdsAgent> getAllEmployeeIdByIdEds(String idEds) {
        return edsAgentRepositoryDao.findEdsByIdEds(idEds);
    }
}
