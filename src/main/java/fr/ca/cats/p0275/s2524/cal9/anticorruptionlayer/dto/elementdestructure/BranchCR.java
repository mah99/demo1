package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class BranchCR {

    @JsonProperty("id")
    private String idCrPortefeuille;
    @JsonProperty("regional_bank_id")
    private String idStructure;
    @JsonProperty("long_label")
    private String longLabel;
    @JsonProperty("short_label")
    private String shortLabel;
    @JsonProperty("external_id")
    private String idPortefeuille;
}
