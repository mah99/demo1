package fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;

public interface AgentClientService {

    Employee getEmployee(String structureId, String agentId);
}
