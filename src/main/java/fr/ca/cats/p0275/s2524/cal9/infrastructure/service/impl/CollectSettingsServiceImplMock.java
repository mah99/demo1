package fr.ca.cats.p0275.s2524.cal9.infrastructure.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectSettingsService;
import fr.ca.cats.p0275.s2524.cal9.model.Setting;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Profile("Mock")
@Service
@Slf4j
@AllArgsConstructor
public class CollectSettingsServiceImplMock implements CollectSettingsService {

    @Override
    public List<Setting> getSettings(String structureId) {
        ObjectMapper mapper = new ObjectMapper();

        List<Setting> settings = new ArrayList<>();
        Resource resource = new ClassPathResource(this.getCheminJsonParIdStructure(structureId));

        try {
            settings = mapper.readValue(resource.getInputStream(), new TypeReference<List<Setting>>() {
            });
        } catch (Exception ignored) {
        }

        return settings;
    }

    /**
     * Retourne le chemin du JSON à charger en fonction de l'id de la structure
     * @param structureId Id de la structure
     * @return String le chemin du JSON
     */
    private String getCheminJsonParIdStructure(String structureId) {
        if("87800".equals(structureId)) {
            return "/static/agc9settings_87800.json";
        }
        return "/static/agc9Settings.json";
    }
}
