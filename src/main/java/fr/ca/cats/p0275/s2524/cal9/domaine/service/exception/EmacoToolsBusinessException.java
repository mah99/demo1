package fr.ca.cats.p0275.s2524.cal9.domaine.service.exception;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.BusinessException;
import org.springframework.http.HttpStatus;

public class EmacoToolsBusinessException extends BusinessException {

    public EmacoToolsBusinessException(Integer status, String businessErrorCode, String message) {
        super(status, businessErrorCode, message);
    }

    public EmacoToolsBusinessException(Integer status, String businessErrorCode, String causeValue, String message) {
        super(status, businessErrorCode, causeValue, message);
    }

    public EmacoToolsBusinessException(String message) {
        super(message);
    }

    public EmacoToolsBusinessException(String message, HttpStatus httpStatus) {
        super(message,httpStatus);
    }
    public EmacoToolsBusinessException(String message, String businessErrorCode, HttpStatus httpStatus) {
        super(message,businessErrorCode,httpStatus);
    }

    public EmacoToolsBusinessException(String message, String businessErrorCode) {
        super(message,businessErrorCode);
    }
}
