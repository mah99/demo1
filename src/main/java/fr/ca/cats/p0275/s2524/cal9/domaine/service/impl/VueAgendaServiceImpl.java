package fr.ca.cats.p0275.s2524.cal9.domaine.service.impl;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Portfolio;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire;
import fr.ca.cats.p0275.s2524.cal9.domaine.enums.TypeVueAgenda;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.VueAgendaService;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.exception.VueAgendaBusinessException;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectAgentService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectPartenaireService;
import fr.ca.cats.p0275.s2524.cal9.model.Agent;
import fr.ca.cats.p0275.s2524.cal9.model.VueAgenda;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants.LOG_CODE;

@Service
@Slf4j
public class VueAgendaServiceImpl implements VueAgendaService {

    private CollectPartenaireService collectPartenaireService;
    private CollectAgentService collectAgentService;
    private final static String REGEX_PARTNER_ID = "^[0-9]{14}$";
    private final static String START_CHAIN_EDS = "00";
    private final static String ZERO = "0";


    public VueAgendaServiceImpl(final CollectPartenaireService collectPartenaireService, final CollectAgentService collectAgentService) {
        this.collectPartenaireService = collectPartenaireService;
        this.collectAgentService = collectAgentService;
    }


    @Override
    public VueAgenda getVueAgenda(final String structureId, final String partnerId) {
        checkQueryParameters(partnerId);
        log.info("ENTREE getVueAgenda structureId:{}, partnerId:{}", structureId, partnerId);
        String typeVueAgenda = TypeVueAgenda.MULTI.getLabel();
        VueAgenda vueAgenda = new VueAgenda();
        Partenaire partenaire = collectPartenaireService.getPartenaire(partnerId, structureId);

        if (Objects.isNull(partenaire)) {
            log.error(String.format(Constants.UNKNOWN_PARTNER.getMessage(), partnerId, structureId) + LOG_CODE + Constants.UNKNOWN_PARTNER.getCode());
            throw new VueAgendaBusinessException(String.format(Constants.UNKNOWN_PARTNER.getMessage(), partnerId, structureId), Constants.UNKNOWN_PARTNER.getCode());
        }
        vueAgenda.setAgenceId(rebuildAgenceId(partenaire.getCodeBureauGestionnaire()));
        if (!StringUtils.isBlank(partenaire.getIdPortefeuille()) && !partenaire.getIdPortefeuille().equals(ZERO)) {
            try {
                Portfolio portfolio = collectAgentService.getPortfolio(structureId, partenaire.getIdPortefeuille());
                if (portfolio != null) {
                    Employee employee = collectAgentService.getEmployee(structureId, portfolio.getMatriculeAgent());
                    if (employee != null) {
                        vueAgenda.setAgent(getAgent(employee));
                        typeVueAgenda = TypeVueAgenda.MONO.getLabel();
                    }
                }
            } catch (Exception e) {
                log.error(String.format(Constants.SDG9_IS_KO.getMessage(), e.getMessage()));
            }
        }

        vueAgenda.setType(typeVueAgenda);
        return vueAgenda;
    }

    private Agent getAgent(final Employee employee) {
        Agent agent = new Agent();
        agent.setId(employee.getIdEmployee());
        agent.setNom(employee.getNom());
        agent.setPrenom(employee.getPrenom());
        agent.setCivilite(employee.getCivilite());
        return agent;
    }

    private String rebuildAgenceId(final String codeBureauGestionnaire) {
        if (StringUtils.isBlank(codeBureauGestionnaire)) {
            return codeBureauGestionnaire;
        }
        return START_CHAIN_EDS + codeBureauGestionnaire;
    }

    private void checkQueryParameters(final String partnerId) {
        String partnerErrorMessage = String.format(Constants.INVALID_PARTNER_ID.getMessage(), partnerId);
        checkPartnerParameter(partnerId, partnerErrorMessage, Constants.INVALID_PARTNER_ID.getCode());
    }

    private void checkPartnerParameter(final String partnerId, final String errorMessage, final String codeError) {
        checkParameter(partnerId, REGEX_PARTNER_ID, errorMessage, codeError);
    }

    private void checkParameter(final String parameter, final String regex, final String errorMessage, final String codeError) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(parameter);

        if (!matcher.matches()) {
            log.error(errorMessage + LOG_CODE + codeError);
            throw new VueAgendaBusinessException(errorMessage, codeError);
        }
    }
}
