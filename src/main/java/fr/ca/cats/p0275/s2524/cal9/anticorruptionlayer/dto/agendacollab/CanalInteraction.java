package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CanalInteraction {
    @JsonProperty("canal")
    private String canal;

    @JsonProperty("agence")
    private Agence agence;

    @JsonProperty("telephone")
    private String telephone;

    @JsonProperty("urlVisio")
    private String urlVisio;
}
