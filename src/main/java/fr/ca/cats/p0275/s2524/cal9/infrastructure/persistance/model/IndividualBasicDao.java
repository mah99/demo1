package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Document("individualBasicEntity")
public class IndividualBasicDao {
    @JsonProperty("regional_bank_id")
    private String structureId;
    @Id
    @JsonProperty("individual_id")
    private String individualId;
    @JsonProperty("mobile_phone_number")
    private String mobilePhoneNumber;
    @JsonProperty("email_address")
    private String emailAddress;
    @JsonProperty("full_name")
    private String fullName;
    private LocalDateTime registeredDate = LocalDateTime.now();
}
