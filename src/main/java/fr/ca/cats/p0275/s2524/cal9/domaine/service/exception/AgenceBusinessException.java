package fr.ca.cats.p0275.s2524.cal9.domaine.service.exception;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.BusinessException;

public class AgenceBusinessException extends BusinessException {

    public AgenceBusinessException(Integer status, String businessErrorCode, String message) {
        super(status, businessErrorCode, message);
    }

    public AgenceBusinessException(Integer status, String businessErrorCode, String causeValue, String message) {
        super(status, businessErrorCode, causeValue, message);
    }

    public AgenceBusinessException(String message) {
        super(message);
    }

    public AgenceBusinessException(String message, String businessErrorCode) {
        super(message,businessErrorCode);
    }
}