package fr.ca.cats.p0275.s2524.cal9.infrastructure.errors;

import lombok.Getter;

@Getter
public enum Constants {
    NULL_PARAM("{} est null", ""),
    EMPTY_PARAM("{} est vide", ""),
    INVALID_PARTENAIRE("partenaire ID n'est pas au bon format", ""),

    BRANCH_ID_NOT_FOUND("La branch de l'agence est manquante", "F_0001"),
    AGENCE_LIBELLE_NOT_FOUND("Le libellé de l'agence est manquant", "F_0002"),
    INVALID_AGENT_ID("L'agentId %s est invalide", "F_0003"),
    REUNION_TYPE_NOT_FOUND("Le type de réunion est manquant", "F_0004"),
    INVALID_PARTENAIRE_ID("Le partenaire id %s est invalide", "F_0005"),
    NOT_FOUND_PARTENAIRE_EMACO("Le partenaire emaco pour le partenaire id %s est introuvable", "F_0006"),
    INVALID_PARTNER_ID("Le partnerId est invalide %s", "F_0007"),
    UNKNOWN_PARTNER("Le partenaire %s est inexistant pour la structure %s", "F_0008"),
    EMPTY_LIST_AGENT("agentIdList et IdEds ne peuvent pas être tous les deux null ou vide", "F_0009"),
    INVALID_LIST_AGENT("agentIdList et IdEds ne peuvent pas contenir de données tous les deux", "F_0010"),
    EMPTY_LIST_EDS_AGENT("Un eds ne peut pas avoir une liste d'agent vide", "F_0011"),
    INVALID_AGENT("agentID n'est pas au bon format", "F_0012"),
    INVALID_STRUCTURE("structureID n'est pas au bon format", "F_0013"),
    NULL_PARTENAIRE("L'information du partenaire %s", "F_0014"),
    AGENT_NOT_FOUND("L'agentId %s est introuvable ", "F_0015"),
    SDG9_IS_KO("Pas de données renvoyées par SDG9 et le message d'erreur est : {} ", "F_0016"),
    BAD_REQUEST_HEADER_VALUES("Erreur lors de la lecture des headers pour l'uri: ", "F_0017"),
    BAD_REQUEST_RESPONSE_VALUES("Erreur lors de la lecture de la réponse pour l'uri: ", "F_0018"),
    MISSING_PARAM_EXCEPTION("Header ou paramètre invalide.", "F_0019"),
    GENERIQUE_ERROR("Une erreur s'est produite. Nous faisons de notre mieux pour régler le problème. Merci de réessayer plus tard", "F_0020"),
    NOT_FOUND_EDS_AGENT("Le secteurAgentMulti pour la caisse %s est introuvable", "F_0021"),
    MONGODB_IS_KO("Pas de données renvoyées par MONGODB et le message d'erreur est : {} ", "F_0022"),

    INTERNAL_ERROR_HEADER_VALUES("Erreur lors du traitement des valeurs du header pour l'uri: ", "T_0001"),
    INTERNAL_ERROR_APPEL_API("Erreur lors de l'appel à l'api externe: ", "T_0002"),
    INVALID_ERROR_AGC9_APPEL_API("Aucune réunion retournée par AGC9 pour l'uri: ", "T_0003"),
    INVALID_ERROR_APPEL_API("Aucune information retournée pour l'api: ", "T_0004");

    private final String message;
    private final String code;
    public static final String LOG_CODE = " - CODE:";


    Constants(String message, String code) {
        this.message = message;
        this.code = code;
    }
}
