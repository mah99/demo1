package fr.ca.cats.p0275.s2524.cal9.infrastructure.util;

import fr.ca.cats.p0275.s2524.cal9.domaine.service.exception.CalendrierBusinessException;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

import static fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants.EMPTY_PARAM;
import static fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants.NULL_PARAM;

@Slf4j
public class Validator {

    private Validator() {
    }

    public static void verifyisNotEmpty(String item, String attribut, String exception, String code) {

        if (StringUtils.isBlank(item)) {
            log.error(EMPTY_PARAM.getMessage(), attribut);
            throw new CalendrierBusinessException(exception, code);
        }
    }

    public static boolean verifyNull(Object item, String attribut) {

        if (Objects.isNull(item)) {
            log.error(NULL_PARAM.getMessage(), attribut);
            return false;
        }
        return true;
    }

    public static boolean isNotEmpty(String item, String attribut) {
        if(StringUtils.isBlank(item)) {
            log.error(EMPTY_PARAM.getMessage(), attribut);
            return true;
        }
        return false;
    }
}
