package fr.ca.cats.p0275.s2524.cal9.infrastructure.service;

import fr.ca.cats.p0275.s2524.cal9.model.Setting;

import java.util.List;

public interface CollectSettingsService {
    List<Setting> getSettings(String structureId);


}
