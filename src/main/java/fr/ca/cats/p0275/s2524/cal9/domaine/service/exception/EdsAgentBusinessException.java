package fr.ca.cats.p0275.s2524.cal9.domaine.service.exception;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.BusinessException;
import org.springframework.http.HttpStatus;

public class EdsAgentBusinessException extends BusinessException {

    public EdsAgentBusinessException(String message, String businessErrorCode, HttpStatus httpStatus) {
        super(message,businessErrorCode,httpStatus);
    }
}
