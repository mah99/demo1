package fr.ca.cats.p0275.s2524.cal9.domaine.service.impl;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Function;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.WorkingHour;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.AgentMapper;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.LocalisationAgendaMapper;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.ReunionMapper;
import fr.ca.cats.p0275.s2524.cal9.domaine.enums.TypeReunion;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.*;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.exception.CalendrierBusinessException;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectAgentService;
import fr.ca.cats.p0275.s2524.cal9.model.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants.LOG_CODE;
import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.Tools.isListEmptyOrNull;
import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.Validator.verifyNull;

@Service
@Slf4j
public class CalendrierServiceImpl implements CalendrierService {


    private static final String REGEX_AGENT_ID = "^[0-9]{5}$";
    private static final int EDS_CHAR_NUMBER = 5;
    private static final String ZERO = "0";
    private final AgentService agentService;
    private final AgenceService agenceService;
    private final PartenaireService partenaireService;
    private final ReunionService reunionService;
    private final ReunionMultiService reunionMultiService;
    private final CollectAgentService collectAgentService;
    @Autowired
    private ReunionMapper reunionMapper;
    @Autowired
    private AgentMapper agentMapper;
    @Autowired
    private LocalisationAgendaMapper localisationAgendaMapper;

    public CalendrierServiceImpl(ReunionMapper reunionMapper, AgentMapper agentMapper, AgentService agentService, AgenceService agenceService, PartenaireService partenaireService,
                                 ReunionService reunionService, LocalisationAgendaMapper localisationAgendaMapper, ReunionMultiService reunionMultiService, CollectAgentService collectAgentService) {
        this.reunionMapper = reunionMapper;
        this.agentMapper = agentMapper;
        this.agentService = agentService;
        this.agenceService = agenceService;
        this.partenaireService = partenaireService;
        this.reunionService = reunionService;
        this.reunionMultiService = reunionMultiService;
        this.localisationAgendaMapper = localisationAgendaMapper;
        this.collectAgentService = collectAgentService;
    }

    @Override
    public Calendrier getCalendrier(String correlationId, String structureId, String agentId, String dateDebut, String dateFin) {
        log.info("ENTREE getCalendrier structureId:{}, agentId:{}, dateDebut:{}, dateFin:{}", structureId, agentId, dateDebut, dateFin);
        agentId = checkIfAgentIdIsValid(agentId, Pattern.compile(REGEX_AGENT_ID));
        Calendrier calendrier = new Calendrier();
        Employee employee = agentService.getEmployeeLocalisation(structureId, agentId);
        if (verifyNull(employee, "employee")) {
            fr.ca.cats.p0275.s2524.cal9.model.Agent agent = agentMapper.employeeToAgent(employee);
            List<fr.ca.cats.p0275.s2524.cal9.model.LocalisationAgenda> localisationsAgenda = getLocalisationsAgenda(employee);
            calendrier.setAgent(agent);
            calendrier.setLocalisationsAgenda(localisationsAgenda);
        }
        List<Reunion> reunionResult = new ArrayList<>();
        reunionService.getReunion().forEach(reunion -> reunionResult.add(computeReunion(reunionMapper.reunionAGC9ToReunion(reunion), structureId, reunion)));
        calendrier.setReunions(reunionResult);
        return calendrier;
    }

    private Reunion computeReunion(Reunion reunionResponse, String structureId, fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion reunion) {
        String typeReunion = reunionResponse.getTypeReunion();
        if (Objects.nonNull(typeReunion)) {
            boolean isRendezVous = Boolean.TRUE.equals(isRendezVous(typeReunion));
            boolean notFerie = Boolean.FALSE.equals(isFerie(typeReunion));
            if (isRendezVous) {
                Adresse adresse = agenceService.getAdresse(structureId, reunion, reunionResponse);
                reunionResponse.getCanalInteraction().getAgence().setAdresse(adresse);
                List<Partenaire> partenaires = partenaireService.getPartenaires(reunion, structureId);
                List<Partenaire> partenairesVizio = partenaireService.getPartenairesVizio(reunion, structureId);

                reunionResponse.setPartenaires(partenaires);
                reunionResponse.setPartenairesVizio(partenairesVizio);

            }
            if (notFerie) {
                fr.ca.cats.p0275.s2524.cal9.model.Agent agentCreateur = agentService.getAgentCreateur(structureId, reunion);
                if (Objects.nonNull(agentCreateur)) {
                    reunionResponse.setAgentCreateur(agentCreateur);
                }
            }
        } else {
            log.error(Constants.REUNION_TYPE_NOT_FOUND.getMessage() + LOG_CODE + Constants.REUNION_TYPE_NOT_FOUND.getCode());
            throw new CalendrierBusinessException(Constants.REUNION_TYPE_NOT_FOUND.getMessage(), Constants.REUNION_TYPE_NOT_FOUND.getCode());
        }
        return reunionResponse;
    }

    @Override
    public List<Calendrier> getCalendrierMulti(final String correlationId, final String structureId, final List<String> agentIdList, final String idEds, final String dateDebut, final String dateFin) {
        Pattern pattern = Pattern.compile(REGEX_AGENT_ID);
        if (StringUtils.isEmpty(idEds) && isListEmptyOrNull(agentIdList)) {
            log.error(Constants.EMPTY_LIST_AGENT.getMessage() + LOG_CODE + Constants.EMPTY_LIST_AGENT.getCode());
            throw new CalendrierBusinessException(Constants.EMPTY_LIST_AGENT.getMessage(), Constants.EMPTY_LIST_AGENT.getCode());
        }

        if (StringUtils.isNotEmpty(idEds) && !isListEmptyOrNull(agentIdList)) {
            log.error(Constants.INVALID_LIST_AGENT.getMessage() + LOG_CODE + Constants.INVALID_LIST_AGENT.getCode());
            throw new CalendrierBusinessException(Constants.INVALID_LIST_AGENT.getMessage(), Constants.INVALID_LIST_AGENT.getCode());
        }
        List<String> employeeIdList;
        log.info("ENTREE getCalendrierMuli idEds:{}, structureId:{}, agentId:{}, dateDebut:{}, dateFin:{}",
                idEds,
                structureId,
                agentIdList,
                dateDebut,
                dateFin);
        List<Reunion> reunionResult = new ArrayList<>();
        if (isListEmptyOrNull(agentIdList) && idEds.length() == EDS_CHAR_NUMBER) {
            StringBuilder edsBuilder = edsBuilder(structureId, idEds);
            employeeIdList = collectAgentService.getAllEmployeeIdByIdEds(edsBuilder.toString())
                    .stream().map(agentId -> checkIfAgentIdIsValid(agentId, pattern))
                    .collect(Collectors.toList());
            if (employeeIdList.isEmpty()) {
                log.error(Constants.EMPTY_LIST_EDS_AGENT.getMessage() + LOG_CODE + Constants.EMPTY_LIST_EDS_AGENT.getCode());
                throw new CalendrierBusinessException(Constants.EMPTY_LIST_EDS_AGENT.getMessage(), Constants.EMPTY_LIST_EDS_AGENT.getCode());
            }
            reunionMultiService.getReunionMulti(
                    employeeIdList,
                    dateDebut,
                    dateFin).forEach(reunion ->
                    reunionResult.add(computeReunion(reunionMapper.reunionAGC9ToReunion(reunion), structureId, reunion))
            );
        } else {

            employeeIdList = agentIdList.stream().map(agentId -> checkIfAgentIdIsValid(agentId, pattern)).collect(Collectors.toList());
            reunionMultiService.getReunionMulti(
                    agentIdList,
                    dateDebut,
                    dateFin).forEach(reunion ->
                    reunionResult.add(computeReunion(reunionMapper.reunionAGC9ToReunion(reunion), structureId, reunion))
            );
        }
        return createCalendrierMulti(employeeIdList, reunionResult, structureId);
    }

    private StringBuilder edsBuilder(String structureId, String idEds) {
        StringBuilder edsBuilder = new StringBuilder();
        edsBuilder.append(structureId);
        edsBuilder.append(idEds);
        return  edsBuilder;
    }

    private String checkIfAgentIdIsValid(String agentId, Pattern pattern) {
        Matcher matcher = pattern.matcher(agentId);
        if (!matcher.matches()) {
            agentId = validate(agentId);
            if (!pattern.matcher(agentId).matches()) {
                log.error(Constants.INVALID_AGENT_ID.getMessage() + LOG_CODE + Constants.INVALID_AGENT_ID.getCode());
                throw new CalendrierBusinessException(String.format(Constants.INVALID_AGENT_ID.getMessage(), agentId), Constants.INVALID_AGENT_ID.getCode());
            }
        }
        return agentId;
    }


    private List<Calendrier> createCalendrierMulti(List<String> employeeIdList, List<Reunion> reunionResult, String structureId) {
        List<Calendrier> calendriers = new ArrayList<>();
        employeeIdList.forEach(employeeId -> {
            List<Reunion> employeeReunions = reunionResult.stream()
                    .filter(reunion -> Objects.nonNull(reunion.getReunionContexte()) && employeeId.equals(reunion.getReunionContexte().getAgentId()))
                    .collect(Collectors.toList());
            Calendrier calendrier = new Calendrier();
            Employee employee = collectAgentService.getEmployeeLocalisation(structureId, employeeId);
            calendrier.setReunions(employeeReunions);
            if (Objects.nonNull(employee)) {
                calendrier.setAgent(agentMapper.employeeToAgent(employee));
                List<LocalisationAgenda> localisationsAgenda = getLocalisationsAgenda(employee);
                if(!localisationsAgenda.isEmpty()){
                    calendrier.setLocalisationsAgenda(localisationsAgenda);
                    calendriers.add(calendrier);
                }
            }
        });
        return calendriers;
    }

    private Boolean isFerie(String typeReunion) {
        return TypeReunion.FERIE.getLabel().equals(typeReunion);
    }

    private Boolean isRendezVous(String typeReunion) {
        return TypeReunion.RENDEZVOUS.getLabel().equals(typeReunion);
    }

    private List<LocalisationAgenda> getLocalisationsAgenda(Employee employee) {
        List<LocalisationAgenda> localisationsAgenda = new ArrayList<>();
        if (Objects.nonNull(employee.getDistributionEntities())) {
            employee.getDistributionEntities().stream()
                    .filter(distributionEntity -> Objects.nonNull(distributionEntity.getEmployeeFunctions()))
                    .forEach(distributionEntity -> {
                        List<WorkingHour> workingHourList = distributionEntity.getEmployeeFunctions().stream()
                                .filter(function -> Objects.nonNull(function.getWorkingHours()))
                                .map(Function::getWorkingHours)
                                .flatMap(List::stream)
                                .collect(Collectors.toList());
                        workingHourList.forEach(workingHour -> {
                            if (Objects.nonNull(workingHour.getDay())) {
                                LocalisationAgenda localisationAgenda = getLocalisationAgenda(distributionEntity, workingHour);
                                localisationsAgenda.add(localisationAgenda);
                            }
                        });
                    });
        }
        return localisationsAgenda;
    }

    private fr.ca.cats.p0275.s2524.cal9.model.LocalisationAgenda getLocalisationAgenda(fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.DistributionEntity distributionEntity, fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.WorkingHour workingHour) {
        if (log.isDebugEnabled()) {
            log.debug("buildLocalisationAgenda AgenceId={}, libelle={}, jour={}, heureDebut={}, heureFin={}", distributionEntity.getIdBranch(), distributionEntity.getLabel(), workingHour.getDay(), workingHour.getStartingHour(), workingHour.getEndingHour());
        }
        return localisationAgendaMapper.informationSDG9ToLocalisationAgenda(distributionEntity, workingHour);
    }


    private String validate(String agentIdParam) {
        StringBuilder agentId = new StringBuilder();
        agentId.append(agentIdParam);
        while (agentId.length() < EDS_CHAR_NUMBER) {
            agentId.insert(Integer.parseInt(ZERO), ZERO);
        }
        return agentId.toString();
    }


}
