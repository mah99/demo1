package fr.ca.cats.p0275.s2524.cal9.application;

import fr.ca.cats.p0275.s2524.cal9.domaine.service.VueAgendaService;
import fr.ca.cats.p0275.s2524.cal9.model.VueAgenda;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import static fr.ca.cats.p0070.s1889.easyrest.authentication.utils.AuthLvl.MIN_COLLAB_0;

@RestController
@Slf4j
public class VueAgendaApiImpl implements VueAgendaApi {

    private VueAgendaService vueAgendaService;

    public VueAgendaApiImpl(VueAgendaService vueAgendaService) {
        this.vueAgendaService = vueAgendaService;
    }

    @PreAuthorize(MIN_COLLAB_0)
    @Override
    public ResponseEntity<VueAgenda> getVueAgenda(String correlationId, String structureId, String partnerId) {
        return ResponseEntity.ok(vueAgendaService.getVueAgenda(structureId, partnerId));
    }
}
