package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.impl;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.EmacoMapper;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.Dao.EmacoRepositoryDao;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.EmacoRepository;
import fr.ca.cats.p0275.s2524.cal9.model.PartenaireEmaco;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

@Repository
@Slf4j
public class EmacoRepositoryImpl implements EmacoRepository {

    private EmacoRepositoryDao emacoRepositoryDao;
    private EmacoMapper emacoMapper;

    public EmacoRepositoryImpl(EmacoRepositoryDao emacoRepositoryDao, EmacoMapper emacoMapper) {
        this.emacoRepositoryDao = emacoRepositoryDao;
        this.emacoMapper = emacoMapper;
    }

    @Override
    public PartenaireEmaco getIdPartenaireEmacoByPartnerId(String idPart) {
        return emacoMapper.partenaireEmacoDaoToPartenaireEmaco(emacoRepositoryDao.getIdPartenaireEmacoByPartnerId(idPart));
    }
}
