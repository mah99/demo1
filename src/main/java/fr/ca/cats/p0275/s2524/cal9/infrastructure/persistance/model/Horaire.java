package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class Horaire {

    @Field("jour")
    private String jour;

    @Field("heure_debut")
    private String heureDebut;

    @Field("heure_fin")
    private String heureFin;


}
