package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;

import java.util.List;

public interface AgentRepository {

    Employee getEmployeeByAgentIdAndStructureId(String agentId, String structureId);

    List<Employee> getAllEmployeeByStructureId(String structureId);

    void addEmployee(Employee employee);

}
