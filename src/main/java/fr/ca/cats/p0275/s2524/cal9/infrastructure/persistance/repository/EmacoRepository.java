package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository;

import fr.ca.cats.p0275.s2524.cal9.model.PartenaireEmaco;

public interface EmacoRepository {

    PartenaireEmaco getIdPartenaireEmacoByPartnerId(String idPart);
}
