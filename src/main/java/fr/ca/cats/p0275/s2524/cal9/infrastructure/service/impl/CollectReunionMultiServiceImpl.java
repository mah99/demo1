package fr.ca.cats.p0275.s2524.cal9.infrastructure.service.impl;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.ReunionMultiClientService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectReunionMultiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class CollectReunionMultiServiceImpl implements CollectReunionMultiService {

    private ReunionMultiClientService reunionMultiClientService;

    public CollectReunionMultiServiceImpl(ReunionMultiClientService reunionMultiClientService) {
        this.reunionMultiClientService = reunionMultiClientService;
    }

    @Override
    public List<Reunion> getReunionMulti(List<String> agentIdList, String dateDebut, String dateFin) {
        return this.reunionMultiClientService.getReunionMulti(agentIdList, dateDebut, dateFin);
    }
}
