package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Branch {

    @JsonProperty("id")
    private String id;

    private String structureId;

    private String codeEds;

    @JsonProperty("short_label")
    private String name;

    @JsonProperty("email_address")
    private String email;

    @JsonProperty("city")
    private String city;

    @JsonProperty("zip_code")
    private String zipCode;

    @JsonProperty("postal_address")
    private PostalAddress postalAddress;

}

