package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class PosteFonctionnel {

    @Field("code")
    private String code;

    @Field("date_debut_affectation")
    private String dateDebutAffectation;

    @Field("date_fin_affectation")
    private String dateFinAffectation;

    @Field("agenda")
    private List<Horaire> agenda;

}
