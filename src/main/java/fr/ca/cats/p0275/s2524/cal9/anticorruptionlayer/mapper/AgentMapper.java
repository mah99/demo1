package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface AgentMapper {

    AgentMapper AGENT_MAPPER = Mappers.getMapper( AgentMapper.class );

    @Mapping(source="idEmployee", target = "id")
    @Mapping(source = "nom", target = "nom")
    @Mapping(source = "prenom", target = "prenom")
    @Mapping(source = "civilite", target = "civilite")
    fr.ca.cats.p0275.s2524.cal9.model.Agent employeeToAgent(Employee employee);
}
