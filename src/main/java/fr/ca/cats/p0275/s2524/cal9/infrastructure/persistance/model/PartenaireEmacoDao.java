package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PartenaireEmacoDao {
    private String partenaireEmacoId;

}
