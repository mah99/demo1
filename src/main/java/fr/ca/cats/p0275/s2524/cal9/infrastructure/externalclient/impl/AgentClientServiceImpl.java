package fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.impl;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.AbstractCallInterApi;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.AgentClientService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.CallInterApiTechnicalException;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile("!Mock")
@Service
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class AgentClientServiceImpl extends AbstractCallInterApi<Employee>implements AgentClientService {

    public static final String QUERY_PARAM_SEPARATOR = "?";
    public static final String REGIONAL_BANK_ID = "regional_bank_id=";
    public static final String APPEL_SDG9 = "SDG9";

    @Value("${aia.SDG09.employee.uri}")
    private String aiaSDG09EmployeeUri;

    @Override
    public Employee getEmployee(String structureId, String agentId){
        log.info("collectEmployee agentId={}, structureId={}", agentId, structureId);
        String uriFinal = aiaSDG09EmployeeUri + agentId + QUERY_PARAM_SEPARATOR + REGIONAL_BANK_ID + structureId ;
        try{
            return callInterAPI(uriFinal, APPEL_SDG9);
        }  catch(CallInterApiTechnicalException e){
            log.error("Aucune donnée employee SDG9 agentId={}, structureId={}", agentId, structureId);
            return null;
        }
    }
}
