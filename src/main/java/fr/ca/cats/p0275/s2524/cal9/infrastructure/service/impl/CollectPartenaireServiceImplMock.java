package fr.ca.cats.p0275.s2524.cal9.infrastructure.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectPartenaireService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

import java.util.Arrays;
import java.util.List;

@Profile("Mock")
@Service
@Slf4j
@AllArgsConstructor
public class CollectPartenaireServiceImplMock implements CollectPartenaireService {

    private final List<String> listePartenairesIdTestIntegration = Arrays.asList(
            "00000303935410", //Partenaire Vizio
            "00000303948580" //Partenaire
    );

    @Override
    public Partenaire getPartenaire(String partenaireId, String StrctureId) {
        ObjectMapper mapper = new ObjectMapper();
        Resource resource;
        if(listePartenairesIdTestIntegration.contains(partenaireId)) {
            return getPartenaireTestIntegration(partenaireId, mapper);
        } else {
            resource = new ClassPathResource("/static/Partenaire.json");
        }
        Partenaire partenaire;
        try {
            List<Partenaire> partenaires =  mapper.readValue(resource.getInputStream(), new TypeReference<>() {});
            partenaire = partenaires.stream().filter(p-> p.getIdPartenaire().equals(partenaireId)).findFirst().orElse(null);
        } catch (Exception e) {
            return null;
        }
        return partenaire;
    }

    /**
     * Mock pour les tests d'intégration avec recherche du partenaire ciblé à partir de son id partenaire
     * @param partenaireId String identifiant du partenaire
     * @param mapper ObjectMapper instance
     * @return Partenaire le partenaire ciblé
     */
    private Partenaire getPartenaireTestIntegration(String partenaireId, ObjectMapper mapper) {
        List<Partenaire> partenaires;
        Resource resource = new ClassPathResource("/static/partenaire_test_integration.json");
        try {
            partenaires = Arrays.asList(mapper.readValue(resource.getInputStream(), Partenaire[].class));
        } catch (Exception e) {
            return null;
        }
        Partenaire partenaireCourant = null;
        // Parcours de la liste des partenaires pour rechercher le partenaire ciblé
        for(Partenaire partenaire: partenaires) {
            if(partenaireId.equals(partenaire.getIdPartenaire())) {
                partenaireCourant = partenaire;
            }
        }
        return partenaireCourant;
    }
}
