package fr.ca.cats.p0275.s2524.cal9.domaine.service.impl;

import fr.ca.cats.p0275.s2524.cal9.domaine.service.ReunionService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectReunionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class ReunionServiceImpl implements ReunionService {

    private CollectReunionService collectReunionService;

    public ReunionServiceImpl(final CollectReunionService collectReunionService) {
        this.collectReunionService = collectReunionService;
    }

    @Override
    public List<fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion> getReunion() {
        return collectReunionService.getReunion();
    }
}
