package fr.ca.cats.p0275.s2524.cal9.infrastructure.errors;

import org.springframework.http.HttpStatus;

public class TechnicalException extends AbstractError implements Error {
    public static final Integer DEFAULT_STATUS = 500;
    public static final HttpStatus DEFAULT_HTTP_STATUS = HttpStatus.INTERNAL_SERVER_ERROR;
    public static final String DEFAULT_CODE = "technical_error";

    public TechnicalException(Integer status, String code, String cause, String message) {
        super(status, code, cause, message);
    }
    public TechnicalException(Integer status, String code, String message) {
        super(status, code, message, message);
    }

    public TechnicalException(String message, HttpStatus httpStatus) {
        super(message, httpStatus);
    }

    public TechnicalException(String message) {
        this(DEFAULT_STATUS, DEFAULT_CODE, message);
    }

    public TechnicalException(String message, String businessErrorCode) {
        super(message, businessErrorCode);
    }

    public TechnicalException(String message, Throwable cause) {
        super(message, cause);
    }

    public TechnicalException(Throwable cause) {
        this(cause.getMessage(), cause);
    }

    protected String getDefaultCode() {
        return DEFAULT_CODE;
    }

    protected Integer getDefaultStatus() {
        return DEFAULT_STATUS;
    }

    protected HttpStatus getDefaultHttpStatus() { return DEFAULT_HTTP_STATUS;}
}
