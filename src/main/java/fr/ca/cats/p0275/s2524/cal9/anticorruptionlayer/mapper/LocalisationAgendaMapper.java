package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.DistributionEntity;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.WorkingHour;
import fr.ca.cats.p0275.s2524.cal9.model.LocalisationAgenda;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface LocalisationAgendaMapper {

    LocalisationAgendaMapper LOCALISATION_AGENDA_MAPPER = Mappers.getMapper( LocalisationAgendaMapper.class );

    @Mapping(source = "distributionEntity.idBranch", target = "agenceId")
    @Mapping(source = "distributionEntity.label", target = "libelle")
    @Mapping(source = "distributionEntity.job.id", target = "posteFonctionnel")
    @Mapping(source = "workingHour.day", target = "jour")
    @Mapping(source = "workingHour.startingHour", target = "heureDebut")
    @Mapping(source = "workingHour.endingHour", target = "heureFin")
    LocalisationAgenda informationSDG9ToLocalisationAgenda(DistributionEntity distributionEntity, WorkingHour workingHour);
}
