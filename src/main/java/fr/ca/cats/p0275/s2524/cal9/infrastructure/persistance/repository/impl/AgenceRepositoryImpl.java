package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.impl;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Branch;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.AgenceMapper;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.AgenceRepository;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.Dao.AgenceRepositoryDao;
import org.springframework.stereotype.Repository;

@Repository
public class AgenceRepositoryImpl implements AgenceRepository {

    private AgenceMapper agenceMapper ;

    private AgenceRepositoryDao agenceRepositoryDao ;


    public AgenceRepositoryImpl(final AgenceMapper agenceMapper,final AgenceRepositoryDao agenceRepositoryDao) {
        this.agenceMapper = agenceMapper;
        this.agenceRepositoryDao = agenceRepositoryDao;
    }

    @Override
    public Branch getBranchByAgenceIdAndStructureId(String agenceId, String structureId) {
        return agenceMapper.branchDaoToBranch(agenceRepositoryDao.findByStructureIdAndCodeEds(structureId, agenceId ));
    }

    @Override
    public void addBranch(final Branch branch) {
        agenceRepositoryDao.save(agenceMapper.branchtoBranchDao(branch));
    }
}
