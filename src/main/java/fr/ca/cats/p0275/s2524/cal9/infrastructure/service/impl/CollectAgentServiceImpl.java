package fr.ca.cats.p0275.s2524.cal9.infrastructure.service.impl;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.*;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.DistributionEntityMapper;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.EmployeeMapper;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.WorkingHourMapper;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.EdsAgentService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.AgentClientService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.AgentPortfolioClientService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.OmAgent;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.OmEdsAgent;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.AgentRepository;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.EdsAgentRepository;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectAgentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.Validator.isNotEmpty;

@Profile("!Mock")
@Service
@Slf4j
public class CollectAgentServiceImpl implements CollectAgentService {

    private final AgentRepository agentRepository;
    private final EdsAgentRepository edsAgentRepository;
    private final AgentPortfolioClientService agentPortfolioClientService;
    private final AgentClientService agentClientService;
    private final EdsAgentService edsAgentService;
    private final DistributionEntityMapper distributionEntityMapper;
    private final WorkingHourMapper workingHourMapper;
    private final EmployeeMapper employeeMapper;

    private static Integer FIRST_ADD = 0;
    private static Integer SECONDE_ADD = 1;
    private static Integer FIRST_INDEX_TO_GET_ID_EDS = 5;
    private static Integer FIRST_INDEX_TO_GET_NIVEAU_EDS = 10;

    public CollectAgentServiceImpl(AgentRepository agentRepository, EdsAgentRepository edsAgentRepository, AgentPortfolioClientService agentPortfolioClientService,
                                   AgentClientService agentClientService, EdsAgentService edsAgentService, WorkingHourMapper workingHourMapper, DistributionEntityMapper distributionEntityMapper, EmployeeMapper employeeMapper) {
        this.agentRepository = agentRepository;
        this.edsAgentRepository = edsAgentRepository;
        this.agentPortfolioClientService = agentPortfolioClientService;
        this.agentClientService = agentClientService;
        this.edsAgentService = edsAgentService;
        this.distributionEntityMapper = distributionEntityMapper;
        this.workingHourMapper = workingHourMapper;
        this.employeeMapper = employeeMapper;
    }

    @Override
    public Employee getEmployee(String structureId, String agentId) {
        if (isNotEmpty(agentId, "agentId")) {
            return new Employee();
        }
        Employee employee = agentRepository.getEmployeeByAgentIdAndStructureId(agentId, structureId);
        if (Objects.isNull(employee)) {
            employee = getEmployeeFromApi(structureId, agentId);
        }
        return employee;
    }

    @Override
    public Employee getEmployeeLocalisation(final String structureId, final String agentId) {
        if (isNotEmpty(agentId, "agentId")) {
            return new Employee();
        }
        Employee employee;
        List<OmEdsAgent> allByIdCaisseRegionaleAndAgentId = edsAgentRepository.getAllByIdCaisseRegionaleAndAgentId(structureId, agentId);
        if(Objects.nonNull(allByIdCaisseRegionaleAndAgentId) && !allByIdCaisseRegionaleAndAgentId.isEmpty())
        {
            List<DistributionEntity> distributionEntities = new ArrayList<>();
            AtomicReference<OmAgent> omAgentPrimaire = new AtomicReference<>();
            transformOmEdsAgent(agentId, allByIdCaisseRegionaleAndAgentId, distributionEntities, omAgentPrimaire);
            if(!distributionEntities.isEmpty() && Objects.nonNull(omAgentPrimaire.get())){
                employee = employeeMapper.omAgentToEmployee(omAgentPrimaire.get());
                employee.setDistributionEntities(distributionEntities);
                return employee;
            }
        }

        return getEmployee(structureId, agentId);
    }

    private void transformOmEdsAgent(String agentId, List<OmEdsAgent> allByIdCaisseRegionaleAndAgentId, List<DistributionEntity> distributionEntities, AtomicReference<OmAgent> omAgentPrimaire) {
        allByIdCaisseRegionaleAndAgentId.forEach(omEdsAgent ->
                omEdsAgent.getFonctions().forEach(fonction ->
                        fonction.getAgents().forEach(omAgent -> {
                            if (agentId.equals(omAgent.getMatricule())) {
                                omAgentPrimaire.set(omAgent);
                                omAgent.getPostesFonctionnels().forEach(posteFonctionnel ->
                                {
                                    String idEds = getIdEds(omEdsAgent);
                                    String libelleEds = getLibelleEds(omEdsAgent);
                                    DistributionEntity distributionEntity = distributionEntityMapper.posteFonctionnelToDistributionEntity(posteFonctionnel, idEds, libelleEds);

                                    List<WorkingHour> workingHourList = new ArrayList<>();
                                    if (Objects.nonNull(posteFonctionnel.getAgenda()) && !posteFonctionnel.getAgenda().isEmpty()) {
                                        posteFonctionnel.getAgenda().forEach(horaire -> {
                                            WorkingHour workingHour = workingHourMapper.horaireToWorkingHour(horaire);
                                            workingHourList.add(workingHour);
                                        });
                                        transformDistributionEntity(distributionEntity, workingHourList);
                                        distributionEntities.add(distributionEntity);
                                    }
                                });
                            }
                        })
                )
        );
    }

    private Employee getEmployeeFromApi(String structureId, String agentId) {
        Employee employee;
        employee = agentClientService.getEmployee(structureId, agentId);

        if (Objects.nonNull(employee)) {
            agentRepository.addEmployee(employee);
        }
        return employee;
    }


    private String getIdEds(OmEdsAgent omEdsAgent) {
        return omEdsAgent.getIdEds().substring(FIRST_INDEX_TO_GET_ID_EDS, FIRST_INDEX_TO_GET_NIVEAU_EDS);
    }

    private String getLibelleEds(OmEdsAgent omEdsAgent) {
        return !omEdsAgent.getLibelle().isBlank() ? omEdsAgent.getLibelle() : "";
    }

    private void transformDistributionEntity(DistributionEntity distributionEntity, List<WorkingHour> workingHourList) {
        List<Function> employeeFunctions = new ArrayList<>();
        Function employeeFunction = getFunction(workingHourList);
        employeeFunctions.add(employeeFunction);
        distributionEntity.setEmployeeFunctions(employeeFunctions);
    }

    private Function getFunction(List<WorkingHour> workingHourList) {
        Function employeeFunction = new Function();
        employeeFunction.setWorkingHours(workingHourList);
        return employeeFunction;
    }

    @Override
    public List<Employee> getAllEmployee(String structureId) {
        return agentRepository.getAllEmployeeByStructureId(structureId);
    }

    @Override
    public Portfolio getPortfolio(String structureId, String portfolioId) {
        return agentPortfolioClientService.getPortfolio(structureId, portfolioId);
    }

    @Override
    public List<String> getAllEmployeeIdByIdEds(String idEds) {
        return edsAgentService.getAllEmployeeIdByIdEds(idEds);
    }

}
