package fr.ca.cats.p0275.s2524.cal9.infrastructure.service;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Branch;

public interface CollectAgenceService {
    Branch getAgence(String branchId, String structureId);
}
