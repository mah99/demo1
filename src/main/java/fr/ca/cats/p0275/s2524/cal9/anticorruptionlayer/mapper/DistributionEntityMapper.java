package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.DistributionEntity;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.PosteFonctionnel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface DistributionEntityMapper {

    DistributionEntityMapper DISTRIBUTION_ENTITY_MAPPER = Mappers.getMapper( DistributionEntityMapper.class );

    @Mapping(target = "idBranch", source = "idEds")
    @Mapping(target = "label", source = "libelle")
    @Mapping(target = "job.id", source = "posteFonctionnel.code")
    DistributionEntity posteFonctionnelToDistributionEntity(PosteFonctionnel posteFonctionnel, String idEds, String libelle);
}
