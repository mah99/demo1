package fr.ca.cats.p0275.s2524.cal9.infrastructure.service.impl;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.SettingsClientService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectSettingsService;
import fr.ca.cats.p0275.s2524.cal9.model.Setting;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

@Profile("!Mock")
@Service
@Slf4j
public class CollectSettingsServiceImpl implements CollectSettingsService {

    private SettingsClientService settingsClientService;

    public CollectSettingsServiceImpl(SettingsClientService settingsClientService) {
        this.settingsClientService = settingsClientService;
    }

    @Override
    public List<Setting> getSettings(String structureId) {
        return settingsClientService.getSettings(structureId);
    }
}
