package fr.ca.cats.p0275.s2524.cal9.domaine.service.impl;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.PartenaireMapper;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.PartenaireService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectPartenaireService;
import fr.ca.cats.p0275.s2524.cal9.model.Partenaire;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants.NULL_PARTENAIRE;
import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.Validator.verifyNull;

@Service
@Slf4j
public class PartenaireServiceImpl implements PartenaireService {

    private CollectPartenaireService collectPartenaireService;
    private PartenaireMapper partenaireMapper;

    public PartenaireServiceImpl(final CollectPartenaireService collectPartenaireService, final PartenaireMapper partenaireMapper) {
        this.collectPartenaireService = collectPartenaireService;
        this.partenaireMapper = partenaireMapper;
    }

    @Override
    public List<Partenaire> getPartenaires(fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion reunion, String structureId) {
        List<Partenaire> partenaires = new ArrayList<>();
        if(Objects.nonNull(reunion.getPartenaires())){
            reunion.getPartenaires().forEach(partenaireReunion -> {
                        fr.ca.cats.p0275.s2524.cal9.model.Partenaire partenaireModel;
                        if(Objects.isNull(partenaireReunion.getId())){
                            partenaireModel  = partenaireMapper.partenaireAGC09nullIdPartenaireToPartenaire(partenaireReunion);

                        }else{
                            partenaireModel = partenaireMapper.partenaireAGC9ToPartenaire(partenaireReunion);
                            fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire partenaire = collectPartenaireService.getPartenaire(partenaireModel.getId(), structureId);
                            if(verifyNull(partenaire,String.format(NULL_PARTENAIRE.getMessage(),partenaireModel.getId())))
                            {
                                partenaireModel.setLibelle(getIntitule(partenaire));
                            }
                        }
                        partenaires.add(partenaireModel);
                    }
            );
        }
        return partenaires;
    }

    @Override
    public List<Partenaire> getPartenairesVizio(fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion reunion, String structureId) {
        List<Partenaire> partenaires = new ArrayList<>();
        if(Objects.nonNull(reunion.getPartenaires())){
            reunion.getPartenaires().forEach(partenaireReunion -> {
                fr.ca.cats.p0275.s2524.cal9.model.Partenaire partenaireModel;
                if(Objects.nonNull(partenaireReunion.getIdPartenaireVisio())){
                    partenaireModel = partenaireMapper.partenaireAGC9VisioToPartenaire(partenaireReunion);
                    fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire partenaire = collectPartenaireService.getPartenaire(partenaireModel.getId(), structureId);
                    if(verifyNull(partenaire,String.format(NULL_PARTENAIRE.getMessage(),partenaireModel.getId())))
                    {
                        partenaireModel.setLibelle(getIntitule(partenaire));
                    }
                    partenaires.add(partenaireModel);
                }
            });
        }
        return partenaires;
    }

    private static String getIntitule(fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire partenaire) {
        if (StringUtils.isNotBlank(partenaire.getIntituleSuite())) {
            return partenaire.getIntitule().concat(partenaire.getIntituleSuite());
        }
        else{
            return  partenaire.getIntitule();
        }
    }
}
