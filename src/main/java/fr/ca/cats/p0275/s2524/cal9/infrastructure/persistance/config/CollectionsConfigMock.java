package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration
@DependsOn("mongoTemplate")
@Profile("Mock")
public class CollectionsConfigMock {


    @Autowired
    private MongoTemplate mongoTemplate;


    public void initIndexes() {

    }

}