package fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ca.cats.p0070.s1889.easyrest.commons.util.CatsHolder;
import fr.ca.cats.p0070.s1889.easyrest.interapi.exception.InterApiException;
import fr.ca.cats.p0070.s1889.easyrest.interapi.service.InterApiRequestService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static fr.ca.cats.p0070.s1889.easyrest.interapi.utils.Constants.CATS_CONSOMMATEUR_ORIGINE_HEADER;
import static fr.ca.cats.p0070.s1889.easyrest.interapi.utils.Constants.ERR02;
import static fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants.LOG_CODE;
import static io.netty.handler.codec.rtsp.RtspResponseStatuses.BAD_REQUEST;
import static io.netty.handler.codec.rtsp.RtspResponseStatuses.INTERNAL_SERVER_ERROR;

@Slf4j
public abstract class AbstractCallInterApi<T> {

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private InterApiRequestService interApiRequestService;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${authentication.config.apimGatewayUrl}")
    private String apimGatewayUrl;

    private Class<T> type;

    public AbstractCallInterApi() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public static final String STRUCTURE_ID_HEADER = "structureId";
    public static final String APPEL_API = "Appel API ";
    public static final String LOG_API_AGC9 = "AGC9 getReunion";

    Class<T> getType() {
        return type;
    }

    public HttpHeaders getAiaHeaders() throws InterApiException {
        return interApiRequestService.getAiaHeaders(request.getHeader(HttpHeaders.AUTHORIZATION),
                request.getHeader(CATS_CONSOMMATEUR_ORIGINE_HEADER),
                CatsHolder.getCorrelationId());
    }

    public List<T> callInterAPIList(String uri, String labelApiExterne) {
        HttpHeaders headers = getHttpHeaders(uri);
        if (Objects.nonNull(headers)) {
            headers.add(STRUCTURE_ID_HEADER, request.getHeader(STRUCTURE_ID_HEADER));
        }

        try {
            log.info("Récupération données {}/{}", LOG_API_AGC9, uri);
            HttpEntity<String> entity = new HttpEntity<>(headers);
            String urlAppelRest = apimGatewayUrl + uri;
            ResponseEntity<List<T>> response = restTemplate.exchange(
                    urlAppelRest,
                    HttpMethod.GET,
                    entity,
                    new ParameterizedTypeReference<>() {
                    });
            return getResponseCallApi(uri, response);

        } catch (Exception e) {
            log.error("{} erreur uri: {}: erreur: {} code:{}", APPEL_API, uri, e.getMessage(), Constants.INTERNAL_ERROR_APPEL_API.getCode());
            throw new CallInterApiTechnicalException(INTERNAL_SERVER_ERROR.code() , Constants.INTERNAL_ERROR_APPEL_API.getCode(), e.getMessage(), Constants.INTERNAL_ERROR_APPEL_API.getMessage() + labelApiExterne);
        }
    }

    private List<T> getResponseCallApi(String uri, ResponseEntity<List<T>> response) {
        List<T> body = response.getBody();
        if(Objects.isNull(body)) {
            log.error(Constants.INVALID_ERROR_AGC9_APPEL_API.getMessage() + LOG_CODE + Constants.INVALID_ERROR_AGC9_APPEL_API.getCode());
            throw new CallInterApiTechnicalException(Constants.INVALID_ERROR_AGC9_APPEL_API.getMessage() + uri, Constants.INVALID_ERROR_AGC9_APPEL_API.getCode());
        }
        return body.stream().map(t -> getObject(uri, t)
        ).filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    private T getObject(String uri, T t) {
        byte[] json;
        try {
            json = new ObjectMapper().writeValueAsBytes(t);
            return new ObjectMapper().readValue(json, getType());
        } catch (Exception e) {
            log.error(Constants.BAD_REQUEST_RESPONSE_VALUES.getMessage() + LOG_CODE + Constants.BAD_REQUEST_RESPONSE_VALUES.getCode());
            throw new CallInterApiBusinessException(BAD_REQUEST.code(), Constants.BAD_REQUEST_RESPONSE_VALUES.getCode(), e.getMessage(), Constants.BAD_REQUEST_RESPONSE_VALUES.getMessage() + uri);
        }
    }

    public HttpHeaders getHttpHeaders(String uri) {
        HttpHeaders headers;
        try {
            headers = getAiaHeaders();
        } catch (InterApiException e) {
            if (ERR02.getMessage().equals(e.getMessage())) {
                log.error(Constants.BAD_REQUEST_HEADER_VALUES.getMessage() + LOG_CODE + Constants.BAD_REQUEST_HEADER_VALUES.getCode());
                throw new CallInterApiBusinessException(BAD_REQUEST.code(), Constants.BAD_REQUEST_HEADER_VALUES.getCode(), Constants.BAD_REQUEST_HEADER_VALUES.getMessage() + uri);
            }
            log.error(Constants.INTERNAL_ERROR_HEADER_VALUES.getMessage() + LOG_CODE + Constants.INTERNAL_ERROR_HEADER_VALUES.getCode());
            throw new CallInterApiHeaderTechnicalException(INTERNAL_SERVER_ERROR.code(), Constants.INTERNAL_ERROR_HEADER_VALUES.getCode(), Constants.INTERNAL_ERROR_HEADER_VALUES.getMessage() + uri);
        }
        return headers;
    }

    public T callInterAPI(String uri, String labelApiExterne) {
        HttpHeaders headers = getHttpHeaders(uri);
        try {
            log.info("Récupération données api: {}", labelApiExterne);
            HttpEntity<String> entity = new HttpEntity<>(headers);
            ResponseEntity<T> response = restTemplate.exchange(
                    apimGatewayUrl + uri,
                    HttpMethod.GET,
                    entity,
                    new ParameterizedTypeReference<>() {
                    });
            T body = response.getBody();
            if(Objects.isNull(body)) {
                log.error(Constants.INVALID_ERROR_APPEL_API.getMessage()  + labelApiExterne + LOG_CODE + Constants.INVALID_ERROR_APPEL_API.getCode());
                throw new CallInterApiTechnicalException(Constants.INVALID_ERROR_APPEL_API.getMessage() + labelApiExterne);
            }
            return getObject(uri, body);
        } catch (Exception e) {
            log.error("{} erreur uri: {}: erreur: {} code:{}", APPEL_API, uri, e.getMessage(), Constants.INTERNAL_ERROR_APPEL_API.getCode());
            throw new CallInterApiTechnicalException(INTERNAL_SERVER_ERROR.code(), Constants.INTERNAL_ERROR_APPEL_API.getCode(),  e.getMessage(), Constants.INTERNAL_ERROR_APPEL_API.getMessage() + labelApiExterne);
        }
    }

    public static String getFullUrl(HttpServletRequest request) {
        if (request.getQueryString() == null) {
            return request.getRequestURI();
        }
        return request.getRequestURI() + "?" + request.getQueryString();
    }
}
