package fr.ca.cats.p0275.s2524.cal9.infrastructure.errors;

import java.util.Map;

public interface Error {
    String MESSAGE_ERREUR_STANDARD = "Une erreur s'est produite. Nous faisons de notre mieux pour régler le problème. Merci de réessayer plus tard.";

    Integer getStatus();

    String getCodeError();

    String getMessage();

    String getCauseOriginError();

    Map<String, String> getErrorDetails();

    Boolean hasErrorDetails();
}
