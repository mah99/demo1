package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class OmAgent {

    @Field("id_agent")
    private String idAgent;

    @Field("matricule")
    private String matricule;

    @Field("nom")
    private String nom;

    @Field("prenom")
    private String prenom;

    @Field("civilite")
    private String civilite;

    @Field("postes_fonctionnels")
    private List<PosteFonctionnel> postesFonctionnels;


}
