package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class Employee {

    @JsonProperty("id")
    private String idEmployee;


    @JsonProperty("regional_bank_id")
    private String structureId;

    @JsonProperty("number")
    private String matriculeAgent;

    @JsonProperty("title")
    private String civilite;

    @JsonProperty("first_name")
    private String prenom;

    @JsonProperty("last_name")
    private String nom;

    @JsonProperty("phone_number")
    private String numeroTelephone;

    @JsonProperty("mobile_phone_number")
    private String numeroTelephoneMobile;

    @JsonProperty("email")
    private String email;

    @JsonProperty("distribution_entities")
    private List<DistributionEntity> distributionEntities;
}
