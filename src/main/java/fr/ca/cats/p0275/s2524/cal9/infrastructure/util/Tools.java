package fr.ca.cats.p0275.s2524.cal9.infrastructure.util;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;

@Slf4j
public class Tools {


    public static List<String> convertStringToListString(java.lang.String element) {
        return Arrays.asList(element.split("\\s*,\\s*"));
    }

    public static boolean isListEmptyOrNull(List<String> list) {
        return list == null || list.isEmpty();
    }
}
