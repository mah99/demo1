package fr.ca.cats.p0275.s2524.cal9.domaine.service;

import fr.ca.cats.p0275.s2524.cal9.model.SecteurAgent;
import fr.ca.cats.p0275.s2524.cal9.model.SecteurAgentMulti;

import java.util.List;

public interface EdsAgentService {
    List<SecteurAgent> getSecteurAgent(String structureId, String agentId);
    List<SecteurAgentMulti> getSecteurAgentMulti(String structureId);

    List<String> getAllEmployeeIdByIdEds(String idEds);
}
