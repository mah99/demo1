package fr.ca.cats.p0275.s2524.cal9.domaine.service;

import fr.ca.cats.p0275.s2524.cal9.model.Adresse;
import fr.ca.cats.p0275.s2524.cal9.model.Reunion;

public interface AgenceService {

    Adresse getAdresse(String structureId, fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion reunion, Reunion reunionResponse);
}
