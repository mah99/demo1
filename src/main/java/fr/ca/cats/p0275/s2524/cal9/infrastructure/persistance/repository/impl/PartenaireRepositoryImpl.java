package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.impl;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.PartenaireMapper;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.Dao.PartenaireRepositoryDao;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.PartenaireRepository;
import org.springframework.stereotype.Repository;

@Repository
public class PartenaireRepositoryImpl implements PartenaireRepository {


    private PartenaireRepositoryDao partenaireRepositoryDao ;

    private PartenaireMapper partenaireMapper ;


    public PartenaireRepositoryImpl(final PartenaireRepositoryDao partenaireRepositoryDao, final PartenaireMapper partenaireMapper) {
        this.partenaireRepositoryDao = partenaireRepositoryDao;
        this.partenaireMapper = partenaireMapper;
    }


    @Override
    public Partenaire getPartenaire(String partenaireId, String structureId) {
        return partenaireMapper.partenaireDaoToPartenaire(partenaireRepositoryDao.findByIdCaisseRegionaleAndIdPartenaire(structureId, partenaireId));
    }

    @Override
    public void addPartenaire(Partenaire partenaire) {
        partenaireRepositoryDao.save(partenaireMapper.partenaireToPartenaireDao(partenaire));
    }
}
