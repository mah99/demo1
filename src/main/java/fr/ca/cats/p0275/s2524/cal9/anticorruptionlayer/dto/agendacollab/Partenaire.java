package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Slf4j
public class Partenaire {

    @JsonProperty("id")
    private String id;

    @JsonProperty("libelle")
    private String libelle;

    @JsonProperty("idPartenaireVisio")
    private String idPartenaireVisio;
}
