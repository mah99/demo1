package fr.ca.cats.p0275.s2524.cal9.infrastructure.service.impl;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.EmacoToolsService;
import fr.ca.cats.p0275.s2524.cal9.model.PartenaireEmaco;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Profile("Mock")
@Service
@Slf4j
public class EmacoToolsServiceImplMock implements EmacoToolsService {


    @Override
    public PartenaireEmaco getIdPartenaireEmacoByPartnerId(String idPart) {
        if(!Objects.equals(idPart, "00000304806206")) {
            return null;
        } else {
            PartenaireEmaco idpn = new PartenaireEmaco();
            idpn.setPartenaireEmacoId("06430548");
            return idpn;
        }
    }
}
