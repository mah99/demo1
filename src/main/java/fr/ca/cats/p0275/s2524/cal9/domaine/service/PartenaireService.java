package fr.ca.cats.p0275.s2524.cal9.domaine.service;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion;
import fr.ca.cats.p0275.s2524.cal9.model.Partenaire;

import java.util.List;

public interface PartenaireService {

    List<Partenaire> getPartenaires(Reunion reunion, String structureId);
    List<Partenaire> getPartenairesVizio(Reunion reunion, String structureId);
}
