package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;


import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.PartenaireDao;
import fr.ca.cats.p0275.s2524.cal9.model.Partenaire;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface PartenaireMapper {

    PartenaireMapper PARTENAIRE_MAPPER = Mappers.getMapper( PartenaireMapper.class );

    @Mapping(target = "libelle",  ignore = true)
    Partenaire partenaireAGC9ToPartenaire(fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Partenaire partenaire);

    @Mapping(target = "id",  ignore = true)
    Partenaire partenaireAGC09nullIdPartenaireToPartenaire(fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Partenaire partenaire);

    @Mapping(target = "libelle",  ignore = true)
    @Mapping(source="idPartenaireVisio", target = "id")
    Partenaire partenaireAGC9VisioToPartenaire(fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Partenaire partenaire);

    PartenaireDao partenaireToPartenaireDao(fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire partenaire);
    fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire partenaireDaoToPartenaire(PartenaireDao partenaireDao);
}
