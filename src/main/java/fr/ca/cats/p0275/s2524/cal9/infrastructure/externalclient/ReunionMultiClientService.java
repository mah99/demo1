package fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion;

import java.util.List;

public interface ReunionMultiClientService {

    List<Reunion> getReunionMulti(List<String> agentIdList, String dateDebut, String dateFin);
}