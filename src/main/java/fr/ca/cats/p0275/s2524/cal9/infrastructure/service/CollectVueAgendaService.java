package fr.ca.cats.p0275.s2524.cal9.infrastructure.service;

import fr.ca.cats.p0275.s2524.cal9.model.VueAgenda;

public interface CollectVueAgendaService {

    VueAgenda getVueAgenda(String partnerId, String agentId);
}
