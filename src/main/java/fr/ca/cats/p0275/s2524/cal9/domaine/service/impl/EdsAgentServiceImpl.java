package fr.ca.cats.p0275.s2524.cal9.domaine.service.impl;

import fr.ca.cats.p0275.s2524.cal9.domaine.service.EdsAgentService;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.exception.EdsAgentBusinessException;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectEdsAgentService;
import fr.ca.cats.p0275.s2524.cal9.model.SecteurAgent;
import fr.ca.cats.p0275.s2524.cal9.model.SecteurAgentMulti;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

import static fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants.LOG_CODE;
import static fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants.NOT_FOUND_EDS_AGENT;

@Service
@Slf4j
public class EdsAgentServiceImpl implements EdsAgentService {


    private CollectEdsAgentService collectEdsAgentService;


    public EdsAgentServiceImpl(CollectEdsAgentService collectEdsAgentService) {
        this.collectEdsAgentService = collectEdsAgentService;
    }


    @Override
    public List<SecteurAgent> getSecteurAgent(String structureId, String agentId) {
        log.info("ENTREE getSecteurAgent structureId:{}, agentId:{}", structureId, agentId);
        return collectEdsAgentService.getSecteurAgent(structureId, agentId);
    }

    @Override
    public List<SecteurAgentMulti> getSecteurAgentMulti(String structureId) {
        log.info("ENTREE getSecteurAgentMulti structureId:{}", structureId);
        List<SecteurAgentMulti> secteurAgentMulti = collectEdsAgentService.getSecteurAgentMulti(structureId);
        if(Objects.isNull(secteurAgentMulti)){
            log.error(Constants.NOT_FOUND_EDS_AGENT.getMessage() + LOG_CODE + Constants.NOT_FOUND_EDS_AGENT.getCode());
            throw new EdsAgentBusinessException(String.format(NOT_FOUND_EDS_AGENT.getMessage(),structureId), NOT_FOUND_EDS_AGENT.getCode(), HttpStatus.NOT_FOUND);
        }
        return secteurAgentMulti;
    }

    @Override
    public List<String> getAllEmployeeIdByIdEds(String idEds) {
        log.info("ENTREE getAllEmployeeIdByIdEds idEds:{}", idEds);
        return collectEdsAgentService.getAllEmployeeIdByIdEds(idEds);
    }
}
