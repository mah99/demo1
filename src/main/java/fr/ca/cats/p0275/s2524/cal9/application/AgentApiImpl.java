package fr.ca.cats.p0275.s2524.cal9.application;

import fr.ca.cats.p0275.s2524.cal9.domaine.service.CalendrierService;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.EdsAgentService;
import fr.ca.cats.p0275.s2524.cal9.model.Calendrier;
import fr.ca.cats.p0275.s2524.cal9.model.SecteurAgent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static fr.ca.cats.p0070.s1889.easyrest.authentication.utils.AuthLvl.MIN_COLLAB_0;
import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.Tools.convertStringToListString;

@Slf4j
@RestController
public class AgentApiImpl implements AgentsApi {

    private final CalendrierService calendrierService;
    private final EdsAgentService edsAgentService;

    public AgentApiImpl(CalendrierService calendrierService, EdsAgentService edsAgentService) {
        this.calendrierService = calendrierService;
        this.edsAgentService = edsAgentService;
    }

    @PreAuthorize(MIN_COLLAB_0)
    @Override
    public ResponseEntity<Calendrier> getCalendrier(String correlationId, String structureId, String agentId, String dateDebut, String dateFin) {
        Calendrier calendrier = calendrierService.getCalendrier(correlationId, structureId, agentId, dateDebut, dateFin);

        return ResponseEntity.ok(calendrier);
    }

    @PreAuthorize(MIN_COLLAB_0)
    @Override
    public ResponseEntity<List<SecteurAgent>> getSecteurAgent(String correlationId, String structureId, String agentId) {
        List<SecteurAgent> secteurAgent = edsAgentService.getSecteurAgent(structureId, agentId);

        return ResponseEntity.ok(secteurAgent);
    }

    @Override
    public ResponseEntity<List<Calendrier>> getCalendrierMulti(String correlationId, String structureId, String dateDebut, String dateFin, String agentIdList, String idEds) {
        List<String> agentIdListFormatted = agentIdList == null ? null : convertStringToListString(agentIdList);

        List<Calendrier> calendriers = calendrierService.getCalendrierMulti(correlationId, structureId, agentIdListFormatted, idEds, dateDebut, dateFin);

        return ResponseEntity.ok(calendriers);
    }


}