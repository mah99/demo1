package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.AgentDao;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.OmAgent;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface EmployeeMapper {

    EmployeeMapper EMPLOYEE_MAPPER = Mappers.getMapper( EmployeeMapper.class );

    @Mapping(source="idEmployee", target = "idAgent")
    AgentDao employeeToEmployeeDao(Employee employee);

    @Mapping(source="idAgent", target = "idEmployee")
    Employee employeeDaoToEmployee(AgentDao agentDao);

    @Mapping(source="matricule", target = "idEmployee")
    @Mapping(source="matricule", target = "matriculeAgent")
    Employee omAgentToEmployee(OmAgent omAgent);

}
