package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab;
import lombok.Getter;
import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReunionContexte {
    private String agentId;
    private String dateReunion;
    private String heureDebutReunion;
}
