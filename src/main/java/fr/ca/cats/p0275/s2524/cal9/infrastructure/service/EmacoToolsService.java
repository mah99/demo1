package fr.ca.cats.p0275.s2524.cal9.infrastructure.service;

import fr.ca.cats.p0275.s2524.cal9.model.PartenaireEmaco;

public interface EmacoToolsService {

    PartenaireEmaco getIdPartenaireEmacoByPartnerId(String idPart);
}
