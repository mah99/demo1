package fr.ca.cats.p0275.s2524.cal9.infrastructure.service.impl;


import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.EmacoRepository;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.EmacoToolsService;
import fr.ca.cats.p0275.s2524.cal9.model.PartenaireEmaco;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile("!Mock")
@Service
@Slf4j
public class EmacoToolsServiceImpl implements EmacoToolsService {

    private EmacoRepository emacoRepository;

    public EmacoToolsServiceImpl(EmacoRepository emacoRepository) {
        this.emacoRepository = emacoRepository;
    }

    @Override
    public PartenaireEmaco getIdPartenaireEmacoByPartnerId(String idPart) {
        return emacoRepository.getIdPartenaireEmacoByPartnerId(idPart);
    }
}
