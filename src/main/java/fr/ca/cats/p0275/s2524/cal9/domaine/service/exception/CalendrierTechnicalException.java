package fr.ca.cats.p0275.s2524.cal9.domaine.service.exception;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.TechnicalException;

public class CalendrierTechnicalException extends TechnicalException {

    public CalendrierTechnicalException(Integer status, String businessErrorCode, String message) {
        super(status, businessErrorCode, message);
    }

    public CalendrierTechnicalException(Integer status, String businessErrorCode, String causeValue, String message) {
        super(status, businessErrorCode, causeValue, message);
    }

    public CalendrierTechnicalException(String message) {
        super(message);
    }

    public CalendrierTechnicalException(String message, String businessErrorCode) {
        super(message,businessErrorCode);
    }
}
