package fr.ca.cats.p0275.s2524.cal9.infrastructure.service.impl;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.PartenaireClientService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.PartenaireRepository;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectPartenaireService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Profile("!Mock")
@Service
@Slf4j
public class CollectPartenaireServiceImpl implements CollectPartenaireService {

    private PartenaireRepository partenaireRepository;

    private PartenaireClientService partenaireClientService;

    public CollectPartenaireServiceImpl(final PartenaireRepository partenaireRepository, final PartenaireClientService partenaireClientService) {
        this.partenaireRepository = partenaireRepository;
        this.partenaireClientService = partenaireClientService;
    }

    @Override
    public Partenaire getPartenaire(final String partnerId, final String structureId) {
        Partenaire partenaire = partenaireRepository.getPartenaire(partnerId, structureId);

        if (Objects.isNull(partenaire)) {
            partenaire = partenaireClientService.getPartenaire(partnerId, structureId);
            if(Objects.nonNull(partenaire)){
                partenaireRepository.addPartenaire(partenaire);
            }
        }
        return partenaire;
    }
}
