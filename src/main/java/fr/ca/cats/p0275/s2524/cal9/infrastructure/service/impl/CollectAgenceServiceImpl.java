package fr.ca.cats.p0275.s2524.cal9.infrastructure.service.impl;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Branch;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.AgenceClientService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.AgenceRepository;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectAgenceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Profile("!Mock")
@Service
@Slf4j
public class CollectAgenceServiceImpl implements CollectAgenceService {

    private AgenceRepository agenceRepository ;

    private AgenceClientService agenceClientService ;

    public CollectAgenceServiceImpl(final AgenceRepository agenceRepository, final  AgenceClientService agenceClientService) {
        this.agenceRepository = agenceRepository;
        this.agenceClientService = agenceClientService;
    }

    @Override
    public Branch getAgence(final String branchId,final  String structureId) {
        Branch branch = agenceRepository.getBranchByAgenceIdAndStructureId(branchId, structureId);

        if (Objects.isNull(branch)) {
            branch = agenceClientService.getAgence(branchId, structureId);
            if(Objects.nonNull(branch)){
                agenceRepository.addBranch(branch);
            }
        }
        return branch ;
    }
}
