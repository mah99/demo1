package fr.ca.cats.p0275.s2524.cal9.infrastructure.errors;


import lombok.Generated;
import org.springframework.http.HttpStatus;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public abstract class AbstractError extends RuntimeException implements Error {
    private HttpStatus httpStatus;
    private String codeError;
    private Integer status;
    private String causeError;
    private Map<String, String> errorDetails;

    public AbstractError(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    public AbstractError(String message, String codeError, HttpStatus httpStatus) {
        super(message);
        this.codeError = codeError;
        this.httpStatus = httpStatus;
    }

    public AbstractError(String message, String codeError) {
        super(message);
        this.codeError = codeError;
    }

    public AbstractError(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractError(Integer status, String codeError, String causeValue, String message) {
        super(message);
        this.causeError = causeValue;
        this.codeError = codeError;
        this.status = status;
    }

    public Integer getStatus() {
        return (Integer) Optional.ofNullable(this.status).orElse(this.getDefaultStatus());
    }

    public HttpStatus getHttpStatus() {
        return (HttpStatus) Optional.ofNullable(this.httpStatus).orElse(this.getDefaultHttpStatus());
    }

    public String getCodeError() {
        return (String)Optional.ofNullable(this.codeError).orElse(this.getDefaultCode());
    }

    public String getCauseOriginError() {
        return (String)Optional.ofNullable(this.causeError).orElseGet(() -> {
            return (String)Optional.ofNullable(this.getCause()).map((cc) -> {
                return cc.getMessage();
            }).orElse("");
        });
    }

    public Error addErrorDetails(Map<String, String> details) {
        this.errorDetails = Collections.unmodifiableMap(details);
        return this;
    }

    public Map<String, String> getErrorDetails() {
        return (Map)Optional.ofNullable(this.errorDetails).orElse(Collections.unmodifiableMap(new HashMap()));
    }

    public Boolean hasErrorDetails() {
        return this.errorDetails != null && this.errorDetails.size() > 0;
    }

    protected abstract String getDefaultCode();
    protected abstract Integer getDefaultStatus();
    protected abstract HttpStatus getDefaultHttpStatus();

    @Generated
    public String toString() {
        String var10000 = this.getCodeError();
        return "AbstractError(codeError=" + var10000 + ", status=" + this.getStatus() + ", causeError=" + this.causeError + ", errorDetails=" + this.getErrorDetails() + ")";
    }
}
