package fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.impl;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.AbstractCallInterApi;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.CallInterApiTechnicalException;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.ReunionMultiClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


@Profile("!Mock")
@Service
@Slf4j
public class ReunionMultiClientServiceImpl extends AbstractCallInterApi<Reunion> implements ReunionMultiClientService {

    public static final String QUERY_PARAM_SEPARATOR = "?";
    public static final String QUERY_PARAM_COMBINATOR = "&";
    public static final String APPEL_AGC9 = "AGC9";

    public static final String AGENT_ID_LIST = "agentIdList=";
    public static final String DATE_DEBUT = "dateDebut=";
    public static final String DATE_FIN = "dateFin=";

    @Value("${aia.agc9.reunions.uri}")
    private String aiaAGC9Uri;

    @Autowired
    private HttpServletRequest request;


    @Override
    public List<Reunion> getReunionMulti(final List<String> agentIdList, final String dateDebut, final String dateFin) {
        List<Reunion> reunions = new ArrayList<>();
        try {
        String uriFinal = aiaAGC9Uri
                + request.getRequestURI() + "/"
                + QUERY_PARAM_SEPARATOR + AGENT_ID_LIST + agentIdList.toString().replace("[", "").replace("]", "")
                + QUERY_PARAM_COMBINATOR + DATE_DEBUT + dateDebut
                + QUERY_PARAM_COMBINATOR + DATE_FIN + dateFin;
        log.info("collectReunionsMulti {}", uriFinal);
        reunions = callInterAPIList(uriFinal, APPEL_AGC9);
        }catch (CallInterApiTechnicalException e){
            log.error("Aucune donnée réunion AGC9 ");
        }
        return reunions;
    }
}