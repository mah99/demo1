package fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Portfolio;

public interface AgentPortfolioClientService {
    Portfolio getPortfolio(String structureId, String portfolioId);

}
