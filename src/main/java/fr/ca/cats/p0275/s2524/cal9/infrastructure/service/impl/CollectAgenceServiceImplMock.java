package fr.ca.cats.p0275.s2524.cal9.infrastructure.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Branch;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectAgenceService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

@Profile("Mock")
@Service
@AllArgsConstructor
@Slf4j
public class CollectAgenceServiceImplMock implements CollectAgenceService {

    @Override
    public Branch getAgence(String branchId, String structureId) {
        ObjectMapper mapper = new ObjectMapper();
        Resource resource = new ClassPathResource("/static/sdg9BranchID.json");
        Branch branch;
        try {
            branch =  mapper.readValue(resource.getInputStream(), Branch.class);
        } catch (Exception e) {
            return null;
        }
        return branch;
    }

}
