package fr.ca.cats.p0275.s2524.cal9.infrastructure.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.SecteurAgentMapper;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.OmEdsAgent;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectEdsAgentService;
import fr.ca.cats.p0275.s2524.cal9.model.SecteurAgent;
import fr.ca.cats.p0275.s2524.cal9.model.SecteurAgentMulti;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.util.*;

import static fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants.AGENT_NOT_FOUND;
import static fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants.NULL_PARAM;

@Profile("Mock")
@Service
@Slf4j
public class CollectEdsAgentServiceImplMock implements CollectEdsAgentService {

    private SecteurAgentMapper secteurAgentMapper;

    public CollectEdsAgentServiceImplMock(SecteurAgentMapper secteurAgentMapper) {
        this.secteurAgentMapper = secteurAgentMapper;
    }

    @Override
    public List<SecteurAgent> getSecteurAgent(String structureId, String agentId) {
        ObjectMapper mapper = new ObjectMapper();
        Resource resourceEdsParent = new ClassPathResource("/static/OMEdsAgentParent.json");

        OmEdsAgent omEdsParent = new OmEdsAgent();
        try {
            omEdsParent = mapper.readValue(resourceEdsParent.getInputStream(), new TypeReference<List<OmEdsAgent>>() {
            }).get(0);
        } catch (Exception ignored) {
        }

        List<OmEdsAgent> omEdsFils = new ArrayList<>();
        Resource resourceEdsFils = new ClassPathResource("/static/OMEdsAgentFils.json");
        try {
            omEdsFils = mapper.readValue(resourceEdsFils.getInputStream(), new TypeReference<>() {
            });
        } catch (Exception ignored) {
        }


        List<SecteurAgent> popins = new ArrayList<>();
        List<OmEdsAgent> edsOfSelectedAgent = List.of(omEdsFils.get(0));
        if (Objects.isNull(edsOfSelectedAgent)) {
            log.error(NULL_PARAM.getMessage(), String.format(AGENT_NOT_FOUND.getMessage(), agentId));
        } else {
            Set<String> idParents = new HashSet<>();
            edsOfSelectedAgent.forEach(omEdsAgent -> idParents.add(omEdsAgent.getIdEdsParent()));

            OmEdsAgent finalOmEdsParent = omEdsParent;
            List<OmEdsAgent> finalOmEdsFils = omEdsFils;
            idParents.forEach(idParent -> {
                OmEdsAgent parentEds = finalOmEdsParent;
                if (Objects.isNull(parentEds)) {
                    parentEds = new OmEdsAgent();
                }
                popins.addAll(getSecteurAgent(parentEds, finalOmEdsFils));
            });
        }


        return popins;
    }

    @Override
    public List<SecteurAgentMulti> getSecteurAgentMulti(String structureId) {
        ObjectMapper mapper = new ObjectMapper();
        List<OmEdsAgent> omEds;
        Resource resourceEdsAgent = new ClassPathResource("/static/OMEdsAgent.json");
        List<SecteurAgentMulti> secteurs = new ArrayList<>();
        try {
            omEds = mapper.readValue(resourceEdsAgent.getInputStream(), new TypeReference<>() {
            });
            omEds.forEach(omEdsAgent -> secteurs.add(secteurAgentMapper.omEdsAgentToEdsMulti(omEdsAgent)));

        } catch (Exception ignored) {
        }
        return secteurs;
    }

    @Override
    public List<String> getAllEmployeeIdByIdEds(String idEds) {
        return null;
    }


    private List<SecteurAgent> getSecteurAgent(OmEdsAgent parentEds, List<OmEdsAgent> omEdsFils) {
        SecteurAgent edsParent = secteurAgentMapper.omEdsAgentToEds(parentEds);
        List<SecteurAgent> secteurAgents = new ArrayList<>();
        secteurAgents.add(edsParent);
        secteurAgents.addAll(secteurAgentMapper.omEdsAgentsToEds(omEdsFils));


        return secteurAgents;
    }
}
