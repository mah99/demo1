package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.Dao;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.BranchDao;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AgenceRepositoryDao extends MongoRepository<BranchDao, String> {


    BranchDao findByStructureIdAndCodeEds(String structureId, String codeEds);

}