package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;


import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Branch;
import fr.ca.cats.p0275.s2524.cal9.model.Adresse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface AdresseMapper {

    AdresseMapper ADRESSE_MAPPER = Mappers.getMapper( AdresseMapper.class );

    @Mapping(source = "postalAddress.postalAddressLine4", target = "rue")
    @Mapping(source = "zipCode", target = "codePostal")
    @Mapping(source = "city", target = "ville")
    Adresse adresseToBranch(Branch branch);

}
