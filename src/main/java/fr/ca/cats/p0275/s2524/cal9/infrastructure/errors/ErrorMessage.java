package fr.ca.cats.p0275.s2524.cal9.infrastructure.errors;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Getter
@NoArgsConstructor
public class ErrorMessage {

    private HttpStatus status;
    private String code;
    private String message;
    private String cause;

    public ErrorMessage(HttpStatus status, String code, String message, String cause) {
        super();
        this.status = status;
        this.code = code;
        this.message = message;
        this.cause = cause;
    }
}

