package fr.ca.cats.p0275.s2524.cal9.domaine.service.impl;

import fr.ca.cats.p0275.s2524.cal9.domaine.service.ReunionMultiService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectReunionMultiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class ReunionMultiServiceImpl implements ReunionMultiService {

    private CollectReunionMultiService collectReunionMultiService;

    public ReunionMultiServiceImpl(final CollectReunionMultiService collectReunionMultiService) {
        this.collectReunionMultiService = collectReunionMultiService;
    }


    @Override
    public List<fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion> getReunionMulti(List<String> agentIdList, String dateDebut, String dateFin) {
        return collectReunionMultiService.getReunionMulti(agentIdList, dateDebut, dateFin);
    }
}
