package fr.ca.cats.p0275.s2524.cal9.infrastructure.service;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire;

public interface CollectPartenaireService {
    Partenaire getPartenaire(String partnerId , String structureId);
}
