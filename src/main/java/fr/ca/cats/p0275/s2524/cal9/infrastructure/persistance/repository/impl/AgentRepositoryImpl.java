package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.impl;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.EmployeeMapper;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.AgentRepository;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.Dao.AgentRepositoryDao;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class AgentRepositoryImpl implements AgentRepository {


    private AgentRepositoryDao agentRepositoryDao;
    private EmployeeMapper employeeMapper;

    public AgentRepositoryImpl(final AgentRepositoryDao agentRepositoryDao, final EmployeeMapper employeeMapper) {
        this.agentRepositoryDao = agentRepositoryDao;
        this.employeeMapper = employeeMapper;
    }

    @Override
    public Employee getEmployeeByAgentIdAndStructureId(final String agentId, final String structureId) {
        return employeeMapper.employeeDaoToEmployee(agentRepositoryDao.findByIdAgentAndStructureId(agentId, structureId));
    }

    @Override
    public void addEmployee(final Employee employee) {
        agentRepositoryDao.save(employeeMapper.employeeToEmployeeDao(employee));

    }


    @Override
    public List<Employee> getAllEmployeeByStructureId(final String structureId) {
        return agentRepositoryDao.findAllByStructureId(structureId)
                .stream()
                .map(agentDao ->
                        employeeMapper.employeeDaoToEmployee(agentDao))
                .collect(Collectors.toList());
    }

}
