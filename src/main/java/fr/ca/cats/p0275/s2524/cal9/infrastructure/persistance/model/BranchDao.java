package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.PostalAddress;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document("branch")
public class BranchDao {

    private String structureId;

    @Id
    private String codeEds;

    @JsonProperty("short_label")
    private String name;

    @JsonProperty("email_address")
    private String email;

    @JsonProperty("city")
    private String city;

    @JsonProperty("zip_code")
    private String zipCode;

    @JsonProperty("postal_address")
    private PostalAddress postalAddress;

    private LocalDateTime registeredDate = LocalDateTime.now();

}

