package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.Dao;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.PartenaireDao;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PartenaireRepositoryDao extends MongoRepository<PartenaireDao, String> {

    PartenaireDao findByIdCaisseRegionaleAndIdPartenaire(String caisseRegionalId, String partenaireId);

}