package fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.impl;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.AbstractCallInterApi;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.CallInterApiTechnicalException;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.PartenaireClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile("!Mock")
@Service
@Slf4j
public class PartenaireClientServiceImpl extends AbstractCallInterApi<Partenaire> implements PartenaireClientService {

    public static final String QUERY_PARAM_SEPARATOR = "?";
    public static final String REGIONAL_BANK_ID = "id_caisse_regionale=";
    public static final String APPEL_PNE9 = "PNE9";


    @Value("${aia.PNE09.partenaire.uri}")
    private String aiaPNE09Uri;

    @Override
    public Partenaire getPartenaire(String partenaireId, String structureId){
        log.info("collectIndividualBasic partenaireId={}, structureId={}", partenaireId, structureId);
        String uriFinal = aiaPNE09Uri + partenaireId  +  QUERY_PARAM_SEPARATOR + REGIONAL_BANK_ID + structureId ;
        try{
            return callInterAPI(uriFinal, APPEL_PNE9);
        }  catch(CallInterApiTechnicalException e){
            log.error("Aucune donnée partenaire PNE9 partenaireId={}, structureId={}", partenaireId, structureId);
            return null;
        }
    }
}
