package fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.ReunionClientService;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Profile("Mock")
@NoArgsConstructor
@Service
public class ReunionClientServiceImplMock implements ReunionClientService {

    private List<Reunion> reunion;

    public HttpHeaders getAiaHeaders() {
        return null;
    }

    @Override
    public List<Reunion> getReunion() {
        ObjectMapper mapper = new ObjectMapper();
        Resource resource = new ClassPathResource("/static/agc9.json");
        List<Reunion> reunions = new ArrayList<>();

        try {
            Object obj = mapper.readValue(resource.getInputStream(), Object.class);
            reunion = new ObjectMapper().convertValue(obj, new TypeReference<>() {
            });
            reunions.add(reunion.get(0));
            reunions.add(reunion.get(1));
            reunions.add(reunion.get(2));
            reunions.add(reunion.get(3));
        } catch (Exception e) {
            return null;
        }
        return reunions;
    }
}