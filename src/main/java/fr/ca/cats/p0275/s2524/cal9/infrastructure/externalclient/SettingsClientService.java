package fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient;

import fr.ca.cats.p0275.s2524.cal9.model.Setting;

import java.util.List;

public interface SettingsClientService {

    List<Setting> getSettings(String structureId);
}
