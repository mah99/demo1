package fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.TechnicalException;

public class CallInterApiTechnicalException extends TechnicalException {

    public CallInterApiTechnicalException(Integer status, String businessErrorCode, String message) {
        super(status, businessErrorCode, message);
    }

    public CallInterApiTechnicalException(Integer status, String businessErrorCode, String causeValue, String message) {
        super(status, businessErrorCode, causeValue, message);
    }

    public CallInterApiTechnicalException(String message) {
        super(message);
    }

    public CallInterApiTechnicalException(String message, String businessErrorCode) {
        super(message,businessErrorCode);
    }
}
