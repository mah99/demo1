package fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire;

public interface PartenaireClientService {

    Partenaire getPartenaire(String partenaireId, String structureId);
}
