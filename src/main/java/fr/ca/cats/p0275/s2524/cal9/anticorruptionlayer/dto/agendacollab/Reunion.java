package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import java.util.List;

@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Reunion {
    @JsonProperty("reunionContexte")
    private ReunionContexte reunionContexte;

    @JsonProperty("action")
    private Action action;

    @JsonProperty("partenaires")
    private List<Partenaire> partenaires;

    @JsonProperty("experts")
    private List<Expert> experts;

    @JsonProperty("dateDebut")
    private String dateDebut;

    @JsonProperty("dateFin")
    private String dateFin;

    @JsonProperty("typeReunion")
    private String typeReunion;

    @JsonProperty("theme")
    private Theme theme;

    @JsonProperty("objet")
    private Objet objet;

    @JsonProperty("canalInteraction")
    private CanalInteraction canalInteraction;

    @JsonProperty("piecesAFournir")
    @Valid
    private List<PiecesAFournirAgc9> piecesAFournir = null;

    @JsonProperty("commentaire")
    private String commentaire;

    @JsonProperty("plageProtegee")
    private Boolean plageProtegee;

}
