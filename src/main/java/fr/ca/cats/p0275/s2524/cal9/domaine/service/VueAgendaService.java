package fr.ca.cats.p0275.s2524.cal9.domaine.service;

import fr.ca.cats.p0275.s2524.cal9.model.VueAgenda;

public interface VueAgendaService {

    VueAgenda getVueAgenda(String structureId, String partnerId);
}
