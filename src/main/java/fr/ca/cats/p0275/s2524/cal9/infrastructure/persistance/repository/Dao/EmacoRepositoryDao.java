package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.Dao;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.PartenaireEmacoDao;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;


public interface EmacoRepositoryDao {

    @Select("SELECT IDPAR5 FROM V0DGCZ06 WHERE IDPART = #{partId} ")
    @Results(value = {
            @Result(column = "IDPAR5", property = "partenaireEmacoId")

    })
    PartenaireEmacoDao getIdPartenaireEmacoByPartnerId(@Param("partId") final String partId);
}
