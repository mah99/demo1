package fr.ca.cats.p0275.s2524.cal9.infrastructure.service.impl;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.SecteurAgentMapper;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.OmAgent;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.OmEdsAgent;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.PosteFonctionnel;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.EdsAgentRepository;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectEdsAgentService;
import fr.ca.cats.p0275.s2524.cal9.model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Profile("!Mock")
@Service
@Slf4j
public class CollectEdsAgentServiceImpl implements CollectEdsAgentService {

    private EdsAgentRepository edsAgentRepository;

    private SecteurAgentMapper secteurAgentMapper;

    private static final String DEFAULT_TYPE_EDS = "06";
    private static final Integer ID_AGENT_DEFAULT_LENGTH = 5;

    public CollectEdsAgentServiceImpl(EdsAgentRepository edsAgentRepository, SecteurAgentMapper secteurAgentMapper) {
        this.edsAgentRepository = edsAgentRepository;
        this.secteurAgentMapper = secteurAgentMapper;
    }

    @Override
    public List<SecteurAgent> getSecteurAgent(String structureId, String agentId) {
        List<SecteurAgent> secteurAgents = new ArrayList<>();
        // recuperer les eds où l'agent est attribué
        List<OmEdsAgent> edsOfSelectedAgent = edsAgentRepository.getIdEdsParentByStructureIdAndAgentId(structureId, agentId);

        if (Objects.isNull(edsOfSelectedAgent)) {
            log.error(Constants.NULL_PARAM.getMessage(), String.format(Constants.AGENT_NOT_FOUND.getMessage(), agentId));
        } else {

            Set<String> idEdsOfSelectedAgent = getIdEdsOfSelectedAgent(edsOfSelectedAgent);

            List<OmEdsAgent> allParentsEds = edsAgentRepository.getByIdEdsIn(new ArrayList<>(idEdsOfSelectedAgent));

            allParentsEds.forEach(omEdsAgent -> secteurAgents.addAll(getSecteurAgent(omEdsAgent.getIdEds(), omEdsAgent)));
            if (idEdsOfSelectedAgent.size() != allParentsEds.size()) {
                secteurAgents.addAll(fillNotFoundedEds(idEdsOfSelectedAgent, allParentsEds));
            }

        }


        return organizeEdsAgent(secteurAgents, agentId);
    }

    @Override
    public List<SecteurAgentMulti> getSecteurAgentMulti(String structureId) {
        List<SecteurAgentMulti> secteurs = new ArrayList<>();
        // recuperer tous les eds et agents avec un agenda non vide
        List<OmEdsAgent> edsAgentMulti = edsAgentRepository.getAllByIdCaisseRegionale(structureId);
        if (Objects.isNull(edsAgentMulti)) {
            log.error(Constants.NULL_PARAM.getMessage(), "Le secteurAgentMulti");
        } else {
            edsAgentMulti.forEach(omEdsAgent -> secteurs.add(secteurAgentMapper.omEdsAgentToEdsMulti(omEdsAgent)));
        }
        return filterValidAgent(organizeEdsAgentMulti(secteurs));
    }

    @Override
    public List<String> getAllEmployeeIdByIdEds(String idEds) {

        List<OmEdsAgent> edsAgentByIdEds = edsAgentRepository.getAllEmployeeIdByIdEds(idEds);
        Set<String> listIds = new HashSet<>();

        if (edsAgentByIdEds != null && !edsAgentByIdEds.isEmpty()) {
            listIds.addAll(edsAgentByIdEds.stream().flatMap(omEdsAgent -> omEdsAgent.getFonctions().stream()
                    .flatMap(fonction -> fonction.getAgents().stream().map(OmAgent::getMatricule)))
                    .collect(Collectors.toList()));
        }

        return new ArrayList<>(listIds);
    }

    private List<SecteurAgent> organizeEdsAgent(List<SecteurAgent> secteurAgents, String agentId) {

        LinkedList<SecteurAgent> edsAgents = new LinkedList<>();
        secteurAgents.sort(Comparator.comparing(SecteurAgent::getLibelle));

        secteurAgents.forEach(secteurAgent ->
                {
                    secteurAgent.getFonctions().sort(Comparator.comparing(Fonction::getLibelle));
                    secteurAgent.getFonctions().forEach(fonctions ->
                            fonctions.getAgents().sort(Comparator.comparing(AgentFonction::getNom)
                                    .thenComparing(AgentFonction::getPrenom)
                            )
                    );
                    if (IsContientAgentConnecte(agentId, secteurAgent)) {
                        SecteurAgent secteurAgentWithConnectedAgentFirst = getSecteurAgentWithConnectedAgentFirstFonction(agentId, secteurAgent);
                        edsAgents.addFirst(secteurAgentWithConnectedAgentFirst);
                    } else {
                        edsAgents.addLast(secteurAgent);
                    }
                }
        );
        return edsAgents;


    }

    private List<SecteurAgentMulti> organizeEdsAgentMulti(List<SecteurAgentMulti> secteurAgentsMulti) {

        LinkedList<SecteurAgentMulti> secteur = new LinkedList<>();
        secteurAgentsMulti.sort(Comparator.nullsLast(Comparator.comparing(SecteurAgentMulti::getLibelle)));

        secteurAgentsMulti.forEach(secteurAgentMulti ->
                {
                    if (Objects.nonNull(secteurAgentMulti.getFonctions())) {
                        if (!isAllNull(secteurAgentMulti)) {
                            secteurAgentMulti.getFonctions().sort(Comparator.nullsLast(Comparator.comparing(FonctionMulti::getLibelle)));
                        }
                    }
                    secteur.addLast(secteurAgentMulti);
                }
        );
        return secteur;
    }

    private boolean isAllNull(SecteurAgentMulti secteurAgentMulti) {
        return secteurAgentMulti.getFonctions().stream().
                filter(fonctionsMulti -> Objects.nonNull(fonctionsMulti.getLibelle()))
                .collect(Collectors.toList()).isEmpty();
    }

    private SecteurAgent getSecteurAgentWithConnectedAgentFirstFonction(String agentId, SecteurAgent secteurAgent) {
        LinkedList<Fonction> fonctionPriorites = new LinkedList<>();
        secteurAgent.getFonctions().forEach(fonctions -> {
            if (isContainConnectedAgentWithFonctions(agentId, fonctions)) {
                fonctionPriorites.addFirst(fonctions);
            } else {
                fonctionPriorites.addLast(fonctions);
            }

        });
        secteurAgent.setFonctions(fonctionPriorites);
        return secteurAgent;
    }

    private boolean IsContientAgentConnecte(String idAgent, SecteurAgent secteurAgent) {
        return !Objects.isNull(secteurAgent.getFonctions()) && secteurAgent.getFonctions()
                .stream()
                .anyMatch(fonctions ->
                        fonctions.getAgents().stream()
                                .anyMatch(agentFonction -> Objects.nonNull(agentFonction.getId()) && agentFonction.getId().equals(idAgent))
                );
    }

    private boolean isContainConnectedAgentWithFonctions(String idAgent, Fonction fonctions) {
        return fonctions.getAgents().stream()
                .anyMatch(agentFonction -> Objects.nonNull(agentFonction.getId()) && agentFonction.getId().equals(idAgent));
    }

    private List<SecteurAgent> fillNotFoundedEds(Set<String> idEdsOfSelectedAgent, List<OmEdsAgent> allParentsEds) {

        List<SecteurAgent> secteurAgents = new ArrayList<>();
        List<String> idEdsUsingFoundedEds = allParentsEds.stream().map(OmEdsAgent::getIdEds).collect(Collectors.toList());
        List<String> idOfNonFoundEds = idEdsOfSelectedAgent.stream().filter(idParent -> !idEdsUsingFoundedEds.contains(idParent)).collect(Collectors.toList());
        idOfNonFoundEds.forEach(idParent -> secteurAgents.addAll(getSecteurAgent(idParent, new OmEdsAgent())));

        return secteurAgents;
    }

    // appliquer la régle du niveau
    // exemple: si l'eds est du niveau 6 -> on récupére l'id parent dans l'objet retourné-> idEdsParent
    // si on n'est pas au niveau 6 -> l'id de l'eds courante est l'id parent -> idEds
    private Set<String> getIdEdsOfSelectedAgent(List<OmEdsAgent> edsOfSelectedAgent) {
        Set<String> idParentsEds = new HashSet<>();
        edsOfSelectedAgent.forEach(eds -> {
            if (DEFAULT_TYPE_EDS.equals(eds.getTypeEds())) {
                idParentsEds.add(eds.getIdEdsParent());
            } else {
                idParentsEds.add(eds.getIdEds());
            }
        });

        return idParentsEds;
    }


    private List<SecteurAgent> getSecteurAgent(String idParent, OmEdsAgent parentEds) {
        List<OmEdsAgent> filsEds = edsAgentRepository.getALlByIdEdsParent(idParent);
        SecteurAgent edsParent = secteurAgentMapper.omEdsAgentToEds(parentEds);
        List<SecteurAgent> secteurAgents = new ArrayList<>();
        if (!Objects.isNull(edsParent.getId())) {
            secteurAgents.add(edsParent);
        }
        secteurAgents.addAll(secteurAgentMapper.omEdsAgentsToEds(filsEds));

        return secteurAgents;
    }


    private List<SecteurAgentMulti> filterValidAgent(List<SecteurAgentMulti> secteurAgentMulti) {

        secteurAgentMulti.forEach(secteur -> secteur.getFonctions().forEach(fonction ->
                fonction.setAgents(fonction.getAgents().stream().filter(agent -> Objects.nonNull(agent.getId()) && ID_AGENT_DEFAULT_LENGTH == agent.getId().length()).collect(Collectors.toList()))));
        return secteurAgentMulti;
    }
}
