package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class DistributionEntity {
    @JsonProperty("branch_id")
    private String idBranch;

    @JsonProperty("type_name")
    private String typeName;

    @JsonProperty("type")
    private String type;

    @JsonProperty("label")
    private String label;

    @JsonProperty("job")
    private Job job;

    @JsonProperty("employee_functions")
    private List<Function> employeeFunctions;

}
