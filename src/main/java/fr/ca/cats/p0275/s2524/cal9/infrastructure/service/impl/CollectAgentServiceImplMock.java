package fr.ca.cats.p0275.s2524.cal9.infrastructure.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Portfolio;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectAgentService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Profile("Mock")
@Service
@Slf4j
@AllArgsConstructor
public class CollectAgentServiceImplMock implements CollectAgentService {

    private final List<String> listeAgentIdTestIntegration = Arrays.asList(
            "40001",
            "40004",
            "40016"
    );

    public Employee getEmployee(String structureId, String agentId) {
        return (listeAgentIdTestIntegration.contains(agentId)) ? getEmployeeTestIntegration() : getEmployeeFromJson(agentId);
    }

    @Override
    public Employee getEmployeeLocalisation(String structureId, String agentId) {
        ObjectMapper mapper = new ObjectMapper();
        Resource resource = new ClassPathResource("/static/sdg9EmployeeNumber.json");
        Employee employee;
        List<Employee> employees;
        try {
            employee = mapper.readValue(resource.getInputStream(), Employee.class);
        } catch (Exception e) {
            return null;
        }
        return employee;
    }


    public Employee getEmployeeFromJson(String agentId) {
        ObjectMapper mapper = new ObjectMapper();
        Resource resource = new ClassPathResource("/static/sdg9EmployeeNumber.json");
        Employee employee;
        List<Employee> employees;
        try {
            employees = mapper.readValue(resource.getInputStream(), new TypeReference<>() {
            });
            employee = employees.stream().filter(employee1 -> employee1.getIdEmployee().equals(agentId)).findFirst().orElse(null);
        } catch (Exception e) {
            return null;
        }
        return employee;
    }


    private Employee getEmployee() {
        ObjectMapper mapper = new ObjectMapper();
        Resource resource = new ClassPathResource("/static/sdg9EmployeeNumber.json");

        List<Employee> employees;
        try {
            employees = mapper.readValue(resource.getInputStream(), new TypeReference<>() {
            });

        } catch (Exception e) {
            return null;
        }
        return employees.stream().findFirst().orElse(null);

    }

    private Employee getEmployeeTestIntegration() {
        ObjectMapper mapper = new ObjectMapper();
        Resource resource = new ClassPathResource("/static/sdg9_test_integration.json");
        Employee employees;
        try {
            employees = mapper.readValue(resource.getInputStream(), Employee.class);
        } catch (Exception e) {
            return null;
        }
        return employees;
    }

    @Override
    public List<Employee> getAllEmployee(String structureId) {
        ObjectMapper mapper = new ObjectMapper();
        Resource resource = new ClassPathResource("/static/sdg9EmployeeNumber.json");
        Employee employee;
        try {
            employee = mapper.readValue(resource.getInputStream(), Employee.class);
        } catch (Exception e) {
            return new ArrayList<>();
        }
        return List.of(employee);
    }

    @Override
    public Portfolio getPortfolio(String structureId, String portfolioId) {
        ObjectMapper mapper = new ObjectMapper();
        Resource resource = new ClassPathResource("/static/sdg9Portfolio.json");
        Portfolio portfolio;
        List<Portfolio> portfolios;
        try {
            portfolios = mapper.readValue(resource.getInputStream(), new TypeReference<>() {
            });

            portfolio = portfolios.stream().filter(pf -> pf.getIdPortefeuille().equals(portfolioId)).findFirst().orElse(null);
        } catch (Exception e) {
            return null;
        }
        return portfolio;
    }

    @Override
    public List<String> getAllEmployeeIdByIdEds(String idEds) {
        ObjectMapper mapper = new ObjectMapper();
        Resource resource = new ClassPathResource("/static/sdg9EmployeeNumber.json");
        Employee employee;
        try {
            employee = mapper.readValue(resource.getInputStream(), Employee.class);
        } catch (Exception e) {
            return null;
        }
        return List.of(employee.getMatriculeAgent());
    }
}
