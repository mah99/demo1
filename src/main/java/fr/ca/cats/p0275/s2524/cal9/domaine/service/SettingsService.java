package fr.ca.cats.p0275.s2524.cal9.domaine.service;

import fr.ca.cats.p0275.s2524.cal9.model.Setting;

import java.util.List;

public interface SettingsService  {

    List<Setting> getSettings(String structureId);
}
