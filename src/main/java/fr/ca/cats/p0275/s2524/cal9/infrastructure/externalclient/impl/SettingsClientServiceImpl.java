package fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.impl;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.AbstractCallInterApi;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.CallInterApiTechnicalException;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.SettingsClientService;
import fr.ca.cats.p0275.s2524.cal9.model.Setting;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@Profile("!Mock")
@Slf4j
public class SettingsClientServiceImpl extends AbstractCallInterApi<Setting> implements SettingsClientService {
    public static final String APPEL_AGC9 = "AGC9";

    @Value("${aia.agc9.reunions.uri}")
    private String aiaAGC9Uri;

    @Autowired
    private HttpServletRequest request;

    @Override
    public List<Setting> getSettings(String structureId) {
        try {

            log.info("collectSettings{}", request.getRequestURI().replaceAll("[\r\n]", ""));
            String uriFinal = aiaAGC9Uri + getFullUrl(request);
            return callInterAPIList(uriFinal, APPEL_AGC9);
        } catch (CallInterApiTechnicalException e) {
            log.error("Aucune donnée agence AGC9  structureId={}", structureId);
            return null;
        }
    }
}
