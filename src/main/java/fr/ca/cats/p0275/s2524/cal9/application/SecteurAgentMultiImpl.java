package fr.ca.cats.p0275.s2524.cal9.application;

import fr.ca.cats.p0275.s2524.cal9.domaine.service.EdsAgentService;
import fr.ca.cats.p0275.s2524.cal9.model.SecteurAgentMulti;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static fr.ca.cats.p0070.s1889.easyrest.authentication.utils.AuthLvl.MIN_COLLAB_0;

@Slf4j
@RestController
public class SecteurAgentMultiImpl implements SecteurAgentMultiApi{

    private final EdsAgentService edsAgentService;

    public SecteurAgentMultiImpl(EdsAgentService edsAgentService) {
        this.edsAgentService = edsAgentService;
    }

    @PreAuthorize(MIN_COLLAB_0)
    @Override
    public ResponseEntity<List<SecteurAgentMulti>> getSecteurAgentMulti(String correlationId, String structureId) {
        List<SecteurAgentMulti> secteurAgent = edsAgentService.getSecteurAgentMulti(structureId);

        return ResponseEntity.ok(secteurAgent);
    }
}
