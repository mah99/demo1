package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;


import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.OmAgent;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.OmEdsAgent;
import fr.ca.cats.p0275.s2524.cal9.model.AgentFonction;
import fr.ca.cats.p0275.s2524.cal9.model.AgentFonctionMulti;
import fr.ca.cats.p0275.s2524.cal9.model.SecteurAgent;
import fr.ca.cats.p0275.s2524.cal9.model.SecteurAgentMulti;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SecteurAgentMapper {

    SecteurAgentMapper SECTEUR_AGENT_MAPPER = Mappers.getMapper(SecteurAgentMapper.class);
    Integer FIRST_INDEX_TO_GET_STRUCTURE = 0;
    Integer FIRST_INDEX_TO_GET_ID_EDS = 5;
    Integer FIRST_INDEX_TO_GET_NIVEAU_EDS = 10;


    @Mapping(source = "matricule", target = "id")
    AgentFonction omAgentToAgentFonction(OmAgent omAgent);

    @Mapping(source = "matricule", target = "id")
    AgentFonctionMulti omAgentToAgentFonctionMulti(OmAgent omAgent);

    @Mapping(source = "idEds", target = "id", qualifiedByName = "getIdEds")
    @Mapping(source = "idEds", target = "structureId", qualifiedByName = "getStructureId")
    @Mapping(source = "idEds", target = "niveauEds", qualifiedByName = "getNiveauEds")
    SecteurAgent omEdsAgentToEds(OmEdsAgent omEdsAgent);

    @Mapping(source = "idEds", target = "id", qualifiedByName = "getIdEds")
    @Mapping(source = "idCaisseRegionale", target = "structureId")
    SecteurAgentMulti omEdsAgentToEdsMulti(OmEdsAgent omEdsAgent);


    List<SecteurAgent> omEdsAgentsToEds(List<OmEdsAgent> omEdsAgents);

    @Named("getIdEds")
    default String getIdEds(String idEds) {
        return idEds != null && !idEds.isEmpty() ? idEds.substring(FIRST_INDEX_TO_GET_ID_EDS, FIRST_INDEX_TO_GET_NIVEAU_EDS) : null;
    }

    @Named("getStructureId")
    default String getStructureId(String idEds) {
        return idEds != null && !idEds.isEmpty() ? idEds.substring(FIRST_INDEX_TO_GET_STRUCTURE, FIRST_INDEX_TO_GET_ID_EDS) : null;
    }

    @Named("getNiveauEds")
    default String getNiveauEds(String idEds) {
        return idEds != null && !idEds.isEmpty() ? idEds.substring(FIRST_INDEX_TO_GET_NIVEAU_EDS) : null;
    }


}
