package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Partenaire {
    @JsonProperty("id_partenaire")
    private String idPartenaire;
    @JsonProperty("id_caisse_regionale")
    private String idCaisseRegionale;
    @JsonProperty("type_partenaire")
    private String typePartenaire;
    @JsonProperty("siret")
    private String siret;
    @JsonProperty("enseigne_etablissement")
    private String enseigneEtablissement;
    @JsonProperty("date_creation")
    private String dateCreation;
    @JsonProperty("date_cloture")
    private String dateCloture;
    @JsonProperty("intitule")
    private String intitule;
    @JsonProperty("intitule_suite")
    private String intituleSuite;
    @JsonProperty("code_marche")
    private String codeMarche;
    @JsonProperty("id_portefeuille")
    private String idPortefeuille;
    @JsonProperty("id_bureau_gestionnaire")
    private String idBureauGestionnaire;
    @JsonProperty("code_bureau_gestionnaire")
    private String codeBureauGestionnaire;
    @JsonProperty("top_partenaire_incomplet")
    private String topPartenaireIncomplet;
}
