package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
@Document("edsAgent")
public class OmEdsAgent {
    @Id
    @Field("_id")
    private String id;


    @Field("id_eds")
    private String idEds;

    @Field("id_caisse_regionale")
    private String idCaisseRegionale;

    @Field("libelle")
    private String libelle;

    @Field("libelle_court")
    private String libelleCourt;

    @Field("type_eds")
    private String typeEds;

    @Field("type_regroupement_eds")
    private String typeRegroupementEds;


    @Field("id_eds_parent")
    private String idEdsParent;


    @Field("fonctions")
    private List<Fonction> fonctions;


}
