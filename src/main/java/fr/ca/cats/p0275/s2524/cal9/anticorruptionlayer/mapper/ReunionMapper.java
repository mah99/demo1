package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;


import fr.ca.cats.p0275.s2524.cal9.model.Reunion;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ReunionMapper {

    ReunionMapper REUNION_MAPPER = Mappers.getMapper( ReunionMapper.class );

    @Mapping(target = "partenaires", ignore = true)
    @Mapping(target = "agentCreateur", ignore = true)
    @Mapping(target = "canalInteraction.adresseExterieur", ignore = true)
    @Mapping(source = "canalInteraction.canal", target = "canalInteraction.canal")
    Reunion reunionAGC9ToReunion(fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion reunion);
}
