package fr.ca.cats.p0275.s2524.cal9.application;

import fr.ca.cats.p0275.s2524.cal9.domaine.service.EmacoService;
import fr.ca.cats.p0275.s2524.cal9.model.PartenaireEmaco;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import static fr.ca.cats.p0070.s1889.easyrest.authentication.utils.AuthLvl.MIN_COLLAB_0;

@Slf4j
@RestController
public class ToolsApiImpl implements ToolsApi {


    private EmacoService emacoService;

    public ToolsApiImpl(EmacoService emacoService) {
        this.emacoService = emacoService;
    }

    @PreAuthorize(MIN_COLLAB_0)
    @Override
    public ResponseEntity<PartenaireEmaco> getIdPartenaireEmaco(String correlationId, String idPart) {
        log.info("ENTREE getIdPartenaireEmaco idPart:{}", idPart);
        return ResponseEntity.ok(emacoService.getIdPartenaireEmacoByPartnerId(idPart));
    }
}
