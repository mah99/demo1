package fr.ca.cats.p0275.s2524.cal9.infrastructure.util;


import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.TechnicalException;
import org.springframework.util.ResourceUtils;

import java.util.List;

public class MapperUtils {

    private MapperUtils() {

    }

    private static final ObjectMapper objectMapper = new ObjectMapper();
    public static <T> T readObjectFile(String resourceFile, Class<T> clazz){
        try{
            JavaType type = objectMapper.getTypeFactory().
                    constructType(clazz);
            return objectMapper.readValue(ResourceUtils.getFile(resourceFile),type);
        }catch (Exception t){
            throw new TechnicalException(t);
        }
    }
    public static <T> List<T> readObjectsFile(String resourceFile, Class<T> clazz){
        try{
            JavaType type = objectMapper.getTypeFactory().
                    constructCollectionType(List.class, clazz);
            return objectMapper.readValue(ResourceUtils.getFile(resourceFile),type);
        }catch (Exception t){
            throw new TechnicalException(t);
        }
    }
}
