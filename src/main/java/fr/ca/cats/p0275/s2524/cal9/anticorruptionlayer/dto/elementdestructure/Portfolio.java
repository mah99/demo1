package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@EqualsAndHashCode(callSuper = false)
@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class Portfolio {

    private String structureId;

    @JsonProperty("id")
    private String idCrPortefeuille;

    @JsonProperty("external_id")
    private String idPortefeuille;

    @JsonProperty("number")
    private String matriculeAgent;
}
