package fr.ca.cats.p0275.s2524.cal9.domaine.service;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;
import fr.ca.cats.p0275.s2524.cal9.model.Agent;

public interface AgentService {

    Agent getAgentCreateur(String structureId, Reunion reunion);

    Employee getEmployee(String structureId, String agentId);

    Employee getEmployeeLocalisation(String structureId, String agentId);
}
