package fr.ca.cats.p0275.s2524.cal9.infrastructure.service;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Portfolio;

import java.util.List;

public interface CollectAgentService {

    Employee getEmployee(String structureId, String agentId);

    Employee getEmployeeLocalisation(String structureId, String agentId);

    List<Employee> getAllEmployee(String structureId);

    Portfolio getPortfolio(String structureId, String portfolioId);

    List<String> getAllEmployeeIdByIdEds(String idEds);
}