package fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.TechnicalException;

public class CallInterApiHeaderTechnicalException extends TechnicalException {

    public CallInterApiHeaderTechnicalException(Integer status, String businessErrorCode, String message) {
        super(status, businessErrorCode, message);
    }

    public CallInterApiHeaderTechnicalException(String message, String businessErrorCode) {
        super(message,businessErrorCode);
    }

}
