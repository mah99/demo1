package fr.ca.cats.p0275.s2524.cal9.domaine.enums;

public enum TypeReunion {
    RENDEZVOUS("RENDEZ-VOUS"),
    ACTIVITE("ACTIVITE"),
    FERIE("FERIE");

    private final String label;

    TypeReunion(String label) {
        this.label = label;
    }

    public String getLabel(){
        return this.label;
    }

}
