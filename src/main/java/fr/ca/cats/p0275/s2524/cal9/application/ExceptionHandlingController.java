package fr.ca.cats.p0275.s2524.cal9.application;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.BusinessException;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.ErrorMessage;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.TechnicalException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

import static fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants.GENERIQUE_ERROR;
import static fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants.MISSING_PARAM_EXCEPTION;

@RestControllerAdvice
public class ExceptionHandlingController {

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ErrorMessage> handle(MethodArgumentTypeMismatchException ex) {
        ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST, MISSING_PARAM_EXCEPTION.getCode(), MISSING_PARAM_EXCEPTION.getMessage(), ex.getLocalizedMessage());
        return new ResponseEntity(errorMessage, new HttpHeaders(), errorMessage.getStatus());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorMessage> handleMethodArgumentNotValid(MethodArgumentNotValidException ex
    ) {
        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        List<ObjectError> globalErrors = ex.getBindingResult().getGlobalErrors();
        List<String> errors = new ArrayList<>(fieldErrors.size() + globalErrors.size());
        String error;
        for (FieldError fieldError : fieldErrors) {
            error = fieldError.getField() + ", " + fieldError.getDefaultMessage();
            errors.add(error);
        }
        for (ObjectError objectError : globalErrors) {
            error = objectError.getObjectName() + ", " + objectError.getDefaultMessage();
            errors.add(error);
        }
        ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST, "", ex.getLocalizedMessage(), "");
        return new ResponseEntity(errorMessage, new HttpHeaders(), errorMessage.getStatus());
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<ErrorMessage> handleMissingServletRequestParameterException(MissingServletRequestParameterException ex
    ) {
        ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST, MISSING_PARAM_EXCEPTION.getCode(), MISSING_PARAM_EXCEPTION.getMessage(), ex.getLocalizedMessage());
        return new ResponseEntity(errorMessage, new HttpHeaders(), errorMessage.getStatus());
    }

    @ExceptionHandler(MissingRequestHeaderException.class)
    public ResponseEntity<ErrorMessage> handleMissingRequestHeaderException(MissingRequestHeaderException ex
    ) {
        ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST, MISSING_PARAM_EXCEPTION.getCode(), MISSING_PARAM_EXCEPTION.getMessage(), ex.getLocalizedMessage());
        return new ResponseEntity(errorMessage, new HttpHeaders(), errorMessage.getStatus());
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ErrorMessage> handleConstraintViolation(
            ConstraintViolationException ex, WebRequest request) {
        List<String> errors = new ArrayList<>();
        for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
            errors.add(violation.getRootBeanClass().getName() + " " +
                    violation.getPropertyPath() + ": " + violation.getMessage());
        }
        ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST, "", ex.getLocalizedMessage(), "");
        return new ResponseEntity(errorMessage, new HttpHeaders(), errorMessage.getStatus());
    }

    @ExceptionHandler(TechnicalException.class)
    public ResponseEntity<ErrorMessage> handle(TechnicalException ex) {
        ErrorMessage errorMessage = new ErrorMessage(ex.getHttpStatus(), ex.getCodeError(), GENERIQUE_ERROR.getMessage() ,ex.getLocalizedMessage());
        return new ResponseEntity(errorMessage, new HttpHeaders(), errorMessage.getStatus());
    }

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<ErrorMessage> handle(BusinessException ex) {
        ErrorMessage errorMessage = new ErrorMessage(ex.getHttpStatus(), ex.getCodeError(), ex.getLocalizedMessage(), "");
        return new ResponseEntity(errorMessage, new HttpHeaders(), errorMessage.getStatus());
    }
}
