package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Branch;

public interface AgenceRepository {

    Branch  getBranchByAgenceIdAndStructureId(String agenceId, String structureId);

    void addBranch(Branch branch);
}
