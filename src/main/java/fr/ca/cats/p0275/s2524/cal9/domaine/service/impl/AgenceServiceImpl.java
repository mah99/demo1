package fr.ca.cats.p0275.s2524.cal9.domaine.service.impl;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Branch;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.AdresseMapper;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.AgenceService;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.exception.AgenceBusinessException;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectAgenceService;
import fr.ca.cats.p0275.s2524.cal9.model.Adresse;
import fr.ca.cats.p0275.s2524.cal9.model.Reunion;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Objects;

import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.Validator.verifyNull;
import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.Validator.verifyisNotEmpty;

@Service
@Slf4j
public class AgenceServiceImpl implements AgenceService {

    private CollectAgenceService collectAgenceService;
    private AdresseMapper adresseMapper;

    public AgenceServiceImpl(final CollectAgenceService collectAgenceService, final AdresseMapper adresseMapper) {
        this.collectAgenceService = collectAgenceService;
        this.adresseMapper = adresseMapper;
    }

    @Override
    public Adresse getAdresse (String structureId, fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion reunion, Reunion reunionResponse){
        String idBranch = reunion.getCanalInteraction().getAgence().getId();
        if(Objects.nonNull(idBranch)) {
            Branch branch = collectAgenceService.getAgence(idBranch, structureId);
            Adresse adresse = new Adresse();
            if(verifyNull(branch, "branch"))
            {
                verifyisNotEmpty(branch.getName(), "agence", Constants.AGENCE_LIBELLE_NOT_FOUND.getMessage(), Constants.AGENCE_LIBELLE_NOT_FOUND.getCode());
                adresse = adresseMapper.adresseToBranch(branch);
                reunionResponse.getCanalInteraction().getAgence().setLibelle(branch.getName());
            }
            return adresse;
        }
        else{
            log.error(Constants.BRANCH_ID_NOT_FOUND.getMessage() + Constants.LOG_CODE + Constants.BRANCH_ID_NOT_FOUND.getCode());
            throw new AgenceBusinessException(Constants.BRANCH_ID_NOT_FOUND.getMessage());
        }
    }
}
