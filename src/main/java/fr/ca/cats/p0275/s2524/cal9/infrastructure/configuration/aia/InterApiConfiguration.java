package fr.ca.cats.p0275.s2524.cal9.infrastructure.configuration.aia;

import fr.ca.cats.p0070.s1889.easyrest.interapi.dto.AiaRestResourcesConfig;
import fr.ca.cats.p0070.s1889.easyrest.interapi.exception.InterApiException;
import fr.ca.cats.p0070.s1889.easyrest.interapi.service.InterApiRequestService;
import fr.ca.cats.p0070.s1889.easyrest.interapi.service.InterApiRequestServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InterApiConfiguration {

    @Value("${authentication.config.apimGatewayUrl}")
    private String apimGatewayUrl;

    @Value("${authentication.config.clientName}")
    private String appName;

    @Value("${authentication.config.clientId}")
    private String appId;

    @Value("${authentication.config.clientSecret}")
    private String appSecret;

    @Value("${aia.aut9.uri}")
    private String aut9Uri;

    @Value("${aia.cache.duration.seconds}")
    private int cacheDurationSeconds;

    @Value("${aia.scopes}")
    private String scopes;

    @Bean
    public InterApiRequestService interApiRequestService() throws InterApiException {
        AiaRestResourcesConfig config = new AiaRestResourcesConfig()
                .setApimGatewayUrl(apimGatewayUrl)
                .setTechnicalAuthUri(aut9Uri)
                .setClientId(appId)
                .setClientSecret(appSecret)
                .setAppName(appName)
                .setCacheDurationSeconds(cacheDurationSeconds)
                .setScopes(scopes);
        return new InterApiRequestServiceImpl(config);
    }

}
