package fr.ca.cats.p0275.s2524.cal9.domaine.service.impl;

import fr.ca.cats.p0275.s2524.cal9.domaine.service.SettingsService;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.exception.SettingsBusinessException;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectSettingsService;
import fr.ca.cats.p0275.s2524.cal9.model.Setting;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants.INVALID_STRUCTURE;
import static fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants.LOG_CODE;

@Service
@Slf4j
public class SettingsServiceImpl implements SettingsService {

    private final CollectSettingsService collectSettingsService;

    public SettingsServiceImpl(final CollectSettingsService collectSettingsService) {
        this.collectSettingsService = collectSettingsService;
    }

    @Override
    public List<Setting> getSettings(String structureId) {
        String REGEX_STRUCTURE_ID = "^[0-9]{5}$";
        Pattern pattern = Pattern.compile(REGEX_STRUCTURE_ID);
        Matcher matcher = pattern.matcher(structureId);
        if (!matcher.matches()) {
            log.error(Constants.INVALID_STRUCTURE.getMessage() + LOG_CODE + Constants.INVALID_STRUCTURE.getCode());
            throw new SettingsBusinessException(INVALID_STRUCTURE.getMessage(), INVALID_STRUCTURE.getCode());
        }
        return collectSettingsService.getSettings(structureId);
    }
}
