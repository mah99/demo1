package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class PostalAddress {
    @JsonProperty("postal_address_line4")
    private String postalAddressLine4;
}
