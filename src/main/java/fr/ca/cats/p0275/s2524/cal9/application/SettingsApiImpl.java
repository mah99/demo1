package fr.ca.cats.p0275.s2524.cal9.application;

import fr.ca.cats.p0275.s2524.cal9.domaine.service.SettingsService;
import fr.ca.cats.p0275.s2524.cal9.model.Setting;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static fr.ca.cats.p0070.s1889.easyrest.authentication.utils.AuthLvl.MIN_COLLAB_0;

@Slf4j
@RestController
public class SettingsApiImpl implements SettingsApi {

    private final SettingsService settingsService;

    public SettingsApiImpl(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    @PreAuthorize(MIN_COLLAB_0)
    @Override
    public ResponseEntity<List<Setting>> getSettingsFindByStructureId(String correlationId, String structureId) {
        log.info("ENTREE getSettings structureId:{}", structureId);
        List<Setting> settings = settingsService.getSettings(structureId);

        return ResponseEntity.ok(settings);
    }

}
