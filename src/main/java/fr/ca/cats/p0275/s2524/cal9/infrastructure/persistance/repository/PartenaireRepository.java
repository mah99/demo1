package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire;

public interface PartenaireRepository {

    Partenaire getPartenaire(String partenaireId, String structureId) ;

    void addPartenaire(Partenaire partenaire);
}
