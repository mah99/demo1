package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.Dao;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.OmEdsAgent;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface EdsAgentRepositoryDao extends MongoRepository<OmEdsAgent, String> {

    @Query(value = "{'id_caisse_regionale':?0 , 'fonctions.agents.matricule':?1, 'type_eds': {$ne : 07 }} ", fields = "{ id_eds_parent : 1 , type_eds:1 , id_eds:1 , _id: 0}")
    List<OmEdsAgent> findIdEdsParentByStructureIdAndAgentId(String structureId, String agentId);

    @Query(value = "{'id_caisse_regionale':?0, 'fonctions.agents.matricule':?1} ", fields = "{ id_eds_parent :0, type_eds:0, _id: 0}")
    List<OmEdsAgent> findAllByIdCaisseRegionaleAndAgentId(String idCaisseRegionale, String agentId);

    List<OmEdsAgent> findALlByIdEdsParent(String idEdsParent);

    List<OmEdsAgent> findByIdEdsIn(List<String> idEds);

    @Query(value = "{'id_caisse_regionale':?0}", fields = "{ id_eds_parent : 0, type_regroupement_eds:0, libelleCourt:0, 'fonctions.type':0, 'fonctions.agents.idAgent':0,type_eds:0, _id: 0}")
    List<OmEdsAgent> findAllByIdCaisseRegionale(String idCaisseRegionale);

    @Query(value = "{'id_eds': { $regex : ?0 , $options: 'i'}}")
    List<OmEdsAgent> findEdsByIdEds(String idEds);
}