package fr.ca.cats.p0275.s2524.cal9.domaine.service.impl;


import fr.ca.cats.p0275.s2524.cal9.domaine.service.EmacoService;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.exception.EmacoToolsBusinessException;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.EmacoToolsService;
import fr.ca.cats.p0275.s2524.cal9.model.PartenaireEmaco;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Objects;

import static fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants.LOG_CODE;

@Service
@Slf4j
public class EmacoServiceImpl implements EmacoService {


    private EmacoToolsService emacoToolsService;

    private final static Integer PARTENAIRE_ID_DEFAULT_LENGTH = 14;

    public EmacoServiceImpl(EmacoToolsService emacoToolsService) {
        this.emacoToolsService = emacoToolsService;
    }

    @Override
    public PartenaireEmaco getIdPartenaireEmacoByPartnerId(String idPart) {
        if (isNotValidPartenaireId(idPart)) {
            log.error(Constants.INVALID_PARTENAIRE_ID.getMessage() + LOG_CODE + Constants.INVALID_PARTENAIRE_ID.getCode());
            throw new EmacoToolsBusinessException(String.format(Constants.INVALID_PARTENAIRE_ID.getMessage(),idPart),Constants.INVALID_PARTENAIRE_ID.getCode());
        }

        PartenaireEmaco partenaireEmaco = emacoToolsService.getIdPartenaireEmacoByPartnerId(idPart);
        if(Objects.isNull(partenaireEmaco)){
            log.error(String.format(Constants.NOT_FOUND_PARTENAIRE_EMACO.getMessage(),idPart) + LOG_CODE + Constants.NOT_FOUND_PARTENAIRE_EMACO.getCode());
            throw new EmacoToolsBusinessException(String.format(Constants.NOT_FOUND_PARTENAIRE_EMACO.getMessage(),idPart), Constants.NOT_FOUND_PARTENAIRE_EMACO.getCode(), HttpStatus.NOT_FOUND);
        }
        return partenaireEmaco;
    }


    private boolean isNotValidPartenaireId(String idPart) {
        return  (!StringUtils.isNumeric(idPart)) || StringUtils.isBlank(idPart) || (idPart.length() != PARTENAIRE_ID_DEFAULT_LENGTH);
    }
}
