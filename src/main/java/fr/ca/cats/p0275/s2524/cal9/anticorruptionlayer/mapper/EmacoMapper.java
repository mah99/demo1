package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;


import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.PartenaireEmacoDao;
import fr.ca.cats.p0275.s2524.cal9.model.PartenaireEmaco;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface EmacoMapper {

    EmacoMapper EMACO_MAPPER = Mappers.getMapper(EmacoMapper.class);

    PartenaireEmaco partenaireEmacoDaoToPartenaireEmaco(PartenaireEmacoDao partenaireEmacoDao);


}
