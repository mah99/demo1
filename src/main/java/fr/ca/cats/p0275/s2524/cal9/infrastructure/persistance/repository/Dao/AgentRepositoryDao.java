package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.Dao;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.AgentDao;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface AgentRepositoryDao extends MongoRepository<AgentDao, String> {

    AgentDao findByIdAgentAndStructureId(String idAgent, String structureId);

    List<AgentDao> findAllByStructureId(String structureId);

}