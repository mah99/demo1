package fr.ca.cats.p0275.s2524.cal9.infrastructure.service;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion;

import java.util.List;

public interface CollectReunionMultiService {
    List<Reunion> getReunionMulti(List<String> agentIdList, String dateDebut, String dateFin);
}
