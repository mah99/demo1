package fr.ca.cats.p0275.s2524.cal9.domaine.service.exception;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.BusinessException;

public class VueAgendaBusinessException extends BusinessException {

    public VueAgendaBusinessException(Integer status, String businessErrorCode, String message) {
        super(status, businessErrorCode, message);
    }

    public VueAgendaBusinessException(Integer status, String businessErrorCode, String causeValue, String message) {
        super(status, businessErrorCode, causeValue, message);
    }

    public VueAgendaBusinessException(String message) {
        super(message);
    }

    public VueAgendaBusinessException(String message, String businessErrorCode) {
        super(message,businessErrorCode);
    }
}
