package fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.BusinessException;

public class CallInterApiBusinessException extends BusinessException {

    public CallInterApiBusinessException(Integer status, String businessErrorCode, String message) {
        super(status, businessErrorCode, message);
    }

    public CallInterApiBusinessException(Integer status, String businessErrorCode, String causeValue, String message) {
        super(status, businessErrorCode, causeValue, message);
    }

    public CallInterApiBusinessException(String message) {
        super(message);
    }
    public CallInterApiBusinessException(String message, String businessErrorCode) {
        super(message,businessErrorCode);
    }

}