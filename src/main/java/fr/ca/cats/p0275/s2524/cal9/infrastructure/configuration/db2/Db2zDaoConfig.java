package fr.ca.cats.p0275.s2524.cal9.infrastructure.configuration.db2;

import fr.ca.cats.p0070.s1889.easyrest.db2.config.MyBatisDb2zCrBeansConfiguration;
import fr.ca.cats.p0070.s1889.easyrest.db2.datasource.MyBatisDataSource;
import fr.ca.cats.p0070.s1889.easyrest.db2.mapper.MyBatisMapperFactoryBean;
import fr.ca.cats.p0070.s1889.easyrest.db2.session.factory.MyBatisSqlSessionFactory;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.Dao.EmacoRepositoryDao;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(MyBatisDb2zCrBeansConfiguration.class)
public class Db2zDaoConfig {

    /**
     * Bean sets the related mapper classes for <b>one DAO layer</b> and the myBatisDataSource bean to be used
     *
     * @param myBatisDb2zDataSource the MyBatis data source (proposed implementations: MyBatisSingleDataSource or MyBatisMultipleDataSource)
     * @return MyBatisSqlSessionFactory to be used
     */
    @Bean
    public MyBatisSqlSessionFactory db2zDaoSqlSessionFactory(@Qualifier("myBatisMultipleDb2zDataSource") MyBatisDataSource myBatisDb2zDataSource) {
        var factory = new MyBatisSqlSessionFactory();

        // ICI SONT REFERENCES LES MAPPERS DE LA COUCHE DAO
        factory.setMapperClasses(new Class[]{EmacoRepositoryDao.class});

        // ICI SONT REFERENCABLES LES MAPPERS XML DE LA COUCHE DAO
        //factory.setMapperLocations(Resource[] resources)

        // ICI EST INDIQUE LE BEAN DE DATASOURCE A PRENDRE EN CONSIDERATION
        factory.setMyBatisDataSource(myBatisDb2zDataSource);

        return factory;
    }

    /**
     * Bean sets with both the <b>mapper interface</b> and the SqlSessionFactory (bean) to be used
     *
     * @param db2zDaoSqlSessionFactory the MyBatis SQL session factory
     * @return MyBatisMapperFactoryBean to be used
     */
    @Bean
    public MyBatisMapperFactoryBean<EmacoRepositoryDao> emacoDao(@Qualifier("db2zDaoSqlSessionFactory") MyBatisSqlSessionFactory db2zDaoSqlSessionFactory) {
        // ICI EST CREE LE POJO DAO Supervision QUI, PAR SON CYCLE DE VIE, PERMET L'INDIRECTION VERS LA BONNE CR (celle du contexte de securité)
        return MyBatisMapperFactoryBean.builder(EmacoRepositoryDao.class)
                .myBatisSqlSessionFactory(db2zDaoSqlSessionFactory).build();
    }
}
