package fr.ca.cats.p0275.s2524.cal9.domaine.service;

import fr.ca.cats.p0275.s2524.cal9.model.PartenaireEmaco;

public interface EmacoService {


    PartenaireEmaco getIdPartenaireEmacoByPartnerId(String idPart);
}
