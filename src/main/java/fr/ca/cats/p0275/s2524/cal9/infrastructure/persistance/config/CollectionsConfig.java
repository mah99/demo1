package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.config;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.AgentDao;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.BranchDao;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.IndividualBasicDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.Index;
import org.springframework.data.mongodb.core.index.IndexInfo;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.List;
import java.util.Optional;

import static fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.Constants.MONGODB_IS_KO;

@Configuration
@DependsOn("mongoTemplate")
@Profile("!Mock")
@Slf4j
public class CollectionsConfig {

    private static final String REGISTERED_DATE = "registeredDate";

    @Value("${spring.data.expiredAfter}")
    private int expireAfter;

    private MongoTemplate mongoTemplate;

    public CollectionsConfig(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @PostConstruct
    public void initIndexes() {
        try {
            updateIndex(AgentDao.class);
            updateIndex(BranchDao.class);
            updateIndex(IndividualBasicDao.class);
        } catch (Exception e) {
            log.error(MONGODB_IS_KO.getMessage(), e.getMessage());
        }

    }


    private void updateIndex(Class<?> className) {

        List<IndexInfo> indexes = mongoTemplate.indexOps(className).getIndexInfo();
        Optional<IndexInfo> index = indexes.stream().filter(indexInfo -> indexInfo.getName().contains(REGISTERED_DATE)).findFirst();

        if (index.isPresent()) {
            Optional<Duration> mongoDBExpireAfter = index.get().getExpireAfter();
            if (mongoDBExpireAfter.isEmpty() || isIndexExpired(mongoDBExpireAfter)) {
                mongoTemplate.indexOps(className).dropIndex(index.get().getName());
                createIndex(className);
            }
        } else {
            createIndex(className);
        }

    }

    public boolean isIndexExpired(Optional<Duration> mongoDBExpireAfter) {
        return Optional.ofNullable(mongoDBExpireAfter).filter((duration) -> duration.map(value -> value.getSeconds() <= expireAfter).orElse(true)).isPresent();
    }

    private void createIndex(Class<?> className) {
        mongoTemplate.indexOps(className)
                .ensureIndex(
                        new Index().on(REGISTERED_DATE, Sort.Direction.ASC).expire(expireAfter));
    }


}