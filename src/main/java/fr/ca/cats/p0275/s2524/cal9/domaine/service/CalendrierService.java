package fr.ca.cats.p0275.s2524.cal9.domaine.service;

import fr.ca.cats.p0275.s2524.cal9.model.Calendrier;

import java.util.List;

public interface CalendrierService {
    Calendrier getCalendrier(String correlationId, String structureId, String agentId, String dateDebut, String dateFin);

    List<Calendrier> getCalendrierMulti(String correlationId, String structureId, List<String> agentIdList, String idEds, String dateDebut, String dateFin);
}
