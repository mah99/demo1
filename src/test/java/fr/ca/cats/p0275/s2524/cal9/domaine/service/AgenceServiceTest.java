package fr.ca.cats.p0275.s2524.cal9.domaine.service;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Branch;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.AdresseMapper;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.exception.AgenceBusinessException;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.exception.CalendrierBusinessException;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.impl.AgenceServiceImpl;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.CallInterApiTechnicalException;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectAgenceService;
import fr.ca.cats.p0275.s2524.cal9.model.Adresse;
import fr.ca.cats.p0275.s2524.cal9.model.CanalInteraction;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.MapperUtils.readObjectFile;
import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.MapperUtils.readObjectsFile;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class AgenceServiceTest {

    private AgenceService agenceService;
    @Mock
    private CollectAgenceService collectAgenceService;
    @Spy
    private AdresseMapper adresseMapper;

    private Branch branch;
    private fr.ca.cats.p0275.s2524.cal9.model.Reunion reunionResponse;
    private List<Reunion> reunions;

    @BeforeEach
    public void init(){
        reunionResponse = new fr.ca.cats.p0275.s2524.cal9.model.Reunion();
        CanalInteraction canalInteraction = new CanalInteraction();
        canalInteraction.setAgence(setupAgence());
        reunionResponse.setCanalInteraction(canalInteraction);
        reunions = readObjectsFile("classpath:static/agc9.json", Reunion.class);
        branch = readObjectFile("classpath:static/sdg9BranchID.json", Branch.class);
        when(collectAgenceService.getAgence(any(), anyString())).thenReturn(branch);
        adresseMapper = Mappers.getMapper(AdresseMapper.class);
        agenceService = new AgenceServiceImpl(collectAgenceService, adresseMapper);
    }

    public fr.ca.cats.p0275.s2524.cal9.model.Agence setupAgence(){
        fr.ca.cats.p0275.s2524.cal9.model.Agence agence = new fr.ca.cats.p0275.s2524.cal9.model.Agence();
        agence.setLibelle("");
        agence.setAdresse(new Adresse());
        agence.setId("12345");
        return agence;
    }

    @Test
    public void givenValidInformationAndSDG9AgenceError_ThenTechnicalException(){
        //given
        when(collectAgenceService.getAgence(anyString(), any())).thenThrow(new CallInterApiTechnicalException("Error"));
        // when
        var result = Assertions.assertThrows(CallInterApiTechnicalException.class, () -> {
            agenceService.getAdresse("12345", reunions.get(0), new fr.ca.cats.p0275.s2524.cal9.model.Reunion());
        });
    }

    @Test
    public void givenValidInformation_ThenOK() {
        // when
        Adresse result = agenceService.getAdresse("12345", reunions.get(0), reunionResponse);
        //then
        Assert.assertNotNull(result);
        Assert.assertEquals("1710", result.getCodePostal());
        Assert.assertEquals("THOIRY", result.getVille());
        Assert.assertEquals("CENTRE COMMERCIAL", result.getRue());
    }

    @Test
    public void givenValidInformationAndAGC9ErrorAgenceIdNullEmpty_ThenBusinessException() {
        //given
        reunions.get(0).getCanalInteraction().getAgence().setId(null);
        // when
        var result = Assertions.assertThrows(AgenceBusinessException.class, () -> {
            agenceService.getAdresse("12345", reunions.get(0), reunionResponse);
        });
        //then
        Assert.assertEquals("La branch de l'agence est manquante", result.getMessage());
    }

    @Test
    public void givenValidInformationAndAGC9ErrorAgenceLibelleEmpty_ThenBusinessException() {
        //given
        branch.setName("");
        // when
        var result = Assertions.assertThrows(CalendrierBusinessException.class, () -> {
            agenceService.getAdresse("12345", reunions.get(0), reunionResponse);
        });
        //then
        Assert.assertEquals("Le libellé de l'agence est manquant", result.getMessage());
    }
}