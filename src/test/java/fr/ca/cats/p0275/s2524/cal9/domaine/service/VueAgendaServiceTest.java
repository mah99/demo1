package fr.ca.cats.p0275.s2524.cal9.domaine.service;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Portfolio;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire;
import fr.ca.cats.p0275.s2524.cal9.domaine.enums.TypeVueAgenda;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.exception.VueAgendaBusinessException;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.impl.VueAgendaServiceImpl;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.BusinessException;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectAgentService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectPartenaireService;
import fr.ca.cats.p0275.s2524.cal9.model.Agent;
import fr.ca.cats.p0275.s2524.cal9.model.VueAgenda;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.utils.MapperUtils.getFirstEmployeeInJson;
import static fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.utils.MapperUtils.getFirstPortfolioInJson;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class VueAgendaServiceTest {

    private VueAgendaService vueAgendaService;

    @Mock
    private CollectAgentService collectAgentService;

    @Mock
    private CollectPartenaireService collectPartenaireService;

    Partenaire partenaire;
    Portfolio portfolio;
    Employee employee;
    VueAgenda vueAgenda;

    @BeforeEach
    public void init() {
        vueAgendaService = new VueAgendaServiceImpl(collectPartenaireService, collectAgentService);
        portfolio = getFirstPortfolioInJson();
        partenaire = new Partenaire();
        partenaire.setIdPortefeuille("9999");
        partenaire.setCodeBureauGestionnaire("480");
        employee = getFirstEmployeeInJson();
        vueAgenda = new VueAgenda();
        vueAgenda.setAgenceId("00480");
    }

    @Test
    public void getVueAgenda_MONO_OK() {
        //given
        vueAgenda.setType(TypeVueAgenda.MONO.getLabel());
        Agent agent = new Agent();
        agent.setId("00597");
        agent.setNom("DUPONT");
        agent.setPrenom("Jean");
        agent.setCivilite("MR");
        vueAgenda.setAgent(agent);
        when(collectPartenaireService.getPartenaire(anyString(), anyString())).thenReturn(partenaire);
        when(collectAgentService.getPortfolio(anyString(), anyString())).thenReturn(portfolio);
        when(collectAgentService.getEmployee(anyString(), anyString())).thenReturn(employee);

        //when
        VueAgenda result = vueAgendaService.getVueAgenda("87800", "00000306172346");

        Assert.assertEquals(result, vueAgenda);
    }

    @Test
    public void getVueAgenda_MULTI_idPortefeuilleIsBlank_OK() {
        //given
        vueAgenda.setType(TypeVueAgenda.MULTI.getLabel());
        partenaire.setIdPortefeuille("");
        when(collectPartenaireService.getPartenaire(anyString(), anyString())).thenReturn(partenaire);
        when(collectAgentService.getPortfolio(anyString(), anyString())).thenReturn(portfolio);
        when(collectAgentService.getEmployee(anyString(), anyString())).thenReturn(employee);

        //when
        VueAgenda result = vueAgendaService.getVueAgenda("87800", "00000306172346");

        Assert.assertEquals(result, vueAgenda);
    }

    @Test
    public void getVueAgenda_MULTI_idPortefeuilleIs0_OK() {
        //given
        vueAgenda.setType(TypeVueAgenda.MULTI.getLabel());
        partenaire.setIdPortefeuille("0");
        when(collectPartenaireService.getPartenaire(anyString(), anyString())).thenReturn(partenaire);
        when(collectAgentService.getPortfolio(anyString(), anyString())).thenReturn(portfolio);
        when(collectAgentService.getEmployee(anyString(), anyString())).thenReturn(employee);

        //when
        VueAgenda result = vueAgendaService.getVueAgenda("87800", "00000306172346");

        Assert.assertEquals(result, vueAgenda);
    }

    @Test
    public void getVueAgenda_MULTI_idPortefeuilleIs0AndSDG9GetPortfolioIsKO_OK() {
        //given
        vueAgenda.setType(TypeVueAgenda.MULTI.getLabel());
        when(collectPartenaireService.getPartenaire(anyString(), anyString())).thenReturn(partenaire);
        when(collectAgentService.getPortfolio(anyString(), anyString())).thenThrow(new BusinessException(null));
        when(collectAgentService.getEmployee(anyString(), anyString())).thenReturn(employee);

        //when
        VueAgenda result = vueAgendaService.getVueAgenda("87800", "00000306172346");

        Assert.assertEquals(result, vueAgenda);
    }

    @Test
    public void getVueAgenda_MULTI_idPortefeuilleIs0AndSDG9GetEmployeeIsKO_OK() {
        //given
        vueAgenda.setType(TypeVueAgenda.MULTI.getLabel());
        when(collectPartenaireService.getPartenaire(anyString(), anyString())).thenReturn(partenaire);
        when(collectAgentService.getPortfolio(anyString(), anyString())).thenReturn(portfolio);
        when(collectAgentService.getEmployee(anyString(), anyString())).thenThrow(new BusinessException(null));

        //when
        VueAgenda result = vueAgendaService.getVueAgenda("87800", "00000306172346");

        Assert.assertEquals(result, vueAgenda);
    }

    @Test
    public void getVueAgenda_MULTI_PortefeuilleIsNull_OK() {
        //given
        vueAgenda.setType(TypeVueAgenda.MULTI.getLabel());
        when(collectPartenaireService.getPartenaire(anyString(), anyString())).thenReturn(partenaire);
        when(collectAgentService.getPortfolio(anyString(), anyString())).thenReturn(null);
        when(collectAgentService.getEmployee(anyString(), anyString())).thenReturn(employee);

        //when
        VueAgenda result = vueAgendaService.getVueAgenda("87800", "00000306172346");

        Assert.assertEquals(result, vueAgenda);
    }

    @Test
    public void getVueAgenda_MULTI_EmployeeIsNull_OK() {
        //given
        vueAgenda.setType(TypeVueAgenda.MULTI.getLabel());
        when(collectPartenaireService.getPartenaire(anyString(), anyString())).thenReturn(partenaire);
        when(collectAgentService.getPortfolio(anyString(), anyString())).thenReturn(portfolio);
        when(collectAgentService.getEmployee(anyString(), anyString())).thenReturn(null);

        //when
        VueAgenda result = vueAgendaService.getVueAgenda("87800", "00000306172346");

        Assert.assertEquals(result, vueAgenda);
    }

    @Test
    public void getVueAgenda_PartenaireIsNull_KO() {
        //given
        vueAgenda.setType(TypeVueAgenda.MULTI.getLabel());
        when(collectPartenaireService.getPartenaire(anyString(), anyString())).thenReturn(null);
        when(collectAgentService.getPortfolio(anyString(), anyString())).thenReturn(portfolio);
        when(collectAgentService.getEmployee(anyString(), anyString())).thenReturn(employee);

        //when
        Assertions.assertThrows(VueAgendaBusinessException.class, () -> {
            vueAgendaService.getVueAgenda("87800", "00000306172346");
        });
    }


    @Test
    public void getVueAgenda_IncorrectPartnerId_KO() {
        //given
        vueAgenda.setType(TypeVueAgenda.MULTI.getLabel());
        when(collectPartenaireService.getPartenaire(anyString(), anyString())).thenReturn(null);
        when(collectAgentService.getPortfolio(anyString(), anyString())).thenReturn(portfolio);
        when(collectAgentService.getEmployee(anyString(), anyString())).thenReturn(employee);

        //when
        Assertions.assertThrows(VueAgendaBusinessException.class, () -> {
            vueAgendaService.getVueAgenda("87800", "0006172346");
        });
    }

    @Test
    public void getVueAgenda_IncorrectAgentIdAndPartnerId_KO() {
        //given
        vueAgenda.setType(TypeVueAgenda.MULTI.getLabel());
        when(collectPartenaireService.getPartenaire(anyString(), anyString())).thenReturn(null);
        when(collectAgentService.getPortfolio(anyString(), anyString())).thenReturn(portfolio);
        when(collectAgentService.getEmployee(anyString(), anyString())).thenReturn(employee);

        //when
        Assertions.assertThrows(VueAgendaBusinessException.class, () -> {
            vueAgendaService.getVueAgenda("87800", "000003065685172346");
        });
    }
}
