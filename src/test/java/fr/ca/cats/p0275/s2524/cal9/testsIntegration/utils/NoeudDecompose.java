package fr.ca.cats.p0275.s2524.cal9.testsIntegration.utils;

import fr.ca.cats.p0070.s1889.easyrest.logging.service.Logger;
import fr.ca.cats.p0070.s1889.easyrest.logging.service.LoggerService;
import lombok.Getter;

import java.util.Arrays;
import java.util.regex.Pattern;

@Getter
public class NoeudDecompose {
    private static final Logger LOG = LoggerService.getLogger(NoeudDecompose.class);
    protected static final String PATTERN_CIBLE_ELEMENT_TABLEAU = "[\\d]";
    protected static final String MARQUEUR_DEBUT_TABLEAU = "[";
    protected static final String SEPARATEUR_PARAM = ".";
    protected static final String NULL_PARAM = "null";
    protected static final String MARQUEUR_SPECIFIQUE_NON_NULL_NON_VIDE = "<NON_NULL>";
    protected static final String MARQUEUR_TABLEAU_VIDE = "[]";
    private String nomNoeudScenario;
    private String nomNoeudSansPattern = null;
    private String[] nomDecompose = null;


    public void setNomNoeudScenario(String nomNoeudScenario) {
        this.nomNoeudScenario = nomNoeudScenario;
        setNomNoeudSansPattern();
        setNoeudComposeEnTableau();
    }

    /**
     * Pour les tableaux JSON, extraction de la position ciblée par le scénario
     * @param noeudATester String titre du scénario contenant le marqueur {@value PATTERN_CIBLE_ELEMENT_TABLEAU}
     * @return int l'index de l'élément du tableau dans le JSON contenant la valeur à tester
     */
    public int extractLigneCiblee(String noeudATester) {
        int ligneCiblee = -1;
        if(noeudATester.contains(PATTERN_CIBLE_ELEMENT_TABLEAU)) {
            int positionChiffre = noeudATester.indexOf(MARQUEUR_DEBUT_TABLEAU);
            ligneCiblee =  Integer.parseInt(noeudATester.substring(positionChiffre, ++positionChiffre));
        }
        return ligneCiblee;
    }

    /**
     * Récupère tous les noeuds parent d'un noeud composé, null si ce n'est pas un noeud composé
     * @return String[] tableau contenant tous les noeuds parent du noeud composé
     */
    public String[] getSousNoeud() {
        if(this.nomDecompose == null) {
            return null;
        }
        return Arrays.copyOf(this.nomDecompose, this.nomDecompose.length - 1);
    }

    /**
     * Récupération du dernier noeud d'un noeud composé
     * @return String le nom du noeud enfant
     */
    public String getDernierNoeud() {
        if(this.nomDecompose == null) {
            return null;
        }
        return this.nomDecompose[this.nomDecompose.length - 1];
    }

    /**
     * Tester si la valeur attendue est un booléen
     * @param parametre String valeur attendu issue de la conversion du tableau Gherkin en Map
     * @return boolean true si la valeur attendue est un booléen, false sinon
     */
    public boolean isBoolean(String parametre) {
        if(parametre == null || parametre.isEmpty()) {
            return false;
        }
        parametre = parametre.toLowerCase();
        return "true".equals(parametre) || "false".equals(parametre);
    }

    /**
     * Vérification si le champ à tester est null
     * @param parametre String paramètre à tester
     * @return boolean true si le paramètre est null, false sinon
     */
    public boolean isNull(String parametre) {
        if(parametre == null || parametre.isEmpty())
            return false;
        return parametre.contains(NULL_PARAM);
    }

    /**
     * Vérification si le champ à tester est un sous élément ou une valeur
     * @param parametre String paramètre à tester
     * @return boolean true si le paramètre contient un sous-élément, false sinon
     */
    public boolean isParametreCompose(String parametre) {
        if(parametre == null || parametre.isEmpty())
            return false;
        return parametre.contains(SEPARATEUR_PARAM);
    }

    /**
     * Vérification si le champ à tester est un marqueur ou une valeur
     * @param parametre String paramètre à tester
     * @return boolean true si le paramètre contient un marqueur, false sinon
     */
    public boolean isParametreSpecifique(String parametre) {
        if(parametre == null || parametre.isEmpty())
            return false;
        boolean isParamSpecifique = parametre.contains(MARQUEUR_SPECIFIQUE_NON_NULL_NON_VIDE);
        // Message d'erreur si la valeur attendu est au format d'un marqueur <XX> mais qu'il n'est pas défini
        // Erreur dans la saisie du scénario ou nouveau marqueur à implementer ?
        if(!isParamSpecifique && parametre.contains("<") && parametre.contains(">")) {
            LOG.error(String.format(Constant.MESSAGE_MARQUEUR_SPECIFIQUE_INCONNU, parametre));
        }
        return parametre.contains(MARQUEUR_SPECIFIQUE_NON_NULL_NON_VIDE);
    }

    /**
     * Vérification si le paramètre est un tableau ou une valeur
     * @param parametre String paramètre à tester
     * @return boolean true si le paramètre est un tableau vide, false sinon
     */
    public boolean isArray(String parametre) {
        if(parametre == null || parametre.isEmpty()) {
            return false;
        }
        return parametre.contains(MARQUEUR_TABLEAU_VIDE);
    }

    /**
     * Affichage des propriétés de l'objet
     * @return String les valeurs des propriétés de l'objet
     */
    public String toString() {
        return "nomNoeudScenario = " + this.nomNoeudScenario
                +", nomNoeudSansPattern = " + this.nomNoeudSansPattern
                +", nomDecompose = " +  Arrays.toString(this.nomDecompose);
    }

    /**
     * Supprime le pattern {@value PATTERN_CIBLE_ELEMENT_TABLEAU} de choix de ligne s'il existe
     * @param nomNoeud nom du noeud à traiter
     * @return String si le pattern existe, le nom du noeud sans le pattern, sinon le nom du noeud
     */
    public String getNomNoeudSansPattern(String nomNoeud) {
        int indexDebutPattern = nomNoeud.indexOf("[");
        int indexFinPattern = nomNoeud.indexOf("]");
        String nomTraite = nomNoeud;
        if(indexDebutPattern > -1 && indexFinPattern > -1) {
            nomTraite = nomNoeud.substring(0, indexDebutPattern) + nomNoeud.substring(indexFinPattern+1);
        }
        return nomTraite;
    }

    private void setNomNoeudSansPattern() {
        this.nomNoeudSansPattern = getNomNoeudSansPattern(nomNoeudScenario);
    }

    private void setNoeudComposeEnTableau() {
        if(isParametreCompose(this.nomNoeudScenario)) {
            // /!\ pour le cas du "." comme séparateur, l'appel de Pattern.quote est obligatoire
            this.nomDecompose = this.nomNoeudScenario.split(Pattern.quote(SEPARATEUR_PARAM));
        }
    }
}
