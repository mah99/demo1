package fr.ca.cats.p0275.s2524.cal9.infrastructure.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Portfolio;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.DistributionEntityMapper;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.EmployeeMapper;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.WorkingHourMapper;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.EdsAgentService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.AgentClientService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.AgentPortfolioClientService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.OmEdsAgent;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.AgentRepository;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.EdsAgentRepository;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.impl.CollectAgentServiceImpl;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class CollectAgentServiceTest {

    @Mock
    private AgentRepository agentRepository;

    @Mock
    private EdsAgentRepository edsAgentRepository;

    @Mock
    private AgentClientService agentClientService;
    @Mock
    private AgentPortfolioClientService agentPortfolioClientService;

    private CollectAgentService collectAgentService;

    @Mock
    private EdsAgentService edsAgentService;

    @Spy
    WorkingHourMapper workingHourMapper = Mappers.getMapper(WorkingHourMapper.class);

    @Spy
    DistributionEntityMapper distributionEntityMapper = Mappers.getMapper(DistributionEntityMapper.class);

    @Spy
    EmployeeMapper employeeMapper = Mappers.getMapper(EmployeeMapper.class);

    private List<OmEdsAgent> omEdsAgent = new ArrayList<>();

    @BeforeEach
    void init() {
        collectAgentService = new CollectAgentServiceImpl(agentRepository, edsAgentRepository, agentPortfolioClientService, agentClientService, edsAgentService, workingHourMapper,distributionEntityMapper, employeeMapper);
        ObjectMapper mapper = new ObjectMapper();
        Resource resourceEdsAgent = new ClassPathResource("/static/OMEdsAgentLocalisation.json");
        try {
            omEdsAgent = mapper.readValue(resourceEdsAgent.getInputStream(), new TypeReference<>() {
            });
        } catch (Exception ignored) {
        }

        when(edsAgentRepository.getAllByIdCaisseRegionaleAndAgentId(any(),any())).thenReturn(omEdsAgent);
    }

    @Test
    void givenAgentIdStructureId_whenGetEmployeeLocalisationFromDB_KO_Then_OK_fromServer() {
        // given
        Employee employee = Employee.builder()
                .idEmployee("123465")
                .structureId("87800")
                .build();
        when(edsAgentRepository.getAllByIdCaisseRegionaleAndAgentId(any(), any())).thenReturn(null);
        when(agentClientService.getEmployee(any(), any())).thenReturn(employee);

        // when
        var result = collectAgentService.getEmployeeLocalisation("87800", "07031");

        // then
        Assert.assertEquals(employee, result);
        verify(edsAgentRepository).getAllByIdCaisseRegionaleAndAgentId(any(), any());
        verify(agentClientService).getEmployee(any(), any());
        verify(agentRepository).addEmployee(any());
    }

    @Test
    void givenAgentIdStructureId_whenGetEmployeeLocalisationFromDB_OK_Then_OK_fromServer() {
        // given
        Employee employee = Employee.builder()
                .idEmployee("07031")
                .matriculeAgent("07031")
                .nom("MAISONNEUVE")
                .prenom("MARIELLE")
                .civilite("MADAME")
                .build();
        when(agentClientService.getEmployee(any(), any())).thenReturn(employee);

        // when
        var result = collectAgentService.getEmployeeLocalisation("87800", "07031");

        // then

        Assert.assertEquals(3, result.getDistributionEntities().size());
        Assert.assertEquals("MADAME", result.getCivilite());
        Assert.assertEquals("MARIELLE", result.getPrenom());
        Assert.assertEquals("MAISONNEUVE", result.getNom());
        Assert.assertEquals("07031", result.getMatriculeAgent());
        Assert.assertEquals("07031", result.getIdEmployee());

        Assert.assertEquals("4T56", result.getDistributionEntities().get(0).getJob().getId());
        Assert.assertEquals("00075", result.getDistributionEntities().get(0).getIdBranch());
        Assert.assertEquals("REGROUPT AGENCES BANQUE PRIVEE", result.getDistributionEntities().get(0).getLabel());
        Assert.assertEquals("MARDI", result.getDistributionEntities().get(0).getEmployeeFunctions().get(0).getWorkingHours().get(0).getDay());
        Assert.assertEquals("MERCREDI", result.getDistributionEntities().get(0).getEmployeeFunctions().get(0).getWorkingHours().get(1).getDay());
        Assert.assertEquals("10:00:00", result.getDistributionEntities().get(0).getEmployeeFunctions().get(0).getWorkingHours().get(0).getStartingHour());
        Assert.assertEquals("17:00:00", result.getDistributionEntities().get(0).getEmployeeFunctions().get(0).getWorkingHours().get(0).getEndingHour());
        Assert.assertEquals("13:00:00", result.getDistributionEntities().get(0).getEmployeeFunctions().get(0).getWorkingHours().get(1).getStartingHour());
        Assert.assertEquals("20:00:00", result.getDistributionEntities().get(0).getEmployeeFunctions().get(0).getWorkingHours().get(1).getEndingHour());

        Assert.assertEquals("4T57", result.getDistributionEntities().get(1).getJob().getId());
        Assert.assertEquals("00086", result.getDistributionEntities().get(1).getIdBranch());
        Assert.assertEquals("DIR. CLIENTELE LYON RIVE DROITE", result.getDistributionEntities().get(1).getLabel());
        Assert.assertEquals("SAMEDI", result.getDistributionEntities().get(1).getEmployeeFunctions().get(0).getWorkingHours().get(0).getDay());
        Assert.assertEquals("10:00:00", result.getDistributionEntities().get(1).getEmployeeFunctions().get(0).getWorkingHours().get(0).getStartingHour());
        Assert.assertEquals("12:00:00", result.getDistributionEntities().get(1).getEmployeeFunctions().get(0).getWorkingHours().get(0).getEndingHour());

        Assert.assertEquals("4T58", result.getDistributionEntities().get(2).getJob().getId());
        Assert.assertEquals("00087", result.getDistributionEntities().get(2).getIdBranch());
        Assert.assertEquals("DIR. CLIENTELE LYON RIVE DROITE", result.getDistributionEntities().get(2).getLabel());
        Assert.assertEquals("SAMEDI", result.getDistributionEntities().get(2).getEmployeeFunctions().get(0).getWorkingHours().get(0).getDay());
        Assert.assertEquals("LUNDI", result.getDistributionEntities().get(2).getEmployeeFunctions().get(0).getWorkingHours().get(1).getDay());
        Assert.assertEquals("13:00:00", result.getDistributionEntities().get(2).getEmployeeFunctions().get(0).getWorkingHours().get(0).getStartingHour());
        Assert.assertEquals("23:00:00", result.getDistributionEntities().get(2).getEmployeeFunctions().get(0).getWorkingHours().get(0).getEndingHour());
        Assert.assertEquals("06:00:00", result.getDistributionEntities().get(2).getEmployeeFunctions().get(0).getWorkingHours().get(1).getStartingHour());
        Assert.assertEquals("22:00:00", result.getDistributionEntities().get(2).getEmployeeFunctions().get(0).getWorkingHours().get(1).getEndingHour());

        verify(edsAgentRepository).getAllByIdCaisseRegionaleAndAgentId(any(), any());
    }

    @Test
    void givenAgentIdStructureId_whenGetEmployeeFromDB_Then_OK() {
        // given
        Employee employee = Employee.builder()
                .idEmployee("123465")
                .structureId("87800")
                .build();
        when(agentRepository.getEmployeeByAgentIdAndStructureId(any(), any())).thenReturn(employee);

        // when
        var result = collectAgentService.getEmployee("87800", "123456");

        // then
        Assert.assertEquals(employee, result);
        verify(agentRepository).getEmployeeByAgentIdAndStructureId(any(), any());
        verify(agentClientService, times(0)).getEmployee(any(), any());
        verify(agentRepository, times(0)).addEmployee(any());


    }


    @Test
    void givenAgentIdStructureId_whenGetEmployeeFromDB_KO_Then_OK_fromServer() {
        // given
        Employee employee = Employee.builder()
                .idEmployee("123465")
                .structureId("87800")
                .build();
        when(agentRepository.getEmployeeByAgentIdAndStructureId(any(), any())).thenReturn(null);
        when(agentClientService.getEmployee(any(), any())).thenReturn(employee);

        // when
        var result = collectAgentService.getEmployee("87800", "123456");

        // then
        Assert.assertEquals(employee, result);
        verify(agentRepository).getEmployeeByAgentIdAndStructureId(any(), any());
        verify(agentClientService).getEmployee(any(), any());
        verify(agentRepository).addEmployee(any());
    }

    @Test
    void givenAgentIdStructureId_whenAgentIdIsNull_Then_OK_fromServer() {
        Employee employee = new Employee();
        var result = collectAgentService.getEmployee("87800", "");

        Assert.assertNotNull(result);
        Assert.assertEquals(employee, result);
    }

    @Test
    void givenAgentIdPartnerIdStructureId_whenGetPortfolio_Then_OK_fromServer() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            Resource resource = new ClassPathResource("/static/sdg9Portfolio.json");
            Portfolio portfolio = mapper.readValue(resource.getInputStream(), Portfolio.class);

            when(agentPortfolioClientService.getPortfolio(any(), any())).thenReturn(portfolio);

            var result = agentPortfolioClientService.getPortfolio("87800", "9999");

            Assert.assertEquals(result, portfolio);

        } catch (Exception e) {
        }
    }


    @Test
    void givenIdEds_whengetAllEmployee_Then_OK() {
        when(edsAgentService.getAllEmployeeIdByIdEds(any())).thenReturn(List.of("40002"));
        var result = collectAgentService.getAllEmployeeIdByIdEds("00463");

        Assert.assertNotNull(result);
        Assert.assertEquals("40002", result.get(0));
    }
}