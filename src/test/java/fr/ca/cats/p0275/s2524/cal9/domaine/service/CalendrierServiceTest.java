package fr.ca.cats.p0275.s2524.cal9.domaine.service;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Branch;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.AgentMapper;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.LocalisationAgendaMapper;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.ReunionMapper;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.exception.CalendrierBusinessException;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.impl.CalendrierServiceImpl;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.BusinessException;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.CallInterApiTechnicalException;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectAgentService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.util.Validator;
import fr.ca.cats.p0275.s2524.cal9.model.Adresse;
import fr.ca.cats.p0275.s2524.cal9.model.Agent;
import fr.ca.cats.p0275.s2524.cal9.model.Calendrier;
import fr.ca.cats.p0275.s2524.cal9.model.PiecesAFournir;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.utils.MapperUtils.getFirstEmployeeInJson;
import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.MapperUtils.readObjectFile;
import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.MapperUtils.readObjectsFile;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class CalendrierServiceTest {

    private CalendrierService calendrierService;
    @Mock
    private PartenaireService partenaireService;
    @Mock
    private ReunionService reunionService;
    @Mock
    private ReunionMultiService reunionMultiService;
    @Mock
    private AgenceService agenceService;
    @Mock
    private AgentService agentService;

    @Mock
    private CollectAgentService collectAgentService;

    private Validator validator;
    @Spy
    private ReunionMapper reunionMapper;
    @Spy
    private AgentMapper agentMapper;
    @Spy
    private LocalisationAgendaMapper localisationAgendaMapper;

    private List<Reunion> reunions;
    private fr.ca.cats.p0275.s2524.cal9.model.Partenaire partenaireModel;
    private fr.ca.cats.p0275.s2524.cal9.model.Partenaire partenaireModelVisio;
    private Branch branch;
    private Employee employee;
    private Adresse adresse;
    private Agent agent;
    private String structureId = "87800";

    @BeforeEach
    public void init(){
        reunions = readObjectsFile("classpath:static/agc9.json", Reunion.class);
        employee = getFirstEmployeeInJson();
        partenaireModelVisio = setupPartenaireVisio();
        partenaireModel = setupPartenaire();
        List<fr.ca.cats.p0275.s2524.cal9.model.Partenaire> partenaireModelList = new ArrayList<>();
        List<fr.ca.cats.p0275.s2524.cal9.model.Partenaire> partenaireModelListVisio = new ArrayList<>();
        partenaireModelList.add(partenaireModel);
        partenaireModelListVisio.add(partenaireModelVisio);
        adresse = setupAdresse();
        agent = setupAgent();
        when(partenaireService.getPartenaires(any(), anyString())).thenReturn(partenaireModelList);
        when(partenaireService.getPartenairesVizio(any(), anyString())).thenReturn(partenaireModelListVisio);
        when(agentService.getEmployee(anyString(), anyString())).thenReturn(employee);
        when(agentService.getEmployeeLocalisation(anyString(), anyString())).thenReturn(employee);
        when(agentService.getAgentCreateur(anyString(), any())).thenReturn(agent);
        when(agenceService.getAdresse(any(), any(), any())).thenReturn(adresse);
        when(reunionService.getReunion()).thenReturn(Collections.singletonList(reunions.get(0)));
        when(collectAgentService.getAllEmployee(any())).thenReturn(List.of(employee));
        when(collectAgentService.getEmployeeLocalisation(anyString(), anyString())).thenReturn(employee);
        when(collectAgentService.getAllEmployeeIdByIdEds(any())).thenReturn(List.of(employee.getMatriculeAgent()));
        agentMapper = Mappers.getMapper(AgentMapper.class);
        reunionMapper = Mappers.getMapper(ReunionMapper.class);
        localisationAgendaMapper = Mappers.getMapper(LocalisationAgendaMapper.class);
        calendrierService = new CalendrierServiceImpl(
                reunionMapper, agentMapper, agentService, agenceService, partenaireService, reunionService, localisationAgendaMapper, reunionMultiService, collectAgentService);
    }

    public fr.ca.cats.p0275.s2524.cal9.model.Partenaire setupPartenaire(){
        fr.ca.cats.p0275.s2524.cal9.model.Partenaire partenaireModel = new fr.ca.cats.p0275.s2524.cal9.model.Partenaire();
        partenaireModel.setLibelle("M.       PLOBUAM YOMMUSQTEST");
        partenaireModel.setId("00000000120639");
//        partenaireModel.setIdPartenaireVisio("40502490600001");
        return partenaireModel;
    }

    public fr.ca.cats.p0275.s2524.cal9.model.Partenaire setupPartenaireVisio(){
        fr.ca.cats.p0275.s2524.cal9.model.Partenaire partenaireModel = new fr.ca.cats.p0275.s2524.cal9.model.Partenaire();
        partenaireModel.setLibelle("M.       PLOBUAM YOMMUSQTES");
        partenaireModel.setId("0000000012063");
        return partenaireModel;
    }

    public fr.ca.cats.p0275.s2524.cal9.model.Adresse setupAdresse(){
        fr.ca.cats.p0275.s2524.cal9.model.Adresse adresse = new fr.ca.cats.p0275.s2524.cal9.model.Adresse();
        adresse.setCodePostal("1710");
        adresse.setRue("CENTRE COMMERCIAL");
        adresse.setVille("THOIRY");
        return adresse;
    }

    public Agent setupAgent(){
        Agent agent = new Agent();
        agent.setId("12345");
        agent.setCivilite("MR");
        agent.setNom("DUPONT");
        agent.setPrenom("Jean");
        return agent;
    }

    @Test
    public void givenValidInformation_ThenOK(){
        // when
        Calendrier result = calendrierService.getCalendrier("12345", "12345", "12345", "2022-05-05", "2022-05-05");
        //then
        Assert.assertNotNull(result);
        verify(partenaireService, times(1)).getPartenaires(any(), any());
        verify(agentService, times(1)).getEmployeeLocalisation(any(), any());
        verify(agenceService, times(1)).getAdresse(any(), any(), any());
        verify(reunionService).getReunion();
        Assert.assertEquals("DUPONT", result.getAgent().getNom());
        Assert.assertEquals("Jean", result.getAgent().getPrenom());
        Assert.assertEquals("MR", result.getAgent().getCivilite());
        Assert.assertEquals("expert@cats.com", result.getReunions().get(0).getExperts().get(0).getMail());
        Assert.assertEquals("expertvisio@cats.com", result.getReunions().get(0).getExperts().get(1).getMail());
        Assert.assertEquals("1710", result.getReunions().get(0).getCanalInteraction().getAgence().getAdresse().getCodePostal());
        Assert.assertEquals("THOIRY", result.getReunions().get(0).getCanalInteraction().getAgence().getAdresse().getVille());
        Assert.assertEquals("CENTRE COMMERCIAL", result.getReunions().get(0).getCanalInteraction().getAgence().getAdresse().getRue());
        Assert.assertEquals("12345", result.getReunions().get(0).getReunionContexte().getAgentId());
        Assert.assertEquals("20210922", result.getReunions().get(0).getReunionContexte().getDateReunion());
        Assert.assertEquals("15.00", result.getReunions().get(0).getReunionContexte().getHeureDebutReunion());
        Assert.assertEquals("MR", result.getReunions().get(0).getAgentCreateur().getCivilite());
        Assert.assertEquals("DUPONT", result.getReunions().get(0).getAgentCreateur().getNom());
        Assert.assertEquals("Jean", result.getReunions().get(0).getAgentCreateur().getPrenom());
        Assert.assertEquals("2021-09-22T10:15:00+02:00", result.getReunions().get(0).getDateDebut());
        Assert.assertEquals("2021-09-23T10:15:00+02:00", result.getReunions().get(0).getDateFin());
        Assert.assertEquals("RENDEZ-VOUS", result.getReunions().get(0).getTypeReunion());
        Assert.assertEquals("BL", result.getReunions().get(0).getTheme().getId());
        Assert.assertEquals("Comptes et cartes", result.getReunions().get(0).getTheme().getLibelle());
        Assert.assertEquals("CD", result.getReunions().get(0).getObjet().getId());
        Assert.assertEquals("Faire des opérations à distance", result.getReunions().get(0).getObjet().getLibelle());
        Assert.assertEquals("AGENCE", result.getReunions().get(0).getCanalInteraction().getCanal());
        Assert.assertEquals("00173", result.getReunions().get(0).getCanalInteraction().getAgence().getId());
        Assert.assertEquals("0634526589", result.getReunions().get(0).getCanalInteraction().getTelephone());
        Assert.assertEquals("string", result.getReunions().get(0).getCanalInteraction().getUrlVisio());
        Assert.assertEquals(2, nombrePIAF(result.getReunions().get(0).getPiecesAFournir()));
        Assert.assertEquals("00000000120639", result.getReunions().get(0).getPartenaires().get(0).getId());
        Assert.assertEquals("M.       PLOBUAM YOMMUSQTEST", result.getReunions().get(0).getPartenaires().get(0).getLibelle());
        Assert.assertEquals("M.       PLOBUAM YOMMUSQTES", result.getReunions().get(0).getPartenairesVizio().get(0).getLibelle());
        Assert.assertEquals("0000000012063", result.getReunions().get(0).getPartenairesVizio().get(0).getId());
        Assert.assertEquals("commentaire", result.getReunions().get(0).getCommentaire());
        Assert.assertEquals("PARIS HALLES", result.getLocalisationsAgenda().get(0).getLibelle());
        Assert.assertEquals("506", result.getLocalisationsAgenda().get(0).getAgenceId());
        Assert.assertEquals("1124", result.getLocalisationsAgenda().get(0).getPosteFonctionnel());
        Assert.assertEquals("07:00:00", result.getLocalisationsAgenda().get(0).getHeureDebut());
        Assert.assertEquals("12:00:00", result.getLocalisationsAgenda().get(0).getHeureFin());
        Assert.assertEquals("LUNDI", result.getLocalisationsAgenda().get(0).getJour());
        Assert.assertEquals("506", result.getLocalisationsAgenda().get(1).getAgenceId());
        Assert.assertEquals("1124", result.getLocalisationsAgenda().get(1).getPosteFonctionnel());
        Assert.assertEquals("12:00:00", result.getLocalisationsAgenda().get(1).getHeureDebut());
        Assert.assertEquals("22:00:00", result.getLocalisationsAgenda().get(1).getHeureFin());
        Assert.assertEquals("LUNDI", result.getLocalisationsAgenda().get(1).getJour());
        Assert.assertEquals("507", result.getLocalisationsAgenda().get(2).getAgenceId());
        Assert.assertEquals("1120", result.getLocalisationsAgenda().get(2).getPosteFonctionnel());
        Assert.assertEquals("PARIS", result.getLocalisationsAgenda().get(2).getLibelle());
        Assert.assertEquals("07:00:00", result.getLocalisationsAgenda().get(2).getHeureDebut());
        Assert.assertEquals("12:00:00", result.getLocalisationsAgenda().get(2).getHeureFin());
        Assert.assertEquals("LUNDI", result.getLocalisationsAgenda().get(2).getJour());
    }

    @Test
    public void givenValidInformationAndTypeReunionFerie_ThenOK(){
        // when
        when(reunionService.getReunion()).thenReturn(Collections.singletonList(reunions.get(2)));
        Calendrier result = calendrierService.getCalendrier("12345", "12345", "12345", "2022-05-05", "2022-05-05");
        //then
        Assert.assertNotNull(result);
        Assert.assertEquals("2021-09-22T10:15:00+02:00", result.getReunions().get(0).getDateDebut());
        Assert.assertEquals("2021-09-23T10:15:00+02:00", result.getReunions().get(0).getDateFin());
    }

    @Test
    public void givenValidInformationAndTypeReunionACTIVITE_ThenOK(){
        // when
        when(reunionService.getReunion()).thenReturn(Collections.singletonList(reunions.get(3)));
        Calendrier result = calendrierService.getCalendrier("12345", "12345", "12345", "2022-05-05", "2022-05-05");
        //then
        Assert.assertNotNull(result);
        Assert.assertEquals("2021-09-22T10:15:00+02:00", result.getReunions().get(0).getDateDebut());
        Assert.assertEquals("2021-09-23T10:15:00+02:00", result.getReunions().get(0).getDateFin());
        Assert.assertEquals("Comptes et cartes", result.getReunions().get(0).getTheme().getLibelle());
        Assert.assertEquals("BL", result.getReunions().get(0).getTheme().getId());
    }

    @Test
    public void givenValidInformationAndTypeReunionACTIVITENonValidAgentIdWithLengthLessThan5_ThenOK(){
        // when
        when(reunionService.getReunion()).thenReturn(Collections.singletonList(reunions.get(3)));
        Calendrier result = calendrierService.getCalendrier("12345", structureId , "2345", "2022-05-05", "2022-05-05");
        //then
        Assert.assertNotNull(result);
        Assert.assertEquals("2021-09-22T10:15:00+02:00", result.getReunions().get(0).getDateDebut());
        Assert.assertEquals("2021-09-23T10:15:00+02:00", result.getReunions().get(0).getDateFin());
        Assert.assertEquals("Comptes et cartes", result.getReunions().get(0).getTheme().getLibelle());
        Assert.assertEquals("BL", result.getReunions().get(0).getTheme().getId());
    }

    @Test
    public void givenInvalidAgentId_ThenBusinessException(){
        //given
        when(agenceService.getAdresse(anyString(), any(), any())).thenThrow(new CalendrierBusinessException("Error"));
        // when
        var result = Assertions.assertThrows(CalendrierBusinessException.class, () -> {
            calendrierService.getCalendrier("12345", "12345", "123456", "2022-05-05", "2022-05-05");
        });
    }

    @Test
    public void givenValidInformationAndAGC9Error_ThenBusinessException(){
        //given
        when(reunionService.getReunion()).thenThrow(new CallInterApiTechnicalException("Error"));
        // when
        var result = Assertions.assertThrows(CallInterApiTechnicalException.class, () -> {
            calendrierService.getCalendrier("12345", "12345", "12345", "2022-05-05", "2022-05-05");
        });
    }

    @Test
    public void givenValidInformationAndAGC9ErrorReunionTypedNullEmpty_ThenBusinessException() {
        //given
        reunions.get(0).setTypeReunion(null);
        // when
        var result = Assertions.assertThrows(CalendrierBusinessException.class, () -> {
            calendrierService.getCalendrier("12345", "12345", "12345", "2022-05-05", "2022-05-05");
        });
        //then
        Assert.assertEquals("Le type de réunion est manquant", result.getMessage());
    }
    private int nombrePIAF(List<PiecesAFournir> piecesAFournir) {
        int sizePiaf = piecesAFournir.size();
        return sizePiaf;
    }

    @Test
    public void testCalendrierMultiWithIdEdsValidInformation_ThenOK() {
        when(collectAgentService.getAllEmployeeIdByIdEds("00463")).thenReturn(List.of("00597"));
        when(collectAgentService.getAllEmployee("88200")).thenReturn(List.of(employee));
        when(collectAgentService.getEmployee("88200", "00597")).thenReturn(employee);

        when(reunionMultiService.getReunionMulti(anyList(),anyString(), anyString())).thenReturn(List.of(reunions.get(4)));
        // when
        List<Calendrier> result = calendrierService.getCalendrierMulti("12345", "88200", List.of(),"00463", "2022-05-05", "2022-05-05");

        //then
        Assert.assertNotNull(result);
        Assert.assertEquals("expert@cats.com", result.get(0).getReunions().get(0).getExperts().get(0).getMail());
        Assert.assertEquals("expertvisio@cats.com", result.get(0).getReunions().get(0).getExperts().get(1).getMail());
        Assert.assertEquals("1710", result.get(0).getReunions().get(0).getCanalInteraction().getAgence().getAdresse().getCodePostal());
        Assert.assertEquals("THOIRY", result.get(0).getReunions().get(0).getCanalInteraction().getAgence().getAdresse().getVille());
        Assert.assertEquals("CENTRE COMMERCIAL", result.get(0).getReunions().get(0).getCanalInteraction().getAgence().getAdresse().getRue());
        Assert.assertEquals("MR", result.get(0).getReunions().get(0).getAgentCreateur().getCivilite());
        Assert.assertEquals("DUPONT", result.get(0).getReunions().get(0).getAgentCreateur().getNom());
        Assert.assertEquals("Jean", result.get(0).getReunions().get(0).getAgentCreateur().getPrenom());
        Assert.assertEquals("2021-09-22T10:15:00+02:00", result.get(0).getReunions().get(0).getDateDebut());
        Assert.assertEquals("2021-09-23T10:15:00+02:00", result.get(0).getReunions().get(0).getDateFin());
        Assert.assertEquals("RENDEZ-VOUS", result.get(0).getReunions().get(0).getTypeReunion());
        Assert.assertEquals("BL", result.get(0).getReunions().get(0).getTheme().getId());
        Assert.assertEquals("Comptes et cartes", result.get(0).getReunions().get(0).getTheme().getLibelle());
        Assert.assertEquals("CD", result.get(0).getReunions().get(0).getObjet().getId());
        Assert.assertEquals("Faire des opérations à distance", result.get(0).getReunions().get(0).getObjet().getLibelle());
        Assert.assertEquals("AGENCE", result.get(0).getReunions().get(0).getCanalInteraction().getCanal());
        Assert.assertEquals("00173", result.get(0).getReunions().get(0).getCanalInteraction().getAgence().getId());
        Assert.assertEquals("0634526589", result.get(0).getReunions().get(0).getCanalInteraction().getTelephone());
        Assert.assertEquals("string", result.get(0).getReunions().get(0).getCanalInteraction().getUrlVisio());
        Assert.assertEquals(2, nombrePIAF(result.get(0).getReunions().get(0).getPiecesAFournir()));
        Assert.assertEquals("00000000120639", result.get(0).getReunions().get(0).getPartenaires().get(0).getId());
        Assert.assertEquals("M.       PLOBUAM YOMMUSQTEST", result.get(0).getReunions().get(0).getPartenaires().get(0).getLibelle());
        Assert.assertEquals("M.       PLOBUAM YOMMUSQTES", result.get(0).getReunions().get(0).getPartenairesVizio().get(0).getLibelle());
        Assert.assertEquals("0000000012063", result.get(0).getReunions().get(0).getPartenairesVizio().get(0).getId());
        Assert.assertEquals("commentaire", result.get(0).getReunions().get(0).getCommentaire());
        Assert.assertEquals("00597", result.get(0).getReunions().get(0).getReunionContexte().getAgentId());


    }
    @Test
    public void testCalendrierMultiWithListOfAgentValidInformation_ThenOK() {
        when(collectAgentService.getEmployee("88200","00597")).thenReturn(employee);
        when(collectAgentService.getAllEmployee("88200")).thenReturn(List.of(employee));
        when(reunionMultiService.getReunionMulti(anyList(),anyString(), anyString())).thenReturn(List.of(reunions.get(4)));
        // when
        List<Calendrier> result = calendrierService.getCalendrierMulti("88200", "88200", List.of("00597"),null, "2022-05-05", "2023-05-05");

        //then
        Assert.assertNotNull(result.get(0));
        Assert.assertEquals("expert@cats.com", result.get(0).getReunions().get(0).getExperts().get(0).getMail());
        Assert.assertEquals("expertvisio@cats.com", result.get(0).getReunions().get(0).getExperts().get(1).getMail());
        Assert.assertEquals("1710", result.get(0).getReunions().get(0).getCanalInteraction().getAgence().getAdresse().getCodePostal());
        Assert.assertEquals("THOIRY", result.get(0).getReunions().get(0).getCanalInteraction().getAgence().getAdresse().getVille());
        Assert.assertEquals("CENTRE COMMERCIAL", result.get(0).getReunions().get(0).getCanalInteraction().getAgence().getAdresse().getRue());
        Assert.assertEquals("00597", result.get(0).getReunions().get(0).getReunionContexte().getAgentId());
        Assert.assertEquals("20210922", result.get(0).getReunions().get(0).getReunionContexte().getDateReunion());
        Assert.assertEquals("15.00", result.get(0).getReunions().get(0).getReunionContexte().getHeureDebutReunion());
        Assert.assertEquals("MR", result.get(0).getReunions().get(0).getAgentCreateur().getCivilite());
        Assert.assertEquals("DUPONT", result.get(0).getReunions().get(0).getAgentCreateur().getNom());
        Assert.assertEquals("Jean", result.get(0).getReunions().get(0).getAgentCreateur().getPrenom());
        Assert.assertEquals("2021-09-22T10:15:00+02:00", result.get(0).getReunions().get(0).getDateDebut());
        Assert.assertEquals("2021-09-23T10:15:00+02:00", result.get(0).getReunions().get(0).getDateFin());
        Assert.assertEquals("RENDEZ-VOUS", result.get(0).getReunions().get(0).getTypeReunion());
        Assert.assertEquals("BL", result.get(0).getReunions().get(0).getTheme().getId());
        Assert.assertEquals("Comptes et cartes", result.get(0).getReunions().get(0).getTheme().getLibelle());
        Assert.assertEquals("CD", result.get(0).getReunions().get(0).getObjet().getId());
        Assert.assertEquals("Faire des opérations à distance", result.get(0).getReunions().get(0).getObjet().getLibelle());
        Assert.assertEquals("AGENCE", result.get(0).getReunions().get(0).getCanalInteraction().getCanal());
        Assert.assertEquals("00173", result.get(0).getReunions().get(0).getCanalInteraction().getAgence().getId());
        Assert.assertEquals("0634526589", result.get(0).getReunions().get(0).getCanalInteraction().getTelephone());
        Assert.assertEquals("string", result.get(0).getReunions().get(0).getCanalInteraction().getUrlVisio());
        Assert.assertEquals(2, nombrePIAF(result.get(0).getReunions().get(0).getPiecesAFournir()));
        Assert.assertEquals("00000000120639", result.get(0).getReunions().get(0).getPartenaires().get(0).getId());
        Assert.assertEquals("M.       PLOBUAM YOMMUSQTEST", result.get(0).getReunions().get(0).getPartenaires().get(0).getLibelle());
        Assert.assertEquals("M.       PLOBUAM YOMMUSQTES", result.get(0).getReunions().get(0).getPartenairesVizio().get(0).getLibelle());
        Assert.assertEquals("0000000012063", result.get(0).getReunions().get(0).getPartenairesVizio().get(0).getId());
        Assert.assertEquals("commentaire", result.get(0).getReunions().get(0).getCommentaire());
    }

    @Test
    public void testCalendrierMultiWithTooMuchParamsValidInformation_ThenOK() {
        Assert.assertThrows(CalendrierBusinessException.class, () -> {
            calendrierService.getCalendrierMulti("12345", structureId, List.of("12345", "67890"),"05642", "2022-05-05", "2022-05-05");
        });
    }
    @Test
    public void testCalendrierMultiWithNotEnoughtParamsValidInformation_ThenOK() {
        Assert.assertThrows(CalendrierBusinessException.class, () -> {
            calendrierService.getCalendrierMulti("12345", structureId, null,null, "2022-05-05", "2022-05-05");
        });
    }

    @Test
    public void testCalendrierMultiWithIdEdsNoReunions_ThenOK() {
        when(reunionMultiService.getReunionMulti(anyList(),anyString(), anyString())).thenReturn(List.of());
        // when
        List<Calendrier> result = calendrierService.getCalendrierMulti("12345", "88200", List.of(),"00641", "2022-05-05", "2022-05-05");

        //then
        Assert.assertNotNull(result);
        Assert.assertTrue(result.get(0).getReunions().isEmpty());
    }
    @Test
    public void testCalendrierMultiWithAgentWithOutReunion_ThenOK() {
        when(collectAgentService.getAllEmployee("88200")).thenReturn(List.of(employee));
        when(reunionMultiService.getReunionMulti(anyList(),anyString(), anyString())).thenReturn(List.of());
        // when
        List<Calendrier> result = calendrierService.getCalendrierMulti("12345", structureId, List.of("12345", "67890"),"", "2022-05-05", "2022-05-05");

        //then
        Assert.assertNotNull(result);
        Assert.assertTrue(result.get(0).getReunions().isEmpty());
    }
    @Test
    public void testCalendrierMultiNoAgentInEds_ThenException() {
        when(collectAgentService.getAllEmployeeIdByIdEds("8820000654")).thenReturn(List.of());
        Assert.assertThrows(BusinessException.class, () -> calendrierService.getCalendrierMulti("12345", "88200", null,"00654", "2022-05-05", "2022-05-05"));
    }
}