package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.PartenaireMapper;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.PartenaireDao;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.Dao.PartenaireRepositoryDao;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.impl.PartenaireRepositoryImpl;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.utils.MapperUtils.getFirstPartenaireDaoInJson;
import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.MapperUtils.readObjectFile;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class PartenaireRepositoryTest {


    private PartenaireRepository partenaireRepository;

    @Mock
    private PartenaireRepositoryDao partenaireRepositorDao;

    @Spy
    PartenaireMapper partenaireMapper = Mappers.getMapper(PartenaireMapper.class);
    private PartenaireDao partenaireDao;

    @BeforeEach
    public void init() {
        partenaireDao = getFirstPartenaireDaoInJson();
        partenaireRepository = new PartenaireRepositoryImpl(partenaireRepositorDao, partenaireMapper);

    }

    @Test
    public void getPartenaireWithPartenaireIdAndStructureId() {
        // given
        when(partenaireRepositorDao.findByIdCaisseRegionaleAndIdPartenaire(any(), any())).thenReturn(partenaireDao);

        // when
        var result = partenaireRepository.getPartenaire("00000062847493", "87800");

        // then
        Assert.assertEquals(partenaireDao.getIdCaisseRegionale(), result.getIdCaisseRegionale());
        Assert.assertEquals(partenaireDao.getIdPartenaire(), result.getIdPartenaire());
        Assert.assertEquals(partenaireDao.getIntitule(), result.getIntitule());
        Assert.assertEquals(partenaireDao.getIntituleSuite(), result.getIntituleSuite());
        verify(partenaireMapper).partenaireDaoToPartenaire(any());
        verify(partenaireRepositorDao).findByIdCaisseRegionaleAndIdPartenaire(any(), any());
    }

    @Test
    public void addIndividualBasic() {

        // when
        partenaireRepository.addPartenaire(Partenaire.builder().build());

        // then
        verify(partenaireMapper).partenaireToPartenaireDao(any());
        verify(partenaireRepositorDao).save(any());
    }
}