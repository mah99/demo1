package fr.ca.cats.p0275.s2524.cal9.domaine.service;

import fr.ca.cats.p0275.s2524.cal9.domaine.service.exception.SettingsBusinessException;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.impl.SettingsServiceImpl;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectSettingsService;
import fr.ca.cats.p0275.s2524.cal9.model.Setting;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.MapperUtils.readObjectsFile;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class SettingsServiceTest {

    private SettingsService settingsService;

    @Mock
    private CollectSettingsService collectSettingsService;

    List<Setting> settings;

    private final String structureId = "87800";


    @BeforeEach
    public void init() {
        settings = readObjectsFile("classpath:static/agc9Settings.json", Setting.class);
        settingsService = new SettingsServiceImpl(collectSettingsService);
    }

    @Test
    public void getSettingsOk() {
        when(collectSettingsService.getSettings(structureId)).thenReturn(settings);

        // when
        var result = settingsService.getSettings(structureId);

        // then
        Assert.assertEquals(settings, result);
        verify(collectSettingsService).getSettings(structureId);
    }

    @Test
    public void getSettingsKo_NegativeId() {
        // given
        when(collectSettingsService.getSettings("-1")).thenThrow(new SettingsBusinessException("Invalid : paramètre structureId négatif"));
        // when
        Assertions.assertThrows(SettingsBusinessException.class, () -> {
            settingsService.getSettings("-1");
        });

        // then
        // exception
    }

    @Test
    public void getSettingsKo_TooShortId() {
        // given
        when(collectSettingsService.getSettings("5800")).thenThrow(new SettingsBusinessException("Invalid : paramètre structureId trop court"));

        // when
        Assertions.assertThrows(SettingsBusinessException.class, () -> {
            settingsService.getSettings("5800");
        });

        // then
        // exception
    }

    @Test
    public void getSettingsKo_TooLongId() {
        // given
        when(collectSettingsService.getSettings("870850")).thenThrow(new SettingsBusinessException("Invalid : paramètre structureId trop long"));

        // when
        Assertions.assertThrows(SettingsBusinessException.class, () -> {
            settingsService.getSettings("870850");
        });

        // then
        // exception
    }

    @Test
    public void getSettingsKo_NonNumericId() {
        // given
        when(collectSettingsService.getSettings("rtgre")).thenThrow(new SettingsBusinessException("Invalid : le paramètre structureId contient des caractères alphanumériques"));

        // when
        Assertions.assertThrows(SettingsBusinessException.class, () -> {
            settingsService.getSettings("rtgre");
        });

        // then
        // exception
    }



}
