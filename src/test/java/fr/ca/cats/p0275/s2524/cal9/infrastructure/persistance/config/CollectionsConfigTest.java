package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.config;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.AgentDao;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.BranchDao;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.IndividualBasicDao;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.IndexInfo;
import org.springframework.data.mongodb.core.index.IndexOperations;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@ExtendWith(SpringExtension.class)
public class CollectionsConfigTest {

    private CollectionsConfig collectionsConfig;

    @Mock
    private MongoTemplate mongoTemplate;

    private IndexOperations indexOperations;

    @BeforeEach
    public void init() {

        collectionsConfig = new CollectionsConfig(mongoTemplate);
        ReflectionTestUtils.setField(collectionsConfig, "expireAfter", 50);
        indexOperations = mock(IndexOperations.class);
        when(mongoTemplate.indexOps(AgentDao.class)).thenReturn(indexOperations);
        when(mongoTemplate.indexOps(BranchDao.class)).thenReturn(indexOperations);
        when(mongoTemplate.indexOps(IndividualBasicDao.class)).thenReturn(indexOperations);
    }


    @Test
    public void givenIndex_whenInitIndex_thenCreate3Index() {
        // given
        when(indexOperations.getIndexInfo()).thenReturn(new ArrayList<>());

        // when
        collectionsConfig.initIndexes();

        // then
        verify(mongoTemplate, times(2)).indexOps(AgentDao.class);
        verify(mongoTemplate, times(2)).indexOps(BranchDao.class);
        verify(mongoTemplate, times(2)).indexOps(IndividualBasicDao.class);
        verify(indexOperations, times(3)).ensureIndex(any());

    }

    @Test
    public void givenIndexWithDifferentExpireDate_whenInitIndex_thenUpdate3Index() {
        // given

        IndexInfo indexInfo = new IndexInfo(new ArrayList<>(), "registeredDate", true, false, "fr");

        when(indexOperations.getIndexInfo()).thenReturn(List.of(indexInfo));

        // when
        collectionsConfig.initIndexes();

        // then
        verify(mongoTemplate, times(3)).indexOps(AgentDao.class);
        verify(mongoTemplate, times(3)).indexOps(BranchDao.class);
        verify(mongoTemplate, times(3)).indexOps(IndividualBasicDao.class);
        verify(indexOperations, times(3)).ensureIndex(any());

    }

    @Test
    public void givenIndexWithExpiredDate() {
        //given
        Optional<Duration> duration = Optional.ofNullable(Duration.ofSeconds(200));

        //when

        //then
        Assertions.assertFalse(collectionsConfig.isIndexExpired(duration));
    }

    @Test
    public void givenIndexWithUnexpiredDate() {
        //given
        Optional<Duration> duration = Optional.ofNullable(Duration.ofSeconds(20));

        //when

        //then
        Assertions.assertTrue(collectionsConfig.isIndexExpired(duration));
    }

    @Test
    public void givenIndexWithExpiredDate_Equals() {
        //given
        Optional<Duration> duration = Optional.ofNullable(Duration.ofSeconds(50));

        //when

        //then
        Assertions.assertTrue(collectionsConfig.isIndexExpired(duration));
    }

    @Test
    public void givenIndexWithNullableDate() {
        //given
        Optional<Duration> duration = Optional.ofNullable(null);

        //when

        //then
        Assertions.assertTrue(collectionsConfig.isIndexExpired(duration));
    }
}