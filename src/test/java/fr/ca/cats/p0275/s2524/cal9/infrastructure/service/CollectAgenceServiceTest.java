package fr.ca.cats.p0275.s2524.cal9.infrastructure.service;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Branch;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.AgenceClientService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.AgenceRepository;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.impl.CollectAgenceServiceImpl;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class CollectAgenceServiceTest {

    @Mock
    private AgenceRepository agenceRepository;
    @Mock
    private AgenceClientService agenceClientService;

    private CollectAgenceService collectAgenceService;

    @BeforeEach
    public void init() {

        collectAgenceService = new CollectAgenceServiceImpl(agenceRepository, agenceClientService);

    }


    @Test
    public void givenBranchIdStructureId_whenGetAgenceFromDB_Then_OK() {
        // given
        Branch branch = Branch.builder()
                .id("123465")
                .city("city")
                .codeEds("8521")
                .structureId("87800")
                .email("email@email.com")
                .build();

        when(agenceRepository.getBranchByAgenceIdAndStructureId(any(), any())).thenReturn(branch);

        // when
        var result = collectAgenceService.getAgence("123456", "87800");

        // then
        Assert.assertEquals(branch, result);
        verify(agenceRepository).getBranchByAgenceIdAndStructureId(any(), any());
        verify(agenceClientService, times(0)).getAgence(any(), any());
        verify(agenceRepository, times(0)).addBranch(any());


    }


    @Test
    public void givenBranchIdStructureId_whenGetAgenceFromDB_KO_Then_OK_fromServer() {
        // given
        Branch branch = Branch.builder()
                .id("123465")
                .city("city")
                .codeEds("8521")
                .structureId("87800")
                .email("email@email.com")
                .build();

        when(agenceRepository.getBranchByAgenceIdAndStructureId(any(), any())).thenReturn(null);
        when(agenceClientService.getAgence(any(), any())).thenReturn(branch);

        // when
        var result = collectAgenceService.getAgence("123456", "87800");

        // then
        Assert.assertEquals(branch, result);
        verify(agenceRepository).getBranchByAgenceIdAndStructureId(any(), any());
        verify(agenceClientService).getAgence(any(), any());
        verify(agenceRepository).addBranch(any());


    }

}