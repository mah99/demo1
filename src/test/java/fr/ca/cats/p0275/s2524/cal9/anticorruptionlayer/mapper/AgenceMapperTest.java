package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Branch;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.BranchDao;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AgenceMapperImpl.class})
public class AgenceMapperTest {

    @InjectMocks
    private AgenceMapper mapper = AgenceMapperImpl.AGENCE_MAPPER;

    @Before
    public void init(){
        AgenceMapper agenceMapper = Mappers.getMapper(AgenceMapper.class);
        ReflectionTestUtils.setField(mapper, "agenceMapper", agenceMapper);
    }

    private static BranchDao branchDao;

    @Test
    public void branchDaoToBranch() {
        Branch branch = mapper.branchDaoToBranch(initBranch());
        Assert.assertEquals("THOIRY", branch.getCity());
        Assert.assertEquals("test", branch.getEmail());
        Assert.assertEquals("VAL THOIRY", branch.getName());
        Assert.assertEquals("CENTRE COMMERCIAL", branch.getPostalAddress().getPostalAddressLine4());
        Assert.assertEquals("1710", branch.getZipCode());
    }

    private static BranchDao initBranch(){
        ObjectMapper mapper = new ObjectMapper();
        Resource resource = new ClassPathResource("/static/sdg9BranchID.json");

        try {
            branchDao =  mapper.readValue(resource.getInputStream(), BranchDao.class);
        } catch (Exception e) {
            return null;
        }
        return branchDao;
    }



}
