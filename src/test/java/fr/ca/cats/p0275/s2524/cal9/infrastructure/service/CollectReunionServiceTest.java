package fr.ca.cats.p0275.s2524.cal9.infrastructure.service;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.ReunionContexte;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.ReunionClientService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.impl.CollectReunionServiceImpl;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class CollectReunionServiceTest {

    @Mock
    private ReunionClientService reunionClientService;

    private CollectReunionService collectReunionService;

    @BeforeEach
    public void init() {

        collectReunionService = new CollectReunionServiceImpl(reunionClientService);

    }

    @Test
    public void getReunion(){
        // given
        Reunion reunion = Reunion.builder()
                .reunionContexte(new ReunionContexte("60001", "20220721", "8.45"))
                .commentaire("commentaire")
                .build();
        when(reunionClientService.getReunion()).thenReturn(List.of(reunion));

        // when
        var result = collectReunionService.getReunion();

        // then
        Assert.assertEquals(reunion, result.get(0));
        verify(reunionClientService).getReunion();
    }

}