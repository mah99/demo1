package fr.ca.cats.p0275.s2524.cal9.infrastructure.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.SecteurAgentMapper;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.OmAgent;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.OmEdsAgent;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.EdsAgentRepository;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.impl.CollectEdsAgentServiceImpl;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class CollectEdsAgentServiceTest {

    @Spy
    SecteurAgentMapper secteurAgentMapper = Mappers.getMapper(SecteurAgentMapper.class);
    @Mock
    private EdsAgentRepository edsAgentRepository;

    private CollectEdsAgentService collectEdsAgentService;

    private String structureId = "87800";
    private String agentId = "07031";
    private List<OmEdsAgent> omEdsAgent = new ArrayList<>();
    private List<OmEdsAgent> omEdsFils = new ArrayList<>();
    private List<OmEdsAgent> omEdsAgentMulti = new ArrayList<>();

    @BeforeEach
    void init() throws Exception {
        collectEdsAgentService = new CollectEdsAgentServiceImpl(edsAgentRepository, secteurAgentMapper);

        ObjectMapper mapper = new ObjectMapper();
        Resource resourceEdsParent = new ClassPathResource("/static/OMEdsAgentParent.json");


        try {
            omEdsAgent = mapper.readValue(resourceEdsParent.getInputStream(), new TypeReference<>() {
            });
        } catch (Exception ignored) {
        }


        Resource resourceEdsFils = new ClassPathResource("/static/OMEdsAgentFils.json");
        try {
            omEdsFils = mapper.readValue(resourceEdsFils.getInputStream(), new TypeReference<>() {
            });
        } catch (Exception ignored) {
        }

        Resource resourceEdsAgentMulti = new ClassPathResource("/static/OMEdsAgent.json");
        try {
            omEdsAgentMulti = mapper.readValue(resourceEdsAgentMulti.getInputStream(), new TypeReference<>() {
            });
        } catch (Exception ignored) {
        }

        when(edsAgentRepository.getByIdEdsIn(any())).thenReturn(omEdsAgent);
        when(edsAgentRepository.getALlByIdEdsParent(any())).thenReturn(omEdsFils);
        when(edsAgentRepository.getAllByIdCaisseRegionale(any())).thenReturn(omEdsAgentMulti);
        when(edsAgentRepository.getIdEdsParentByStructureIdAndAgentId(any(), any())).thenReturn(List.of(omEdsFils.get(0)));
    }


    @Test
    void givenAgentIdAndStructureId_whenSecteurAgent_Ok() {
        // given
        when(edsAgentRepository.getByIdEdsIn(any())).thenReturn(
                omEdsAgent.stream().filter(omEdsAgent1 -> omEdsAgent1.getTypeEds().equals("06")).collect(Collectors.toList()));

        // when
        var result = collectEdsAgentService.getSecteurAgent(structureId, agentId);

        // then

        Assert.assertNotNull(result);
        Assert.assertEquals(4, result.size());
        Assert.assertTrue(result.get(0).getFonctions().get(0).getAgents().stream().anyMatch(agentFonction -> agentFonction.getId().equals(agentId)));
        verify(edsAgentRepository).getALlByIdEdsParent(any());
        verify(edsAgentRepository).getByIdEdsIn(any());
        verify(edsAgentRepository).getIdEdsParentByStructureIdAndAgentId(any(), any());

    }

    @Test
    void given_whenSecteurAgentMultiAndTriAlphaEdsFonction_Ok() {
        // given
        when(edsAgentRepository.getAllByIdCaisseRegionale(any())).thenReturn(List.of(omEdsAgentMulti.get(0),omEdsAgentMulti.get(2)));
        // when
        var result = collectEdsAgentService.getSecteurAgentMulti("12345");

        // then

        Assert.assertNotNull(result);
        Assert.assertEquals(2, result.size());
        Assert.assertEquals(result.get(0).getLibelle(),"DIR. CLIENTELE LYON RIVE DROITE");
        Assert.assertEquals(result.get(0).getFonctions().get(0).getLibelle(),"CONSEILLER PRIVE");
        Assert.assertTrue(result.get(0).getFonctions().get(0).getAgents().stream().anyMatch(agentFonction -> agentFonction.getId().equals("07031")));
        verify(edsAgentRepository).getAllByIdCaisseRegionale(any());
    }

    @Test
    void given_whenSecteurAgentMultiWithNonValidAgent_Ok() {
        // given
        omEdsAgentMulti.get(0).getFonctions().get(0).getAgents().add(OmAgent.builder().matricule("854").idAgent("8780051120").build());
        when(edsAgentRepository.getAllByIdCaisseRegionale(any())).thenReturn(omEdsAgentMulti);
        // when
        var result = collectEdsAgentService.getSecteurAgentMulti("12345");

        // then

        Assert.assertNotNull(result);
        Assert.assertEquals(3, result.size());
        Assert.assertEquals(1,result.get(0).getFonctions().get(0).getAgents().size());
        Assert.assertEquals(1,result.get(1).getFonctions().get(0).getAgents().size());
        verify(edsAgentRepository).getAllByIdCaisseRegionale(any());
    }

    @Test
    void givenAgentIdAndStructureIdNonValidEdsParent_whenGetSecteurAgent_Ok() {
        // given

        when(edsAgentRepository.getByIdEdsIn(any())).thenReturn(new ArrayList<>());

        // when
        var result = collectEdsAgentService.getSecteurAgent(structureId, agentId);

        // then

        Assert.assertNotNull(result);
        Assert.assertEquals(3, result.size());
        verify(edsAgentRepository).getALlByIdEdsParent(any());
        verify(edsAgentRepository).getByIdEdsIn(any());
        verify(edsAgentRepository).getIdEdsParentByStructureIdAndAgentId(any(), any());

    }


    @Test
    void givenEdsParentLevel05_whenGetSecteurAgent_Ok() {
        // given
        when(edsAgentRepository.getIdEdsParentByStructureIdAndAgentId(any(), any())).thenReturn(
                omEdsAgent.stream().filter(omEdsAgent1 -> omEdsAgent1.getTypeEds().equals("05")).collect(Collectors.toList()));

        when(edsAgentRepository.getByIdEdsIn(List.of("878000008605"))).thenReturn(omEdsAgent.stream().filter(omEdsAgent1 -> omEdsAgent1.getTypeEds().equals("05")).collect(Collectors.toList()));

        // when
        var result = collectEdsAgentService.getSecteurAgent(structureId, agentId);

        // then

        Assert.assertNotNull(result);
        Assert.assertEquals(4, result.size());
        verify(edsAgentRepository).getALlByIdEdsParent(any());
        verify(edsAgentRepository).getByIdEdsIn(any());
        verify(edsAgentRepository).getIdEdsParentByStructureIdAndAgentId(any(), any());

    }

    @Test
    void givenAgentIdAndStructureId_whenGetSecteurAgent_NotFound() {
        // given
        when(edsAgentRepository.getIdEdsParentByStructureIdAndAgentId(any(), any())).thenReturn(null);
        // when
        var result = collectEdsAgentService.getSecteurAgent(structureId, agentId);

        // then

        Assert.assertNotNull(result);
        Assert.assertEquals(0, result.size());
    }

    @Test
    void given_whenSecteurAgentMulti_NotFound() {
        // given
        when(edsAgentRepository.getAllByIdCaisseRegionale(any())).thenReturn(null);
        // when
        var result = collectEdsAgentService.getSecteurAgentMulti("123454");

        // then

        Assert.assertNotNull(result);
        Assert.assertEquals(0, result.size());
        verify(edsAgentRepository).getAllByIdCaisseRegionale(any());
    }


    @Test
    void givenAgentIdAndStructureId_whenSecteurAgent_TriAlphaAgent() {
        // given
        when(edsAgentRepository.getByIdEdsIn(any())).thenReturn(
                omEdsAgent.stream().filter(omEdsAgent1 -> omEdsAgent1.getTypeEds().equals("06")).collect(Collectors.toList()));

        // when
        var result = collectEdsAgentService.getSecteurAgent(structureId, agentId);

        // then

        Assert.assertNotNull(result);
        Assert.assertEquals(4, result.size());
        Assert.assertEquals("AMRI", result.get(2).getFonctions().get(0).getAgents().get(0).getNom());
        Assert.assertEquals("JEAN LUC", result.get(2).getFonctions().get(0).getAgents().get(0).getPrenom());
        Assert.assertEquals("THUILLER", result.get(2).getFonctions().get(0).getAgents().get(1).getNom());
        Assert.assertEquals("JEAN LUC", result.get(2).getFonctions().get(0).getAgents().get(1).getPrenom());
        verify(edsAgentRepository).getALlByIdEdsParent(any());
        verify(edsAgentRepository).getByIdEdsIn(any());
        verify(edsAgentRepository).getIdEdsParentByStructureIdAndAgentId(any(), any());

    }

    @Test
    void givenAgentIdAndStructureId_whenSecteurAgent_TriAlphaFonctions() {
        // given
        when(edsAgentRepository.getByIdEdsIn(any())).thenReturn(
                omEdsAgent.stream().filter(omEdsAgent1 -> omEdsAgent1.getTypeEds().equals("06")).collect(Collectors.toList()));

        // when
        var result = collectEdsAgentService.getSecteurAgent(structureId, agentId);

        // then

        Assert.assertNotNull(result);
        Assert.assertEquals(4, result.size());
        Assert.assertEquals("CONSEILLER PRIVE", result.get(2).getFonctions().get(0).getLibelle());
        Assert.assertEquals("DIRECTEUR AGENCE", result.get(2).getFonctions().get(1).getLibelle());
        verify(edsAgentRepository).getALlByIdEdsParent(any());
        verify(edsAgentRepository).getByIdEdsIn(any());
        verify(edsAgentRepository).getIdEdsParentByStructureIdAndAgentId(any(), any());

    }

    @Test
    void givenAgentIdAndStructureId_whenSecteurAgent_TriAlphaEds() {
        // given
        when(edsAgentRepository.getByIdEdsIn(any())).thenReturn(
                omEdsAgent.stream().filter(omEdsAgent1 -> omEdsAgent1.getTypeEds().equals("06")).collect(Collectors.toList()));

        // when
        var result = collectEdsAgentService.getSecteurAgent(structureId, agentId);

        // then

        Assert.assertNotNull(result);
        Assert.assertEquals(4, result.size());
        Assert.assertEquals("CENTRE PATRIMOINE MACON", result.get(1).getLibelle());
        Assert.assertEquals("LYON PATRIMOINE", result.get(2).getLibelle());
        Assert.assertEquals("REGROUPT AGENCES BANQUE PRIVEE", result.get(3).getLibelle());
        verify(edsAgentRepository).getALlByIdEdsParent(any());
        verify(edsAgentRepository).getByIdEdsIn(any());
        verify(edsAgentRepository).getIdEdsParentByStructureIdAndAgentId(any(), any());

    }


    @Test
    void givenIdEds_whenGetAllEmployeeIdByIdEds_Then_Ok() {
        // given
        when(edsAgentRepository.getAllEmployeeIdByIdEds(any())).thenReturn(
                omEdsAgent.stream().filter(omEdsAgent1 -> omEdsAgent1.getIdCaisseRegionale().equals("87800")).collect(Collectors.toList()));

        // when
        var result = collectEdsAgentService.getAllEmployeeIdByIdEds("00436");

        // then

        Assert.assertNotNull(result);
        Assert.assertEquals(2, result.size());
        Assert.assertEquals(List.of("03052","07031"), result);
        verify(edsAgentRepository).getAllEmployeeIdByIdEds(any());

    }

    @Test
    void givenIdEdsEmptyList_whenGetAllEmployeeIdByIdEds_Then_Ok() {
        // given
        when(edsAgentRepository.getAllEmployeeIdByIdEds(any())).thenReturn(new ArrayList<>());

        // when
        var result = collectEdsAgentService.getAllEmployeeIdByIdEds("00436");

        // then

        Assert.assertNotNull(result);
        Assert.assertEquals(0, result.size());

    }


}