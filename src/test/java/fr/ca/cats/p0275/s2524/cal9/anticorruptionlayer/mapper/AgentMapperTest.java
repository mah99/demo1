package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import static fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.utils.MapperUtils.getFirstEmployeeInJson;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AgentMapperImpl.class})
public class AgentMapperTest {

    @InjectMocks
    private AgentMapper mapper = AgentMapperImpl.AGENT_MAPPER;

    @Before
    public void init(){
        AgentMapper agentMapper = Mappers.getMapper(AgentMapper.class);
        ReflectionTestUtils.setField(mapper, "agentMapper", agentMapper);
    }

    private static Employee employee;

    @Test
    public void agentToAgentDistributionEntityTest() {
        fr.ca.cats.p0275.s2524.cal9.model.Agent agent = mapper.employeeToAgent(getFirstEmployeeInJson());
        Assert.assertEquals("00597", agent.getId());
        Assert.assertEquals("MR", agent.getCivilite());
        Assert.assertEquals("DUPONT", agent.getNom());
        Assert.assertEquals("Jean", agent.getPrenom());
    }


}
