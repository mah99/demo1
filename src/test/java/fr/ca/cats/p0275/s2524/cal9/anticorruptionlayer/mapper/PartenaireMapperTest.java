package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Partenaire;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {PartenaireMapperImpl.class})
public class PartenaireMapperTest {

    @InjectMocks
    private PartenaireMapper mapper = PartenaireMapperImpl.PARTENAIRE_MAPPER;

    @Before
    public void init(){
        PartenaireMapper partenaireMapper = Mappers.getMapper(PartenaireMapper.class);
        ReflectionTestUtils.setField(mapper, "partenaireMapper", partenaireMapper);
    }

    @Test
    public void partenaireToPartenairePne09partenaireTest() {
        fr.ca.cats.p0275.s2524.cal9.model.Partenaire partenaire = mapper.partenaireAGC9ToPartenaire(initPartenaire());
        Assert.assertEquals("123456789", partenaire.getId());
    }

    @Test
    public void partenaireAGC09nullpartenaireToPartenaireTest() {
        Partenaire partenaire1 = initPartenaire();
        partenaire1.setId(null);
        partenaire1.setLibelle("test");
        fr.ca.cats.p0275.s2524.cal9.model.Partenaire partenaire = mapper.partenaireAGC09nullIdPartenaireToPartenaire(partenaire1);
        Assert.assertEquals(null, partenaire.getId());
        Assert.assertEquals("test", partenaire.getLibelle());
    }

    @Test
    public void partenaireAGC9VisioToPartenaireTest() {
        Partenaire partenaire1 = initPartenaire();
        partenaire1.setIdPartenaireVisio("12345");
        fr.ca.cats.p0275.s2524.cal9.model.Partenaire partenaire = mapper.partenaireAGC9VisioToPartenaire(partenaire1);
        Assert.assertEquals("12345", partenaire.getId());
    }

    private static Partenaire initPartenaire(){
        Partenaire partenaire = new Partenaire();
        partenaire.setId("123456789");
        return partenaire;
    }

}
