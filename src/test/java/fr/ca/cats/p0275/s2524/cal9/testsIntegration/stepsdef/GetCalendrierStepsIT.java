package fr.ca.cats.p0275.s2524.cal9.testsIntegration.stepsdef;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ca.cats.p0070.s1889.easyrest.logging.service.Logger;
import fr.ca.cats.p0070.s1889.easyrest.logging.service.LoggerService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.BusinessException;
import fr.ca.cats.p0275.s2524.cal9.testsIntegration.utils.Constant;
import fr.ca.cats.p0275.s2524.cal9.testsIntegration.utils.EcrireFichier;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.fr.Alors;
import io.cucumber.java.fr.Quand;
import io.cucumber.java.fr.Soit;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("Mock")
public class GetCalendrierStepsIT extends DefautStepsIT {

    private final String dateDebut;
    private final String dateFin;
    private String agentId;
    private JSONArray reunionsJson;
    private JSONObject reunionCouranteJson;

    private static final String CLE_REUNIONS = "reunions";

    public GetCalendrierStepsIT(RestTemplate restTemplate) {
        super.restTemplate = restTemplate;
        this.dateDebut = "2023-01-01";
        this.dateFin = "2023-01-31";
    }

    @Soit("getCalendrier parametre")
    public void getcalendrierParametre(DataTable donnees) {
        this.agentId = getParamRequete(donnees, Constant.CLE_AGENT);
        setParametrePourMessage("agentID", agentId);
        assertNotNull(donnees);
        assertNotNull(this.agentId);

    }

    @Quand("getCalendrier on envoie la requete {string}")
    public void getCalendrierOnEnvoieLaRequete(String nomEndpoint) throws URISyntaxException, JsonProcessingException {
        String url = String.format("%s/agents/%s?dateDebut=%s&dateFin=%s", getBaseUrl(), agentId, dateDebut, dateFin);
        URI uri = new URI(url);
        affichageTraces(String.format("%s : URL : %s", nomEndpoint, uri));
        HttpEntity<String> entity = new HttpEntity<>(getHeader());
        ObjectMapper mapper = new ObjectMapper();

        try {
            response = restTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    entity,
                    new ParameterizedTypeReference<>() {
                    });

            responseStatus = response.getStatusCodeValue();
            if (responseStatus == 200) {
                if(response.getBody() != null) {
                    responseBody = Objects.requireNonNull(response.getBody().toString());
                }
                responseStatus = response.getStatusCodeValue();
                // Conversion du body de la réponse en JSON String
                String json = super.conversionBodyReponseEnJSONString(response);
                // Conversion du JSON au format String en un objet JSON
                JSONObject calendrierJson = new JSONObject(json);
                if(configuration.afficherTraces()) {
                    ecrireFichier.ecrireFichierJson(calendrierJson, "./target/cucumber-reports/retourGetCalendrier.json");
                }
                // Récupération du tableau des réunions au format JSON
                if(!calendrierJson.isNull(CLE_REUNIONS)) {
                    reunionsJson = calendrierJson.getJSONArray(CLE_REUNIONS);
                }
            } else {
                businessException = mapper.readValue(Objects.requireNonNull(response.getBody()).toString(), BusinessException.class);
            }
        } catch (RestClientResponseException rcrex) {
            reunionCouranteJson = super.getErreurMessage(rcrex);
            // Mise à jour de la valeur du statut de la réponse
            responseStatus = reunionCouranteJson.getInt(Constant.CLE_REPONSE);
            LOG.info("responseErreur : " + reunionCouranteJson.toString());
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }

    @Alors("getCalendrier reponse")
    public void getcalendrierReponse(DataTable donnees) {
        assertNotNull(donnees, "aucune donnée transmise du scénario");
        List<Map<String, String>> mapDonnees = convertDatatableToListMap(donnees);
        for(Map<String, String> mapCourante: mapDonnees) {
            // Test sur le statut
            assertEquals(mapCourante.get(Constant.CLE_REPONSE), String.valueOf(responseStatus), Constant.CLE_REPONSE);
            if(responseStatus == 200) {
                // Récupération de la réunion JSON courante pour l'agent id
                reunionCouranteJson = this.getReunionJsonParAgentId(reunionsJson, agentId);
            }
            recuperationDonneesJSONPourValeurTestee(reunionCouranteJson, mapCourante);
        }
    }

    /**
     * Récupération d'une réunion à partir de l'identifiant de l'agent
     * @param reunionsJson JSON des réunions
     * @param agentId identifiant de l'agent
     * @return Reunion pour l'agent ciblé, null sinon
     */
    private JSONObject getReunionJsonParAgentId(JSONArray reunionsJson, String agentId) {

        if(reunionsJson != null && !reunionsJson.isEmpty()) {
            for(int indexJson=0; indexJson < reunionsJson.length(); indexJson++) {
                JSONObject reunionCourante = reunionsJson.getJSONObject(indexJson);
                // Récupération de la valeur de l'id dans le noeud agent
                String idAgentJSON = reunionCourante.getJSONObject(Constant.CLE_JSON_REUNION_CONTEXTE).getString(Constant.CLE_JSON_AGENT_ID);
                if (agentId.equals(idAgentJSON)) {
                    // Création d'un fichier JSON de la réunion de l'agent ciblé dans target/cucumber-reports
                    if(configuration.afficherTraces()) {
                        try {
                            ecrireFichier.ecrireFichierJson(reunionCourante, "./target/cucumber-reports/GetCalendrier_" + agentId + ".json");
                        } catch (IOException e) {
                            LOG.error(e.getMessage());
                        }
                    }
                    return reunionCourante;
                }
            }
        }
        return null;
    }
}