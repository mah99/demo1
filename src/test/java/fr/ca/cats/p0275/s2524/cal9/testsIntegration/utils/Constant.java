package fr.ca.cats.p0275.s2524.cal9.testsIntegration.utils;

public class Constant {

    public static final String CLE_JSON_REUNION_CONTEXTE = "reunionContexte";
    public static final String CLE_JSON_AGENT_ID = "agentId";
    public static final String CLE_AGENT = "agent";
    public static final String CLE_REPONSE = "reponse";
    public static final String CLE_MESSAGE = "message";
    public static final String CLE_CAISSE_REGIONALE = "CR";

    // Messages
    public static final String MESSAGE_VALEUR_REELLE_NON_TROUVEE = "La valeur dans le JSON est introuvable pour : %s %s";
    public static final String MESSAGE_TEST_EN_ERREUR = "Erreur sur le test pour le titre du scenario : %s %s";
    public static final String MESSAGE_MARQUEUR_SPECIFIQUE_INCONNU = "Le marqueur specifique %s est inconnu";
    public static final String CLE_PARTENAIRE = "partnerId";

}