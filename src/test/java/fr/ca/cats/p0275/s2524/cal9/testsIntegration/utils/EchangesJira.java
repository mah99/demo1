package fr.ca.cats.p0275.s2524.cal9.testsIntegration.utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe gérant la communication avec Jira XRay
 *  - Création du ticket d'exécution des tests
 *  - Rattachement du ticket de plan de test au ticket d'exécution
 *  - Mise à jour du titre et de l'application dans le fichier d'exécution
 *  - Ajout en pièce jointe du ticket d'exécution des tests du rapport au format HTML
 */
public class EchangesJira {

    // Endpoint XRay pour récupérer la liste des tests à partir du plan de test
    private static final String ENDPOINT_XRAY_GET_LIST_TEST_FROM_TEST_PLAN = "/rest/raven/1.0/api/testplan/%s/test";
    // Endpoint XRay pour l'import des fichiers features
    private static final String ENDPOINT_XRAY_GET_FEATURES = "/rest/raven/1.0/export/test?keys=%s";
    // Endpoint XRay pour l'import des fichiers features sous forme d'archive
    private static final String ENDPOINT_XRAY_GET_FEATURES_ZIP = "/rest/raven/1.0/export/test?keys=%s&fz=true";
    // Endpoint XRay pour la création du ticket d'exécution
    private static final String ENDPOINT_XRAY_POST_SEND_REPORT = "/rest/raven/1.0/import/execution/cucumber";
    // Endpoint XRay pour l'ajout du lien du ticket de plan de test au ticket d'exécution des tests
    private static final String ENDPOINT_XRAY_POST_ATTACH_EXEC_RESULT_TICKET_TO_TEST_PLAN = "/rest/raven/1.0/api/testplan/%s/testexecution";
    // Endpoint JIRA pour l'ajout en pièce jointe du rapport Cucumber
    private static final String ENDPOINT_XRAY_POST_ADD_ATTACHMENT_TICKET_RESULT_TEST = "/rest/api/2/issue/%s/attachments";
    // Endpoint Jira pour la mise à jour des libellés du ticket d'exécution des tests
    private static final String ENDPOINT_JIRA_PUT_UPDATE_LABELS_TICKET_RESULT_TEST = "/rest/api/2/issue/%s";
    // Identifiant du ticket d'exécution de test
    private static String idTicketResultatExecution;
    private static String cheminFichierTempJson;
    private static String commandeExecution = "curl.exe";
    private static String token;
    private static String baseUrlJiraXray;
    public static boolean afficherTrace = false;

    /**
     * Initialisation de curl :
     *  - en local, utilisation de curl.exe
     *  - sur Gitlab, utilisation de curl (environnement linux)
     *  Initialisation du token et de l'URL de base pour Jira/XRay
     *  Initialisation de l'affichage des traces du curl
     * @param tokenJira String token d'authentification à Jira/XRay
     * @param baseUrlJira String url de base pour l'accès Jira/Xray
     * @param afficherTraces boolean true si affichage du retour de curl, false sinon
     */
    public void init(String tokenJira, String baseUrlJira, boolean afficherTraces) {
        commandeExecution = "curl";
        token = tokenJira;
        baseUrlJiraXray = baseUrlJira;
        afficherTrace = afficherTraces;
    }

    public String getIdTicketResultatExecution() {
        return idTicketResultatExecution;
    }

    /**
     * Récupération des tickets de test rattachés au plan de test
     * @param clePlanTest String Identifiant du plan de test
     * @return List Liste des identifiants des tickets de test rattachés au plan de test
     */
    public List<String> getTestFromPlanTest(String clePlanTest, String cheminFichierTempJson) {
        String urlEndpoint = baseUrlJiraXray + String.format(ENDPOINT_XRAY_GET_LIST_TEST_FROM_TEST_PLAN, clePlanTest);
        String[] commande = {commandeExecution, "-D-",
                "-X", "GET",
                "-H", "Content-Type: application/json",
                "-H", "Authorization: Bearer " + token,
                urlEndpoint
        };

        final String messSucces = "Récupération des tests depuis le plan de test";
        final String messErreur = "Erreur lors de la récupération des tests depuis le plan de test";
        // Création d'un processus pour l'envoi du rapport JSON à XRay
        List<String> retourCurl = executerCurl(commande, messSucces, messErreur, false);

        // Récupération des clés des tickets de test
        List<String> clesTicketsTest = new ArrayList<>();
        for(String ligne : retourCurl) {
            // Identification du corps de la réponse (tableau JSON)
            if(ligne.startsWith("[")) {
                JSONArray retour = new JSONArray(ligne);
                // Parcours pour récupérer l'identifiant du ticket de test
                for(int index=0; index < retour.length(); index++) {
                    JSONObject elementJSON = retour.getJSONObject(index);
                    if(afficherTrace) {
                        String nomFichierTemp = cheminFichierTempJson + "ticketsTestFromPlanTest.json";
                        try {
                            ecrireFichierJson(retour, null, nomFichierTemp);
                        } catch (IOException ignored) {

                        }

                    }
                    clesTicketsTest.add(elementJSON.getString("key"));
                }
            }
        }
        return clesTicketsTest;
    }

    /**
     * Import des fichiers feature depuis Jira
     * @param clesTestsJira List Liste des clés des tickets de test XRay
     * @param cheminDestination String Chemin du fichier feature
     */
    public void importFichiersFeatures(List<String> clesTestsJira, String cheminDestination) {
        for(String cleTestJira : clesTestsJira) {
            String urlEndpoint = baseUrlJiraXray + String.format(ENDPOINT_XRAY_GET_FEATURES, cleTestJira);
            String cheminFichierFeature = String.format(cheminDestination+"%s.feature", cleTestJira);
            String[] commande = {commandeExecution, "-D-",
                    "-X", "GET",
                    "-H", "Authorization: Bearer " + token,
                    urlEndpoint,
                    "-o", cheminFichierFeature
            };

            final String messSucces = "Import des fichiers feature";
            final String messErreur = "Erreur lors de l'import des fichiers feature";
            // Création d'un processus pour l'import des fichiers feature
            executerCurl(commande, messSucces, messErreur, false);
        }
    }

    /**
     * Import des fichiers feature depuis Jira sous forme d'une archive zip
     * @param clesTestsJira List Liste des clés des tickets de test XRay
     * @param cheminDestination String Chemin du dossier temporaire
     */
    public void importFichiersFeaturesZip(List<String> clesTestsJira, String cheminDestination) {
        String listeClesTestJira = String.join(";", clesTestsJira);
        String urlEndpoint = baseUrlJiraXray + String.format(ENDPOINT_XRAY_GET_FEATURES_ZIP, listeClesTestJira);
        String cheminFichierArchive = String.format(cheminDestination+"%s", "ArchiveFeatures.zip");
        String[] commande = {commandeExecution, "-D-",
                "-X", "GET",
                "-H", "Authorization: Bearer " + token,
                urlEndpoint,
                "-o", cheminFichierArchive
        };

        final String messSucces = "Import de l'archive des fichiers feature";
        final String messErreur = "Erreur lors de l'import de l'archive des fichiers feature";
        // Création d'un processus pour l'import des fichiers feature
        executerCurl(commande, messSucces, messErreur, false);
    }

    /**
     * Envoi du rapport Cucumber au format JSON à Xray
     * @param cheminRapportJson String chemin du fichier contenant les données à envoyer à XRay
     * @param cheminFichierTraceReponse String chemin du fichier JSON contenant la réponse lors de la création du ticket de résultat d'exécution
     */
    public void exportRapportJSONToJIRA(String cheminRapportJson, String cheminFichierTraceReponse) {
        cheminFichierTempJson = cheminFichierTraceReponse;

        String[] commande = {commandeExecution, "-D-", "-X", "POST",
                "-H", "Content-Type: application/json",
                "-H", "Authorization: Bearer " + token,
                "--data", "@" + cheminRapportJson,
                baseUrlJiraXray + ENDPOINT_XRAY_POST_SEND_REPORT
        };

        final String messSucces = "Création du ticket de résultat d'exécution";
        final String messErreur = "Erreur lors de la création du ticket d'exécution";
        // Création d'un processus pour l'envoi du rapport JSON à XRay
        executerCurl(commande, messSucces, messErreur, true);
    }

    /**
     * Ajout du lien vers le ticket de plan de test pour le ticket d'exécution des tests
     * @param ticketExecKey String identifiant du ticket de test
     * @param ticketTestPlan String identifiant du ticket de plan de test
     * @param cheminFichierTemp String chemin du fichier contenant les données à envoyer à XRay
     */
    public void ajoutLienTicketExecToPlanTest(String ticketExecKey, String ticketTestPlan, String cheminFichierTemp) {
        // Génération du json à transmettre
        JSONArray ajout = new JSONArray();
        ajout.put(ticketExecKey);
        JSONObject data = new JSONObject();
        data.put("add", ajout);
        data.put("remove", new JSONArray());

        // Ecriture du fichier json à transmettre au endpoint
        String cheminFichier;
        try {
            cheminFichier = ecrireFichierJson(null, data, cheminFichierTemp);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        String urlEndpoint = baseUrlJiraXray + String.format(ENDPOINT_XRAY_POST_ATTACH_EXEC_RESULT_TICKET_TO_TEST_PLAN, ticketTestPlan);

        String[] commande = {commandeExecution, "-D-", "-X", "POST",
                "-H", "Content-Type: application/json",
                "-H", "Authorization: Bearer " + token,
                "--data", "@" + cheminFichier,
                urlEndpoint
        };

        final String messSucces = "Rattachement du ticket d'exécution au plan de test";
        final String messErreur = "Erreur lors du rattachement du ticket d'exécution au plan de test";
        // Création d'un processus pour l'envoi du rapport JSON à XRay
        executerCurl(commande, messSucces, messErreur, false);
    }

    /**
     * Mise à jour des libellés du ticket d'exécution
     * @param ticketExecKey String identifiant du ticket de résultat d'exécution
     * @param cheminFichierTemp String chemin du fichier contenant les données à envoyer à XRay
     */
    public void majTicketJira(String ticketExecKey, String cheminFichierTemp) {
        // Génération du json à transmettre
        JSONObject application = new JSONObject();
        application.put("name", "CAL9");
        JSONArray tabApplication = new JSONArray();
        tabApplication.put(application);
        JSONObject maj = new JSONObject();
        maj.put("set", tabApplication);
        JSONArray tabComposant = new JSONArray();
        tabComposant.put(maj);

        JSONObject titre = new JSONObject();
        titre.put("set", "Résultat des tests d'intégration CAL9");
        JSONArray tabTitre = new JSONArray();
        tabTitre.put(titre);

        JSONObject composant = new JSONObject();
        composant.put("components", tabComposant);
        composant.put("summary", tabTitre);
        JSONObject data = new JSONObject();
        data.put("update", composant);

        // Ecriture du fichier json à transmettre au endpoint
        try {
            ecrireFichierJson(null, data, cheminFichierTemp);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        String urlEndpoint = baseUrlJiraXray + String.format(ENDPOINT_JIRA_PUT_UPDATE_LABELS_TICKET_RESULT_TEST, ticketExecKey);

        String[] commande = {commandeExecution, "-D-",
                "-X", "PUT",
                "-H", "Authorization: Bearer " + token,
                "-H", "Content-Type: application/json",
                "--data", "@" + cheminFichierTemp,
                urlEndpoint
        };

        final String messSucces = "MAJ des libellés du ticket d'exécution des tests";
        final String messErreur = "Erreur lors de la mise à jour du ticket d'exécution";
        // Création d'un processus pour l'envoi du rapport JSON à XRay
        executerCurl(commande, messSucces, messErreur, false);
    }

    public void exportRapportHTMLToTestExec(String cheminRapportHTML, String ticketExecKey) {
        String urlEndpoint = baseUrlJiraXray + String.format(ENDPOINT_XRAY_POST_ADD_ATTACHMENT_TICKET_RESULT_TEST, ticketExecKey);

        String[] commande = {commandeExecution, "-D-", "-X", "POST",
                "-H", "Authorization: Bearer " + token,
                "-H", "X-Atlassian-Token: no-check",
                "--form", "file=@" + cheminRapportHTML,
                urlEndpoint
        };

        final String messSucces = "Ajout du rapport HTML en pièce jointe du ticket d'exécution";
        final String messErreur = "Erreur lors de l'ajout du rapport HTML en pièce jointe du ticket d'exécution";
        // Création d'un processus pour l'envoi du rapport JSON à XRay
        executerCurl(commande, messSucces, messErreur, false);
    }

    /**
     * Gestion de l'affichage des traces et exécution de la commande curl avec traitement sur la réponse du curl
     * @param commande String[] tableau des commandes à exécuter
     * @param messageSucces String message à afficher en cas de succès
     * @param messageErreur String message à afficher en cas d'erreur
     * @param isCreationTicketExec boolean true, si création du ticket d'exécution de test, false sinon
     */
    private List<String> executerCurl(String[] commande, String messageSucces, String messageErreur, boolean isCreationTicketExec) {

        // Création d'un processus pour l'envoi du rapport JSON à XRay
        ProcessBuilder processusBuilder = new ProcessBuilder(commande);
        Process processus;
        List<String> reponse = new ArrayList<>();

        try {
            processus = processusBuilder.start();
            InputStream is = processus.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String ligne;
            // Parcours des lignes de retour du curl et sauvegarde
            while ((ligne = br.readLine()) != null) {
                reponse.add(ligne);
            }

            try {
                int exitValue = processus.waitFor();

                return retourCommande(reponse, isCreationTicketExec);
            } catch (InterruptedException e) {
                throw new RuntimeException(e.getMessage());
            }
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Traitement du retour du curl avec récupération de l'id du ticket d'exécution dans la réponse pour l'endpoint de création du ticket d'exécution
     * @param reponse String retour du curl
     * @param isCreationTicketExec boolean true, récupération de l'id du ticket d'exécution des tests et écriture d'un fichier de trace, false sinon
     */
    private List<String> retourCommande(List<String> reponse, boolean isCreationTicketExec) {
        for(String ligneReponse: reponse) {
            if(isCreationTicketExec) {
                if(ligneReponse.startsWith("{")) {
                    JSONObject reponseJSON = new JSONObject(ligneReponse);
                    try {
                        ecrireFichierJson(null, reponseJSON, cheminFichierTempJson);
                    } catch (IOException ignored) {
                    }
                    JSONObject ticketExecJSON = reponseJSON.getJSONObject("testExecIssue");
                    idTicketResultatExecution = ticketExecJSON.getString("key");
                }
            }
        }
        return reponse;
    }


    private String ecrireFichierJson(JSONArray tabDonnees, JSONObject donnees, String cheminFichier) throws IOException {
        if(tabDonnees == null && donnees == null) {
            return null;
        }
        // JSONArray à écrire
        if(tabDonnees != null && !tabDonnees.isEmpty()) {
            StringBuilder donneesAEcrire = new StringBuilder("[");
            for(int index=0; index < tabDonnees.length(); index++) {
                donneesAEcrire.append(tabDonnees.getJSONObject(index).toString());
                if(index < tabDonnees.length() - 1) {
                    donneesAEcrire.append(",");
                }
            }
            donneesAEcrire.append("]");
            return ecrireFichier(donneesAEcrire.toString(), cheminFichier);
        } //JSONObject à écrire
        else if(donnees != null && !donnees.isEmpty()) {
            return ecrireFichier(donnees.toString(), cheminFichier);
        }
        return null;
    }

    private String ecrireFichier(String donnees, String cheminFichier) throws IOException {
        FileWriter fichier = new FileWriter(cheminFichier, StandardCharsets.UTF_8);
        fichier.write(donnees);
        fichier.close();
        return cheminFichier;
    }
}
