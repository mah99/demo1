package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.OmEdsAgent;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.Dao.EdsAgentRepositoryDao;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.impl.EdsAgentRepositoryImpl;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class EdsAgentRepositoryTest {

    private EdsAgentRepository edsAgentRepository;

    @Mock
    private EdsAgentRepositoryDao edsAgentRepositoryDao;

    private List<OmEdsAgent> omEdsAgentList;

    @BeforeEach
    public void init() {
        ObjectMapper mapper = new ObjectMapper();
        Resource resourceEdsAgentMulti = new ClassPathResource("/static/OMEdsAgent.json");
        try {
            omEdsAgentList = mapper.readValue(resourceEdsAgentMulti.getInputStream(), new TypeReference<>() {
            });
        } catch (Exception ignored) {
        }
        edsAgentRepository = new EdsAgentRepositoryImpl(edsAgentRepositoryDao);
    }

    @Test
    public void getSecteurAgentFull() {
        // given
        when(edsAgentRepositoryDao.findAllByIdCaisseRegionale(any())).thenReturn(omEdsAgentList);

        // when
        var result = edsAgentRepository.getAllByIdCaisseRegionale("12345");

        // then
        Assert.assertNotNull(result);
        Assert.assertEquals(3, result.size());
        Assert.assertTrue(result.get(0).getFonctions().get(0).getAgents().stream().anyMatch(agentFonction -> agentFonction.getMatricule().equals("03052")));
        verify(edsAgentRepositoryDao).findAllByIdCaisseRegionale(any());
    }

    @Test
    public void getSpecificWithStructureIdAgentFromOmEdsAgent() {
        // given
        when(edsAgentRepositoryDao.findAllByIdCaisseRegionaleAndAgentId(any(), any())).thenReturn(omEdsAgentList);

        // when
        var result = edsAgentRepository.getAllByIdCaisseRegionaleAndAgentId("87800", "40001");

        // then
        Assert.assertNotNull(result);
        Assert.assertEquals(3, result.size());
        verify(edsAgentRepositoryDao).findAllByIdCaisseRegionaleAndAgentId(any(), any());
    }
}
