package fr.ca.cats.p0275.s2524.cal9.domaine.service;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.impl.ReunionServiceImpl;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectReunionService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.MapperUtils.readObjectsFile;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class ReunionServiceTest {

    private ReunionService reunionService;
    @Mock
    private CollectReunionService collectReunionService;
    private List<Reunion> reunions;

    @BeforeEach
    public void init(){
        reunions = readObjectsFile("classpath:static/agc9.json", Reunion.class);
        when(collectReunionService.getReunion()).thenReturn(reunions);
        reunionService = new ReunionServiceImpl(collectReunionService);
    }

    @Test
    public void getReunion(){
        // when
        var result = reunionService.getReunion();
        // then
        Assert.assertEquals(reunions, result);
        verify(collectReunionService).getReunion();
    }
}