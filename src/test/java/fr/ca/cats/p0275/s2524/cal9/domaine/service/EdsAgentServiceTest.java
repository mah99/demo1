package fr.ca.cats.p0275.s2524.cal9.domaine.service;

import fr.ca.cats.p0275.s2524.cal9.domaine.service.exception.EdsAgentBusinessException;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.impl.EdsAgentServiceImpl;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectEdsAgentService;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(SpringExtension.class)
public class EdsAgentServiceTest {

    private EdsAgentService edsAgentService;

    @Mock
    private CollectEdsAgentService collectEdsAgentService;

    private String structureId = "87800";
    private String agentId = "00315";


    @BeforeEach
    public void init() {
        edsAgentService = new EdsAgentServiceImpl(collectEdsAgentService);
    }

    @Test
    public void givenStructureIdAndAgentId_whenGetSecteur_then_Ok() {
        // given
        when(collectEdsAgentService.getSecteurAgent(any(), any())).thenReturn(new ArrayList<>());

        // when

        var result = edsAgentService.getSecteurAgent(structureId, agentId);

        // then
        verify(collectEdsAgentService).getSecteurAgent(any(), any());

        Assert.assertTrue(result.isEmpty());
    }
    @Test
    public void given_whenGetSecteurAgentMulti_then_Ok() {
        // given
        when(collectEdsAgentService.getSecteurAgentMulti(any())).thenReturn(new ArrayList<>());

        // when

        var result = edsAgentService.getSecteurAgentMulti("12345");

        // then
        verify(collectEdsAgentService).getSecteurAgentMulti(any());
        Assert.assertTrue(result.isEmpty());
    }

    @Test
    public void given_whenGetSecteurAgentMultiAndResultNull_then_Exception() {
        // given
        when(collectEdsAgentService.getSecteurAgentMulti(any())).thenReturn(null);

        // when
        Assertions.assertThrows(EdsAgentBusinessException.class, () -> {
            edsAgentService.getSecteurAgentMulti("12345");
        });
        // then
    }

    @Test
    public void givenIdEds_whenGetAllEMployeeIdEds_then_Ok() {
        // given
        when(collectEdsAgentService.getAllEmployeeIdByIdEds(any())).thenReturn(List.of("40002"));

        // when

        var result = edsAgentService.getAllEmployeeIdByIdEds("000463");

        // then
        verify(collectEdsAgentService).getAllEmployeeIdByIdEds(any());
        Assert.assertEquals("40002", result.get(0));
    }


}