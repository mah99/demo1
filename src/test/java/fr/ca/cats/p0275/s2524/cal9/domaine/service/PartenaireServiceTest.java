package fr.ca.cats.p0275.s2524.cal9.domaine.service;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.PartenaireMapper;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.impl.PartenaireServiceImpl;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.CallInterApiTechnicalException;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectPartenaireService;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.utils.MapperUtils.getFirstPartenaireInJson;
import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.MapperUtils.readObjectFile;
import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.MapperUtils.readObjectsFile;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class PartenaireServiceTest {

    private PartenaireService partenaireService;
    @Mock
    private CollectPartenaireService collectPartenaireService;
    @Spy
    private PartenaireMapper partenaireMapper;

    private Partenaire partenaire;
    private List<Reunion> reunions;

    @BeforeEach
    public void init(){
        reunions = readObjectsFile("classpath:static/agc9.json", Reunion.class);
        partenaire = getFirstPartenaireInJson();
        when(collectPartenaireService.getPartenaire(any(), anyString())).thenReturn(partenaire);
        partenaireMapper = Mappers.getMapper(PartenaireMapper.class);
        partenaireService = new PartenaireServiceImpl(collectPartenaireService, partenaireMapper);
    }

    @Test
    public void givenValidInformationAndLibelleSuiteNonRemplit_ThenOK() {
        //given
        partenaire.setIntituleSuite("");
        // when
        List<fr.ca.cats.p0275.s2524.cal9.model.Partenaire> result = partenaireService.getPartenaires(reunions.get(0), "87800");
        //then
        Assert.assertNotNull(result);
        Assert.assertEquals("M.       PLOBUAM YOMMUSQ", result.get(0).getLibelle());
    }
    @Test
    public void givenValidInformationVisioAndLibelleSuiteNonRemplit_ThenOK() {
        //given
        partenaire.setIntituleSuite("");
        // when
        List<fr.ca.cats.p0275.s2524.cal9.model.Partenaire> result = partenaireService.getPartenairesVizio(reunions.get(0), "87800");
        //then
        Assert.assertNotNull(result);
        Assert.assertEquals("M.       PLOBUAM YOMMUSQ", result.get(0).getLibelle());
    }

    @Test
    public void givenValidInformationAndLibelleSuiteRemplit_ThenOK() {
        // when
        List<fr.ca.cats.p0275.s2524.cal9.model.Partenaire> result = partenaireService.getPartenaires(reunions.get(0), "87800");
        //then
        Assert.assertNotNull(result);
        Assert.assertEquals("M.       PLOBUAM YOMMUSQTEST", result.get(0).getLibelle());
    }

    @Test
    public void givenValidInformationVisioAndLibelleSuiteRemplit_ThenOK() {
        // when
        List<fr.ca.cats.p0275.s2524.cal9.model.Partenaire> result = partenaireService.getPartenaires(reunions.get(0), "87800");
        //then
        Assert.assertNotNull(result);
        Assert.assertEquals("M.       PLOBUAM YOMMUSQTEST", result.get(0).getLibelle());
    }

    @Test
    public void givenValidInformationAndIdPartNull_ThenOK() {
        // when
        List<fr.ca.cats.p0275.s2524.cal9.model.Partenaire> result = partenaireService.getPartenaires(reunions.get(1), "87800");
        //then
        Assert.assertNotNull(result);
        Assert.assertNull(result.get(0).getId());
        Assert.assertEquals("Mme Joel EUVRARD", result.get(0).getLibelle());
    }

    @Test
    public void givenValidInformationVisioAndIdPartNull_ThenOK() {
        // when
        List<fr.ca.cats.p0275.s2524.cal9.model.Partenaire> result = partenaireService.getPartenairesVizio(reunions.get(1), "87800");
        //then
        Assert.assertTrue(result.isEmpty());
    }

    @Test
    public void givenValidInformationAndPNE09Error_ThenBusinessException(){
        //given
        when(collectPartenaireService.getPartenaire(any(), anyString())).thenThrow(new CallInterApiTechnicalException("Error"));
        // when
        var result = Assertions.assertThrows(CallInterApiTechnicalException.class, () -> {
            partenaireService.getPartenaires(reunions.get(0), "87800");
        });
    }

    @Test
    public void givenValidInformationVisioAndPNE09Error_ThenBusinessException(){
        //given
        when(collectPartenaireService.getPartenaire(any(), anyString())).thenThrow(new CallInterApiTechnicalException("Error"));
        // when
        var result = Assertions.assertThrows(CallInterApiTechnicalException.class, () -> {
            partenaireService.getPartenairesVizio(reunions.get(0), "87800");
        });
    }
}