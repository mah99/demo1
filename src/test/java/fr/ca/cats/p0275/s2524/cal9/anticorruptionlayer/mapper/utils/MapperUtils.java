package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Portfolio;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.AgentDao;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.PartenaireDao;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.List;

public class MapperUtils {


    public static Employee getFirstEmployeeInJson(){
        ObjectMapper mapper = new ObjectMapper();
        Resource resource = new ClassPathResource("static/sdg9EmployeeNumber.json");

        Object obj = null;
        try {
            obj = mapper.readValue(resource.getInputStream(), Object.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<Employee> employeeList = new ObjectMapper().convertValue(obj, new TypeReference<>() {
        });
        return employeeList.stream().findFirst().orElse(null);

    }

    public static Portfolio getFirstPortfolioInJson(){
        ObjectMapper mapper = new ObjectMapper();
        Resource resource = new ClassPathResource("static/sdg9Portfolio.json");

        Object obj = null;
        try {
            obj = mapper.readValue(resource.getInputStream(), Object.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<Portfolio> portfolios = new ObjectMapper().convertValue(obj, new TypeReference<>() {
        });
        return portfolios.stream().findFirst().orElse(null);

    }

    public static AgentDao getFirstAgentDaoInJson(){
        ObjectMapper mapper = new ObjectMapper();
        Resource resource = new ClassPathResource("static/sdg9EmployeeNumber.json");

        Object obj = null;
        try {
            obj = mapper.readValue(resource.getInputStream(), Object.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<AgentDao> agentDaos = new ObjectMapper().convertValue(obj, new TypeReference<>() {
        });
        return agentDaos.stream().findFirst().orElse(null);

    }

    public static Partenaire getFirstPartenaireInJson(){
        ObjectMapper mapper = new ObjectMapper();
        Resource resource = new ClassPathResource("static/Partenaire.json");

        Object obj = null;
        try {
            obj = mapper.readValue(resource.getInputStream(), Object.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<Partenaire> partenaires = new ObjectMapper().convertValue(obj, new TypeReference<>() {
        });
        return partenaires.stream().findFirst().orElse(null);

    }


    public static PartenaireDao getFirstPartenaireDaoInJson(){
        ObjectMapper mapper = new ObjectMapper();
        Resource resource = new ClassPathResource("static/Partenaire.json");

        Object obj = null;
        try {
            obj = mapper.readValue(resource.getInputStream(), Object.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<PartenaireDao> partenaires = new ObjectMapper().convertValue(obj, new TypeReference<>() {
        });
        return partenaires.stream().findFirst().orElse(null);

    }



}
