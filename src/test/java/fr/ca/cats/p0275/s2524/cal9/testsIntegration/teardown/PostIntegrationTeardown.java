package fr.ca.cats.p0275.s2524.cal9.testsIntegration.teardown;

import fr.ca.cats.p0070.s1889.easyrest.logging.service.Logger;
import fr.ca.cats.p0070.s1889.easyrest.logging.service.LoggerService;
import org.springframework.context.annotation.ComponentScan;

/**
 * Classe pour la phase de post-test-integration
 */
@ComponentScan("fr.ca.cats.p0275.s2524.cal9.testsIntegration")
public class PostIntegrationTeardown {

    private static final Logger LOG = LoggerService.getLogger(PostIntegrationTeardown.class);

    public static void main(String args[]) {
        LOG.info("Teardown");
    }
}
