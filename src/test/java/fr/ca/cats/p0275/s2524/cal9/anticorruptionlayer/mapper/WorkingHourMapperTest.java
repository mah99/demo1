package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.Horaire;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {WorkingHourMapperImpl.class})
public class WorkingHourMapperTest {

    @InjectMocks
    private WorkingHourMapper mapper = WorkingHourMapper.WORKING_HOUR_MAPPER;

    @Test
    public void horaireToWorkingHour() {
        // given
        Horaire horaire= Horaire.builder()
                .jour("LUNDI")
                .heureDebut("12:00:00")
                .heureFin("22:00:00")
                .build();
        // when
        var result = mapper.horaireToWorkingHour(horaire);
        // then
        Assert.assertEquals(horaire.getJour(), result.getDay());
        Assert.assertEquals(horaire.getHeureDebut(), result.getStartingHour());
        Assert.assertEquals(horaire.getHeureFin(), result.getEndingHour());

    }
}