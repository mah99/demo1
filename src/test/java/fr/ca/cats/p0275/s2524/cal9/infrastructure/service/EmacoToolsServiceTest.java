package fr.ca.cats.p0275.s2524.cal9.infrastructure.service;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.EmacoRepository;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.impl.EmacoToolsServiceImpl;
import fr.ca.cats.p0275.s2524.cal9.model.PartenaireEmaco;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.Mockito.verify;


@ExtendWith(SpringExtension.class)
public class EmacoToolsServiceTest {


    private EmacoToolsService emacoToolsService;

    private String idPartenaire = "00000302864911";

    @Mock
    private EmacoRepository emacoRepository;

    @BeforeEach
    public void init() {
        emacoToolsService = new EmacoToolsServiceImpl(emacoRepository);

    }


    @Test
    public void givenPartenaireId_whenCallGetIdpnByPartnerId_thenReturnId() {
        // given
        PartenaireEmaco partenaireIdpn = new PartenaireEmaco();
        partenaireIdpn.setPartenaireEmacoId("05289704");
        Mockito.when(emacoRepository.getIdPartenaireEmacoByPartnerId(idPartenaire)).thenReturn(partenaireIdpn);

        // when
        var result = emacoToolsService.getIdPartenaireEmacoByPartnerId(idPartenaire);

        // then

        verify(emacoRepository).getIdPartenaireEmacoByPartnerId(idPartenaire);
        Assert.assertEquals(partenaireIdpn, result);
    }


}