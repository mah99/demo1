package fr.ca.cats.p0275.s2524.cal9.testsIntegration;

import fr.ca.cats.p0070.s1889.easyrest.logging.service.Logger;
import fr.ca.cats.p0070.s1889.easyrest.logging.service.LoggerService;
import fr.ca.cats.p0275.s2524.cal9.Cal9Application;
import fr.ca.cats.p0275.s2524.cal9.testsIntegration.configuration.CucumberConfig;
import fr.ca.cats.p0275.s2524.cal9.testsIntegration.utils.EchangesJira;
import fr.ca.cats.p0275.s2524.cal9.testsIntegration.utils.LectureConfiguration;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.spring.CucumberContextConfiguration;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;


@RunWith(Cucumber.class)
@ActiveProfiles("Mock")
@CucumberContextConfiguration
@CucumberOptions(
        features = "src/test/resources/features",
        glue={"fr.ca.cats.p0275.s2524.cal9.testsIntegration"},
        plugin = { "pretty",
                "json:target/cucumber-reports/CucumberReport.json",
                "html:target/cucumber-reports/CucumberReport.html"},
        monochrome = true
)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes ={Cal9Application.class, CucumberConfig.class})
public class RunCucumberIT {

    private static final Logger LOG = LoggerService.getLogger(RunCucumberIT.class);

    private static LectureConfiguration configuration;

    private static EchangesJira echangesJira;

    /**
     * Initialisation : chargement de la configuration
     * EXECUTION AVANT LES TESTS
     */
    @BeforeClass
    public static void initialisation() {
        LOG.info("Chargement des données du fichier de configuration");
        configuration = new LectureConfiguration();
        echangesJira = new EchangesJira();
        affichageTraces("Valeur du ticket de plan de test : " + configuration.getIdTicketPlanTest());
        // Initialisation
        echangesJira.init(configuration.getJiraToken(), configuration.getBaseURLJira(), configuration.afficherTraces());
    }

    /**
     * Traitement du résultat des tests : envoi à Jira/Xray
     * EXECUTION APRES LES TESTS
     */
    @AfterClass
    public static void envoiRapport() {
        if(configuration.exporterRapportsToJira()) {
            LOG.info("Fin des tests cucumber. Envoi des rapports");
            // Création du ticket de résultat d'exécution
            affichageTraces("Création du ticket de résultat d'exécution");
            String cheminFichierJsonTempReponse = configuration.getCheminDossierTempJson() + "TmpReponseCreationExecRes.json";
            echangesJira.exportRapportJSONToJIRA(configuration.getCheminRapportJSON(), cheminFichierJsonTempReponse);
            String idTicketExec = echangesJira.getIdTicketResultatExecution();
            // URL du ticket d'exécution créé
            LOG.info("URL du ticket d'exécution : " + configuration.getBaseURLJira() + "/browse/" + idTicketExec);

            // Rattachement du ticket de résultat d'exécution au ticket de plan de test
            affichageTraces(String.format("Rattachement du ticket de résultat d'exécution %s au ticket de plan de test %s", idTicketExec, configuration.getIdTicketPlanTest()));
            String cheminFichierTempJson = configuration.getCheminDossierTempJson() + configuration.getNomFichierJSONLienExecResultTestPlan();
            echangesJira.ajoutLienTicketExecToPlanTest(idTicketExec, configuration.getIdTicketPlanTest(), cheminFichierTempJson);

            // Modification du ticket de plan d'exécution pour mettre à jour les informations
            affichageTraces("Modification du ticket de plan d'exécution pour mettre à jour les informations");
            cheminFichierTempJson = configuration.getCheminDossierTempJson() + configuration.getNomFichierJSONMAJExecResult();
            echangesJira.majTicketJira(idTicketExec, cheminFichierTempJson);

            // Ajout du rapport au format HTML au ticket de test d'exécution
            affichageTraces("Ajout du rapport au format HTML au ticket de test d'exécution");
            echangesJira.exportRapportHTMLToTestExec(configuration.getCheminRapportHTML(), idTicketExec);
            LOG.info("Fin de l'envoi des rapports");
        } else {
            LOG.info("Le rapport HTML est disponible ici : " + configuration.getCheminRapportHTML());
        }
    }

    private static void affichageTraces(String message) {
        if(configuration.afficherTraces()) {
            LOG.info(message);
        }
    }
}
