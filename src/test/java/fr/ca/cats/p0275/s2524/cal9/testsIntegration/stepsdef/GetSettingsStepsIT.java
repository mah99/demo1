package fr.ca.cats.p0275.s2524.cal9.testsIntegration.stepsdef;

import com.fasterxml.jackson.core.JsonProcessingException;
import fr.ca.cats.p0070.s1889.easyrest.logging.service.Logger;
import fr.ca.cats.p0070.s1889.easyrest.logging.service.LoggerService;
import fr.ca.cats.p0275.s2524.cal9.testsIntegration.utils.Constant;
import fr.ca.cats.p0275.s2524.cal9.testsIntegration.utils.EcrireFichier;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.fr.Alors;
import io.cucumber.java.fr.Quand;
import io.cucumber.java.fr.Soit;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class GetSettingsStepsIT extends DefautStepsIT {
    private String caisseRegionale;
    private JSONArray settingsJson;
    private JSONObject settingJsonCourant;
    private static final String CLE_CODE_ACTIVITE = "codeActivite";

    @Autowired
    public EcrireFichier ecrireFichier;

    public GetSettingsStepsIT(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Soit("getSettings parametre")
    public void getSettingsParametre(DataTable donnees) {
        this.caisseRegionale = getParamRequete(donnees, Constant.CLE_CAISSE_REGIONALE);
        setParametrePourMessage("CR", this.caisseRegionale);
        assertNotNull(this.caisseRegionale);
    }
    @Quand("getSettings on envoie la requete {string}")
    public void getSettingsOnEnvoieLaRequete(String nomEndpoint) throws URISyntaxException, JsonProcessingException {
        URI uri = new URI(getBaseUrl() + "/settings/" + caisseRegionale);
        affichageTraces(String.format("%s : URL : %s", nomEndpoint, uri));
        HttpEntity<String> entity = new HttpEntity<>(super.getHeader());

        try {
            response = restTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    entity,
                    new ParameterizedTypeReference<>() {
                    });


            if (response.getStatusCodeValue() == 200) {
                if(response.getBody() != null) {
                    responseBody = Objects.requireNonNull(response.getBody().toString());
                }
                responseStatus = response.getStatusCodeValue();
                // Conversion du body de la réponse en JSON String
                String json = super.conversionBodyReponseEnJSONString(response);
                // Conversion du JSON au format String en un objet JSON
                this.settingsJson = new JSONArray(json);
                if(configuration.afficherTraces()) {
                    ecrireFichier.ecrireFichierJson(this.settingsJson, "./target/cucumber-reports/retourGetSettings.json");
                }
            }
        } catch (RestClientResponseException rcrex) {
            settingJsonCourant = super.getErreurMessage(rcrex);
            // Mise à jour de la valeur du statut de la réponse
            responseStatus = settingJsonCourant.getInt(Constant.CLE_REPONSE);
            LOG.info("responseErreur : " + settingJsonCourant.toString());
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }
    @Alors("getSettings reponse")
    public void getSettingsReponse(DataTable donnees) {
        assertNotNull(donnees, "aucune donnée transmise du scénario");
        List<Map<String, String>> mapDonnees = convertDatatableToListMap(donnees);
        for(Map<String, String> mapCourante: mapDonnees) {
            // Test sur le statut
            assertEquals(mapCourante.get(Constant.CLE_REPONSE), String.valueOf(responseStatus));
            if(responseStatus == 200) {
                // Récupération du settings en fonction du code activité
                settingJsonCourant = this.getSettingParCodeActivite(settingsJson, mapCourante.get(CLE_CODE_ACTIVITE));
            }
            recuperationDonneesJSONPourValeurTestee(settingJsonCourant, mapCourante);
        }
    }

    /**
     * Récupère les settings pour le code de l'activié passé en paramètre
     * @param settingsJson JSON contenant les settings
     * @param codeActivite code de l'activité recherchée
     * @return JSON pour le settings de l'activité recherchée, null sinon
     */
    private JSONObject getSettingParCodeActivite(JSONArray settingsJson, String codeActivite) {
        if (settingsJson != null && !settingsJson.isEmpty()) {
            for(int indexSettings = 0; indexSettings < settingsJson.length(); indexSettings++) {
                JSONObject settingJsonCourant = settingsJson.getJSONObject(indexSettings);
                if(codeActivite.equals(settingJsonCourant.get(CLE_CODE_ACTIVITE))) {
                    return settingJsonCourant;
                }
            }
        }
        return null;
    }
}
