package fr.ca.cats.p0275.s2524.cal9.application;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.ErrorMessage;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.bind.MissingServletRequestParameterException;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ActiveProfiles("Mock")
@SpringBootTest()
@AutoConfigureMockMvc
@TestPropertySource(properties = {"management.endpoints.web.exposure.include=health,info"})
public class VueAgendaApiImplTest {

    public final String MISSING_PARAM_EXCEPTION = "Header ou paramètre invalide.";
    @Autowired
    private MockMvc mockMvc;

    @Test
    void whenValidUrlAndMethodAndContentType_thenReturns200() throws Exception {

        mockMvc.perform(get("/vueAgenda")
                        .queryParam("partnerId", "00000306172346")
                        .header("structureId", "87800")
                        .header("correlationId", "12345")
                        .contentType("application/json"))
                .andExpect(status().isOk());
    }


    @Test
    void whenValidUrlAndMethodAndContentType_nonMissingPartenaireId_thenReturnsException() throws Exception {

        MvcResult mvcResult = mockMvc.perform(get("/vueAgenda")
                        .header("structureId", "87800")
                        .header("correlationId", "12345")
                        .contentType("application/json"))
                .andExpect(result ->
                        assertTrue(result.getResolvedException() instanceof MissingServletRequestParameterException))
                .andExpect(result ->
                        Assert.assertEquals("Required request parameter 'partnerId' for method parameter type String is not present", result.getResolvedException().getMessage())).andReturn();
        mvcResult.getResponse().setDefaultCharacterEncoding("UTF8");
        String contentAsString = mvcResult.getResponse().getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        ErrorMessage errorResult = objectMapper.readValue(contentAsString, ErrorMessage.class);
        Assertions.assertEquals(errorResult.getMessage(), MISSING_PARAM_EXCEPTION);
    }


}