package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.EmacoMapper;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.PartenaireEmacoDao;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.Dao.EmacoRepositoryDao;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.impl.EmacoRepositoryImpl;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class EmacoRepositoryTest {

    @Spy
    EmacoMapper emacoMapper = Mappers.getMapper(EmacoMapper.class);
    private EmacoRepository emacoRepository;
    @Mock
    private EmacoRepositoryDao emacoRepositoryDao;
    private String idPartenaire = "00000302864911";

    @BeforeEach
    public void init() {
        emacoRepository = new EmacoRepositoryImpl(emacoRepositoryDao, emacoMapper);

    }


    @Test
    public void getIdpnByPartnerId() {
        // given
        PartenaireEmacoDao partenaireEmacoDao = PartenaireEmacoDao.builder()
                .partenaireEmacoId("02543")
                .build();

        when(emacoRepositoryDao.getIdPartenaireEmacoByPartnerId(idPartenaire)).thenReturn(partenaireEmacoDao);


        // when

        var result = emacoRepository.getIdPartenaireEmacoByPartnerId(idPartenaire);

        // then
        verify(emacoMapper).partenaireEmacoDaoToPartenaireEmaco(partenaireEmacoDao);
        verify(emacoRepositoryDao).getIdPartenaireEmacoByPartnerId(idPartenaire);
        Assert.assertEquals(partenaireEmacoDao.getPartenaireEmacoId(), result.getPartenaireEmacoId());


    }
}