package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;
import fr.ca.cats.p0275.s2524.cal9.model.LocalisationAgenda;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import static fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.utils.MapperUtils.getFirstEmployeeInJson;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {LocalisationAgendaMapperImpl.class})
public class LocalisationAgendaMapperTest {

    @InjectMocks
    private LocalisationAgendaMapper mapper = LocalisationAgendaMapper.LOCALISATION_AGENDA_MAPPER;

    @Before
    public void init(){
        LocalisationAgendaMapper localisationAgendaMapper = Mappers.getMapper(LocalisationAgendaMapper.class);
        ReflectionTestUtils.setField(mapper, "localisationAgendaMapper", localisationAgendaMapper);
    }

    private static Employee employee;


    @Test
    public void reunionAGC9ToReunionTest() {
        LocalisationAgenda localisationAgenda = mapper.informationSDG9ToLocalisationAgenda(getFirstEmployeeInJson().getDistributionEntities().get(0), getFirstEmployeeInJson().getDistributionEntities().get(0).getEmployeeFunctions().get(0).getWorkingHours().get(0));
        Assert.assertEquals("506", localisationAgenda.getAgenceId());
        Assert.assertEquals("1124", localisationAgenda.getPosteFonctionnel());
        Assert.assertEquals("PARIS HALLES", localisationAgenda.getLibelle());
        Assert.assertEquals("07:00:00", localisationAgenda.getHeureDebut());
        Assert.assertEquals("12:00:00", localisationAgenda.getHeureFin());
        Assert.assertEquals("LUNDI", localisationAgenda.getJour());
    }



}
