package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.PartenaireEmacoDao;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AgentMapperImpl.class})
public class EmacoMapperTest {

    @InjectMocks
    private EmacoMapper mapper = EmacoMapper.EMACO_MAPPER;

    @Test
    public void partenaireIdpnDaoToPartenaireIdp() {
        // given
        PartenaireEmacoDao partenaireEmacoDao = PartenaireEmacoDao.builder()
                .partenaireEmacoId("0251")
                .build();


        // when
        var result = mapper.partenaireEmacoDaoToPartenaireEmaco(partenaireEmacoDao);

        // then

        Assert.assertEquals(partenaireEmacoDao.getPartenaireEmacoId(), result.getPartenaireEmacoId());

    }
}