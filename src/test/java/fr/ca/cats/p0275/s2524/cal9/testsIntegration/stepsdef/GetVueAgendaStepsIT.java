package fr.ca.cats.p0275.s2524.cal9.testsIntegration.stepsdef;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ca.cats.p0070.s1889.easyrest.logging.service.Logger;
import fr.ca.cats.p0070.s1889.easyrest.logging.service.LoggerService;
import fr.ca.cats.p0275.s2524.cal9.testsIntegration.utils.Constant;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.fr.Alors;
import io.cucumber.java.fr.Quand;
import io.cucumber.java.fr.Soit;
import org.json.JSONObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class GetVueAgendaStepsIT extends DefautStepsIT {

    private String partnerId;
    private ResponseEntity<?> response;
    private JSONObject responseJSON;

    private String responseBody;
    private int responseStatus;

    private static final Logger LOG = LoggerService.getLogger(GetVueAgendaStepsIT.class);
    private final RestTemplate restTemplate;


    public GetVueAgendaStepsIT(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;

    }

    @Soit("getvueAgenda parametre")
    public void getVueAgendaParametre(DataTable donnees) {
        this.partnerId = getParamRequete(donnees, Constant.CLE_PARTENAIRE);
        assertNotNull(this.partnerId);
    }

    @Quand("getvueAgenda on envoie la requete {string}")
    public void getVueAgendaOnEnvoieLaRequete(String string) throws URISyntaxException, JsonProcessingException {
        URI uri = new URI(getBaseUrl() + "/vueAgenda?partnerId=" + partnerId);
        LOG.info(uri.toString());
        HttpHeaders headers = super.getHeader();
        headers.remove("structureId");
        headers.add("structureId", "87800");
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ObjectMapper mapper = new ObjectMapper();
        try {
            response = restTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    entity,
                    new ParameterizedTypeReference<>() {
                    });

            if (response.getStatusCodeValue() == 200) {
                if (response.getBody() != null) {
                    responseBody = Objects.requireNonNull(response.getBody().toString());
                }
                responseStatus = response.getStatusCodeValue();
                // Conversion du body de la réponse en JSON String
                String json = super.conversionBodyReponseEnJSONString(response);
                // Conversion du JSON au format String en un objet JSON
                responseJSON = new JSONObject(json);
            }
        } catch (RestClientResponseException rcrex) {
            responseJSON = super.getErreurMessage(rcrex);
            // Mise à jour de la valeur du statut de la réponse
            responseStatus = responseJSON.getInt(Constant.CLE_REPONSE);
            LOG.info("responseErreur : " + responseJSON.toString());
        } catch (Exception e) {
            LOG.error(e.getMessage());
        }
    }

    @Alors("getvueAgenda reponse")
    public void getVueAgendaReponse(DataTable donnees) {
        List<Map<String, String>> mapDonnees = convertDatatableToListMap(donnees);
        assertNotNull(mapDonnees);
        for (Map<String, String> mapCourante : mapDonnees) {
            // Test sur le statut
            assertEquals(mapCourante.get(Constant.CLE_REPONSE), String.valueOf(responseStatus), Constant.CLE_REPONSE);
            // Test sur les autres champs
            recuperationDonneesJSONPourValeurTestee(responseJSON, mapCourante);
        }
    }

    /**
     * Récupération de toutes les valeurs à comparer dans le JSON à partir de la map des valeurs souhaitées
     *
     * @param donneesCourante JSONObject objet JSON de la réunion pour l'agent id ciblé
     * @param mapCourante     Map<String, String> map des clés valeurs des données attendues
     */
    public void recuperationDonneesJSONPourValeurTestee(JSONObject donneesCourante, Map<String, String> mapCourante) {
        // Récupération des clés de la map et suppression de la clé reponse déjà testée
        Set<String> setClesMap = mapCourante.keySet();
        List<String> listeClesMap = new ArrayList<>(setClesMap);
        listeClesMap.remove(Constant.CLE_REPONSE);

        // Parcours des clés
        for (String cleCouranteMap : listeClesMap) {
            String valeurATester = mapCourante.get(cleCouranteMap);
            getValeurJSONFromCleMap(donneesCourante, cleCouranteMap, valeurATester);
        }
    }

    /**
     * Récupération de la valeur à comparer dans le JSON de retour de l'appel de l'API
     *
     * @param donneesCourante JSONObject objet JSON de la réunion pour l'agent id ciblé
     * @param cleCouranteMap  String noeud ou sous-noeud à comparer
     * @param valeurAttendue  String valeur attendu provenant du scénario
     */
    private void getValeurJSONFromCleMap(JSONObject donneesCourante, String cleCouranteMap, String valeurAttendue) {
        Object valeurJSONObj = getValeurATesterDansJSON(cleCouranteMap, donneesCourante);
        affichageTraces(String.format("Clé du scénario : %s : Valeur attendue : %s  | Valeur réelle : %s", cleCouranteMap, valeurAttendue, valeurJSONObj));
        super.testValeurAttendueValeurJson(valeurAttendue, valeurJSONObj, cleCouranteMap);
    }
}
