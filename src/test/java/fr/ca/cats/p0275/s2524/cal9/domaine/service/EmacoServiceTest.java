package fr.ca.cats.p0275.s2524.cal9.domaine.service;

import fr.ca.cats.p0275.s2524.cal9.domaine.service.exception.EmacoToolsBusinessException;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.impl.EmacoServiceImpl;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.EmacoToolsService;
import fr.ca.cats.p0275.s2524.cal9.model.PartenaireEmaco;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.Mockito.verify;


@ExtendWith(SpringExtension.class)
public class EmacoServiceTest {


    private EmacoServiceImpl emacoService;
    private String idPartenaire = "00000302864911";

    @Mock
    private EmacoToolsService emacoToolsService;

    @BeforeEach
    public void init() {
        emacoService = new EmacoServiceImpl(emacoToolsService);
    }

    @Test
    public void givenPartenaireId_whenCallGetIdpnByPartnerId_thenReturnId() {
        // given
        PartenaireEmaco partenaireIdpn = new PartenaireEmaco();
        partenaireIdpn.setPartenaireEmacoId("05289704");
        Mockito.when(emacoToolsService.getIdPartenaireEmacoByPartnerId(idPartenaire)).thenReturn(partenaireIdpn);

        // when
        var result = emacoService.getIdPartenaireEmacoByPartnerId(idPartenaire);

        // then

        verify(emacoToolsService).getIdPartenaireEmacoByPartnerId(idPartenaire);
        Assert.assertEquals(partenaireIdpn, result);


    }

    @Test
    public void givenPartenaireId_whenCallGetIdpnByPartnerIdKo_thenException() {
        // given
        Mockito.when(emacoToolsService.getIdPartenaireEmacoByPartnerId(idPartenaire)).thenReturn(null);

        // when
        Assertions.assertThrows(EmacoToolsBusinessException.class, () -> {
            emacoService.getIdPartenaireEmacoByPartnerId(idPartenaire);
        });
        // then
    }


    @Test
    public void givenPartenaireIdNonLength_whenCallGetIdpnByPartnerId_thenException() {

        // when
        Assertions.assertThrows(EmacoToolsBusinessException.class, () -> {
            emacoService.getIdPartenaireEmacoByPartnerId("84545");
        });
        // then
        // exception

    }


    @Test
    public void givenPartenaireIdNull_whenCallGetIdpnByPartnerId_thenException() {

        // when
        Assertions.assertThrows(EmacoToolsBusinessException.class, () -> {
            emacoService.getIdPartenaireEmacoByPartnerId(null);
        });
        // then
        // exception

    }


    @Test
    public void givenPartenaireIdEmpty_whenCallGetIdpnByPartnerId_thenException() {

        // when
        Assertions.assertThrows(EmacoToolsBusinessException.class, () -> {
            emacoService.getIdPartenaireEmacoByPartnerId("");
        });
        // then
        // exception

    }

    @Test
    public void givenPartenaireIdAlphanumeric_whenCallGetIdpnByPartnerId_thenException() {

        // when
        Assertions.assertThrows(EmacoToolsBusinessException.class, () -> {
            emacoService.getIdPartenaireEmacoByPartnerId("A0000304806206");
        });
        // then
        // exception

    }

}