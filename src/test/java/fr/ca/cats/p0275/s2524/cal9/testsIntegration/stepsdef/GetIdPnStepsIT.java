package fr.ca.cats.p0275.s2524.cal9.testsIntegration.stepsdef;

import com.fasterxml.jackson.core.JsonProcessingException;
import fr.ca.cats.p0275.s2524.cal9.testsIntegration.utils.Constant;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.fr.Alors;
import io.cucumber.java.fr.Quand;
import io.cucumber.java.fr.Soit;
import org.json.JSONObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class GetIdPnStepsIT extends DefautStepsIT {
    private String partnerId;
    private JSONObject idPnJSON;

    public GetIdPnStepsIT(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Soit("getIdpn parametre")
    public void getIdPnParametre(DataTable donnees) {
        this.partnerId = getParamRequete(donnees, Constant.CLE_PARTENAIRE);
        assertNotNull(this.partnerId);
    }

    @Quand("getIdpn on envoie la requete {string}")
    public void getIdPnOnEnvoieLaRequete(String nomEndpoint) throws URISyntaxException, JsonProcessingException {
        URI uri = new URI(getBaseUrl() + "/tools/emaco/retrieve_emaco_id/?partnerId=" + partnerId);
        affichageTraces(String.format("%s : URL : %s", nomEndpoint, uri));
        HttpHeaders headers = super.getHeader();
        headers.remove("structureId");
        HttpEntity<String> entity = new HttpEntity<>(headers);

        try {
            response = restTemplate.exchange(
                    uri,
                    HttpMethod.GET,
                    entity,
                    new ParameterizedTypeReference<>() {
                    });

            if (response.getStatusCodeValue() == 200) {
                if (response.getBody() != null) {
                    responseBody = Objects.requireNonNull(response.getBody().toString());
                }
                responseStatus = response.getStatusCodeValue();
                // Conversion du body de la réponse en JSON String
                String json = super.conversionBodyReponseEnJSONString(response);
                // Conversion du JSON au format String en un objet JSON
                this.idPnJSON = new JSONObject(json);
                if (configuration.afficherTraces()) {
                    ecrireFichier.ecrireFichierJson(idPnJSON, "./target/cucumber-reports/retourGetIdPn.json");
                }
            }
        } catch (RestClientResponseException rcrex) {
            idPnJSON = super.getErreurMessage(rcrex);
            // Mise à jour de la valeur du statut de la réponse
            responseStatus = idPnJSON.getInt(Constant.CLE_REPONSE);
            LOG.info("responseErreur : " + idPnJSON.toString());
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }

    @Alors("getIdpn reponse")
    public void getIdPnReponse(DataTable donnees) {
        assertNotNull(donnees, "Aucune donnée transmise du scénario");
        List<Map<String, String>> mapDonnees = convertDatatableToListMap(donnees);
        for (Map<String, String> mapCourante : mapDonnees) {
            assertEquals(mapCourante.get(Constant.CLE_REPONSE), String.valueOf(responseStatus));
            recuperationDonneesJSONPourValeurTestee(idPnJSON, mapCourante);
        }
    }
}
