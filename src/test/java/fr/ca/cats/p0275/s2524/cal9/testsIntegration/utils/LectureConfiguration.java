package fr.ca.cats.p0275.s2524.cal9.testsIntegration.utils;

import fr.ca.cats.p0070.s1889.easyrest.logging.service.Logger;
import fr.ca.cats.p0070.s1889.easyrest.logging.service.LoggerService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.ConfigurationException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ActiveProfiles;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * Classe permettant de lire le fichier de configuration pour les tests d'intégration
 */
@ActiveProfiles("Mock")
public class LectureConfiguration {

    private static final Logger LOG = LoggerService.getLogger(LectureConfiguration.class);
    private static final String CHEMIN_FICHIER_CONFIGURATION = "src/test/resources/configTestIntegration.properties";
    private static final String CONST_OUI = "Y";
    private static final String PARAM_AFFICHER_TRACES = "afficherTraces";
    private static final String PARAM_IMPORT = "import";
    private static final String PARAM_EXPORT = "export";
    private static final String PARAM_TOKEN = "jiraToken";
    private static final String CLE_CONFIG_BASE_URL_JIRA = "JIRA_BASE_URL";
    private static final String CLE_CONFIG_ID_TICKET_PLAN = "JIRA_TESTPLAN_ID";
    private static final String CLE_CONFIG_CHEMIN_RAPPORT_JSON = "CHEMIN_RAPPORT_JSON";
    private static final String CLE_CONFIG_CHEMIN_RAPPORT_HTML = "CHEMIN_RAPPORT_HTML";
    private static final String CLE_CONFIG_CHEMIN_DOSSIER_TEMP_JSON = "CHEMIN_DOSSIER_TEMP_JSON";
    private static final String CLE_CONFIG_NOM_FICHIER_LIEN_TEST_PLAN_TEST_EXEC = "NOM_FICHIER_LIEN_TEST_PLAN_TEST_EXEC";
    private static final String CLE_CONFIG_NOM_FICHIER_MAJ_TICKET_EXEC = "NOM_FICHIER_MAJ_TICKET_EXEC";
    private static final String CLE_CONFIG_CHEMIN_DEPOT_FEATURES = "CHEMIN_DEPOT_FEATURES";
    private final Properties configuration;

    @Value("${afficherTraces}")
    private String paramAfficherTraces;
    @Value("${import}")
    private String paramImport;
    @Value("${export}")
    private String paramExport;
    @Value("${jiraToken}")
    private String paramJiraToken;

    /**
     * Constructeur permettant de lire le fichier de configuration et de charger la configuration
     */
    public LectureConfiguration() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(CHEMIN_FICHIER_CONFIGURATION));
            this.configuration = new Properties();
            configuration.load(reader);
            reader.close();
        } catch (IOException e) {
            throw new ConfigurationException(String.format("Le fichier de configuration %s est requis", CHEMIN_FICHIER_CONFIGURATION));
        }
    }

    /**
     * Récupération de l'option pour afficher des traces
     * @return boolean true si les traces sont affichées dans la console, false sinon
     */
    public boolean afficherTraces() {
        String valeur = getParam(PARAM_AFFICHER_TRACES);
        LOG.info("ParamAfficherTraces : " + paramAfficherTraces + "(" + valeur + ")");
        return paramAfficherTraces == null ? CONST_OUI.equals(valeur) : CONST_OUI.equals(paramAfficherTraces);
    }

    /**
     * Récupération de l'option pour importer les scénarios depuis XRay
     * @return boolean true si les scénarios doivent être importés depuis vers XRay, false sinon
     */
    public boolean importerFeatures() {
        String valeur = getParam(PARAM_IMPORT);
        LOG.info("paramImport : " + paramImport + "(" + valeur + ")");
        return paramImport == null ? CONST_OUI.equals(valeur) : CONST_OUI.equals(paramImport);}

    /**
     * Récupération de l'option pour exporter les rapports vers XRay
     * @return boolean true si les rapports sont exportés vers XRay, false sinon
     */
    public boolean exporterRapportsToJira() {
        String valeur = getParam(PARAM_EXPORT);
        LOG.info("paramExport : " + paramExport + "(" + valeur + ")");
        return paramExport == null ? CONST_OUI.equals(valeur) : CONST_OUI.equals(paramExport);}

    /**
     * Récupération du token Jira
     * @return String Token d'authentification à Jira XRay
     */
    public String getJiraToken() {
        String valeur = getParam(PARAM_TOKEN);
        LOG.info("paramJiraToken : " + paramJiraToken + " (" + valeur + ")");
        if(valeur == null) {
            throw new ConfigurationException("Le token Jira n'est pas renseigné dans la commande maven", PARAM_TOKEN);
        }
        return paramJiraToken==null?valeur:paramJiraToken;
    }

    /**
     * Récupération de l'url de base Jira (domaine)
     * @return String Url de base de Jira
     */
    public String getBaseURLJira() {
        String url = configuration.getProperty(CLE_CONFIG_BASE_URL_JIRA);
        if(url == null || url.isBlank()) {
            throw new ConfigurationException("La base de l'url de base de Jira n'est pas renseigné dans le fichier de configuration", CLE_CONFIG_BASE_URL_JIRA);
        }
        return url;
    }

    /**
     * Récupération de l'identifiant du ticket XRay de plan de tests
     * @return String Identifiant du ticket de plan de test XRay
     */
    public String getIdTicketPlanTest() {
        String id = configuration.getProperty(CLE_CONFIG_ID_TICKET_PLAN);
        if(id == null || id.isBlank()) {
            throw new ConfigurationException("L'identifiant du ticket de plan de test n'est pas renseigné dans le fichier de configuration", CLE_CONFIG_ID_TICKET_PLAN);
        }
        return id;
    }

    /**
     * Récupération du chemin du rapport au format JSON
     * @return String Chemin du rapport au format JSON
     */
    public String getCheminRapportJSON() {
        String chemin = configuration.getProperty(CLE_CONFIG_CHEMIN_RAPPORT_JSON);
        if(chemin == null || chemin.isBlank()) {
            throw new ConfigurationException("Le chemin du rapport au format JSON n'est pas renseigné dans le fichier de configuration", CLE_CONFIG_CHEMIN_RAPPORT_JSON);
        }
        return chemin;
    }

    /**
     * Récupération du chemin du rapport au format HTML
     * @return String Chemin du rapport au format HTML
     */
    public String getCheminRapportHTML() {
        String chemin = configuration.getProperty(CLE_CONFIG_CHEMIN_RAPPORT_HTML);
        if(chemin == null || chemin.isBlank()) {
            throw new ConfigurationException("Le chemin du rapport au format HTML n'est pas renseigné dans le fichier de configuration", CLE_CONFIG_CHEMIN_RAPPORT_HTML);
        }
        return chemin;
    }

    /**
     * Récupération du chemin du dossier temporaire pour la génération des fichiers JSON
     * @return String Chemin du dossier temporaire contenant les fichiers JSON générés
     */
    public String getCheminDossierTempJson() {
        String chemin = configuration.getProperty(CLE_CONFIG_CHEMIN_DOSSIER_TEMP_JSON);
        if(chemin == null || chemin.isBlank()) {
            throw new ConfigurationException("Le chemin du dossier temporaire pour la génération des fichier JSON n'est pas renseigné dans le fichier de configuration", CLE_CONFIG_CHEMIN_DOSSIER_TEMP_JSON);
        }
        return chemin;
    }

    /**
     * Récupération du chemin du dossier temporaire pour la génération des fichiers JSON
     * @return String Chemin du dossier temporaire contenant les fichiers JSON générés
     */
    public String getNomFichierJSONLienExecResultTestPlan() {
        String chemin = configuration.getProperty(CLE_CONFIG_NOM_FICHIER_LIEN_TEST_PLAN_TEST_EXEC);
        if(chemin == null || chemin.isBlank()) {
            throw new ConfigurationException("Le nom du fichier JSON pour le lien entre le ticket de résultat d'exécution et le test plan n'est pas renseigné dans le fichier de configuration", CLE_CONFIG_NOM_FICHIER_LIEN_TEST_PLAN_TEST_EXEC);
        }
        return chemin;
    }

    /**
     * Récupération du nom du fichier contenant les éléments à mettre à jour sur le ticket d'exécution pour la génération des fichiers JSON
     * @return String Nom du fichier temporaire JSON contenant les informations de MAJ du ticket d'exécution
     */
    public String getNomFichierJSONMAJExecResult() {
        String chemin = configuration.getProperty(CLE_CONFIG_NOM_FICHIER_MAJ_TICKET_EXEC);
        if(chemin == null || chemin.isBlank()) {
            throw new ConfigurationException("Le nom du fichier JSON pour la mise à jour du ticket de résultat d'exécution n'est pas renseigné dans le fichier de configuration", CLE_CONFIG_NOM_FICHIER_MAJ_TICKET_EXEC);
        }
        return chemin;
    }

    /**
     * Récupération du chemin de destination pour les fichiers feature
     * @return String Chemin du dossier de dépôt des fichiers feature
     */
    public String getCheminDestinationFeatures() {
        String chemin = configuration.getProperty(CLE_CONFIG_CHEMIN_DEPOT_FEATURES);
        if(chemin == null || chemin.isBlank()) {
            throw new ConfigurationException("Le chemin du dossier de destination pour les fichiers feature n'est pas renseigné dans le fichier de configuration", CLE_CONFIG_CHEMIN_DEPOT_FEATURES);
        }
        return chemin;
    }

    private String getParam(String nomParam) {
        String valeur = System.getenv(nomParam);
        if(valeur == null) {
            valeur = System.getProperty(nomParam);
        }
        return valeur;
    }
}
