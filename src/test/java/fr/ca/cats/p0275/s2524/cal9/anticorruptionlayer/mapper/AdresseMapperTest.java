package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Branch;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.PostalAddress;
import fr.ca.cats.p0275.s2524.cal9.model.Adresse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AdresseMapperImpl.class})
public class AdresseMapperTest {

    @InjectMocks
    private AdresseMapper mapper = AdresseMapperImpl.ADRESSE_MAPPER;

    @Before
    public void init(){
        AdresseMapper adresseMapper = Mappers.getMapper(AdresseMapper.class);
        ReflectionTestUtils.setField(mapper, "adresseMapper", adresseMapper);
    }

    @Test
    public void AdresseToBranchTest() {
        Adresse adresse = mapper.adresseToBranch(initBranch());
        Assert.assertEquals("74556", adresse.getCodePostal());
        Assert.assertEquals("annecy", adresse.getVille());
        Assert.assertEquals("123 rue de tesc", adresse.getRue());

    }

    private static Branch initBranch(){
        Branch branch = new Branch();
        PostalAddress postalAddress = new PostalAddress();
        postalAddress.setPostalAddressLine4("123 rue de tesc");
        branch.setCity("annecy");
        branch.setZipCode("74556");
        branch.setPostalAddress(postalAddress);
        return branch;
    }



}
