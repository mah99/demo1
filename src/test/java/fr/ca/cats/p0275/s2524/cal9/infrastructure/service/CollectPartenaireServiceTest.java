package fr.ca.cats.p0275.s2524.cal9.infrastructure.service;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.PartenaireClientService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.PartenaireRepository;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.impl.CollectPartenaireServiceImpl;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class CollectPartenaireServiceTest {

    @Mock
    private PartenaireRepository partenaireRepository;
    @Mock
    private PartenaireClientService partenaireClientService;

    private CollectPartenaireService collectPartenaireService;

    @BeforeEach
    public void init() {

        collectPartenaireService = new CollectPartenaireServiceImpl(partenaireRepository, partenaireClientService);

    }


    @Test
    public void givenPartnerIdStructureId_whenGetIndividualBasicFromDB_Then_OK() {
        // given
        Partenaire partenaire = Partenaire.builder()
                .idPartenaire("00000305879046")
                .idCaisseRegionale("87800")
                .intitule("M.       PLOBUAM YOMMUSQ")
                .intituleSuite("")
                .build();

        when(partenaireRepository.getPartenaire(any(), any())).thenReturn(partenaire);

        // when
        var result = collectPartenaireService.getPartenaire("123456", "87800");

        // then
        Assert.assertEquals(partenaire, result);
        verify(partenaireRepository).getPartenaire(any(), any());
        verify(partenaireClientService, times(0)).getPartenaire(any(), any());
        verify(partenaireRepository, times(0)).addPartenaire(any());


    }


    @Test
    public void givenPartnerIdStructureId_whenGetIndividualBasicFromDB_KO_Then_OK_fromServer() {
        // given
        Partenaire partenaire = Partenaire.builder()
                .idPartenaire("00000305879046")
                .idCaisseRegionale("87800")
                .intitule("M.       PLOBUAM YOMMUSQ")
                .intituleSuite("")
                .build();
        when(partenaireRepository.getPartenaire(any(), any())).thenReturn(null);
        when(partenaireClientService.getPartenaire(any(), any())).thenReturn(partenaire);

        // when
        var result = collectPartenaireService.getPartenaire("123456", "87800");

        // then
        Assert.assertEquals(partenaire, result);
        verify(partenaireRepository).getPartenaire(any(), any());
        verify(partenaireClientService).getPartenaire(any(), any());
        verify(partenaireRepository).addPartenaire(any());


    }

}