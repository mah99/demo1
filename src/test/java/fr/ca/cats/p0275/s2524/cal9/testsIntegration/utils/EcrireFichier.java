package fr.ca.cats.p0275.s2524.cal9.testsIntegration.utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class EcrireFichier {

    /**
     * Génération d'un fichier JSON
     * @param donnees JSONObject données à insérer dans le fichier
     * @param cheminFichier String chemin du fichier
     * @return String le chemin du fichier créé
     * @throws IOException Erreur lors de l'écriture du fichier
     */
    public String ecrireFichierJson(JSONObject donnees, String cheminFichier) throws IOException {
        if(donnees != null && !donnees.isEmpty()) {
            return ecrireFichier(donnees.toString(), cheminFichier);
        }
        return null;
    }

    /**
     * Génération d'un fichier JSON
     * @param donnees JSONArray données à insérer dans le fichier
     * @param cheminFichier String chemin du fichier
     * @return String le chemin du fichier créé
     * @throws IOException Erreur lors de l'écriture du fichier
     */
    public String ecrireFichierJson(JSONArray donnees, String cheminFichier) throws IOException {
        if(donnees != null && !donnees.isEmpty()) {
            StringBuilder donneesAEcrire = new StringBuilder("[");
            for(int index=0; index < donnees.length(); index++) {
                donneesAEcrire.append(donnees.getJSONObject(index).toString());
                if(index < donnees.length() - 1) {
                    donneesAEcrire.append(",");
                }
            }
            donneesAEcrire.append("]");
            return ecrireFichier(donneesAEcrire.toString(), cheminFichier);
        }
        return null;
    }

    /**
     * Ecriture de données au format String dans un fichier
     * @param donnees String Données à écrire dans un fichier
     * @param cheminFichier String Chemin du fichier
     * @return String Le chemin du fichier
     * @throws IOException Erreur lors de l'écriture du fichier
     */
    private String ecrireFichier(String donnees, String cheminFichier) throws IOException {
        FileWriter fichier = new FileWriter(cheminFichier, StandardCharsets.UTF_8);
        fichier.write(donnees);
        fichier.close();
        return cheminFichier;
    }
}
