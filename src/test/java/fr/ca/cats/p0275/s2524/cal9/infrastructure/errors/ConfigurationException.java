package fr.ca.cats.p0275.s2524.cal9.infrastructure.errors;

public class ConfigurationException extends RuntimeException {

    public ConfigurationException() {
        super();
    }

    publiConfigurationException(String message) {
        super(message);
    }

    public ConfigurationException(String message, String cleConfiguration) {
        super(String.format("%s (clé %s)", message, cleConfiguration));
    }

    public ConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConfigurationException(String message, String cleConfiguration, Throwable cause) {
        super(String.format("%s (clé %s)", message, cleConfiguration), cause);
    }
}
