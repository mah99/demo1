package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.AgentDao;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import static fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.utils.MapperUtils.getFirstEmployeeInJson;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {EmployeeMapperImpl.class})
public class EmployeeMapperTest {

    @InjectMocks
    private EmployeeMapper mapper = EmployeeMapperImpl.EMPLOYEE_MAPPER;

    @Before
    public void init(){
        EmployeeMapper employeeMapper = Mappers.getMapper(EmployeeMapper.class);
        ReflectionTestUtils.setField(mapper, "employeeMapper", employeeMapper);
    }

    private static Employee employee;

    @Test
    public void agentToAgentDistributionEntityTest() {
        AgentDao agent = mapper.employeeToEmployeeDao(getFirstEmployeeInJson());
        Assert.assertEquals("00597", agent.getIdAgent());
        Assert.assertEquals("MR", agent.getCivilite());
        Assert.assertEquals("DUPONT", agent.getNom());
        Assert.assertEquals("Jean", agent.getPrenom());
    }




}
