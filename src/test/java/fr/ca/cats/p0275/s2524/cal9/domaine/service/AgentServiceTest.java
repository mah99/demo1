package fr.ca.cats.p0275.s2524.cal9.domaine.service;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.AgentMapper;
import fr.ca.cats.p0275.s2524.cal9.domaine.service.impl.AgentServiceImpl;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.CallInterApiTechnicalException;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.CollectAgentService;
import fr.ca.cats.p0275.s2524.cal9.model.Agent;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.utils.MapperUtils.getFirstEmployeeInJson;
import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.MapperUtils.readObjectFile;
import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.MapperUtils.readObjectsFile;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class AgentServiceTest {

    private AgentService agentService;
    @Mock
    private CollectAgentService collectAgentService;
    @Spy
    private AgentMapper agentMapper;

    private List<Reunion> reunions;
    private Employee employee;

    @BeforeEach
    public void init(){
        reunions = readObjectsFile("classpath:static/agc9.json", Reunion.class);
        employee = getFirstEmployeeInJson();
        when(collectAgentService.getEmployee(any(), anyString())).thenReturn(employee);
        agentMapper = Mappers.getMapper(AgentMapper.class);
        agentService = new AgentServiceImpl(collectAgentService, agentMapper);
    }

    @Test
    public void givenValidInformationAndSDG9AgentCreateurError_ThenTechnicalException(){
        //given
        when(collectAgentService.getEmployee(anyString(), anyString())).thenThrow(new CallInterApiTechnicalException("Error"));
        // when
        Assert.assertNull(agentService.getAgentCreateur("12345", reunions.get(0)));
    }

    @Test
    public void givenValidInformationAndSDG9EmployeeError_ThenTechnicalException(){
        //given
        when(collectAgentService.getEmployee(anyString(), anyString())).thenThrow(new CallInterApiTechnicalException("Error"));
        // when
        var result = Assertions.assertThrows(CallInterApiTechnicalException.class, () -> {
            agentService.getEmployee("12345", "12345");
        });
    }

    @Test
    public void givenValidInformationAgentCreateur_ThenOK() {
        // when
        Agent result = agentService.getAgentCreateur("12345", reunions.get(1));
        //then
        Assert.assertNotNull(result);
        Assert.assertEquals("DUPONT", result.getNom());
        Assert.assertEquals("Jean", result.getPrenom());
        Assert.assertEquals("MR", result.getCivilite());
    }

    @Test
    public void givenValidInformationAgent_ThenOK() {
        // when
        Employee result = agentService.getEmployee("12345", "12345");
        //then
        Assert.assertNotNull(result);
        Assert.assertEquals("DUPONT", result.getNom());
        Assert.assertEquals("Jean", result.getPrenom());
        Assert.assertEquals("MR", result.getCivilite());
    }

    @Test
    public void givenValidInformationAndAGC9ErrorAgentIdNullEmpty_ThenBusinessException() {
        //given
        reunions.get(0).getAction().setAgentCreateurId(null);
        // when
        var result = agentService.getAgentCreateur("12345", reunions.get(0));
        //then
        Assert.assertNull(result);
    }

    @Test
    public void givenValidInformationAndAGC9ErrorAgentNullEmpty_ThenBusinessException() {
        //given
        reunions.get(0).setAction(null);
        // when
        var result = agentService.getAgentCreateur("12345", reunions.get(0));
        //then
        Assert.assertNull(result);
    }
}