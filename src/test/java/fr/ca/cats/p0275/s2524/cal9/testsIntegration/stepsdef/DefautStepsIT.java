package fr.ca.cats.p0275.s2524.cal9.testsIntegration.stepsdef;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ca.cats.p0070.s1889.easyrest.logging.service.Logger;
import fr.ca.cats.p0070.s1889.easyrest.logging.service.LoggerService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.errors.BusinessException;
import fr.ca.cats.p0275.s2524.cal9.testsIntegration.utils.*;
import io.cucumber.datatable.DataTable;
import lombok.Getter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

/**
 *  Cette classe contient :
 *  - L'initialisation de la requête http
 *  - La récupération de l'URL de base de l'API
 *  - La conversion du body de la réponse en JSON String
 */

@ActiveProfiles("Mock")
public class DefautStepsIT {

    protected int responseStatus;
    protected String responseBody;
    protected BusinessException businessException;
    protected ResponseEntity<?> response;
    protected RestTemplate restTemplate;
    protected static final Logger LOG = LoggerService.getLogger(GetCalendrierStepsIT.class);

    @Value("${local.server.port}")
    private int port;

    @Autowired
    protected LectureConfiguration configuration;
    @Autowired
    public NoeudDecompose noeudDecompose;

    @Autowired
    public EcrireFichier ecrireFichier;
    @Getter
    private String parametrePourMessage = "";

    /**
     * Génération d'un String pour l'identifiant passé en paramètre de la requète au endpoint sous le format (libelle = valeur)
     * Cette valeur sera présent dans le rapport HTML en cas de test en erreur
     * @param libelleParametre String libelle du paramètre de la requête
     * @param parametreId String valeur du paramètre de la requête
     */
    public void setParametrePourMessage(String libelleParametre, String parametreId) {
        parametrePourMessage =  String.format("(%s = %s)", libelleParametre, parametreId);
    }

    /**
     * Retourne l'URL de base de l'API
     * @return String domaine et port de l'URL de l'API
     */
    protected String getBaseUrl() {
        return "http://localhost:"+ port;
    }

    /**
     * Retourne un HTTPHeaders initialisé avec le format d'envoi et de retour accepté, l'id correlation et l'id structure
     * @return HTTPHeaders l'entête paramétrée
     */
    protected HttpHeaders getHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("correlationId", "12345");
        headers.add("structureId", "87800");
        return headers;
    }

    /**
     * Converti le contenu du body de la réponse en JSON String
     * @param response la réponse de l'appel à l'API
     * @return JSON String du body de la réponse
     * @throws JsonProcessingException Exception
     */
    protected String conversionBodyReponseEnJSONString(ResponseEntity response) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(response.getBody());
    }

    /**
     * Comparaison que la valeur actuelle est un JSONArray vide ([])
     * @param valeurActuelle JSONArray valeur réelle du JSON de retour de l'appel à l'API
     */
    protected void testArray(JSONArray valeurActuelle, String titreChampScenario) {
        assertEquals(new JSONArray().isEmpty(), valeurActuelle.isEmpty(), titreChampScenario);
    }

    /**
     * Conversion de la valeur attendue en booléen et comparaison avec la valeur réelle du JSON
     * @param valeurAttendue String valeur attendue au format String ("true" ou "false")
     * @param valeurActuelle boolean valeur réelle du JSON de retour de l'appel à l'API
     */
    protected void testBoolean(String valeurAttendue, boolean valeurActuelle, String titreChampScenario) {
        // /!\ Il est possible que dans le scénario, true ne soit pas en minuscule
        // Conversion en booléen de la valeur attendue
        boolean valeurAttendueBool = "true".equalsIgnoreCase(valeurAttendue);
        if(valeurAttendueBool) {
            assertTrue(valeurActuelle, titreChampScenario);
        } else {
            assertFalse(valeurActuelle, titreChampScenario);
        }
    }

    /**
     * Comparaison de la valeur actuelle du JSON selon les critères suivants : non null et non vide (le contenu de la valeur actuelle n'est pas comparée)
     * @param valeurActuelle String valeur du JSON
     * @param titreChampScenario String titre du scénario pour la valeur à tester
     */
    protected void testNonNulNonVide(String valeurActuelle, String titreChampScenario) {
        assertNotNull(valeurActuelle);
        assertNotEquals("", valeurActuelle, titreChampScenario);
    }

    /**
     * Comparaison avec la valeur réelle dont la valeur attendue est nulle
     * @param valeurActuelle Object valeur du JSON
     */
    protected void testNull(Object valeurActuelle, String titreChampScenario) {
        assertEquals(JSONObject.NULL, valeurActuelle, titreChampScenario);
    }

    /**
     * Converti un ddtatable Cucumber en une liste de map
     * @param dataTable objet Cucumber à convertir
     * @return List<Map<String, String>> Une liste de map
     */
    protected List<Map<String, String>> convertDatatableToListMap(DataTable dataTable) {
        return dataTable.asMaps();
    }

    /**
     * Méthode qui récupère la valeur du paramètre de l'url en fonction de la valeur de l'entête de la dataTable
     * @param dataTable Objet Cucumber
     * @param cleParam Valeur de l'entête du dataTable
     * @return String La valeur pour l'entête du dataTable passé en paramêtre
     */
    protected String getParamRequete(DataTable dataTable, String cleParam) {
        return convertDatatableToListMap(dataTable).get(0).get(cleParam);
    }

    /**
     * Récupération d'un élément pour les noeuds composés
     * /!\ Le type de l'objet retourné est à tester après l'appel de cette méthode (instanceof)
     * @param cleCouranteMap String noeud composé
     * @param jsonObjet JSONObject JSON contenant le noeud recherché
     * @return Object La valeur recherchée sous forme d'objet (valeurs possibles : JSONArray, JSONObject, type primitif)
     */
    protected Object getValeurATesterDansJSON(String cleCouranteMap, JSONObject jsonObjet) {
        this.noeudDecompose.setNomNoeudScenario(cleCouranteMap);
        affichageTraces(this.noeudDecompose.toString());
        Object valeurJSONATester;
        int numeroElementCible;
        if(!noeudDecompose.isParametreCompose(cleCouranteMap)) {
            valeurJSONATester = jsonObjet.get(noeudDecompose.getNomNoeudSansPattern());
        } else {
            String derniereCle = noeudDecompose.getDernierNoeud();
            String[] tabSousNoeuds = noeudDecompose.getSousNoeud();
            JSONObject sousNoeudJson = null;
            // Récupération du premier noeud
            Object jsonNiveau1 = jsonObjet.get(noeudDecompose.getNomNoeudSansPattern(tabSousNoeuds[0]));
            if (jsonNiveau1 instanceof JSONObject) {
                sousNoeudJson = (JSONObject) jsonNiveau1;
            } else if (jsonNiveau1 instanceof JSONArray) {
                numeroElementCible = noeudDecompose.extractLigneCiblee(tabSousNoeuds[0]);
                sousNoeudJson = ((JSONArray) jsonNiveau1).getJSONObject(numeroElementCible == -1 ? 0 : numeroElementCible);
            }

            // S'il existe un niveau 2, récupération du json à partir de sousNoeudJson
            if (tabSousNoeuds.length == 2 && sousNoeudJson != null) {
                Object jsonNiveau2 = sousNoeudJson.get(noeudDecompose.getNomNoeudSansPattern(tabSousNoeuds[1]));
                if (jsonNiveau2 == null) {
                    return null;
                }
                if (jsonNiveau1 instanceof JSONObject) {
                    sousNoeudJson = (JSONObject) jsonNiveau2;
                } else if (jsonNiveau1 instanceof JSONArray) {
                    numeroElementCible = noeudDecompose.extractLigneCiblee(tabSousNoeuds[1]);
                    sousNoeudJson = ((JSONArray) jsonNiveau2).getJSONObject(numeroElementCible == -1 ? 0 : numeroElementCible);
                }
            }

            valeurJSONATester = sousNoeudJson != null ? sousNoeudJson.get(derniereCle) : null;
        }
        return valeurJSONATester;
    }

    /**
     * Comparaison entre la valeur attendue et la valeur réelle provenant du JSON de retour suite à l'appel de l'API
     * @param valeurAttendue String valeur attendue provenant du scénario
     * @param valeurJSON Object valeur récupérée dans le JSON de retour suite à l'appel de l'API
     */
    protected void testValeurAttendueValeurJson(String valeurAttendue, Object valeurJSON, String titreCleScenario) {
        // Test que la valeur à tester est non nulle
        assertNotNull(valeurJSON, String.format(Constant.MESSAGE_VALEUR_REELLE_NON_TROUVEE, titreCleScenario, getParametrePourMessage()));
        // Cas du tableau vide
        if(this.noeudDecompose.isArray(valeurAttendue)) {
            if(valeurJSON instanceof JSONArray) {
                testArray((JSONArray) valeurJSON, String.format(Constant.MESSAGE_TEST_EN_ERREUR, titreCleScenario, getParametrePourMessage()));
            } else {
                assertEquals(new JSONArray(), valeurJSON, String.format(Constant.MESSAGE_TEST_EN_ERREUR, titreCleScenario, getParametrePourMessage()));
            }
        } else if(this.noeudDecompose.isNull(valeurAttendue)) {
            testNull(valeurJSON, String.format(Constant.MESSAGE_TEST_EN_ERREUR, titreCleScenario, getParametrePourMessage()));
        } else if(this.noeudDecompose.isBoolean(valeurAttendue)) {
            testBoolean(valeurAttendue, convertObjetToBooleen(valeurJSON), String.format(Constant.MESSAGE_TEST_EN_ERREUR, titreCleScenario, getParametrePourMessage()));
        } else if(this.noeudDecompose.isParametreSpecifique(valeurAttendue)) {
            testNonNulNonVide(String.valueOf(valeurJSON), String.format(Constant.MESSAGE_TEST_EN_ERREUR, titreCleScenario, getParametrePourMessage()));
        } else {
            assertEquals(valeurAttendue, String.valueOf(valeurJSON), String.format(Constant.MESSAGE_TEST_EN_ERREUR, titreCleScenario, getParametrePourMessage()));
        }
    }

    /**
     * Converti un objet en booléen
     * @param objet objet à convertir en booléen
     * @return true si l'objet est "true", false si l'objet est "false"
     */
    protected boolean convertObjetToBooleen(Object objet) {
        return (Boolean) objet;
    }

    /**
     * Récupération de toutes les valeurs à comparer dans le JSON à partir de la map des valeurs souhaitées
     *
     * @param donneesCourante JSONObject objet JSON de la réunion pour l'agent id ciblé
     * @param mapCourante     Map<String, String> map des clés valeurs des données attendues
     */
    public void recuperationDonneesJSONPourValeurTestee(JSONObject donneesCourante, Map<String, String> mapCourante) {
        // Récupération des clés de la map et suppression de la clé reponse déjà testée
        Set<String> setClesMap = mapCourante.keySet();
        List<String> listeClesMap = new ArrayList<>(setClesMap);
        listeClesMap.remove(Constant.CLE_REPONSE);

        // Parcours des clés
        for (String cleCouranteMap : listeClesMap) {
            String valeurATester = mapCourante.get(cleCouranteMap);
            getValeurJSONFromCleMap(donneesCourante, cleCouranteMap, valeurATester, cleCouranteMap);
        }
    }

    /**
     * Récupération de la valeur à comparer dans le JSON de retour de l'appel de l'API
     *
     * @param donneesCourante JSONObject objet JSON de la réunion pour l'agent id ciblé
     * @param cleCouranteMap  String noeud ou sous-noeud à comparer
     * @param valeurAttendue  String valeur attendu provenant du scénario
     */
    private void getValeurJSONFromCleMap(JSONObject donneesCourante, String cleCouranteMap, String valeurAttendue, String titreCleScenario) {
        Object valeurJSONObj = getValeurATesterDansJSON(cleCouranteMap, donneesCourante);
        System.out.println(valeurAttendue + " | " + valeurJSONObj);
        testValeurAttendueValeurJson(valeurAttendue, valeurJSONObj, titreCleScenario);
    }

    /**
     * Affichage de log si dans le fichier de configuration l'option AFFICHER_TRACES = Y
     * @param message String le message à afficher dans la console
     */
    public void affichageTraces(String message) {
        if(configuration.afficherTraces()) {
            LOG.info(message);
        }
    }

    /**
     * Transforme une exception (statusCode != 200) en un jsonObject formatté pour les tests d'intégration
     * @param rcrex RestClientResponseException exception
     * @return JSONObject contenant les clés reponse et message
     * @throws JsonProcessingException
     */
    public JSONObject getErreurMessage(RestClientResponseException rcrex) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        // Conversion en string du body de l'exception
        String ret = rcrex.getResponseBodyAsString();
        // Conversion en ErreurMessage
        ErreurMessage erreurMessage = mapper.readValue(ret, ErreurMessage.class);
        // Renvoie le json pour les tests ({reponse: XX, message: YYY})
        return erreurMessage.getJSON();
    }

}
