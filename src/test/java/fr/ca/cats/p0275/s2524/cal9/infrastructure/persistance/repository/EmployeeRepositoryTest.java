package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.EmployeeMapper;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.AgentDao;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.Dao.AgentRepositoryDao;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.impl.AgentRepositoryImpl;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.utils.MapperUtils.getFirstAgentDaoInJson;
import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.MapperUtils.readObjectFile;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class EmployeeRepositoryTest {


    private AgentRepository agentRepository;

    @Mock
    private AgentRepositoryDao agentRepositoryDao;

    @Spy
    EmployeeMapper employeeMapper = Mappers.getMapper(EmployeeMapper.class);

    private AgentDao agentDao;

    @BeforeEach
    public void init() {
        agentDao = getFirstAgentDaoInJson();
        agentRepository = new AgentRepositoryImpl(agentRepositoryDao, employeeMapper);

    }

    @Test
    public void getEmployeeByAgentIdAndStructureId() {
        // given
        when(agentRepositoryDao.findByIdAgentAndStructureId(any(), any())).thenReturn(agentDao);

        // when
        var result = agentRepository.getEmployeeByAgentIdAndStructureId("00597", "88200");

        // then
        Assert.assertEquals(agentDao.getStructureId(), result.getStructureId());
        Assert.assertEquals(agentDao.getCivilite(), result.getCivilite());
        Assert.assertEquals(agentDao.getIdAgent(), result.getIdEmployee());
        verify(employeeMapper).employeeDaoToEmployee(any());
        verify(agentRepositoryDao).findByIdAgentAndStructureId(any(), any());
    }

    @Test
    public void addEmployee() {

        // when
        agentRepository.addEmployee(Employee.builder().build());

        // then
        verify(employeeMapper).employeeToEmployeeDao(any());
        verify(agentRepositoryDao).save(any());
    }
}