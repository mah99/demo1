package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion;
import fr.ca.cats.p0275.s2524.cal9.model.PiecesAFournir;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;

import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.MapperUtils.readObjectsFile;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ReunionMapperImpl.class})
public class ReunionMapperTest {

    @InjectMocks
    private ReunionMapper mapper = ReunionMapperImpl.REUNION_MAPPER;

    @Before
    public void init(){
        ReunionMapper reunionMapper = Mappers.getMapper(ReunionMapper.class);
        ReflectionTestUtils.setField(mapper, "reunionMapper", reunionMapper);
    }

    private static List<Reunion> reunion;

    @Test
    public void reunionAGC9ToReunionTest() {
        fr.ca.cats.p0275.s2524.cal9.model.Reunion reunion = mapper.reunionAGC9ToReunion(initReunion());
        Assert.assertEquals("12345", reunion.getReunionContexte().getAgentId());
        Assert.assertEquals("20210922", reunion.getReunionContexte().getDateReunion());
        Assert.assertEquals("15.00", reunion.getReunionContexte().getHeureDebutReunion());
        Assert.assertEquals("2021-09-22T10:15:00+02:00", reunion.getDateDebut());
        Assert.assertEquals("2021-09-23T10:15:00+02:00", reunion.getDateFin());
        Assert.assertEquals("RENDEZ-VOUS", reunion.getTypeReunion());
        Assert.assertEquals("BL", reunion.getTheme().getId());
        Assert.assertEquals("Comptes et cartes", reunion.getTheme().getLibelle());
        Assert.assertEquals("CD", reunion.getObjet().getId());
        Assert.assertEquals("Faire des opérations à distance", reunion.getObjet().getLibelle());
        Assert.assertEquals("AGENCE", reunion.getCanalInteraction().getCanal());
        Assert.assertEquals("00173", reunion.getCanalInteraction().getAgence().getId());
        Assert.assertEquals("0634526589", reunion.getCanalInteraction().getTelephone());
        Assert.assertEquals("string", reunion.getCanalInteraction().getUrlVisio());
        Assert.assertEquals("expert@cats.com", reunion.getExperts().get(0).getMail());
       Assert.assertEquals(2,nombrePIAF(reunion.getPiecesAFournir()));
        Assert.assertEquals("commentaire", reunion.getCommentaire());
        Assert.assertEquals(true, reunion.getPlageProtegee());
        Assert.assertNull(reunion.getAgentCreateur());
    }
    @Test
    public void reunionAGC9ToReunionTestWithAgentCreateur() {
        fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion reunionAcg9 = readObjectsFile("classpath:static/agc9.json", Reunion.class).get(1);
        fr.ca.cats.p0275.s2524.cal9.model.Reunion reunion = mapper.reunionAGC9ToReunion(reunionAcg9);
        Assert.assertEquals("12346", reunion.getReunionContexte().getAgentId());
        Assert.assertEquals("20210922", reunion.getReunionContexte().getDateReunion());
        Assert.assertEquals("15.00", reunion.getReunionContexte().getHeureDebutReunion());
        Assert.assertEquals("2021-09-22T10:15:00+02:00", reunion.getDateDebut());
        Assert.assertEquals("2021-09-23T10:15:00+02:00", reunion.getDateFin());
        Assert.assertEquals("RENDEZ-VOUS", reunion.getTypeReunion());
        Assert.assertEquals("BL", reunion.getTheme().getId());
        Assert.assertEquals("Comptes et cartes", reunion.getTheme().getLibelle());
        Assert.assertEquals("CD", reunion.getObjet().getId());
        Assert.assertEquals("Faire des opérations à distance", reunion.getObjet().getLibelle());
        Assert.assertEquals("AGENCE", reunion.getCanalInteraction().getCanal());
        Assert.assertEquals("00950", reunion.getCanalInteraction().getAgence().getId());
        Assert.assertEquals("0634526589", reunion.getCanalInteraction().getTelephone());
        Assert.assertEquals("string", reunion.getCanalInteraction().getUrlVisio());
        Assert.assertEquals(1,nombrePIAF(reunion.getPiecesAFournir()));
        Assert.assertEquals("commentaire", reunion.getCommentaire());
        Assert.assertEquals(true, reunion.getPlageProtegee());
    }

    private int nombrePIAF(List<PiecesAFournir> piecesAFournir) {
        int sizePiaf = piecesAFournir.size();
        return sizePiaf;
    }

    private static Reunion initReunion(){
        reunion =  readObjectsFile("classpath:static/agc9.json", Reunion.class);
        return reunion.get(0);
    }
}
