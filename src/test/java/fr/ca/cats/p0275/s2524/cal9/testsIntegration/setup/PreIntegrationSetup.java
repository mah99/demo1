package fr.ca.cats.p0275.s2524.cal9.testsIntegration.setup;

import fr.ca.cats.p0070.s1889.easyrest.logging.service.Logger;
import fr.ca.cats.p0070.s1889.easyrest.logging.service.LoggerService;
import fr.ca.cats.p0275.s2524.cal9.testsIntegration.utils.EchangesJira;
import fr.ca.cats.p0275.s2524.cal9.testsIntegration.utils.LectureConfiguration;
import org.springframework.context.annotation.ComponentScan;

import java.util.List;

/**
 * Classe pour la phase de pre-integration-test :
 * - lecture de la configuration
 * - initialisation de la classe EchangeJira et import des features depuis Xray
 * Commande pour exécuter cette classe : mvn clean install
 * /!\ Cette commande doit impérativement être faite avant l'exécution des tests d'intégration
 */
@ComponentScan("fr.ca.cats.p0275.s2524.cal9.testsIntegration")
public class PreIntegrationSetup {

    private static final Logger LOG = LoggerService.getLogger(PreIntegrationSetup.class);

    public static void main(String[] args) {
        LOG.info("Import des scénarios depuis Jira");
        LectureConfiguration configuration = new LectureConfiguration();
        String paramImport = System.getenv("import");
        String paramToken = System.getenv("jiraToken");
        String propertyToken = System.getProperty("jiraToken");
        String token= configuration.getJiraToken();
        String propertyImport = System.getProperty("import");
        boolean afficherTraces = configuration.afficherTraces();
        boolean isImport = configuration.importerFeatures();
        // Initialisation
        LOG.info("afficher traces : " + isImport + " ("+System.getenv("afficherTraces")+")");
        LOG.info("token : " + token + " ("+System.getenv("jiraToken")+")" + " ("+propertyToken+")");
        LOG.info("Import des scénarios : " + isImport + " ("+paramImport+")" + " ("+propertyImport+")");
        EchangesJira echangesJira = new EchangesJira();
        echangesJira.init(configuration.getJiraToken(), configuration.getBaseURLJira(), configuration.afficherTraces());
        // Récupération de la liste des tickets de test
        LOG.info("Params getTestFromPlanTest : " + configuration.getIdTicketPlanTest(), configuration.getCheminDossierTempJson());
        List<String> ticketsTest = echangesJira.getTestFromPlanTest(configuration.getIdTicketPlanTest(), configuration.getCheminDossierTempJson());
        LOG.info("Liste des identifiants des tickets de tests à récupérer : " + ticketsTest);
        // Import des features
        echangesJira.importFichiersFeatures(ticketsTest, configuration.getCheminDestinationFeatures());
        LOG.info("Params importFichiersFeatures : " + ticketsTest, configuration.getCheminDestinationFeatures());
        if(afficherTraces) {
            echangesJira.importFichiersFeaturesZip(ticketsTest, configuration.getCheminDossierTempJson());
        }

    }
}

