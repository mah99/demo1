package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.OmAgent;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.OmEdsAgent;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {SecteurAgentMapperImpl.class})
public class SecteurAgentMapperTest {

    @InjectMocks
    SecteurAgentMapper secteurAgentMapper = SecteurAgentMapper.SECTEUR_AGENT_MAPPER;

    private OmEdsAgent omEdsAgent;
    private OmEdsAgent omEdsAgentMulti;

    @Before
    public void setUp() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Resource resourceEdsParent = new ClassPathResource("/static/OMEdsAgentParent.json");
        Resource resourceEdsAgentMulti = new ClassPathResource("/static/OMEdsAgent.json");

        omEdsAgent = new OmEdsAgent();
        omEdsAgentMulti = new OmEdsAgent();
        try {
            omEdsAgent = mapper.readValue(resourceEdsParent.getInputStream(), new TypeReference<List<OmEdsAgent>>() {
            }).get(0);
            omEdsAgentMulti = mapper.readValue(resourceEdsAgentMulti.getInputStream(), new TypeReference<List<OmEdsAgent>>() {
            }).get(0);
        } catch (Exception ignored) {
        }
    }

    @Test
    public void omAgentToAgentFonction() {
        // given
        OmAgent omAgent = omEdsAgent.getFonctions().get(0).getAgents().get(0);

        // when

        var result = secteurAgentMapper.omAgentToAgentFonction(omAgent);

        // then

        Assert.assertNotNull(result);
        Assert.assertEquals(omAgent.getMatricule(), result.getId());
        Assert.assertEquals(omAgent.getNom(), result.getNom());
        Assert.assertEquals(omAgent.getPrenom(), result.getPrenom());

    }

    @Test
    public void omAgentToAgentFonctionMulti() {
        // given+
        OmAgent omAgent = omEdsAgent.getFonctions().get(0).getAgents().get(0);

        // when

        var result = secteurAgentMapper.omAgentToAgentFonctionMulti(omAgent);

        // then

        Assert.assertNotNull(result);
        Assert.assertEquals(omAgent.getMatricule(), result.getId());
        Assert.assertEquals(omAgent.getNom(), result.getNom());
        Assert.assertEquals(omAgent.getPrenom(), result.getPrenom());

    }

    @Test
    public void omEdsAgentToEds() {
        // given
        OmEdsAgent omEdsParent = omEdsAgent;

        // when

        var result = secteurAgentMapper.omEdsAgentToEds(omEdsParent);

        // then

        Assert.assertNotNull(result);
        Assert.assertEquals("00075", result.getId());
        Assert.assertEquals("87800", result.getStructureId());
        Assert.assertEquals("05", result.getNiveauEds());
        Assert.assertEquals(omEdsParent.getLibelle(), result.getLibelle());
        Assert.assertEquals(omEdsParent.getLibelleCourt(), result.getLibelleCourt());
        Assert.assertEquals(omEdsParent.getFonctions().get(0).getLibelle(), result.getFonctions().get(0).getLibelle());
        Assert.assertEquals(omEdsParent.getFonctions().get(0).getAgents().get(0).getMatricule(), result.getFonctions().get(0).getAgents().get(0).getId());
        Assert.assertEquals(omEdsParent.getFonctions().get(0).getAgents().get(0).getNom(), result.getFonctions().get(0).getAgents().get(0).getNom());
        Assert.assertEquals(omEdsParent.getFonctions().get(0).getAgents().get(0).getPrenom(), result.getFonctions().get(0).getAgents().get(0).getPrenom());

    }

    @Test
    public void omEdsAgentMultiToEds() {
        // given
        OmEdsAgent omEdsParent = omEdsAgentMulti;

        // when

        var result = secteurAgentMapper.omEdsAgentToEdsMulti(omEdsParent);

        // then

        Assert.assertNotNull(result);
        Assert.assertEquals("00075", result.getId());
        Assert.assertEquals("87800", result.getStructureId());

        Assert.assertEquals(omEdsParent.getLibelle(), result.getLibelle());
        Assert.assertEquals(omEdsParent.getFonctions().get(0).getLibelle(), result.getFonctions().get(0).getLibelle());
        Assert.assertEquals(omEdsParent.getFonctions().get(0).getCode(), result.getFonctions().get(0).getCode());
        Assert.assertEquals(omEdsParent.getFonctions().get(0).getAgents().get(0).getMatricule(), result.getFonctions().get(0).getAgents().get(0).getId());
        Assert.assertEquals(omEdsParent.getFonctions().get(0).getAgents().get(0).getNom(), result.getFonctions().get(0).getAgents().get(0).getNom());
        Assert.assertEquals(omEdsParent.getFonctions().get(0).getAgents().get(0).getPrenom(), result.getFonctions().get(0).getAgents().get(0).getPrenom());
        Assert.assertEquals(omEdsParent.getFonctions().get(0).getAgents().get(0).getCivilite(), result.getFonctions().get(0).getAgents().get(0).getCivilite());
    }
}