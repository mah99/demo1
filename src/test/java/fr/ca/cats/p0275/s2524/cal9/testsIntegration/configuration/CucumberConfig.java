package fr.ca.cats.p0275.s2524.cal9.testsIntegration.configuration;

import fr.ca.cats.p0275.s2524.cal9.testsIntegration.utils.EchangesJira;
import fr.ca.cats.p0275.s2524.cal9.testsIntegration.utils.EcrireFichier;
import fr.ca.cats.p0275.s2524.cal9.testsIntegration.utils.LectureConfiguration;
import fr.ca.cats.p0275.s2524.cal9.testsIntegration.utils.NoeudDecompose;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
@RunWith(Cucumber.class)
public class CucumberConfig {

    @Bean
    public LectureConfiguration lectureConfiguration(){
        return new LectureConfiguration();
    }

    @Bean
    public EchangesJira echangesJira() {
        return new EchangesJira();
    }

    @Bean
    public EcrireFichier ecrireFichier() {
        return new EcrireFichier();
    }

    @Bean
    public NoeudDecompose noeudDecompose() {
        return new NoeudDecompose();
    }
}