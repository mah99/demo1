package fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Branch;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper.AgenceMapper;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.BranchDao;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.Dao.AgenceRepositoryDao;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.repository.impl.AgenceRepositoryImpl;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.MapperUtils.readObjectFile;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class AgenceRepositoryTest {


    private AgenceRepository agenceRepository;

    @Mock
    private AgenceRepositoryDao agenceRepositoryDao;

    @Spy
    AgenceMapper agenceMapper = Mappers.getMapper(AgenceMapper.class);

    private BranchDao branchDao;

    @BeforeEach
    public void init() {
        branchDao = readObjectFile("classpath:static/sdg9BranchID.json", BranchDao.class);
        agenceRepository = new AgenceRepositoryImpl(agenceMapper, agenceRepositoryDao);

    }

    @Test
    public void getBranchByAgenceIdAndStructureId() {
        // given
        when(agenceRepositoryDao.findByStructureIdAndCodeEds(any(), any())).thenReturn(branchDao);

        // when
        Branch result = agenceRepository.getBranchByAgenceIdAndStructureId("8542", "87800");

        // then
        Assert.assertEquals(branchDao.getCodeEds(), result.getCodeEds());
        Assert.assertEquals(branchDao.getStructureId(), result.getStructureId());
        Assert.assertEquals(branchDao.getCity(), result.getCity());
        verify(agenceMapper).branchDaoToBranch(any());
        verify(agenceRepositoryDao).findByStructureIdAndCodeEds(any(), any());
    }

    @Test
    public void addBranch() {

        // when
        agenceRepository.addBranch(Branch.builder().build());

        // then
        verify(agenceMapper).branchtoBranchDao(any());
        verify(agenceRepositoryDao).save(any());
    }
}