package fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient;

import fr.ca.cats.p0070.s1889.easyrest.interapi.exception.InterApiException;
import fr.ca.cats.p0070.s1889.easyrest.interapi.service.InterApiRequestService;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.impl.SettingsClientServiceImpl;
import fr.ca.cats.p0275.s2524.cal9.model.Setting;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static fr.ca.cats.p0070.s1889.easyrest.interapi.utils.Constants.CATS_CONSOMMATEUR_ORIGINE_HEADER;
import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.MapperUtils.readObjectsFile;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class SettingsClientServiceTest {

    @Mock
    private HttpServletRequest request;

    @Mock
    private AbstractCallInterApi<Setting> abstractCallInterApi;

    @Mock
    private InterApiRequestService interApiRequestService;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private final SettingsClientServiceImpl agc9ClientService = new SettingsClientServiceImpl();


    @Value("${aia.aut9.uri:/technical_authentication/v1/token}")
    private String aut9Uri;

    @Value("${aia.cache.duration.seconds:530}")
    private int cacheDurationSeconds;

    @Value("${aia.scopes:openid}")
    private String scopes;

    @Value("${authentification.config.apimGatewayUrl:https://dev1-private.api.credit-agricole.fr}")
    private String apimGatewayUrl;

    private Class<Reunion> type;

    private static final String AUTH = "Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6InBJVGprQk82b1JOckNRTjdUaFZNNEd6MmRMcFFWMlEzWEhOTkVvajlfaTQifQ.eyJzdWIiOiJPVzYwMDAxIiwic2NvcGUiOiJvcGVuaWQiLCJpc3MiOiJzZGpfY29sbGFib3JhdGV1ci1kZXZ0dSIsInN0cnVjdHVyZV9pZCI6Ijg3ODAwIiwidHlwZSI6ImNvbGxhYm9yYXRldXIiLCJpYXQiOjE2MzUyMzkxMDksImF1dGhvcml6YXRpb25fY29udGV4dCI6eyJzZXNzaW9uX2lkIjoiOWMyMDkyOTgtOTE3Yy00YzY4LTliMDctOWI2NjBhMjliMTAwIiwidXNlcl90eXBlIjoiMDEiLCJ1c2VyX2lkIjoiT1c2MDAwMSIsImNsaWVudF9pZCI6ImVjNGE3YTdlNTA0ZTU0YzBiMzJlNTJkMWFjNWJiMTliZDE5YmE4YWY5OTA0OGZkZDM2Y2M5NzJhZDVhZDFjMzUiLCJ1c2VyX3VvbV9jb2RlIjoiODc4MDAifSwiYXV0aF90aW1lIjoxNjM1MjM5MTA5LCJkYXQiOnsidHlwb2xvZ2llIjoiY29sbGFib3JhdGV1ciIsImF1dGhlbnRpY2F0aW9uX2xldmVsIjoiMiJ9LCJleHAiOjE5NDUyNDUzOTQsImF1ZCI6WyJjYS1lc2IiXSwiQVNfSTAwMDFfdHlwZSI6ImNvbGxhYm9yYXRldXIiLCJqdGkiOiI1MWMyMDY5ZS1jNWMzLTQ1NmEtYjhjYi1iZWFmMThlZDYwYTMiLCJhY3IiOiIyMCJ9.GD1TP5n9zpbgSql8ipNNfEliMB0CPR7JVeurpAxuTA0rvxHUpjQBSs3eOFmhfzqdiJXhef6JPAqxYrXMpU4mI-7u2k5E1hAo3_4dcj6Mb0MQSYJNRwWn34oXHe1-hLQcIphLLforr_PA3rgYFV9MFYPeunEzhfXhy-BKa-i372VEyDsiTEWjDq80rQuKgCxEM4Z-Z-SAEMRN3JcYquRxUAdLjI9dzSp-_dHL67Ucwcdj72iLrZFFpPn07za-m7BpFzW4-xpLPs2EKxOzGIWMnw_aRnNIUteBd4kD8bw7V5WjfOSGP2RI0CXye_jTavvBZGf-kvMSdi8j04QM-XSvmA";
    private static final String CONSO_ORIGINE = "cats_consommateurorigine";
    private static final String CORR_ID = "CORR";
    private List<Setting> settings;

    private String structureId = "87800";


    @BeforeEach
    public void init(){
        when(request.getRequestURI()).thenReturn("/settings");
        settings = readObjectsFile("classpath:static/agc9Settings.json", Setting.class);
        mockInitialRequestInfos();
    }
    private void mockInitialRequestInfos() {
        Mockito.when(request.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn(AUTH);
        Mockito.when(request.getHeader(CATS_CONSOMMATEUR_ORIGINE_HEADER)).thenReturn(CONSO_ORIGINE);

        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setAttribute("CorrelationID", CORR_ID);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    }

    @Test
    public void givenValidCallInfoAndResultNullBody_ThenNull() throws InterApiException {
        //given
        HttpHeaders headers = new HttpHeaders();
        Mockito.when(interApiRequestService.getAiaHeaders(AUTH, CONSO_ORIGINE, CORR_ID)).thenReturn(headers);
        Mockito.when(abstractCallInterApi.getHttpHeaders(anyString())).thenReturn(headers);
        Mockito.when(restTemplate.exchange(anyString(),any(),any(), (ParameterizedTypeReference<Object>) any())).thenReturn(new ResponseEntity<>(null, headers, HttpStatus.OK));

        // when
        var result = agc9ClientService.getSettings(structureId);

       assertNull(result);
    }

    @Test
    public void givenValidCallInfoAndResultOk_Then200() throws InterApiException {
        //given
        HttpHeaders headers = new HttpHeaders();
        Mockito.when(interApiRequestService.getAiaHeaders(AUTH, CONSO_ORIGINE, CORR_ID)).thenReturn(headers);
        Mockito.when(abstractCallInterApi.getHttpHeaders(anyString())).thenReturn(headers);
        Mockito.when(restTemplate.exchange(anyString(),any(),any(), (ParameterizedTypeReference<Object>) any())).thenReturn(new ResponseEntity<>(settings, headers, HttpStatus.OK));

        // when
        var result = agc9ClientService.getSettings(structureId);
        assertNotNull(result);
    }
}
