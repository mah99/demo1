package fr.ca.cats.p0275.s2524.cal9.testsIntegration.utils;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;

@Getter
@ToString
public class ErreurMessage {

    @JsonProperty("status")
    private HttpStatus status;

    @JsonProperty("code")
    private String code;
    @JsonProperty("message")
    private String message;

    @JsonProperty("cause")
    private String cause;

    /**
     * Retourne le json pour les tests avec les clés reponse et message. Les autres champs sont ignorés
     * @return JSONObject jsonObject formatté pour les tests d'intégration ({reponse: 000, message: "XXXX"}
     */
    public JSONObject getJSON() {
        JSONObject jsonObject = new JSONObject();
        // Conversion du message en int puis ajout au JSON
        jsonObject.put(Constant.CLE_REPONSE, this.status.value());
        jsonObject.put(Constant.CLE_MESSAGE, this.message);
        return jsonObject;
    }
}