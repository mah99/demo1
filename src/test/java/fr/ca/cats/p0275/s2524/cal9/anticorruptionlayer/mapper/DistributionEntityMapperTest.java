package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.Horaire;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.PosteFonctionnel;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {DistributionEntityMapperImpl.class})
public class DistributionEntityMapperTest {

    @InjectMocks
    private DistributionEntityMapper mapper = DistributionEntityMapper.DISTRIBUTION_ENTITY_MAPPER;

    @Test
    public void posteFonctionnelToDistributionEntity() {
        // given
        List<Horaire> horaireList = getHoraires();
        PosteFonctionnel posteFonctionnel= PosteFonctionnel.builder()
                .code("123")
                .agenda(horaireList)
                .build();
        // when
        var result = mapper.posteFonctionnelToDistributionEntity(posteFonctionnel,"12345" ,"centre val loire");
        // then
        Assert.assertEquals("12345", result.getIdBranch());
        Assert.assertEquals(posteFonctionnel.getCode(), result.getJob().getId());
        Assert.assertEquals("centre val loire", result.getLabel());
    }

    private List<Horaire> getHoraires() {
        List<Horaire> horaireList = new ArrayList<>();
        Horaire horaire= Horaire.builder()
                .jour("LUNDI")
                .heureDebut("12:00:00")
                .heureFin("22:00:00")
                .build();
        horaireList.add(horaire);
        return horaireList;
    }
}