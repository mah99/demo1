package fr.ca.cats.p0275.s2524.cal9.infrastructure.service;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.SettingsClientService;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.service.impl.CollectSettingsServiceImpl;
import fr.ca.cats.p0275.s2524.cal9.model.Setting;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class CollectSettingsServiceTest {

    @Mock
    private SettingsClientService settingsClientService;

    private CollectSettingsService collectSettingsService;

    private String structureId = "87800";

    @BeforeEach
    public void init() {

        collectSettingsService = new CollectSettingsServiceImpl(settingsClientService);

    }

    @Test
    public void getSettings(){
        // given
        Setting setting = new Setting();
        setting.setCodeActivite("ABSENCE CONG");
        setting.setCouleurActivite("4FC068");
        setting.setLibelleActivite("ABSENCES, CONGES");
        when(settingsClientService.getSettings(structureId)).thenReturn(List.of(setting));

        // when
        var result = collectSettingsService.getSettings(structureId);

        // then
        Assert.assertEquals(setting, result.get(0));
        verify(settingsClientService).getSettings(structureId);
    }

}