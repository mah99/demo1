package fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ca.cats.p0070.s1889.easyrest.interapi.exception.InterApiException;
import fr.ca.cats.p0070.s1889.easyrest.interapi.service.InterApiRequestService;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.externalclient.impl.AgentClientServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

import java.io.IOException;
import java.util.List;

import static fr.ca.cats.p0070.s1889.easyrest.interapi.utils.Constants.CATS_CONSOMMATEUR_ORIGINE_HEADER;
import static fr.ca.cats.p0275.s2524.cal9.infrastructure.util.MapperUtils.readObjectFile;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class AgentClientServiceTest {

    @Mock
    private HttpServletRequest request;

    @Mock
    private AbstractCallInterApi<Reunion> abstractCallInterApi;

    @Mock
    private InterApiRequestService interApiRequestService;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private final AgentClientServiceImpl employeeClientService = new AgentClientServiceImpl();


    @Value("${aia.aut9.uri:/technical_authentication/v1/token}")
    private String aut9Uri;

    @Value("${aia.cache.duration.seconds:530}")
    private int cacheDurationSeconds;

    @Value("${aia.scopes:openid}")
    private String scopes;

    @Value("${authentification.config.apimGatewayUrl:https://dev1-private.api.credit-agricole.fr}")
    private String apimGatewayUrl;

    private Class<Employee> type;

    private static final String AUTH = "Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6InBJVGprQk82b1JOckNRTjdUaFZNNEd6MmRMcFFWMlEzWEhOTkVvajlfaTQifQ.eyJzdWIiOiJPVzYwMDAxIiwic2NvcGUiOiJvcGVuaWQiLCJpc3MiOiJzZGpfY29sbGFib3JhdGV1ci1kZXZ0dSIsInN0cnVjdHVyZV9pZCI6Ijg3ODAwIiwidHlwZSI6ImNvbGxhYm9yYXRldXIiLCJpYXQiOjE2MzUyMzkxMDksImF1dGhvcml6YXRpb25fY29udGV4dCI6eyJzZXNzaW9uX2lkIjoiOWMyMDkyOTgtOTE3Yy00YzY4LTliMDctOWI2NjBhMjliMTAwIiwidXNlcl90eXBlIjoiMDEiLCJ1c2VyX2lkIjoiT1c2MDAwMSIsImNsaWVudF9pZCI6ImVjNGE3YTdlNTA0ZTU0YzBiMzJlNTJkMWFjNWJiMTliZDE5YmE4YWY5OTA0OGZkZDM2Y2M5NzJhZDVhZDFjMzUiLCJ1c2VyX3VvbV9jb2RlIjoiODc4MDAifSwiYXV0aF90aW1lIjoxNjM1MjM5MTA5LCJkYXQiOnsidHlwb2xvZ2llIjoiY29sbGFib3JhdGV1ciIsImF1dGhlbnRpY2F0aW9uX2xldmVsIjoiMiJ9LCJleHAiOjE5NDUyNDUzOTQsImF1ZCI6WyJjYS1lc2IiXSwiQVNfSTAwMDFfdHlwZSI6ImNvbGxhYm9yYXRldXIiLCJqdGkiOiI1MWMyMDY5ZS1jNWMzLTQ1NmEtYjhjYi1iZWFmMThlZDYwYTMiLCJhY3IiOiIyMCJ9.GD1TP5n9zpbgSql8ipNNfEliMB0CPR7JVeurpAxuTA0rvxHUpjQBSs3eOFmhfzqdiJXhef6JPAqxYrXMpU4mI-7u2k5E1hAo3_4dcj6Mb0MQSYJNRwWn34oXHe1-hLQcIphLLforr_PA3rgYFV9MFYPeunEzhfXhy-BKa-i372VEyDsiTEWjDq80rQuKgCxEM4Z-Z-SAEMRN3JcYquRxUAdLjI9dzSp-_dHL67Ucwcdj72iLrZFFpPn07za-m7BpFzW4-xpLPs2EKxOzGIWMnw_aRnNIUteBd4kD8bw7V5WjfOSGP2RI0CXye_jTavvBZGf-kvMSdi8j04QM-XSvmA";
    private static final String CONSO_ORIGINE = "cats_consommateurorigine";
    private static final String CORR_ID = "CORR";

    @Value("${SDG09.employee.uri:/elements_structure/v1/employees/}")
    private String aiaAGC9Uri;

    private Employee employee;


    @BeforeEach
    public void init() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Resource resource = new ClassPathResource("static/sdg9EmployeeNumber.json");

        Object obj = mapper.readValue(resource.getInputStream(), Object.class);
        List<Employee> employeeList = new ObjectMapper().convertValue(obj, new TypeReference<>() {
        });
        employee = employeeList.stream().findFirst().orElse(null);
        mockInitialRequestInfos();
    }
    private void mockInitialRequestInfos() {
        when(request.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn(AUTH);
        when(request.getHeader(CATS_CONSOMMATEUR_ORIGINE_HEADER)).thenReturn(CONSO_ORIGINE);

        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setAttribute("CorrelationID", CORR_ID);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    }

    @Test
   public void getAiaHeaders() throws Exception {
        HttpHeaders headers = new HttpHeaders();

        when(interApiRequestService.getAiaHeaders(any(), any(), any())).thenReturn(headers);
        assertEquals(headers, employeeClientService.getAiaHeaders());
    }

    @Test
    public void getAiaHeadersCasErreur02() throws Exception {
        when(employeeClientService.getAiaHeaders())
                .thenThrow(new InterApiException("Required argument is null or empty"));

        var result = Assertions.assertThrows(CallInterApiBusinessException.class, () -> {
            employeeClientService.getEmployee("12345","");
        });
        assertEquals("Erreur lors de la lecture des headers pour l'uri: null?regional_bank_id=12345", result.getMessage());
    }

    @Test
    public void givenInvalidCallInfoHeader_ThenTechnicalException() throws InterApiException {
        //given
        HttpHeaders headers = new HttpHeaders();
        when(interApiRequestService.getAiaHeaders(any(), any(), any())).thenThrow(new InterApiException("Response from AUT9: null"));

        //when
        var result = Assertions.assertThrows(CallInterApiHeaderTechnicalException.class, () -> {
            employeeClientService.getEmployee("12345","");
        });
        //then
        assertEquals("Erreur lors du traitement des valeurs du header pour l'uri: null?regional_bank_id=12345", result.getMessage());
    }

    @Test
    public void givenValidCallInfoAndResultNullBody_ThenOk() throws InterApiException {
        //given
        HttpHeaders headers = new HttpHeaders();
        when(interApiRequestService.getAiaHeaders(any(), any(), any())).thenReturn(headers);
        when(abstractCallInterApi.getHttpHeaders(anyString())).thenReturn(headers);
        when(restTemplate.exchange(anyString(),any(),any(), (ParameterizedTypeReference<Object>) any())).thenReturn(new ResponseEntity<>(null, headers, HttpStatus.BAD_REQUEST));

        // when
        var result = employeeClientService.getEmployee("12345","");
        assertNull(result);
    }

    @Test
    public void givenInvalidCallInfo_ThenTechnicResilianceOk() throws InterApiException {
        //given
        HttpHeaders headers = new HttpHeaders();
        when(interApiRequestService.getAiaHeaders(any(), any(), any())).thenReturn(headers);

        when(abstractCallInterApi.getHttpHeaders(anyString())).thenReturn(headers);

        // when
        var result = employeeClientService.getEmployee("12345","12345");
        assertNull(result);
    }

    @Test
    public void givenValidCallInfoAndResultOk_Then200() throws InterApiException {
        //given
        HttpHeaders headers = new HttpHeaders();
        when(interApiRequestService.getAiaHeaders(any(), any(), any())).thenReturn(headers);
        when(abstractCallInterApi.getHttpHeaders(anyString())).thenReturn(headers);
        when(restTemplate.exchange(anyString(),any(),any(), (ParameterizedTypeReference<Object>) any())).thenReturn(new ResponseEntity<>(employee, headers, HttpStatus.OK));

        // when
        var result = employeeClientService.getEmployee("12345","12345");
        assertNotNull(result);
    }
}
