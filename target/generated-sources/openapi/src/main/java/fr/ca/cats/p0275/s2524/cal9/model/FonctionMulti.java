package fr.ca.cats.p0275.s2524.cal9.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import fr.ca.cats.p0275.s2524.cal9.model.AgentFonctionMulti;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * FonctionMulti
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-07-16T17:11:10.536411100+02:00[Europe/Paris]")
public class FonctionMulti   {
  @JsonProperty("code")
  private String code;

  @JsonProperty("libelle")
  private String libelle;

  @JsonProperty("agents")
  @Valid
  private List<AgentFonctionMulti> agents = null;

  public FonctionMulti code(String code) {
    this.code = code;
    return this;
  }

  /**
   * Code de la fonction
   * @return code
  */
  @ApiModelProperty(example = "0440", value = "Code de la fonction")


  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public FonctionMulti libelle(String libelle) {
    this.libelle = libelle;
    return this;
  }

  /**
   * Libellé de la fonction
   * @return libelle
  */
  @ApiModelProperty(example = "ATTACHE DE CLIENTELE", value = "Libellé de la fonction")


  public String getLibelle() {
    return libelle;
  }

  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  public FonctionMulti agents(List<AgentFonctionMulti> agents) {
    this.agents = agents;
    return this;
  }

  public FonctionMulti addAgentsItem(AgentFonctionMulti agentsItem) {
    if (this.agents == null) {
      this.agents = new ArrayList<>();
    }
    this.agents.add(agentsItem);
    return this;
  }

  /**
   * Get agents
   * @return agents
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<AgentFonctionMulti> getAgents() {
    return agents;
  }

  public void setAgents(List<AgentFonctionMulti> agents) {
    this.agents = agents;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FonctionMulti fonctionMulti = (FonctionMulti) o;
    return Objects.equals(this.code, fonctionMulti.code) &&
        Objects.equals(this.libelle, fonctionMulti.libelle) &&
        Objects.equals(this.agents, fonctionMulti.agents);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, libelle, agents);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class FonctionMulti {\n");
    
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    libelle: ").append(toIndentedString(libelle)).append("\n");
    sb.append("    agents: ").append(toIndentedString(agents)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

