package fr.ca.cats.p0275.s2524.cal9.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import fr.ca.cats.p0275.s2524.cal9.model.Adresse;
import fr.ca.cats.p0275.s2524.cal9.model.Agence;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CanalInteraction
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-07-16T17:11:10.536411100+02:00[Europe/Paris]")
public class CanalInteraction   {
  @JsonProperty("canal")
  private String canal;

  @JsonProperty("adresseExterieur")
  private Adresse adresseExterieur;

  @JsonProperty("agence")
  private Agence agence;

  @JsonProperty("telephone")
  private String telephone;

  @JsonProperty("urlVisio")
  private String urlVisio;

  public CanalInteraction canal(String canal) {
    this.canal = canal;
    return this;
  }

  /**
   * Type de contact
   * @return canal
  */
  @ApiModelProperty(example = "AGENCE", value = "Type de contact")

@Pattern(regexp = "^.*(AGENCE|TELEPHONE|VISIO|EXTERIEUR).*$") 
  public String getCanal() {
    return canal;
  }

  public void setCanal(String canal) {
    this.canal = canal;
  }

  public CanalInteraction adresseExterieur(Adresse adresseExterieur) {
    this.adresseExterieur = adresseExterieur;
    return this;
  }

  /**
   * Get adresseExterieur
   * @return adresseExterieur
  */
  @ApiModelProperty(value = "")

  @Valid

  public Adresse getAdresseExterieur() {
    return adresseExterieur;
  }

  public void setAdresseExterieur(Adresse adresseExterieur) {
    this.adresseExterieur = adresseExterieur;
  }

  public CanalInteraction agence(Agence agence) {
    this.agence = agence;
    return this;
  }

  /**
   * Get agence
   * @return agence
  */
  @ApiModelProperty(value = "")

  @Valid

  public Agence getAgence() {
    return agence;
  }

  public void setAgence(Agence agence) {
    this.agence = agence;
  }

  public CanalInteraction telephone(String telephone) {
    this.telephone = telephone;
    return this;
  }

  /**
   * Téléphone
   * @return telephone
  */
  @ApiModelProperty(example = "0634526589", value = "Téléphone")


  public String getTelephone() {
    return telephone;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  public CanalInteraction urlVisio(String urlVisio) {
    this.urlVisio = urlVisio;
    return this;
  }

  /**
   * Url de la visio agent
   * @return urlVisio
  */
  @ApiModelProperty(value = "Url de la visio agent")


  public String getUrlVisio() {
    return urlVisio;
  }

  public void setUrlVisio(String urlVisio) {
    this.urlVisio = urlVisio;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CanalInteraction canalInteraction = (CanalInteraction) o;
    return Objects.equals(this.canal, canalInteraction.canal) &&
        Objects.equals(this.adresseExterieur, canalInteraction.adresseExterieur) &&
        Objects.equals(this.agence, canalInteraction.agence) &&
        Objects.equals(this.telephone, canalInteraction.telephone) &&
        Objects.equals(this.urlVisio, canalInteraction.urlVisio);
  }

  @Override
  public int hashCode() {
    return Objects.hash(canal, adresseExterieur, agence, telephone, urlVisio);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CanalInteraction {\n");
    
    sb.append("    canal: ").append(toIndentedString(canal)).append("\n");
    sb.append("    adresseExterieur: ").append(toIndentedString(adresseExterieur)).append("\n");
    sb.append("    agence: ").append(toIndentedString(agence)).append("\n");
    sb.append("    telephone: ").append(toIndentedString(telephone)).append("\n");
    sb.append("    urlVisio: ").append(toIndentedString(urlVisio)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

