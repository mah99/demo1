package fr.ca.cats.p0275.s2524.cal9.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Partenaire
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-07-16T17:11:10.536411100+02:00[Europe/Paris]")
public class Partenaire   {
  @JsonProperty("id")
  private String id;

  @JsonProperty("libelle")
  private String libelle;

  public Partenaire id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Id du partenaire
   * @return id
  */
  @ApiModelProperty(example = "12345678912345", value = "Id du partenaire")

@Size(min = 14, max = 14) 
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Partenaire libelle(String libelle) {
    this.libelle = libelle;
    return this;
  }

  /**
   * Libellé du partenaire
   * @return libelle
  */
  @ApiModelProperty(example = "Mme Joel EUVRARD", value = "Libellé du partenaire")

@Size(min = 1, max = 255) 
  public String getLibelle() {
    return libelle;
  }

  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Partenaire partenaire = (Partenaire) o;
    return Objects.equals(this.id, partenaire.id) &&
        Objects.equals(this.libelle, partenaire.libelle);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, libelle);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Partenaire {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    libelle: ").append(toIndentedString(libelle)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

