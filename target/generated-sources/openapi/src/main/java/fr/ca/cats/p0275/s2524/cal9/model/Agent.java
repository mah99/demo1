package fr.ca.cats.p0275.s2524.cal9.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Agent
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-07-16T17:11:10.536411100+02:00[Europe/Paris]")
public class Agent   {
  @JsonProperty("id")
  private String id;

  @JsonProperty("nom")
  private String nom;

  @JsonProperty("prenom")
  private String prenom;

  @JsonProperty("civilite")
  private String civilite;

  public Agent id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Id de l'agent
   * @return id
  */
  @ApiModelProperty(example = "12345", value = "Id de l'agent")

@Size(min = 5, max = 5) 
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Agent nom(String nom) {
    this.nom = nom;
    return this;
  }

  /**
   * Nom de l'agent
   * @return nom
  */
  @ApiModelProperty(example = "Duchesne", value = "Nom de l'agent")

@Size(min = 1, max = 255) 
  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public Agent prenom(String prenom) {
    this.prenom = prenom;
    return this;
  }

  /**
   * Prénom de l'agent
   * @return prenom
  */
  @ApiModelProperty(example = "Marc", value = "Prénom de l'agent")

@Size(min = 1, max = 255) 
  public String getPrenom() {
    return prenom;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }

  public Agent civilite(String civilite) {
    this.civilite = civilite;
    return this;
  }

  /**
   * Civilité de l'agent
   * @return civilite
  */
  @ApiModelProperty(example = "Mr", value = "Civilité de l'agent")

@Size(min = 1, max = 14) 
  public String getCivilite() {
    return civilite;
  }

  public void setCivilite(String civilite) {
    this.civilite = civilite;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Agent agent = (Agent) o;
    return Objects.equals(this.id, agent.id) &&
        Objects.equals(this.nom, agent.nom) &&
        Objects.equals(this.prenom, agent.prenom) &&
        Objects.equals(this.civilite, agent.civilite);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, nom, prenom, civilite);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Agent {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    nom: ").append(toIndentedString(nom)).append("\n");
    sb.append("    prenom: ").append(toIndentedString(prenom)).append("\n");
    sb.append("    civilite: ").append(toIndentedString(civilite)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

