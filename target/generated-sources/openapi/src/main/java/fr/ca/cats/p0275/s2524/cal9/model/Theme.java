package fr.ca.cats.p0275.s2524.cal9.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Theme
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-07-16T17:11:10.536411100+02:00[Europe/Paris]")
public class Theme   {
  @JsonProperty("id")
  private String id;

  @JsonProperty("libelle")
  private String libelle;

  public Theme id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Id du thème de la réunion
   * @return id
  */
  @ApiModelProperty(example = "BL", value = "Id du thème de la réunion")

@Size(min = 2) 
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Theme libelle(String libelle) {
    this.libelle = libelle;
    return this;
  }

  /**
   * Libellé du thème de la réunion
   * @return libelle
  */
  @ApiModelProperty(example = "Comptes et cartes", value = "Libellé du thème de la réunion")

@Size(min = 1, max = 60) 
  public String getLibelle() {
    return libelle;
  }

  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Theme theme = (Theme) o;
    return Objects.equals(this.id, theme.id) &&
        Objects.equals(this.libelle, theme.libelle);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, libelle);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Theme {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    libelle: ").append(toIndentedString(libelle)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

