package fr.ca.cats.p0275.s2524.cal9.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import fr.ca.cats.p0275.s2524.cal9.model.Agent;
import fr.ca.cats.p0275.s2524.cal9.model.CanalInteraction;
import fr.ca.cats.p0275.s2524.cal9.model.Expert;
import fr.ca.cats.p0275.s2524.cal9.model.Objet;
import fr.ca.cats.p0275.s2524.cal9.model.Partenaire;
import fr.ca.cats.p0275.s2524.cal9.model.PiecesAFournir;
import fr.ca.cats.p0275.s2524.cal9.model.ReunionContexte;
import fr.ca.cats.p0275.s2524.cal9.model.Theme;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Reunion
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-07-16T17:11:10.536411100+02:00[Europe/Paris]")
public class Reunion   {
  @JsonProperty("reunionContexte")
  private ReunionContexte reunionContexte;

  @JsonProperty("agentCreateur")
  private Agent agentCreateur;

  @JsonProperty("dateDebut")
  private String dateDebut;

  @JsonProperty("dateFin")
  private String dateFin;

  @JsonProperty("typeReunion")
  private String typeReunion;

  @JsonProperty("partenaires")
  @Valid
  private List<Partenaire> partenaires = null;

  @JsonProperty("partenairesVizio")
  @Valid
  private List<Partenaire> partenairesVizio = null;

  @JsonProperty("experts")
  @Valid
  private List<Expert> experts = null;

  @JsonProperty("theme")
  private Theme theme;

  @JsonProperty("objet")
  private Objet objet;

  @JsonProperty("canalInteraction")
  private CanalInteraction canalInteraction;

  @JsonProperty("piecesAFournir")
  @Valid
  private List<PiecesAFournir> piecesAFournir = null;

  @JsonProperty("commentaire")
  private String commentaire;

  @JsonProperty("plageProtegee")
  private Boolean plageProtegee;

  public Reunion reunionContexte(ReunionContexte reunionContexte) {
    this.reunionContexte = reunionContexte;
    return this;
  }

  /**
   * Get reunionContexte
   * @return reunionContexte
  */
  @ApiModelProperty(value = "")

  @Valid

  public ReunionContexte getReunionContexte() {
    return reunionContexte;
  }

  public void setReunionContexte(ReunionContexte reunionContexte) {
    this.reunionContexte = reunionContexte;
  }

  public Reunion agentCreateur(Agent agentCreateur) {
    this.agentCreateur = agentCreateur;
    return this;
  }

  /**
   * Get agentCreateur
   * @return agentCreateur
  */
  @ApiModelProperty(value = "")

  @Valid

  public Agent getAgentCreateur() {
    return agentCreateur;
  }

  public void setAgentCreateur(Agent agentCreateur) {
    this.agentCreateur = agentCreateur;
  }

  public Reunion dateDebut(String dateDebut) {
    this.dateDebut = dateDebut;
    return this;
  }

  /**
   * Date de début de la reunion
   * @return dateDebut
  */
  @ApiModelProperty(example = "2021-09-22T08:15:00.000+0000", value = "Date de début de la reunion")


  public String getDateDebut() {
    return dateDebut;
  }

  public void setDateDebut(String dateDebut) {
    this.dateDebut = dateDebut;
  }

  public Reunion dateFin(String dateFin) {
    this.dateFin = dateFin;
    return this;
  }

  /**
   * Date de fin de la reunion
   * @return dateFin
  */
  @ApiModelProperty(example = "2021-09-23T08:15:00.000+0000", value = "Date de fin de la reunion")


  public String getDateFin() {
    return dateFin;
  }

  public void setDateFin(String dateFin) {
    this.dateFin = dateFin;
  }

  public Reunion typeReunion(String typeReunion) {
    this.typeReunion = typeReunion;
    return this;
  }

  /**
   * Type de la réunion
   * @return typeReunion
  */
  @ApiModelProperty(example = "ACTIVITE,RENDEZ_VOUS", value = "Type de la réunion")

@Pattern(regexp = "^.*(ACTIVITE|RENDEZ_VOUS).*$") 
  public String getTypeReunion() {
    return typeReunion;
  }

  public void setTypeReunion(String typeReunion) {
    this.typeReunion = typeReunion;
  }

  public Reunion partenaires(List<Partenaire> partenaires) {
    this.partenaires = partenaires;
    return this;
  }

  public Reunion addPartenairesItem(Partenaire partenairesItem) {
    if (this.partenaires == null) {
      this.partenaires = new ArrayList<>();
    }
    this.partenaires.add(partenairesItem);
    return this;
  }

  /**
   * Get partenaires
   * @return partenaires
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<Partenaire> getPartenaires() {
    return partenaires;
  }

  public void setPartenaires(List<Partenaire> partenaires) {
    this.partenaires = partenaires;
  }

  public Reunion partenairesVizio(List<Partenaire> partenairesVizio) {
    this.partenairesVizio = partenairesVizio;
    return this;
  }

  public Reunion addPartenairesVizioItem(Partenaire partenairesVizioItem) {
    if (this.partenairesVizio == null) {
      this.partenairesVizio = new ArrayList<>();
    }
    this.partenairesVizio.add(partenairesVizioItem);
    return this;
  }

  /**
   * Get partenairesVizio
   * @return partenairesVizio
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<Partenaire> getPartenairesVizio() {
    return partenairesVizio;
  }

  public void setPartenairesVizio(List<Partenaire> partenairesVizio) {
    this.partenairesVizio = partenairesVizio;
  }

  public Reunion experts(List<Expert> experts) {
    this.experts = experts;
    return this;
  }

  public Reunion addExpertsItem(Expert expertsItem) {
    if (this.experts == null) {
      this.experts = new ArrayList<>();
    }
    this.experts.add(expertsItem);
    return this;
  }

  /**
   * Get experts
   * @return experts
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<Expert> getExperts() {
    return experts;
  }

  public void setExperts(List<Expert> experts) {
    this.experts = experts;
  }

  public Reunion theme(Theme theme) {
    this.theme = theme;
    return this;
  }

  /**
   * Get theme
   * @return theme
  */
  @ApiModelProperty(value = "")

  @Valid

  public Theme getTheme() {
    return theme;
  }

  public void setTheme(Theme theme) {
    this.theme = theme;
  }

  public Reunion objet(Objet objet) {
    this.objet = objet;
    return this;
  }

  /**
   * Get objet
   * @return objet
  */
  @ApiModelProperty(value = "")

  @Valid

  public Objet getObjet() {
    return objet;
  }

  public void setObjet(Objet objet) {
    this.objet = objet;
  }

  public Reunion canalInteraction(CanalInteraction canalInteraction) {
    this.canalInteraction = canalInteraction;
    return this;
  }

  /**
   * Get canalInteraction
   * @return canalInteraction
  */
  @ApiModelProperty(value = "")

  @Valid

  public CanalInteraction getCanalInteraction() {
    return canalInteraction;
  }

  public void setCanalInteraction(CanalInteraction canalInteraction) {
    this.canalInteraction = canalInteraction;
  }

  public Reunion piecesAFournir(List<PiecesAFournir> piecesAFournir) {
    this.piecesAFournir = piecesAFournir;
    return this;
  }

  public Reunion addPiecesAFournirItem(PiecesAFournir piecesAFournirItem) {
    if (this.piecesAFournir == null) {
      this.piecesAFournir = new ArrayList<>();
    }
    this.piecesAFournir.add(piecesAFournirItem);
    return this;
  }

  /**
   * Get piecesAFournir
   * @return piecesAFournir
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<PiecesAFournir> getPiecesAFournir() {
    return piecesAFournir;
  }

  public void setPiecesAFournir(List<PiecesAFournir> piecesAFournir) {
    this.piecesAFournir = piecesAFournir;
  }

  public Reunion commentaire(String commentaire) {
    this.commentaire = commentaire;
    return this;
  }

  /**
   * Commentaire attaché à la réunion
   * @return commentaire
  */
  @ApiModelProperty(example = "Un commentaire en bonne et due forme.", value = "Commentaire attaché à la réunion")

@Size(min = 0, max = 240) 
  public String getCommentaire() {
    return commentaire;
  }

  public void setCommentaire(String commentaire) {
    this.commentaire = commentaire;
  }

  public Reunion plageProtegee(Boolean plageProtegee) {
    this.plageProtegee = plageProtegee;
    return this;
  }

  /**
   * Réunion déplaçable
   * @return plageProtegee
  */
  @ApiModelProperty(example = "true", value = "Réunion déplaçable")


  public Boolean getPlageProtegee() {
    return plageProtegee;
  }

  public void setPlageProtegee(Boolean plageProtegee) {
    this.plageProtegee = plageProtegee;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Reunion reunion = (Reunion) o;
    return Objects.equals(this.reunionContexte, reunion.reunionContexte) &&
        Objects.equals(this.agentCreateur, reunion.agentCreateur) &&
        Objects.equals(this.dateDebut, reunion.dateDebut) &&
        Objects.equals(this.dateFin, reunion.dateFin) &&
        Objects.equals(this.typeReunion, reunion.typeReunion) &&
        Objects.equals(this.partenaires, reunion.partenaires) &&
        Objects.equals(this.partenairesVizio, reunion.partenairesVizio) &&
        Objects.equals(this.experts, reunion.experts) &&
        Objects.equals(this.theme, reunion.theme) &&
        Objects.equals(this.objet, reunion.objet) &&
        Objects.equals(this.canalInteraction, reunion.canalInteraction) &&
        Objects.equals(this.piecesAFournir, reunion.piecesAFournir) &&
        Objects.equals(this.commentaire, reunion.commentaire) &&
        Objects.equals(this.plageProtegee, reunion.plageProtegee);
  }

  @Override
  public int hashCode() {
    return Objects.hash(reunionContexte, agentCreateur, dateDebut, dateFin, typeReunion, partenaires, partenairesVizio, experts, theme, objet, canalInteraction, piecesAFournir, commentaire, plageProtegee);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Reunion {\n");
    
    sb.append("    reunionContexte: ").append(toIndentedString(reunionContexte)).append("\n");
    sb.append("    agentCreateur: ").append(toIndentedString(agentCreateur)).append("\n");
    sb.append("    dateDebut: ").append(toIndentedString(dateDebut)).append("\n");
    sb.append("    dateFin: ").append(toIndentedString(dateFin)).append("\n");
    sb.append("    typeReunion: ").append(toIndentedString(typeReunion)).append("\n");
    sb.append("    partenaires: ").append(toIndentedString(partenaires)).append("\n");
    sb.append("    partenairesVizio: ").append(toIndentedString(partenairesVizio)).append("\n");
    sb.append("    experts: ").append(toIndentedString(experts)).append("\n");
    sb.append("    theme: ").append(toIndentedString(theme)).append("\n");
    sb.append("    objet: ").append(toIndentedString(objet)).append("\n");
    sb.append("    canalInteraction: ").append(toIndentedString(canalInteraction)).append("\n");
    sb.append("    piecesAFournir: ").append(toIndentedString(piecesAFournir)).append("\n");
    sb.append("    commentaire: ").append(toIndentedString(commentaire)).append("\n");
    sb.append("    plageProtegee: ").append(toIndentedString(plageProtegee)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

