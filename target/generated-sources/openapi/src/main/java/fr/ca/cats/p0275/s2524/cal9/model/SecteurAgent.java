package fr.ca.cats.p0275.s2524.cal9.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import fr.ca.cats.p0275.s2524.cal9.model.Fonction;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SecteurAgent
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-07-16T17:11:10.536411100+02:00[Europe/Paris]")
public class SecteurAgent   {
  @JsonProperty("id")
  private String id;

  @JsonProperty("niveauEds")
  private String niveauEds;

  @JsonProperty("structureId")
  private String structureId;

  @JsonProperty("libelle")
  private String libelle;

  @JsonProperty("libelleCourt")
  private String libelleCourt;

  @JsonProperty("fonctions")
  @Valid
  private List<Fonction> fonctions = null;

  public SecteurAgent id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Id de l'eds
   * @return id
  */
  @ApiModelProperty(example = "00950", value = "Id de l'eds")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public SecteurAgent niveauEds(String niveauEds) {
    this.niveauEds = niveauEds;
    return this;
  }

  /**
   * le niveau de l'eds
   * @return niveauEds
  */
  @ApiModelProperty(example = "05", value = "le niveau de l'eds")


  public String getNiveauEds() {
    return niveauEds;
  }

  public void setNiveauEds(String niveauEds) {
    this.niveauEds = niveauEds;
  }

  public SecteurAgent structureId(String structureId) {
    this.structureId = structureId;
    return this;
  }

  /**
   * Identifiant de la caisse régionale
   * @return structureId
  */
  @ApiModelProperty(example = "87800", value = "Identifiant de la caisse régionale")


  public String getStructureId() {
    return structureId;
  }

  public void setStructureId(String structureId) {
    this.structureId = structureId;
  }

  public SecteurAgent libelle(String libelle) {
    this.libelle = libelle;
    return this;
  }

  /**
   * Libellé de l'EDS
   * @return libelle
  */
  @ApiModelProperty(example = "Louhans", value = "Libellé de l'EDS")


  public String getLibelle() {
    return libelle;
  }

  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  public SecteurAgent libelleCourt(String libelleCourt) {
    this.libelleCourt = libelleCourt;
    return this;
  }

  /**
   * Libellé court de l'EDS
   * @return libelleCourt
  */
  @ApiModelProperty(example = "Louhans", value = "Libellé court de l'EDS")


  public String getLibelleCourt() {
    return libelleCourt;
  }

  public void setLibelleCourt(String libelleCourt) {
    this.libelleCourt = libelleCourt;
  }

  public SecteurAgent fonctions(List<Fonction> fonctions) {
    this.fonctions = fonctions;
    return this;
  }

  public SecteurAgent addFonctionsItem(Fonction fonctionsItem) {
    if (this.fonctions == null) {
      this.fonctions = new ArrayList<>();
    }
    this.fonctions.add(fonctionsItem);
    return this;
  }

  /**
   * Get fonctions
   * @return fonctions
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<Fonction> getFonctions() {
    return fonctions;
  }

  public void setFonctions(List<Fonction> fonctions) {
    this.fonctions = fonctions;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SecteurAgent secteurAgent = (SecteurAgent) o;
    return Objects.equals(this.id, secteurAgent.id) &&
        Objects.equals(this.niveauEds, secteurAgent.niveauEds) &&
        Objects.equals(this.structureId, secteurAgent.structureId) &&
        Objects.equals(this.libelle, secteurAgent.libelle) &&
        Objects.equals(this.libelleCourt, secteurAgent.libelleCourt) &&
        Objects.equals(this.fonctions, secteurAgent.fonctions);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, niveauEds, structureId, libelle, libelleCourt, fonctions);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SecteurAgent {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    niveauEds: ").append(toIndentedString(niveauEds)).append("\n");
    sb.append("    structureId: ").append(toIndentedString(structureId)).append("\n");
    sb.append("    libelle: ").append(toIndentedString(libelle)).append("\n");
    sb.append("    libelleCourt: ").append(toIndentedString(libelleCourt)).append("\n");
    sb.append("    fonctions: ").append(toIndentedString(fonctions)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

