package fr.ca.cats.p0275.s2524.cal9.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * PiecesAFournir
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-07-16T17:11:10.536411100+02:00[Europe/Paris]")
public class PiecesAFournir   {
  @JsonProperty("id")
  private String id;

  @JsonProperty("libelle")
  private String libelle;

  @JsonProperty("ordre")
  private Integer ordre;

  public PiecesAFournir id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Identifiant de la pièce justificative
   * @return id
  */
  @ApiModelProperty(required = true, value = "Identifiant de la pièce justificative")
  @NotNull


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public PiecesAFournir libelle(String libelle) {
    this.libelle = libelle;
    return this;
  }

  /**
   * Libellé de la pièce justificative
   * @return libelle
  */
  @ApiModelProperty(required = true, value = "Libellé de la pièce justificative")
  @NotNull


  public String getLibelle() {
    return libelle;
  }

  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  public PiecesAFournir ordre(Integer ordre) {
    this.ordre = ordre;
    return this;
  }

  /**
   * Numéro d'affichage
   * @return ordre
  */
  @ApiModelProperty(required = true, value = "Numéro d'affichage")
  @NotNull


  public Integer getOrdre() {
    return ordre;
  }

  public void setOrdre(Integer ordre) {
    this.ordre = ordre;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PiecesAFournir piecesAFournir = (PiecesAFournir) o;
    return Objects.equals(this.id, piecesAFournir.id) &&
        Objects.equals(this.libelle, piecesAFournir.libelle) &&
        Objects.equals(this.ordre, piecesAFournir.ordre);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, libelle, ordre);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PiecesAFournir {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    libelle: ").append(toIndentedString(libelle)).append("\n");
    sb.append("    ordre: ").append(toIndentedString(ordre)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

