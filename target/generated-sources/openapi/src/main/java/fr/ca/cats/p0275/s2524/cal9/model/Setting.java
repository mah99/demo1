package fr.ca.cats.p0275.s2524.cal9.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Setting
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-07-16T17:11:10.536411100+02:00[Europe/Paris]")
public class Setting   {
  @JsonProperty("codeActivite")
  private String codeActivite;

  @JsonProperty("libelleActivite")
  private String libelleActivite;

  @JsonProperty("couleurActivite")
  private String couleurActivite;

  public Setting codeActivite(String codeActivite) {
    this.codeActivite = codeActivite;
    return this;
  }

  /**
   * Code de l'activité
   * @return codeActivite
  */
  @ApiModelProperty(example = "LIM", required = true, value = "Code de l'activité")
  @NotNull

@Size(min = 3, max = 3) 
  public String getCodeActivite() {
    return codeActivite;
  }

  public void setCodeActivite(String codeActivite) {
    this.codeActivite = codeActivite;
  }

  public Setting libelleActivite(String libelleActivite) {
    this.libelleActivite = libelleActivite;
    return this;
  }

  /**
   * Libelle de l'activité
   * @return libelleActivite
  */
  @ApiModelProperty(example = "LIMITES HORAIRES", required = true, value = "Libelle de l'activité")
  @NotNull


  public String getLibelleActivite() {
    return libelleActivite;
  }

  public void setLibelleActivite(String libelleActivite) {
    this.libelleActivite = libelleActivite;
  }

  public Setting couleurActivite(String couleurActivite) {
    this.couleurActivite = couleurActivite;
    return this;
  }

  /**
   * code couleur de l'activité
   * @return couleurActivite
  */
  @ApiModelProperty(example = "FF5733", value = "code couleur de l'activité")


  public String getCouleurActivite() {
    return couleurActivite;
  }

  public void setCouleurActivite(String couleurActivite) {
    this.couleurActivite = couleurActivite;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Setting setting = (Setting) o;
    return Objects.equals(this.codeActivite, setting.codeActivite) &&
        Objects.equals(this.libelleActivite, setting.libelleActivite) &&
        Objects.equals(this.couleurActivite, setting.couleurActivite);
  }

  @Override
  public int hashCode() {
    return Objects.hash(codeActivite, libelleActivite, couleurActivite);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Setting {\n");
    
    sb.append("    codeActivite: ").append(toIndentedString(codeActivite)).append("\n");
    sb.append("    libelleActivite: ").append(toIndentedString(libelleActivite)).append("\n");
    sb.append("    couleurActivite: ").append(toIndentedString(couleurActivite)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

