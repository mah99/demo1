package fr.ca.cats.p0275.s2524.cal9.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * AgentFonction
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-07-16T17:11:10.536411100+02:00[Europe/Paris]")
public class AgentFonction   {
  @JsonProperty("id")
  private String id;

  @JsonProperty("nom")
  private String nom;

  @JsonProperty("prenom")
  private String prenom;

  public AgentFonction id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Identifiant de l'agent
   * @return id
  */
  @ApiModelProperty(example = "8780000109", value = "Identifiant de l'agent")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public AgentFonction nom(String nom) {
    this.nom = nom;
    return this;
  }

  /**
   * Nom de l'agent
   * @return nom
  */
  @ApiModelProperty(example = "Dupuis", value = "Nom de l'agent")


  public String getNom() {
    return nom;
  }

  public void setNom(String nom) {
    this.nom = nom;
  }

  public AgentFonction prenom(String prenom) {
    this.prenom = prenom;
    return this;
  }

  /**
   * Prénom de l'agent
   * @return prenom
  */
  @ApiModelProperty(example = "Pierre", value = "Prénom de l'agent")


  public String getPrenom() {
    return prenom;
  }

  public void setPrenom(String prenom) {
    this.prenom = prenom;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AgentFonction agentFonction = (AgentFonction) o;
    return Objects.equals(this.id, agentFonction.id) &&
        Objects.equals(this.nom, agentFonction.nom) &&
        Objects.equals(this.prenom, agentFonction.prenom);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, nom, prenom);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AgentFonction {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    nom: ").append(toIndentedString(nom)).append("\n");
    sb.append("    prenom: ").append(toIndentedString(prenom)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

