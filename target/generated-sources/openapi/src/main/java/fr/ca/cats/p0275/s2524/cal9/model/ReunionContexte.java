package fr.ca.cats.p0275.s2524.cal9.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ReunionContexte
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-07-16T17:11:10.536411100+02:00[Europe/Paris]")
public class ReunionContexte   {
  @JsonProperty("agentId")
  private String agentId;

  @JsonProperty("dateReunion")
  private String dateReunion;

  @JsonProperty("heureDebutReunion")
  private String heureDebutReunion;

  public ReunionContexte agentId(String agentId) {
    this.agentId = agentId;
    return this;
  }

  /**
   * le matricule de l'agent
   * @return agentId
  */
  @ApiModelProperty(example = "40300", value = "le matricule de l'agent")


  public String getAgentId() {
    return agentId;
  }

  public void setAgentId(String agentId) {
    this.agentId = agentId;
  }

  public ReunionContexte dateReunion(String dateReunion) {
    this.dateReunion = dateReunion;
    return this;
  }

  /**
   * date de la reunion
   * @return dateReunion
  */
  @ApiModelProperty(example = "20220714", required = true, value = "date de la reunion")
  @NotNull


  public String getDateReunion() {
    return dateReunion;
  }

  public void setDateReunion(String dateReunion) {
    this.dateReunion = dateReunion;
  }

  public ReunionContexte heureDebutReunion(String heureDebutReunion) {
    this.heureDebutReunion = heureDebutReunion;
    return this;
  }

  /**
   * heure de debut de la reunion
   * @return heureDebutReunion
  */
  @ApiModelProperty(example = "10.30", value = "heure de debut de la reunion")


  public String getHeureDebutReunion() {
    return heureDebutReunion;
  }

  public void setHeureDebutReunion(String heureDebutReunion) {
    this.heureDebutReunion = heureDebutReunion;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ReunionContexte reunionContexte = (ReunionContexte) o;
    return Objects.equals(this.agentId, reunionContexte.agentId) &&
        Objects.equals(this.dateReunion, reunionContexte.dateReunion) &&
        Objects.equals(this.heureDebutReunion, reunionContexte.heureDebutReunion);
  }

  @Override
  public int hashCode() {
    return Objects.hash(agentId, dateReunion, heureDebutReunion);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ReunionContexte {\n");
    
    sb.append("    agentId: ").append(toIndentedString(agentId)).append("\n");
    sb.append("    dateReunion: ").append(toIndentedString(dateReunion)).append("\n");
    sb.append("    heureDebutReunion: ").append(toIndentedString(heureDebutReunion)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

