package fr.ca.cats.p0275.s2524.cal9.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import fr.ca.cats.p0275.s2524.cal9.model.Adresse;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Agence
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-07-16T17:11:10.536411100+02:00[Europe/Paris]")
public class Agence   {
  @JsonProperty("id")
  private String id;

  @JsonProperty("libelle")
  private String libelle;

  @JsonProperty("adresse")
  private Adresse adresse;

  public Agence id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Id de l'agence de la réunion
   * @return id
  */
  @ApiModelProperty(example = "00950", value = "Id de l'agence de la réunion")

@Size(min = 5, max = 5) 
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Agence libelle(String libelle) {
    this.libelle = libelle;
    return this;
  }

  /**
   * Libellé de l'agence de la réunion
   * @return libelle
  */
  @ApiModelProperty(example = "Louhans", value = "Libellé de l'agence de la réunion")

@Size(max = 255) 
  public String getLibelle() {
    return libelle;
  }

  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  public Agence adresse(Adresse adresse) {
    this.adresse = adresse;
    return this;
  }

  /**
   * Get adresse
   * @return adresse
  */
  @ApiModelProperty(value = "")

  @Valid

  public Adresse getAdresse() {
    return adresse;
  }

  public void setAdresse(Adresse adresse) {
    this.adresse = adresse;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Agence agence = (Agence) o;
    return Objects.equals(this.id, agence.id) &&
        Objects.equals(this.libelle, agence.libelle) &&
        Objects.equals(this.adresse, agence.adresse);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, libelle, adresse);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Agence {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    libelle: ").append(toIndentedString(libelle)).append("\n");
    sb.append("    adresse: ").append(toIndentedString(adresse)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

