package fr.ca.cats.p0275.s2524.cal9.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import fr.ca.cats.p0275.s2524.cal9.model.Agent;
import fr.ca.cats.p0275.s2524.cal9.model.LocalisationAgenda;
import fr.ca.cats.p0275.s2524.cal9.model.Reunion;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Calendrier
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-07-16T17:11:10.536411100+02:00[Europe/Paris]")
public class Calendrier   {
  @JsonProperty("agent")
  private Agent agent;

  @JsonProperty("reunions")
  @Valid
  private List<Reunion> reunions = null;

  @JsonProperty("localisationsAgenda")
  @Valid
  private List<LocalisationAgenda> localisationsAgenda = null;

  public Calendrier agent(Agent agent) {
    this.agent = agent;
    return this;
  }

  /**
   * Get agent
   * @return agent
  */
  @ApiModelProperty(value = "")

  @Valid

  public Agent getAgent() {
    return agent;
  }

  public void setAgent(Agent agent) {
    this.agent = agent;
  }

  public Calendrier reunions(List<Reunion> reunions) {
    this.reunions = reunions;
    return this;
  }

  public Calendrier addReunionsItem(Reunion reunionsItem) {
    if (this.reunions == null) {
      this.reunions = new ArrayList<>();
    }
    this.reunions.add(reunionsItem);
    return this;
  }

  /**
   * Get reunions
   * @return reunions
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<Reunion> getReunions() {
    return reunions;
  }

  public void setReunions(List<Reunion> reunions) {
    this.reunions = reunions;
  }

  public Calendrier localisationsAgenda(List<LocalisationAgenda> localisationsAgenda) {
    this.localisationsAgenda = localisationsAgenda;
    return this;
  }

  public Calendrier addLocalisationsAgendaItem(LocalisationAgenda localisationsAgendaItem) {
    if (this.localisationsAgenda == null) {
      this.localisationsAgenda = new ArrayList<>();
    }
    this.localisationsAgenda.add(localisationsAgendaItem);
    return this;
  }

  /**
   * Get localisationsAgenda
   * @return localisationsAgenda
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<LocalisationAgenda> getLocalisationsAgenda() {
    return localisationsAgenda;
  }

  public void setLocalisationsAgenda(List<LocalisationAgenda> localisationsAgenda) {
    this.localisationsAgenda = localisationsAgenda;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Calendrier calendrier = (Calendrier) o;
    return Objects.equals(this.agent, calendrier.agent) &&
        Objects.equals(this.reunions, calendrier.reunions) &&
        Objects.equals(this.localisationsAgenda, calendrier.localisationsAgenda);
  }

  @Override
  public int hashCode() {
    return Objects.hash(agent, reunions, localisationsAgenda);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Calendrier {\n");
    
    sb.append("    agent: ").append(toIndentedString(agent)).append("\n");
    sb.append("    reunions: ").append(toIndentedString(reunions)).append("\n");
    sb.append("    localisationsAgenda: ").append(toIndentedString(localisationsAgenda)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

