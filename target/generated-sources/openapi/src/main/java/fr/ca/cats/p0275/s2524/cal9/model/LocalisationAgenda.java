package fr.ca.cats.p0275.s2524.cal9.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * LocalisationAgenda
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-07-16T17:11:10.536411100+02:00[Europe/Paris]")
public class LocalisationAgenda   {
  @JsonProperty("jour")
  private String jour;

  @JsonProperty("heureDebut")
  private String heureDebut;

  @JsonProperty("heureFin")
  private String heureFin;

  @JsonProperty("posteFonctionnel")
  private String posteFonctionnel;

  @JsonProperty("agenceId")
  private String agenceId;

  @JsonProperty("libelle")
  private String libelle;

  public LocalisationAgenda jour(String jour) {
    this.jour = jour;
    return this;
  }

  /**
   * Jour de la localisation
   * @return jour
  */
  @ApiModelProperty(example = "LUNDI", value = "Jour de la localisation")


  public String getJour() {
    return jour;
  }

  public void setJour(String jour) {
    this.jour = jour;
  }

  public LocalisationAgenda heureDebut(String heureDebut) {
    this.heureDebut = heureDebut;
    return this;
  }

  /**
   * Heure de début de la localisation
   * @return heureDebut
  */
  @ApiModelProperty(example = "10:15:00", value = "Heure de début de la localisation")


  public String getHeureDebut() {
    return heureDebut;
  }

  public void setHeureDebut(String heureDebut) {
    this.heureDebut = heureDebut;
  }

  public LocalisationAgenda heureFin(String heureFin) {
    this.heureFin = heureFin;
    return this;
  }

  /**
   * Heure de fin de la localisation
   * @return heureFin
  */
  @ApiModelProperty(example = "10:15:00", value = "Heure de fin de la localisation")


  public String getHeureFin() {
    return heureFin;
  }

  public void setHeureFin(String heureFin) {
    this.heureFin = heureFin;
  }

  public LocalisationAgenda posteFonctionnel(String posteFonctionnel) {
    this.posteFonctionnel = posteFonctionnel;
    return this;
  }

  /**
   * Poste fonctionnel
   * @return posteFonctionnel
  */
  @ApiModelProperty(example = "0302", value = "Poste fonctionnel")

@Size(min = 4, max = 4) 
  public String getPosteFonctionnel() {
    return posteFonctionnel;
  }

  public void setPosteFonctionnel(String posteFonctionnel) {
    this.posteFonctionnel = posteFonctionnel;
  }

  public LocalisationAgenda agenceId(String agenceId) {
    this.agenceId = agenceId;
    return this;
  }

  /**
   * Id de l'agence
   * @return agenceId
  */
  @ApiModelProperty(example = "00302", value = "Id de l'agence")

@Size(min = 5, max = 5) 
  public String getAgenceId() {
    return agenceId;
  }

  public void setAgenceId(String agenceId) {
    this.agenceId = agenceId;
  }

  public LocalisationAgenda libelle(String libelle) {
    this.libelle = libelle;
    return this;
  }

  /**
   * Libellé de l'agence
   * @return libelle
  */
  @ApiModelProperty(example = "Louhans", value = "Libellé de l'agence")

@Size(max = 255) 
  public String getLibelle() {
    return libelle;
  }

  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LocalisationAgenda localisationAgenda = (LocalisationAgenda) o;
    return Objects.equals(this.jour, localisationAgenda.jour) &&
        Objects.equals(this.heureDebut, localisationAgenda.heureDebut) &&
        Objects.equals(this.heureFin, localisationAgenda.heureFin) &&
        Objects.equals(this.posteFonctionnel, localisationAgenda.posteFonctionnel) &&
        Objects.equals(this.agenceId, localisationAgenda.agenceId) &&
        Objects.equals(this.libelle, localisationAgenda.libelle);
  }

  @Override
  public int hashCode() {
    return Objects.hash(jour, heureDebut, heureFin, posteFonctionnel, agenceId, libelle);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LocalisationAgenda {\n");
    
    sb.append("    jour: ").append(toIndentedString(jour)).append("\n");
    sb.append("    heureDebut: ").append(toIndentedString(heureDebut)).append("\n");
    sb.append("    heureFin: ").append(toIndentedString(heureFin)).append("\n");
    sb.append("    posteFonctionnel: ").append(toIndentedString(posteFonctionnel)).append("\n");
    sb.append("    agenceId: ").append(toIndentedString(agenceId)).append("\n");
    sb.append("    libelle: ").append(toIndentedString(libelle)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

