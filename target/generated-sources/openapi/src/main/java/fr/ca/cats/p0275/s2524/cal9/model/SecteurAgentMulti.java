package fr.ca.cats.p0275.s2524.cal9.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import fr.ca.cats.p0275.s2524.cal9.model.FonctionMulti;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SecteurAgentMulti
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-07-16T17:11:10.536411100+02:00[Europe/Paris]")
public class SecteurAgentMulti   {
  @JsonProperty("id")
  private String id;

  @JsonProperty("structureId")
  private String structureId;

  @JsonProperty("libelle")
  private String libelle;

  @JsonProperty("fonctions")
  @Valid
  private List<FonctionMulti> fonctions = null;

  public SecteurAgentMulti id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Id de l'eds
   * @return id
  */
  @ApiModelProperty(example = "00950", value = "Id de l'eds")


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public SecteurAgentMulti structureId(String structureId) {
    this.structureId = structureId;
    return this;
  }

  /**
   * Identifiant de la caisse régionale
   * @return structureId
  */
  @ApiModelProperty(example = "87800", value = "Identifiant de la caisse régionale")


  public String getStructureId() {
    return structureId;
  }

  public void setStructureId(String structureId) {
    this.structureId = structureId;
  }

  public SecteurAgentMulti libelle(String libelle) {
    this.libelle = libelle;
    return this;
  }

  /**
   * Libellé de l'EDS
   * @return libelle
  */
  @ApiModelProperty(example = "Louhans", value = "Libellé de l'EDS")


  public String getLibelle() {
    return libelle;
  }

  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  public SecteurAgentMulti fonctions(List<FonctionMulti> fonctions) {
    this.fonctions = fonctions;
    return this;
  }

  public SecteurAgentMulti addFonctionsItem(FonctionMulti fonctionsItem) {
    if (this.fonctions == null) {
      this.fonctions = new ArrayList<>();
    }
    this.fonctions.add(fonctionsItem);
    return this;
  }

  /**
   * Get fonctions
   * @return fonctions
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<FonctionMulti> getFonctions() {
    return fonctions;
  }

  public void setFonctions(List<FonctionMulti> fonctions) {
    this.fonctions = fonctions;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SecteurAgentMulti secteurAgentMulti = (SecteurAgentMulti) o;
    return Objects.equals(this.id, secteurAgentMulti.id) &&
        Objects.equals(this.structureId, secteurAgentMulti.structureId) &&
        Objects.equals(this.libelle, secteurAgentMulti.libelle) &&
        Objects.equals(this.fonctions, secteurAgentMulti.fonctions);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, structureId, libelle, fonctions);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SecteurAgentMulti {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    structureId: ").append(toIndentedString(structureId)).append("\n");
    sb.append("    libelle: ").append(toIndentedString(libelle)).append("\n");
    sb.append("    fonctions: ").append(toIndentedString(fonctions)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

