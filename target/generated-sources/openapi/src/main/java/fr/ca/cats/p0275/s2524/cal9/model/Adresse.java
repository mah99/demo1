package fr.ca.cats.p0275.s2524.cal9.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Adresse
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-07-16T17:11:10.536411100+02:00[Europe/Paris]")
public class Adresse   {
  @JsonProperty("rue")
  private String rue;

  @JsonProperty("codePostal")
  private String codePostal;

  @JsonProperty("ville")
  private String ville;

  public Adresse rue(String rue) {
    this.rue = rue;
    return this;
  }

  /**
   * Numéro + nom de la voie
   * @return rue
  */
  @ApiModelProperty(example = "11-13 avenue Voltaire", value = "Numéro + nom de la voie")

@Size(max = 255) 
  public String getRue() {
    return rue;
  }

  public void setRue(String rue) {
    this.rue = rue;
  }

  public Adresse codePostal(String codePostal) {
    this.codePostal = codePostal;
    return this;
  }

  /**
   * Code postal
   * @return codePostal
  */
  @ApiModelProperty(example = "01210", value = "Code postal")

@Size(max = 255) 
  public String getCodePostal() {
    return codePostal;
  }

  public void setCodePostal(String codePostal) {
    this.codePostal = codePostal;
  }

  public Adresse ville(String ville) {
    this.ville = ville;
    return this;
  }

  /**
   * Ville
   * @return ville
  */
  @ApiModelProperty(example = "FERNEY", value = "Ville")

@Size(max = 255) 
  public String getVille() {
    return ville;
  }

  public void setVille(String ville) {
    this.ville = ville;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Adresse adresse = (Adresse) o;
    return Objects.equals(this.rue, adresse.rue) &&
        Objects.equals(this.codePostal, adresse.codePostal) &&
        Objects.equals(this.ville, adresse.ville);
  }

  @Override
  public int hashCode() {
    return Objects.hash(rue, codePostal, ville);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Adresse {\n");
    
    sb.append("    rue: ").append(toIndentedString(rue)).append("\n");
    sb.append("    codePostal: ").append(toIndentedString(codePostal)).append("\n");
    sb.append("    ville: ").append(toIndentedString(ville)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

