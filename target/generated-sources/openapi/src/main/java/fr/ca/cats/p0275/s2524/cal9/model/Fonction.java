package fr.ca.cats.p0275.s2524.cal9.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import fr.ca.cats.p0275.s2524.cal9.model.AgentFonction;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Fonction
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-07-16T17:11:10.536411100+02:00[Europe/Paris]")
public class Fonction   {
  @JsonProperty("libelle")
  private String libelle;

  @JsonProperty("agents")
  @Valid
  private List<AgentFonction> agents = null;

  public Fonction libelle(String libelle) {
    this.libelle = libelle;
    return this;
  }

  /**
   * Libellé de la fonction
   * @return libelle
  */
  @ApiModelProperty(example = "ATTACHE DE CLIENTELE", value = "Libellé de la fonction")


  public String getLibelle() {
    return libelle;
  }

  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }

  public Fonction agents(List<AgentFonction> agents) {
    this.agents = agents;
    return this;
  }

  public Fonction addAgentsItem(AgentFonction agentsItem) {
    if (this.agents == null) {
      this.agents = new ArrayList<>();
    }
    this.agents.add(agentsItem);
    return this;
  }

  /**
   * Get agents
   * @return agents
  */
  @ApiModelProperty(value = "")

  @Valid

  public List<AgentFonction> getAgents() {
    return agents;
  }

  public void setAgents(List<AgentFonction> agents) {
    this.agents = agents;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Fonction fonction = (Fonction) o;
    return Objects.equals(this.libelle, fonction.libelle) &&
        Objects.equals(this.agents, fonction.agents);
  }

  @Override
  public int hashCode() {
    return Objects.hash(libelle, agents);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Fonction {\n");
    
    sb.append("    libelle: ").append(toIndentedString(libelle)).append("\n");
    sb.append("    agents: ").append(toIndentedString(agents)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

