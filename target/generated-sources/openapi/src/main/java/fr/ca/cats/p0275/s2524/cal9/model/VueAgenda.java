package fr.ca.cats.p0275.s2524.cal9.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import fr.ca.cats.p0275.s2524.cal9.model.Agent;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * VueAgenda
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-07-16T17:11:10.536411100+02:00[Europe/Paris]")
public class VueAgenda   {
  @JsonProperty("type")
  private String type;

  @JsonProperty("agent")
  private Agent agent;

  @JsonProperty("agenceId")
  private String agenceId;

  public VueAgenda type(String type) {
    this.type = type;
    return this;
  }

  /**
   * Type de la vue agenda
   * @return type
  */
  @ApiModelProperty(example = "MONO / MULTI", value = "Type de la vue agenda")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public VueAgenda agent(Agent agent) {
    this.agent = agent;
    return this;
  }

  /**
   * Get agent
   * @return agent
  */
  @ApiModelProperty(value = "")

  @Valid

  public Agent getAgent() {
    return agent;
  }

  public void setAgent(Agent agent) {
    this.agent = agent;
  }

  public VueAgenda agenceId(String agenceId) {
    this.agenceId = agenceId;
    return this;
  }

  /**
   * Id de l'eds
   * @return agenceId
  */
  @ApiModelProperty(example = "00480", value = "Id de l'eds")


  public String getAgenceId() {
    return agenceId;
  }

  public void setAgenceId(String agenceId) {
    this.agenceId = agenceId;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VueAgenda vueAgenda = (VueAgenda) o;
    return Objects.equals(this.type, vueAgenda.type) &&
        Objects.equals(this.agent, vueAgenda.agent) &&
        Objects.equals(this.agenceId, vueAgenda.agenceId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, agent, agenceId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class VueAgenda {\n");
    
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    agent: ").append(toIndentedString(agent)).append("\n");
    sb.append("    agenceId: ").append(toIndentedString(agenceId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

