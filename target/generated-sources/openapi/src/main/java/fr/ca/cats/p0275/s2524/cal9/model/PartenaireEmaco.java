package fr.ca.cats.p0275.s2524.cal9.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * PartenaireEmaco
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-07-16T17:11:10.536411100+02:00[Europe/Paris]")
public class PartenaireEmaco   {
  @JsonProperty("partenaireEmacoId")
  private String partenaireEmacoId;

  public PartenaireEmaco partenaireEmacoId(String partenaireEmacoId) {
    this.partenaireEmacoId = partenaireEmacoId;
    return this;
  }

  /**
   * Identifiant EMACO du partenaire
   * @return partenaireEmacoId
  */
  @ApiModelProperty(example = "00950854", value = "Identifiant EMACO du partenaire")


  public String getPartenaireEmacoId() {
    return partenaireEmacoId;
  }

  public void setPartenaireEmacoId(String partenaireEmacoId) {
    this.partenaireEmacoId = partenaireEmacoId;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PartenaireEmaco partenaireEmaco = (PartenaireEmaco) o;
    return Objects.equals(this.partenaireEmacoId, partenaireEmaco.partenaireEmacoId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(partenaireEmacoId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PartenaireEmaco {\n");
    
    sb.append("    partenaireEmacoId: ").append(toIndentedString(partenaireEmacoId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

