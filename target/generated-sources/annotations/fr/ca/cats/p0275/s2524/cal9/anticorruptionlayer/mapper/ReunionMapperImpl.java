package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Agence;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.CanalInteraction;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Expert;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.PiecesAFournirAgc9;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Reunion;
import fr.ca.cats.p0275.s2524.cal9.model.Objet;
import fr.ca.cats.p0275.s2524.cal9.model.PiecesAFournir;
import fr.ca.cats.p0275.s2524.cal9.model.ReunionContexte;
import fr.ca.cats.p0275.s2524.cal9.model.Theme;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-07-16T17:11:21+0200",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.5 (AdoptOpenJDK)"
)
@Component
public class ReunionMapperImpl implements ReunionMapper {

    @Override
    public fr.ca.cats.p0275.s2524.cal9.model.Reunion reunionAGC9ToReunion(Reunion reunion) {
        if ( reunion == null ) {
            return null;
        }

        fr.ca.cats.p0275.s2524.cal9.model.Reunion reunion1 = new fr.ca.cats.p0275.s2524.cal9.model.Reunion();

        reunion1.setCanalInteraction( canalInteractionToCanalInteraction( reunion.getCanalInteraction() ) );
        reunion1.setReunionContexte( reunionContexteToReunionContexte( reunion.getReunionContexte() ) );
        reunion1.setDateDebut( reunion.getDateDebut() );
        reunion1.setDateFin( reunion.getDateFin() );
        reunion1.setTypeReunion( reunion.getTypeReunion() );
        reunion1.setExperts( expertListToExpertList( reunion.getExperts() ) );
        reunion1.setTheme( themeToTheme( reunion.getTheme() ) );
        reunion1.setObjet( objetToObjet( reunion.getObjet() ) );
        reunion1.setPiecesAFournir( piecesAFournirAgc9ListToPiecesAFournirList( reunion.getPiecesAFournir() ) );
        reunion1.setCommentaire( reunion.getCommentaire() );
        reunion1.setPlageProtegee( reunion.getPlageProtegee() );

        return reunion1;
    }

    protected fr.ca.cats.p0275.s2524.cal9.model.Agence agenceToAgence(Agence agence) {
        if ( agence == null ) {
            return null;
        }

        fr.ca.cats.p0275.s2524.cal9.model.Agence agence1 = new fr.ca.cats.p0275.s2524.cal9.model.Agence();

        agence1.setId( agence.getId() );

        return agence1;
    }

    protected fr.ca.cats.p0275.s2524.cal9.model.CanalInteraction canalInteractionToCanalInteraction(CanalInteraction canalInteraction) {
        if ( canalInteraction == null ) {
            return null;
        }

        fr.ca.cats.p0275.s2524.cal9.model.CanalInteraction canalInteraction1 = new fr.ca.cats.p0275.s2524.cal9.model.CanalInteraction();

        canalInteraction1.setCanal( canalInteraction.getCanal() );
        canalInteraction1.setAgence( agenceToAgence( canalInteraction.getAgence() ) );
        canalInteraction1.setTelephone( canalInteraction.getTelephone() );
        canalInteraction1.setUrlVisio( canalInteraction.getUrlVisio() );

        return canalInteraction1;
    }

    protected ReunionContexte reunionContexteToReunionContexte(fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.ReunionContexte reunionContexte) {
        if ( reunionContexte == null ) {
            return null;
        }

        ReunionContexte reunionContexte1 = new ReunionContexte();

        reunionContexte1.setAgentId( reunionContexte.getAgentId() );
        reunionContexte1.setDateReunion( reunionContexte.getDateReunion() );
        reunionContexte1.setHeureDebutReunion( reunionContexte.getHeureDebutReunion() );

        return reunionContexte1;
    }

    protected fr.ca.cats.p0275.s2524.cal9.model.Expert expertToExpert(Expert expert) {
        if ( expert == null ) {
            return null;
        }

        fr.ca.cats.p0275.s2524.cal9.model.Expert expert1 = new fr.ca.cats.p0275.s2524.cal9.model.Expert();

        expert1.setMail( expert.getMail() );

        return expert1;
    }

    protected List<fr.ca.cats.p0275.s2524.cal9.model.Expert> expertListToExpertList(List<Expert> list) {
        if ( list == null ) {
            return null;
        }

        List<fr.ca.cats.p0275.s2524.cal9.model.Expert> list1 = new ArrayList<fr.ca.cats.p0275.s2524.cal9.model.Expert>( list.size() );
        for ( Expert expert : list ) {
            list1.add( expertToExpert( expert ) );
        }

        return list1;
    }

    protected Theme themeToTheme(fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Theme theme) {
        if ( theme == null ) {
            return null;
        }

        Theme theme1 = new Theme();

        theme1.setId( theme.getId() );
        theme1.setLibelle( theme.getLibelle() );

        return theme1;
    }

    protected Objet objetToObjet(fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Objet objet) {
        if ( objet == null ) {
            return null;
        }

        Objet objet1 = new Objet();

        objet1.setId( objet.getId() );
        objet1.setLibelle( objet.getLibelle() );

        return objet1;
    }

    protected PiecesAFournir piecesAFournirAgc9ToPiecesAFournir(PiecesAFournirAgc9 piecesAFournirAgc9) {
        if ( piecesAFournirAgc9 == null ) {
            return null;
        }

        PiecesAFournir piecesAFournir = new PiecesAFournir();

        piecesAFournir.setId( piecesAFournirAgc9.getId() );
        piecesAFournir.setLibelle( piecesAFournirAgc9.getLibelle() );
        piecesAFournir.setOrdre( piecesAFournirAgc9.getOrdre() );

        return piecesAFournir;
    }

    protected List<PiecesAFournir> piecesAFournirAgc9ListToPiecesAFournirList(List<PiecesAFournirAgc9> list) {
        if ( list == null ) {
            return null;
        }

        List<PiecesAFournir> list1 = new ArrayList<PiecesAFournir>( list.size() );
        for ( PiecesAFournirAgc9 piecesAFournirAgc9 : list ) {
            list1.add( piecesAFournirAgc9ToPiecesAFournir( piecesAFournirAgc9 ) );
        }

        return list1;
    }
}
