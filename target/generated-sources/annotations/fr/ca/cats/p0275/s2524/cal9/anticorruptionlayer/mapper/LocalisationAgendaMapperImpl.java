package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.DistributionEntity;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Job;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.WorkingHour;
import fr.ca.cats.p0275.s2524.cal9.model.LocalisationAgenda;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-07-16T17:11:21+0200",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.5 (AdoptOpenJDK)"
)
@Component
public class LocalisationAgendaMapperImpl implements LocalisationAgendaMapper {

    @Override
    public LocalisationAgenda informationSDG9ToLocalisationAgenda(DistributionEntity distributionEntity, WorkingHour workingHour) {
        if ( distributionEntity == null && workingHour == null ) {
            return null;
        }

        LocalisationAgenda localisationAgenda = new LocalisationAgenda();

        if ( distributionEntity != null ) {
            localisationAgenda.setAgenceId( distributionEntity.getIdBranch() );
            localisationAgenda.setLibelle( distributionEntity.getLabel() );
            localisationAgenda.setPosteFonctionnel( distributionEntityJobId( distributionEntity ) );
        }
        if ( workingHour != null ) {
            localisationAgenda.setJour( workingHour.getDay() );
            localisationAgenda.setHeureDebut( workingHour.getStartingHour() );
            localisationAgenda.setHeureFin( workingHour.getEndingHour() );
        }

        return localisationAgenda;
    }

    private String distributionEntityJobId(DistributionEntity distributionEntity) {
        if ( distributionEntity == null ) {
            return null;
        }
        Job job = distributionEntity.getJob();
        if ( job == null ) {
            return null;
        }
        String id = job.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
