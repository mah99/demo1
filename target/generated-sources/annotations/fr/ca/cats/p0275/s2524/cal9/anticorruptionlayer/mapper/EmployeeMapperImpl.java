package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.DistributionEntity;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.AgentDao;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.OmAgent;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-07-16T17:11:20+0200",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.5 (AdoptOpenJDK)"
)
@Component
public class EmployeeMapperImpl implements EmployeeMapper {

    @Override
    public AgentDao employeeToEmployeeDao(Employee employee) {
        if ( employee == null ) {
            return null;
        }

        AgentDao agentDao = new AgentDao();

        agentDao.setIdAgent( employee.getIdEmployee() );
        agentDao.setStructureId( employee.getStructureId() );
        agentDao.setMatriculeAgent( employee.getMatriculeAgent() );
        agentDao.setCivilite( employee.getCivilite() );
        agentDao.setPrenom( employee.getPrenom() );
        agentDao.setNom( employee.getNom() );
        agentDao.setNumeroTelephone( employee.getNumeroTelephone() );
        agentDao.setNumeroTelephoneMobile( employee.getNumeroTelephoneMobile() );
        agentDao.setEmail( employee.getEmail() );
        List<DistributionEntity> list = employee.getDistributionEntities();
        if ( list != null ) {
            agentDao.setDistributionEntities( new ArrayList<DistributionEntity>( list ) );
        }

        return agentDao;
    }

    @Override
    public Employee employeeDaoToEmployee(AgentDao agentDao) {
        if ( agentDao == null ) {
            return null;
        }

        Employee employee = new Employee();

        employee.setIdEmployee( agentDao.getIdAgent() );
        employee.setStructureId( agentDao.getStructureId() );
        employee.setMatriculeAgent( agentDao.getMatriculeAgent() );
        employee.setCivilite( agentDao.getCivilite() );
        employee.setPrenom( agentDao.getPrenom() );
        employee.setNom( agentDao.getNom() );
        employee.setNumeroTelephone( agentDao.getNumeroTelephone() );
        employee.setNumeroTelephoneMobile( agentDao.getNumeroTelephoneMobile() );
        employee.setEmail( agentDao.getEmail() );
        List<DistributionEntity> list = agentDao.getDistributionEntities();
        if ( list != null ) {
            employee.setDistributionEntities( new ArrayList<DistributionEntity>( list ) );
        }

        return employee;
    }

    @Override
    public Employee omAgentToEmployee(OmAgent omAgent) {
        if ( omAgent == null ) {
            return null;
        }

        Employee employee = new Employee();

        employee.setIdEmployee( omAgent.getMatricule() );
        employee.setMatriculeAgent( omAgent.getMatricule() );
        employee.setCivilite( omAgent.getCivilite() );
        employee.setPrenom( omAgent.getPrenom() );
        employee.setNom( omAgent.getNom() );

        return employee;
    }
}
