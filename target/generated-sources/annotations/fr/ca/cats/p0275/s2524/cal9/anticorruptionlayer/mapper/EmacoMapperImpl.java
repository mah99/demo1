package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.PartenaireEmacoDao;
import fr.ca.cats.p0275.s2524.cal9.model.PartenaireEmaco;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-07-16T17:11:21+0200",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.5 (AdoptOpenJDK)"
)
@Component
public class EmacoMapperImpl implements EmacoMapper {

    @Override
    public PartenaireEmaco partenaireEmacoDaoToPartenaireEmaco(PartenaireEmacoDao partenaireEmacoDao) {
        if ( partenaireEmacoDao == null ) {
            return null;
        }

        PartenaireEmaco partenaireEmaco = new PartenaireEmaco();

        partenaireEmaco.setPartenaireEmacoId( partenaireEmacoDao.getPartenaireEmacoId() );

        return partenaireEmaco;
    }
}
