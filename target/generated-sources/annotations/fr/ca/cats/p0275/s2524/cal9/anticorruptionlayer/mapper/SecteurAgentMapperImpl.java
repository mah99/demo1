package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.Fonction;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.OmAgent;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.OmEdsAgent;
import fr.ca.cats.p0275.s2524.cal9.model.AgentFonction;
import fr.ca.cats.p0275.s2524.cal9.model.AgentFonctionMulti;
import fr.ca.cats.p0275.s2524.cal9.model.FonctionMulti;
import fr.ca.cats.p0275.s2524.cal9.model.SecteurAgent;
import fr.ca.cats.p0275.s2524.cal9.model.SecteurAgentMulti;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-07-16T17:11:21+0200",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.5 (AdoptOpenJDK)"
)
@Component
public class SecteurAgentMapperImpl implements SecteurAgentMapper {

    @Override
    public AgentFonction omAgentToAgentFonction(OmAgent omAgent) {
        if ( omAgent == null ) {
            return null;
        }

        AgentFonction agentFonction = new AgentFonction();

        agentFonction.setId( omAgent.getMatricule() );
        agentFonction.setNom( omAgent.getNom() );
        agentFonction.setPrenom( omAgent.getPrenom() );

        return agentFonction;
    }

    @Override
    public AgentFonctionMulti omAgentToAgentFonctionMulti(OmAgent omAgent) {
        if ( omAgent == null ) {
            return null;
        }

        AgentFonctionMulti agentFonctionMulti = new AgentFonctionMulti();

        agentFonctionMulti.setId( omAgent.getMatricule() );
        agentFonctionMulti.setCivilite( omAgent.getCivilite() );
        agentFonctionMulti.setNom( omAgent.getNom() );
        agentFonctionMulti.setPrenom( omAgent.getPrenom() );

        return agentFonctionMulti;
    }

    @Override
    public SecteurAgent omEdsAgentToEds(OmEdsAgent omEdsAgent) {
        if ( omEdsAgent == null ) {
            return null;
        }

        SecteurAgent secteurAgent = new SecteurAgent();

        secteurAgent.setId( getIdEds( omEdsAgent.getIdEds() ) );
        secteurAgent.setStructureId( getStructureId( omEdsAgent.getIdEds() ) );
        secteurAgent.setNiveauEds( getNiveauEds( omEdsAgent.getIdEds() ) );
        secteurAgent.setLibelle( omEdsAgent.getLibelle() );
        secteurAgent.setLibelleCourt( omEdsAgent.getLibelleCourt() );
        secteurAgent.setFonctions( fonctionListToFonctionList( omEdsAgent.getFonctions() ) );

        return secteurAgent;
    }

    @Override
    public SecteurAgentMulti omEdsAgentToEdsMulti(OmEdsAgent omEdsAgent) {
        if ( omEdsAgent == null ) {
            return null;
        }

        SecteurAgentMulti secteurAgentMulti = new SecteurAgentMulti();

        secteurAgentMulti.setId( getIdEds( omEdsAgent.getIdEds() ) );
        secteurAgentMulti.setStructureId( omEdsAgent.getIdCaisseRegionale() );
        secteurAgentMulti.setLibelle( omEdsAgent.getLibelle() );
        secteurAgentMulti.setFonctions( fonctionListToFonctionMultiList( omEdsAgent.getFonctions() ) );

        return secteurAgentMulti;
    }

    @Override
    public List<SecteurAgent> omEdsAgentsToEds(List<OmEdsAgent> omEdsAgents) {
        if ( omEdsAgents == null ) {
            return null;
        }

        List<SecteurAgent> list = new ArrayList<SecteurAgent>( omEdsAgents.size() );
        for ( OmEdsAgent omEdsAgent : omEdsAgents ) {
            list.add( omEdsAgentToEds( omEdsAgent ) );
        }

        return list;
    }

    protected List<AgentFonction> omAgentListToAgentFonctionList(List<OmAgent> list) {
        if ( list == null ) {
            return null;
        }

        List<AgentFonction> list1 = new ArrayList<AgentFonction>( list.size() );
        for ( OmAgent omAgent : list ) {
            list1.add( omAgentToAgentFonction( omAgent ) );
        }

        return list1;
    }

    protected fr.ca.cats.p0275.s2524.cal9.model.Fonction fonctionToFonction(Fonction fonction) {
        if ( fonction == null ) {
            return null;
        }

        fr.ca.cats.p0275.s2524.cal9.model.Fonction fonction1 = new fr.ca.cats.p0275.s2524.cal9.model.Fonction();

        fonction1.setLibelle( fonction.getLibelle() );
        fonction1.setAgents( omAgentListToAgentFonctionList( fonction.getAgents() ) );

        return fonction1;
    }

    protected List<fr.ca.cats.p0275.s2524.cal9.model.Fonction> fonctionListToFonctionList(List<Fonction> list) {
        if ( list == null ) {
            return null;
        }

        List<fr.ca.cats.p0275.s2524.cal9.model.Fonction> list1 = new ArrayList<fr.ca.cats.p0275.s2524.cal9.model.Fonction>( list.size() );
        for ( Fonction fonction : list ) {
            list1.add( fonctionToFonction( fonction ) );
        }

        return list1;
    }

    protected List<AgentFonctionMulti> omAgentListToAgentFonctionMultiList(List<OmAgent> list) {
        if ( list == null ) {
            return null;
        }

        List<AgentFonctionMulti> list1 = new ArrayList<AgentFonctionMulti>( list.size() );
        for ( OmAgent omAgent : list ) {
            list1.add( omAgentToAgentFonctionMulti( omAgent ) );
        }

        return list1;
    }

    protected FonctionMulti fonctionToFonctionMulti(Fonction fonction) {
        if ( fonction == null ) {
            return null;
        }

        FonctionMulti fonctionMulti = new FonctionMulti();

        fonctionMulti.setCode( fonction.getCode() );
        fonctionMulti.setLibelle( fonction.getLibelle() );
        fonctionMulti.setAgents( omAgentListToAgentFonctionMultiList( fonction.getAgents() ) );

        return fonctionMulti;
    }

    protected List<FonctionMulti> fonctionListToFonctionMultiList(List<Fonction> list) {
        if ( list == null ) {
            return null;
        }

        List<FonctionMulti> list1 = new ArrayList<FonctionMulti>( list.size() );
        for ( Fonction fonction : list ) {
            list1.add( fonctionToFonctionMulti( fonction ) );
        }

        return list1;
    }
}
