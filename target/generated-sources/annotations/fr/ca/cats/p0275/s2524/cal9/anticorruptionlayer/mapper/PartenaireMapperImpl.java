package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.agendacollab.Partenaire;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.PartenaireDao;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-07-16T17:11:21+0200",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.5 (AdoptOpenJDK)"
)
@Component
public class PartenaireMapperImpl implements PartenaireMapper {

    @Override
    public fr.ca.cats.p0275.s2524.cal9.model.Partenaire partenaireAGC9ToPartenaire(Partenaire partenaire) {
        if ( partenaire == null ) {
            return null;
        }

        fr.ca.cats.p0275.s2524.cal9.model.Partenaire partenaire1 = new fr.ca.cats.p0275.s2524.cal9.model.Partenaire();

        partenaire1.setId( partenaire.getId() );

        return partenaire1;
    }

    @Override
    public fr.ca.cats.p0275.s2524.cal9.model.Partenaire partenaireAGC09nullIdPartenaireToPartenaire(Partenaire partenaire) {
        if ( partenaire == null ) {
            return null;
        }

        fr.ca.cats.p0275.s2524.cal9.model.Partenaire partenaire1 = new fr.ca.cats.p0275.s2524.cal9.model.Partenaire();

        partenaire1.setLibelle( partenaire.getLibelle() );

        return partenaire1;
    }

    @Override
    public fr.ca.cats.p0275.s2524.cal9.model.Partenaire partenaireAGC9VisioToPartenaire(Partenaire partenaire) {
        if ( partenaire == null ) {
            return null;
        }

        fr.ca.cats.p0275.s2524.cal9.model.Partenaire partenaire1 = new fr.ca.cats.p0275.s2524.cal9.model.Partenaire();

        partenaire1.setId( partenaire.getIdPartenaireVisio() );

        return partenaire1;
    }

    @Override
    public PartenaireDao partenaireToPartenaireDao(fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire partenaire) {
        if ( partenaire == null ) {
            return null;
        }

        PartenaireDao partenaireDao = new PartenaireDao();

        partenaireDao.setIdPartenaire( partenaire.getIdPartenaire() );
        partenaireDao.setIdCaisseRegionale( partenaire.getIdCaisseRegionale() );
        partenaireDao.setTypePartenaire( partenaire.getTypePartenaire() );
        partenaireDao.setSiret( partenaire.getSiret() );
        partenaireDao.setEnseigneEtablissement( partenaire.getEnseigneEtablissement() );
        partenaireDao.setDateCreation( partenaire.getDateCreation() );
        partenaireDao.setDateCloture( partenaire.getDateCloture() );
        partenaireDao.setIntitule( partenaire.getIntitule() );
        partenaireDao.setIntituleSuite( partenaire.getIntituleSuite() );
        partenaireDao.setCodeMarche( partenaire.getCodeMarche() );
        partenaireDao.setIdPortefeuille( partenaire.getIdPortefeuille() );
        partenaireDao.setIdBureauGestionnaire( partenaire.getIdBureauGestionnaire() );
        partenaireDao.setCodeBureauGestionnaire( partenaire.getCodeBureauGestionnaire() );
        partenaireDao.setTopPartenaireIncomplet( partenaire.getTopPartenaireIncomplet() );

        return partenaireDao;
    }

    @Override
    public fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire partenaireDaoToPartenaire(PartenaireDao partenaireDao) {
        if ( partenaireDao == null ) {
            return null;
        }

        fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire partenaire = new fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.person.partenaire.Partenaire();

        partenaire.setIdPartenaire( partenaireDao.getIdPartenaire() );
        partenaire.setIdCaisseRegionale( partenaireDao.getIdCaisseRegionale() );
        partenaire.setTypePartenaire( partenaireDao.getTypePartenaire() );
        partenaire.setSiret( partenaireDao.getSiret() );
        partenaire.setEnseigneEtablissement( partenaireDao.getEnseigneEtablissement() );
        partenaire.setDateCreation( partenaireDao.getDateCreation() );
        partenaire.setDateCloture( partenaireDao.getDateCloture() );
        partenaire.setIntitule( partenaireDao.getIntitule() );
        partenaire.setIntituleSuite( partenaireDao.getIntituleSuite() );
        partenaire.setCodeMarche( partenaireDao.getCodeMarche() );
        partenaire.setIdPortefeuille( partenaireDao.getIdPortefeuille() );
        partenaire.setIdBureauGestionnaire( partenaireDao.getIdBureauGestionnaire() );
        partenaire.setCodeBureauGestionnaire( partenaireDao.getCodeBureauGestionnaire() );
        partenaire.setTopPartenaireIncomplet( partenaireDao.getTopPartenaireIncomplet() );

        return partenaire;
    }
}
