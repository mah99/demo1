package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Branch;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.PostalAddress;
import fr.ca.cats.p0275.s2524.cal9.model.Adresse;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-07-16T17:11:21+0200",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.5 (AdoptOpenJDK)"
)
@Component
public class AdresseMapperImpl implements AdresseMapper {

    @Override
    public Adresse adresseToBranch(Branch branch) {
        if ( branch == null ) {
            return null;
        }

        Adresse adresse = new Adresse();

        adresse.setRue( branchPostalAddressPostalAddressLine4( branch ) );
        adresse.setCodePostal( branch.getZipCode() );
        adresse.setVille( branch.getCity() );

        return adresse;
    }

    private String branchPostalAddressPostalAddressLine4(Branch branch) {
        if ( branch == null ) {
            return null;
        }
        PostalAddress postalAddress = branch.getPostalAddress();
        if ( postalAddress == null ) {
            return null;
        }
        String postalAddressLine4 = postalAddress.getPostalAddressLine4();
        if ( postalAddressLine4 == null ) {
            return null;
        }
        return postalAddressLine4;
    }
}
