package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Branch;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.BranchDao;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-07-16T17:11:21+0200",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.5 (AdoptOpenJDK)"
)
@Component
public class AgenceMapperImpl implements AgenceMapper {

    @Override
    public BranchDao branchtoBranchDao(Branch branch) {
        if ( branch == null ) {
            return null;
        }

        BranchDao branchDao = new BranchDao();

        branchDao.setStructureId( branch.getStructureId() );
        branchDao.setCodeEds( branch.getCodeEds() );
        branchDao.setName( branch.getName() );
        branchDao.setEmail( branch.getEmail() );
        branchDao.setCity( branch.getCity() );
        branchDao.setZipCode( branch.getZipCode() );
        branchDao.setPostalAddress( branch.getPostalAddress() );

        return branchDao;
    }

    @Override
    public Branch branchDaoToBranch(BranchDao branchDao) {
        if ( branchDao == null ) {
            return null;
        }

        Branch branch = new Branch();

        branch.setStructureId( branchDao.getStructureId() );
        branch.setCodeEds( branchDao.getCodeEds() );
        branch.setName( branchDao.getName() );
        branch.setEmail( branchDao.getEmail() );
        branch.setCity( branchDao.getCity() );
        branch.setZipCode( branchDao.getZipCode() );
        branch.setPostalAddress( branchDao.getPostalAddress() );

        return branch;
    }
}
