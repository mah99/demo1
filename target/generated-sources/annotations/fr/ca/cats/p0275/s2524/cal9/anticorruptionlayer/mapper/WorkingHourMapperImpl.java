package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.WorkingHour;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.Horaire;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-07-16T17:11:21+0200",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.5 (AdoptOpenJDK)"
)
@Component
public class WorkingHourMapperImpl implements WorkingHourMapper {

    @Override
    public WorkingHour horaireToWorkingHour(Horaire horaire) {
        if ( horaire == null ) {
            return null;
        }

        WorkingHour workingHour = new WorkingHour();

        workingHour.setDay( horaire.getJour() );
        workingHour.setStartingHour( horaire.getHeureDebut() );
        workingHour.setEndingHour( horaire.getHeureFin() );

        return workingHour;
    }
}
