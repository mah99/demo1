package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.DistributionEntity;
import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Job;
import fr.ca.cats.p0275.s2524.cal9.infrastructure.persistance.model.PosteFonctionnel;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-07-16T17:11:21+0200",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.5 (AdoptOpenJDK)"
)
@Component
public class DistributionEntityMapperImpl implements DistributionEntityMapper {

    @Override
    public DistributionEntity posteFonctionnelToDistributionEntity(PosteFonctionnel posteFonctionnel, String idEds, String libelle) {
        if ( posteFonctionnel == null && idEds == null && libelle == null ) {
            return null;
        }

        DistributionEntity distributionEntity = new DistributionEntity();

        distributionEntity.setJob( posteFonctionnelToJob( posteFonctionnel ) );
        distributionEntity.setIdBranch( idEds );
        distributionEntity.setLabel( libelle );

        return distributionEntity;
    }

    protected Job posteFonctionnelToJob(PosteFonctionnel posteFonctionnel) {
        if ( posteFonctionnel == null ) {
            return null;
        }

        Job job = new Job();

        job.setId( posteFonctionnel.getCode() );

        return job;
    }
}
