package fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.mapper;

import fr.ca.cats.p0275.s2524.cal9.anticorruptionlayer.dto.elementdestructure.Employee;
import fr.ca.cats.p0275.s2524.cal9.model.Agent;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-07-16T17:11:21+0200",
    comments = "version: 1.5.2.Final, compiler: javac, environment: Java 11.0.5 (AdoptOpenJDK)"
)
@Component
public class AgentMapperImpl implements AgentMapper {

    @Override
    public Agent employeeToAgent(Employee employee) {
        if ( employee == null ) {
            return null;
        }

        Agent agent = new Agent();

        agent.setId( employee.getIdEmployee() );
        agent.setNom( employee.getNom() );
        agent.setPrenom( employee.getPrenom() );
        agent.setCivilite( employee.getCivilite() );

        return agent;
    }
}
